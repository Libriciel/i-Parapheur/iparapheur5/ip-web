/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTenantPopupLimitationsComponent } from './admin-tenant-popup-limitations.component';
import { TenantDto } from '@libriciel/iparapheur-legacy';

describe('AdminTenantPopupLimitationsComponent', () => {
  let component: AdminTenantPopupLimitationsComponent;
  let fixture: ComponentFixture<AdminTenantPopupLimitationsComponent>;

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
      declarations: [ AdminTenantPopupLimitationsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminTenantPopupLimitationsComponent);
    component = fixture.componentInstance;

    const dummyPendingTenant: TenantDto = {
      id: null, name: null, zipCode: null, deskLimit: null, userLimit: null
    };
    component.pendingTenant = dummyPendingTenant;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
