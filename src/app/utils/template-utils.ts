/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { TemplateType } from '@libriciel/iparapheur-legacy';

export class TemplateUtils {

  static readonly SEAL_TEMPLATE_TYPES: TemplateType[] = [
    TemplateType.SealMedium, TemplateType.SealLarge, TemplateType.SealAlternate
  ];

  static readonly SIGNATURE_TEMPLATE_TYPES: TemplateType[] = [
    TemplateType.SignatureSmall, TemplateType.SignatureMedium, TemplateType.SignatureLarge, TemplateType.SignatureAlternate1, TemplateType.SignatureAlternate2
  ];

  static readonly ADMIN_SEAL_AND_SIGNATURE_TEMPLATE_TYPES: TemplateType[] = [
    TemplateType.SealAutomatic,
    ...TemplateUtils.SEAL_TEMPLATE_TYPES,
    ...TemplateUtils.SIGNATURE_TEMPLATE_TYPES
  ];

  static readonly MAIL_TEMPLATE_TYPES: TemplateType[] = [
    TemplateType.MailActionSend,
    TemplateType.MailNotificationSingle,
    TemplateType.MailNotificationDigest
  ];

  static readonly PDF_TEMPLATE_TYPES: TemplateType[] = [
    TemplateType.Docket
  ];

}
