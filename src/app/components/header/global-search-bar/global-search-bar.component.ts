/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { isNotEmpty } from '../../../utils/string-utils';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { FolderService } from '../../../services/ip-core/folder.service';
import { Folder } from '../../../models/folder/folder';
import { CommonMessages } from '../../../shared/common-messages';
import { NotificationsService } from '../../../services/notifications.service';
import { GlobalSearchMessages } from './global-search-messages';
import { Subscription, fromEvent } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { CommonIcons } from '@libriciel/ls-elements';
import { SearchService } from '../../../services/ip-core/search.service';
import { DeskService } from '../../../services/ip-core/desk.service';
import { FolderUtils } from '../../../utils/folder-utils';
import { TenantRepresentation } from '@libriciel/iparapheur-legacy';
import { Task } from '../../../models/task';

@Component({
  selector: 'app-global-search-bar',
  templateUrl: './global-search-bar.component.html',
  styleUrls: ['./global-search-bar.component.scss']
})
export class GlobalSearchBarComponent implements AfterViewInit, OnDestroy {

  public static MAX_SEARCH_RESULTS: number = 10;

  @ViewChild('searchInput') searchInput: ElementRef;
  searchInputSubscription: Subscription;
  globalSearchMessages = GlobalSearchMessages;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  isProcessing = false;
  hasSearchedAtLeastOnce = false;
  foldersDataList: {
    tenantId: string,
    folder: Folder,
    currentDeskId?: string;
  }[] = [];
  tenantId: string;
  page: number = 0;
  pageSize: number = GlobalSearchBarComponent.MAX_SEARCH_RESULTS;
  search: string = "";
  tenants: TenantRepresentation[];


  constructor(public searchService: SearchService,
              public deskService: DeskService,
              private folderService: FolderService,
              private route: ActivatedRoute,
              private router: Router,
              private notificationService: NotificationsService) {}


  ngAfterViewInit(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd && event.url.includes("/tenant/")) {
        const args = event.url.split("/");

        this.tenantId = args[args.indexOf('tenant') + 1];
      }
    });

    this.searchInputSubscription = fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
        map((event: any) => event.target.value),
        debounceTime(350),
        distinctUntilChanged()
      )
      .subscribe(value => {
        this.search = value;
        this.getData();
      });
  }


  getFolderCurrentDeskId(folder: Folder): string {
    const currentTask: Task = folder.stepList.find(task => FolderUtils.isValidCurrentTask(task));
    return currentTask?.desks[0]?.id || null;
  }


  getData(): void {
    this.isProcessing = true;
    this.hasSearchedAtLeastOnce = true;
    const search = isNotEmpty(this.search) ? this.search : null;
    this.foldersDataList = [];

    this.searchService
      .searchFolders(this.tenantId, this.page, this.pageSize, search)
      .subscribe(data => {
        this.isProcessing = false;
        data.data.forEach(folderData => {
          this.foldersDataList.push(({
            tenantId: folderData.tenantId,
            folder: folderData.folder,
            currentDeskId: this.getFolderCurrentDeskId(folderData.folder)
          }));
        });
      });
  }

  navigateToFolder(folderData: { tenantId: string, folder: Folder, currentDeskId?: string }): void {
    this.router
      .navigate([this.deskService.getUrlToViewFolderAsDesk(folderData.tenantId, folderData.currentDeskId, folderData.folder.id, folderData.currentDeskId)])
      .then(
        () => {/* Not used */},
        () => this.notificationService.showErrorMessage(GlobalSearchMessages.ERROR_NAVIGATING_TO_FOLDER)
      );
  };

  ngOnDestroy(): void {
    this.searchInputSubscription.unsubscribe();
  }


}
