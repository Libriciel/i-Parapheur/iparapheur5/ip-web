/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { CommonIcons, Style } from '@libriciel/ls-elements';
import { CommonMessages } from '../../common-messages';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { faUnlink, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-validation-popup',
  templateUrl: './validation-popup.component.html',
  styleUrls: ['./validation-popup.component.scss']
})
export class ValidationPopupComponent {


  static readonly INJECTABLE_TITLE_KEY: string = 'title';
  static readonly INJECTABLE_MESSAGE_KEY: string = 'message';
  static readonly INJECTABLE_ICON_KEY: string = 'icon';
  static readonly INJECTABLE_SHOW_CANCEL_BUTTON_KEY: string = 'showCancelButton';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly styles = Style;
  readonly unlinkIcon: IconDefinition = faUnlink;


  buttonIcon: IconDefinition = null;
  buttonText: string = CommonMessages.CONTENT_READ;

  constructor(public activeModal: NgbActiveModal,
              @Inject(ValidationPopupComponent.INJECTABLE_MESSAGE_KEY) public message: string,
              @Inject(ValidationPopupComponent.INJECTABLE_ICON_KEY) public icon?: 'delete' | 'unlink',
              @Inject(ValidationPopupComponent.INJECTABLE_TITLE_KEY) public title?: string,
              @Inject(ValidationPopupComponent.INJECTABLE_SHOW_CANCEL_BUTTON_KEY) public showCancelButton: boolean = true) {

    if (this.icon === "unlink") {
      this.buttonIcon = this.unlinkIcon;
    } else if (this.icon === "delete") {
      this.buttonIcon = CommonIcons.DELETE_ICON;
    }

    if (this.icon === "unlink") {
      this.buttonText = CommonMessages.UNLINK;
    } else if (this.icon === "delete") {
      this.buttonText = CommonMessages.DELETE;
    }
  }

}
