/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { UserEditSubPanelsMessages } from '../../user-edit-page-subpanels/user-edit-sub-panels-messages';
import { TableName, LabelledColumn, TableLayoutDto } from '@libriciel/iparapheur-internal';
import { UserDto } from '@libriciel/iparapheur-provisioning';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { SelectedUserPreferencesService } from '../../../../../services/selected-user-preferences.service';
import { FilterService } from '../../../../../services/filter.service';
import { ActivatedRoute } from '@angular/router';
import { FolderFilter } from '../../../../../models/folder/folder-filter';
import { ProfileUpdateService } from '../../../../../shared/service/profile-update.service';

@Component({
  selector: 'app-user-settings-dashboard',
  templateUrl: './user-settings-dashboard.component.html',
  styleUrls: ['./user-settings-dashboard.component.scss']
})
export class UserSettingsDashboardComponent implements OnInit {

  protected readonly messages = UserEditSubPanelsMessages;
  protected readonly tableNameEnum = TableName;

  currentUser: UserDto;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  isProcessing: boolean = false;

  constructor(private filterService: FilterService,
              private profileUpdateService: ProfileUpdateService,
              private route: ActivatedRoute,
              private selectedUserPreferencesService: SelectedUserPreferencesService) { }


  ngOnInit(): void {
    this.currentUser = this.route.parent?.snapshot.data["currentUser"];

    this.selectedUserPreferencesService.currentUserPreferences$.subscribe(userPreferences => {
      this.userPreferences = userPreferences;

      this.initUserPreferences();
    });
  }


  initUserPreferences(): void {
    console.log('profilePage - initUserPreferences');
    this.userPreferences?.tableLayoutList?.forEach((tableLayout: TableLayoutDto) => {
      if (!!tableLayout) {
        tableLayout.columnList = tableLayout.labelledColumnList.map(c => c.id);
      }
    });
  }


  updateColumnList(labelledColumnList: LabelledColumn[], tableName: TableName) {
    const tableLayout: TableLayoutDto = this.userPreferences.tableLayoutList
      .find(tl => tl.tableName === tableName && tl.deskId == null);
    if (!!tableLayout) {
      tableLayout.columnList = labelledColumnList.map(c => c.id);
    } else {
      console.error("Could not retrieve the table layout associated with the tableName :" + tableName);
    }
  }


  saveForm() {
    this.userPreferences.tableLayoutList.forEach(tableLayout => {
      (tableLayout as any).labelledColumnList = tableLayout.labelledColumnList.filter(elem => tableLayout.columnList?.includes(elem.id));
    });

    if (!this.userPreferences.currentFilter?.id) {
      this.userPreferences.currentFilter = null;
    }
    this.userPreferences.currentFilterId = this.userPreferences.currentFilter?.id ?? null;
    this.filterService.setDefaultFilter(Object.assign(new FolderFilter(), this.userPreferences.currentFilter));

    this.isProcessing = true;

    this.profileUpdateService.updateUserPreferences(this.userPreferences)
      .then(r => this.isProcessing = false);
  }
}
