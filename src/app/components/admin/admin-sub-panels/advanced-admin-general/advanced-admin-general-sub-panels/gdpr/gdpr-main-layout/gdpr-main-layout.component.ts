/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { AdminMessages } from '../../../../admin-messages';
import { Style, CommonIcons } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../../../../shared/common-messages';
import { ActivatedRoute } from '@angular/router';
import { AdminAdvancedConfigService, GdprInformationDetailsDto, ServerInfoService, ServerInfoDto } from '@libriciel/iparapheur-internal';
import { FormGroup, FormControl } from '@angular/forms';
import { NotificationsService } from '../../../../../../../services/notifications.service';


@Component({
  selector: 'app-gdpr-main-layout',
  templateUrl: './gdpr-main-layout.component.html',
  styleUrls: ['./gdpr-main-layout.component.scss']
})
export class GdprMainLayoutComponent implements OnInit {

  readonly messages = AdminMessages;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly style = Style;
  readonly entityFormFields = [
    {id: 'entityNameInput', controlName: 'entityName', label: this.messages.ADMIN_NAME},
    {id: 'entityAddressInput', controlName: 'entityAddress', label: this.messages.ADMIN_ADDRESS},
    {id: 'entitySiretInput', controlName: 'entitySiret', label: this.messages.ADMIN_SIRET},
    {id: 'entityApeCodeInput', controlName: 'entityApeCode', label: this.messages.ADMIN_RGPD_ENTITY_APE_CODE},
    {id: 'entityPhoneInput', controlName: 'entityPhone', label: this.messages.ADMIN_RGPD_ENTITY_PHONE},
    {id: 'entityMailInput', controlName: 'entityMail', label: this.messages.ADMIN_MAIL},
  ];
  readonly respFormFields = [
    {id: 'respNameInput', controlName: 'respName', label: this.messages.ADMIN_NAME},
    {id: 'respTitleInput', controlName: 'respTitle', label: this.messages.ADMIN_RGPD_RESP_TITLE},
  ];
  readonly dpoFormFields = [
    {id: 'dpoNameInput', controlName: 'dpoName', label: this.messages.ADMIN_NAME},
    {id: 'dpoMailInput', controlName: 'dpoMail', label: this.messages.ADMIN_MAIL},
  ];
  readonly hostingFormFields = [
    {id: 'hostingNameInput', controlName: 'hostingName', label: this.messages.ADMIN_NAME},
    {id: 'hostingAddressInput', controlName: 'hostingAddress', label: this.messages.ADMIN_ADDRESS},
    {id: 'hostingSiretInput', controlName: 'hostingSiret', label: this.messages.ADMIN_SIRET},
    {id: 'hostingCommentInput', controlName: 'hostingComment', label: this.messages.ADMIN_COMMENT},
  ];
  readonly maintenanceFormFields = [
    {id: 'maintenanceNameInput', controlName: 'maintenanceName', label: this.messages.ADMIN_NAME},
    {id: 'maintenanceAddressInput', controlName: 'maintenanceAddress', label: this.messages.ADMIN_ADDRESS},
    {id: 'maintenanceSiretInput', controlName: 'maintenanceSiret', label: this.messages.ADMIN_SIRET},
  ];

  gdprForm: FormGroup;
  loadingData: boolean = false;
  savingData: boolean = false;
  gdprInformationDetailsDto: GdprInformationDetailsDto = null;
  isMaintenanceSameAsHost: boolean;
  isHostSameAsEntity: boolean;


  constructor(public adminAdvancedConfigService: AdminAdvancedConfigService,
              private notificationsService: NotificationsService,
              public route: ActivatedRoute,
              public serverInfoService: ServerInfoService) { }


  ngOnInit(): void {
    this.getGdprInformationDetails();
  }


  getGdprInformationDetails(): void {
    this.loadingData = true;
    this.route.parent?.parent?.data.subscribe({
        next: data => {
          const serverInfo: ServerInfoDto = data['serverInfo'];
          this.gdprInformationDetailsDto = serverInfo.gdprInformationDetailsDto;
          this.initForm();
          this.isHostSameAsEntity = this.gdprForm.get("isHostSameAsEntity").value;
          this.isMaintenanceSameAsHost = this.gdprForm.get("isMaintenanceSameAsHost").value;
          this.loadingData = false;
        },
        error: e => this.notificationsService.showErrorMessage(CommonMessages.CANNOT_LOAD_SERVER_PARAMETERS, e),
      });
  }


  initForm() {
    this.gdprForm = new FormGroup({
      entityName: new FormControl(this.gdprInformationDetailsDto.declaringEntity.name),
      entityAddress: new FormControl(this.gdprInformationDetailsDto.declaringEntity.address),
      entitySiret: new FormControl(this.gdprInformationDetailsDto.declaringEntity.siret),
      entityApeCode: new FormControl(this.gdprInformationDetailsDto.declaringEntity.apeCode),
      entityPhone: new FormControl(this.gdprInformationDetailsDto.declaringEntity.phoneNumber),
      entityMail: new FormControl(this.gdprInformationDetailsDto.declaringEntity.mail),
      respName: new FormControl(this.gdprInformationDetailsDto.declaringEntity.responsible.name),
      respTitle: new FormControl(this.gdprInformationDetailsDto.declaringEntity.responsible.title),
      dpoName: new FormControl(this.gdprInformationDetailsDto.declaringEntity.dpo.name),
      dpoMail: new FormControl(this.gdprInformationDetailsDto.declaringEntity.dpo.mail),
      hostingName: new FormControl(this.gdprInformationDetailsDto.hostingEntity.name),
      hostingAddress: new FormControl(this.gdprInformationDetailsDto.hostingEntity.address),
      hostingSiret: new FormControl(this.gdprInformationDetailsDto.hostingEntity.siret),
      hostingComment: new FormControl(this.gdprInformationDetailsDto.hostingEntityComments),
      maintenanceName: new FormControl(this.gdprInformationDetailsDto.maintenanceEntity.name),
      maintenanceAddress: new FormControl(this.gdprInformationDetailsDto.maintenanceEntity.address),
      maintenanceSiret: new FormControl(this.gdprInformationDetailsDto.maintenanceEntity.siret),
      isHostSameAsEntity: new FormControl(this.gdprInformationDetailsDto.hostingEntity.siret == this.gdprInformationDetailsDto.declaringEntity.siret),
      isMaintenanceSameAsHost: new FormControl(this.gdprInformationDetailsDto.maintenanceEntity.siret == this.gdprInformationDetailsDto.hostingEntity.siret),
    });
  }


  onSaveButtonClicked() {
    this.savingData = true;

    this.gdprInformationDetailsDto.declaringEntity.name = this.gdprForm.get("entityName").value;
    this.gdprInformationDetailsDto.declaringEntity.address = this.gdprForm.get("entityAddress").value;
    this.gdprInformationDetailsDto.declaringEntity.siret = this.gdprForm.get("entitySiret").value;
    this.gdprInformationDetailsDto.declaringEntity.apeCode = this.gdprForm.get("entityApeCode").value;
    this.gdprInformationDetailsDto.declaringEntity.mail = this.gdprForm.get("entityMail").value;
    this.gdprInformationDetailsDto.declaringEntity.phoneNumber = this.gdprForm.get("entityPhone").value;

    this.gdprInformationDetailsDto.declaringEntity.responsible.name = this.gdprForm.get("respName").value;
    this.gdprInformationDetailsDto.declaringEntity.responsible.title = this.gdprForm.get("respTitle").value;

    this.gdprInformationDetailsDto.declaringEntity.dpo.name = this.gdprForm.get("dpoName").value;
    this.gdprInformationDetailsDto.declaringEntity.dpo.mail = this.gdprForm.get("dpoMail").value;

    if (this.gdprForm.get("isHostSameAsEntity").value) {
      this.gdprInformationDetailsDto.hostingEntity.name = this.gdprForm.get("entityName").value;
      this.gdprInformationDetailsDto.hostingEntity.address = this.gdprForm.get("entityAddress").value;
      this.gdprInformationDetailsDto.hostingEntity.siret = this.gdprForm.get("entitySiret").value;
    } else {
      this.gdprInformationDetailsDto.hostingEntity.name = this.gdprForm.get("hostingName").value;
      this.gdprInformationDetailsDto.hostingEntity.address = this.gdprForm.get("hostingAddress").value;
      this.gdprInformationDetailsDto.hostingEntity.siret = this.gdprForm.get("hostingSiret").value;
    }
    this.gdprInformationDetailsDto.hostingEntityComments = this.gdprForm.get("hostingComment").value;

    if (this.gdprForm.get("isMaintenanceSameAsHost").value) {
      this.gdprInformationDetailsDto.maintenanceEntity.name = this.gdprInformationDetailsDto.hostingEntity.name;
      this.gdprInformationDetailsDto.maintenanceEntity.address = this.gdprInformationDetailsDto.hostingEntity.address;
      this.gdprInformationDetailsDto.maintenanceEntity.siret = this.gdprInformationDetailsDto.hostingEntity.siret;
    } else {
      this.gdprInformationDetailsDto.maintenanceEntity.name = this.gdprForm.get("maintenanceName").value;
      this.gdprInformationDetailsDto.maintenanceEntity.address = this.gdprForm.get("maintenanceAddress").value;
      this.gdprInformationDetailsDto.maintenanceEntity.siret = this.gdprForm.get("maintenanceSiret").value;
    }

    this.adminAdvancedConfigService
      .updateGdprInformationDetails(this.gdprInformationDetailsDto)
      .subscribe({
        next: () => {
          this.getGdprInformationDetails();
          this.notificationsService.showSuccessMessage(this.messages.RECORDING_SUCCESSFUL);
        },
        error: e => this.notificationsService.showErrorMessage(e.message)
      })
      .add(() => this.savingData = false);
  }


  updateHostSameAsEntity() {
    this.isHostSameAsEntity = this.gdprForm.get("isHostSameAsEntity").value;
  }


  updateMaintenanceSameAsHost() {
    this.isMaintenanceSameAsHost = this.gdprForm.get("isMaintenanceSameAsHost").value;
  }


}
