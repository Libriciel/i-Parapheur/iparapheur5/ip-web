/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Style, Weight } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../../shared/common-messages';
import { StepValidationMode } from '../../../../../models/workflows/step-validation-mode';
import { WorkflowEditorMessages } from '../workflow-editor-messages';
import { StepEventAction, WorkflowActor, WorkflowMetadata, WorkflowStepEvent, WorkflowStep, WorkflowDefinition, WorkflowStepColor } from '@libriciel/ls-workflow';
import { Action } from '@libriciel/iparapheur-internal';
import { ActionNamePipe } from '../../../../../shared/utils/action-name.pipe';
import { Observable } from 'rxjs';
import { PageDeskRepresentation, DeskRepresentation, AdminDeskService } from '@libriciel/iparapheur-provisioning';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StepForm } from '../../../../../shared/models/custom-types';
import { arrayNotEmptyOrUndefinedValidator } from '../../../../../shared/utils/formValidators/custom-validators';

@Component({
  selector: 'app-workflow-step-form',
  templateUrl: './workflow-step-form.component.html',
  styleUrls: ['./workflow-step-form.component.scss']
})
export class WorkflowStepFormComponent implements OnChanges {

  protected readonly styles = Style;
  protected readonly weight = Weight;
  protected readonly commonMessages = CommonMessages;
  protected readonly validationModes = StepValidationMode;
  protected readonly messages = WorkflowEditorMessages;
  protected readonly stepEventActions = StepEventAction;
  protected readonly actions = Action;

  protected readonly stepActions: Action[] = [Action.Visa, Action.Seal, Action.Signature, Action.ExternalSignature, Action.SecureMail];
  protected readonly stepValidationModes: StepValidationMode[] = [StepValidationMode.Simple, StepValidationMode.And, StepValidationMode.Or];

  @Input() tenantId!: string;
  @Input() workflowDefinition!: WorkflowDefinition;
  @Input() metadataList!: WorkflowMetadata[];

  @Input() selectedStepEvent: WorkflowStepEvent | null;

  @Output() workflowDefinitionChange: EventEmitter<WorkflowDefinition> = new EventEmitter<WorkflowDefinition>();
  @Output() selectedStepEventChange: EventEmitter<WorkflowStepEvent | null> = new EventEmitter<WorkflowStepEvent | null>();


  stepForm?: FormGroup<StepForm>;
  inEditionStep?: WorkflowStep;
  desks: DeskRepresentation[] = [];
  notifiedDesks: DeskRepresentation[] = [];

  constructor(private adminDeskService: AdminDeskService) {}

  ngOnChanges(_: SimpleChanges): void {
    this.manageSteps();
  }

  manageSteps() {
    this.inEditionStep = null;
    this.desks = [];
    this.notifiedDesks = [];

    if (!this.selectedStepEvent) {
      this.stepForm = null;
      return;
    }

    if (this.selectedStepEvent.action === StepEventAction.EDITION) {
      Object.assign(this.desks, this.selectedStepEvent.step.validators as DeskRepresentation[]);
      Object.assign(this.notifiedDesks, this.selectedStepEvent.step.notifiedValidators as DeskRepresentation[]);
    }

    if (!this.selectedStepEvent.step) {
      this.selectedStepEvent.step = new WorkflowStep();
      this.selectedStepEvent.step.type = Action.Visa;
      this.selectedStepEvent.step.name = "Visa";
      this.selectedStepEvent.step.validationMode = StepValidationMode.Simple;
    }

    this.inEditionStep = new WorkflowStep();

    Object.assign(this.inEditionStep, this.selectedStepEvent.step);

    this.stepForm = new FormGroup<StepForm>({
      name: new FormControl<string>(this.inEditionStep.name, [Validators.required]),
      type: new FormControl<string>(this.inEditionStep.type, [Validators.required]),
      validationMode: new FormControl<string>(this.inEditionStep.validationMode, [Validators.required]),
      desks: new FormControl<WorkflowActor[]>([...this.inEditionStep.validators], [Validators.required, arrayNotEmptyOrUndefinedValidator]),
      notifiedDesks: new FormControl<WorkflowActor[]>(this.inEditionStep.notifiedValidators),
      validationMetadata: new FormControl<WorkflowMetadata[]>(this.inEditionStep.validationMetadata),
      rejectionMetadata: new FormControl<WorkflowMetadata[]>(this.inEditionStep.rejectionMetadata),
    });
  }

  validateStep() {
    if (!this.selectedStepEvent || !this.inEditionStep) {
      return;
    }

    this.inEditionStep.name = ActionNamePipe.compute(this.inEditionStep.type);

    if (this.selectedStepEvent.index === -1) {
      this.workflowDefinition.steps.push(this.inEditionStep);
    } else {
      const deleteCount: number = this.selectedStepEvent.action === StepEventAction.EDITION ? 1 : 0;
      this.workflowDefinition.steps.splice(this.selectedStepEvent.index, deleteCount, this.inEditionStep);
    }

    this.selectedStepEvent = null;
    this.selectedStepEventChange.emit(null);
  }

  selectAction(event: string) {
    this.inEditionStep.name = event;
    this.inEditionStep.type = event.toUpperCase();

    if (this.isExternalAction(this.inEditionStep.type)) {
      this.inEditionStep.validationMode = StepValidationMode.Simple;
      this.selectValidationMode(this.inEditionStep.validationMode);
    }

    this.stepForm.patchValue({
      name: event,
      type: event.toUpperCase()
    });
  }

  isExternalAction(action: Action): boolean {
    return [Action.ExternalSignature.toUpperCase(), Action.SecureMail.toUpperCase()].includes(action.toUpperCase());
  }

  isMultiple(validationMode: StepValidationMode): boolean {
    return [StepValidationMode.And, StepValidationMode.Or].includes(validationMode);
  }

  selectValidationMode(event: string) {
    this.inEditionStep.validationMode = event.toUpperCase();

    if (event === StepValidationMode.Simple) {
      this.inEditionStep.validators = [this.stepForm.value.desks[0]];
      this.stepForm.patchValue({desks: [this.stepForm.value.desks[0]]});
    }
    this.stepForm.patchValue({validationMode: event.toUpperCase()});
  }

  setValidators(validators: WorkflowActor[]) {
    this.inEditionStep.validators = validators;
    this.stepForm.patchValue({desks: validators});
  }

  setNotifiedValidators(validators: WorkflowActor[]) {
    this.inEditionStep.notifiedValidators = validators;
    this.stepForm.patchValue({notifiedDesks: validators});
  }

  setValidationMetadataList(metadata: WorkflowMetadata[]) {
    this.inEditionStep.validationMetadata = metadata;
    this.stepForm.patchValue({validationMetadata: metadata});
  }

  setRejectionMetadataList(metadata: WorkflowMetadata[]) {
    this.inEditionStep.rejectionMetadata = metadata;
    this.stepForm.patchValue({rejectionMetadata: metadata});
  }

  retrieveDesksAsAdminFn = (page: number, pageSize: number, searchTerm: string): Observable<PageDeskRepresentation> => {
    return this.adminDeskService.listDesks(this.tenantId, page, pageSize, [], searchTerm);
  };

  public stepTypeToColorClassFunction(stepType: string): string {
    switch (stepType?.toUpperCase()) {
      case Action.Visa:
        return WorkflowStepColor.GREEN;
      case Action.Seal:
        return WorkflowStepColor.BLUE;
      case Action.ExternalSignature:
        return WorkflowStepColor.ORANGE;
      case Action.Signature:
        return WorkflowStepColor.RED;
      case Action.SecureMail:
        return WorkflowStepColor.LIGHT_BLUE;
      default:
        return WorkflowStepColor.WHITE;
    }
  }

  delete() {
    this.workflowDefinition.steps.splice(this.selectedStepEvent.index, 1);
    this.selectedStepEvent = null;
    this.selectedStepEventChange.emit(null);
  }

  cancel() {
    if (this.selectedStepEvent.action === StepEventAction.CREATION) {
      this.workflowDefinition.steps.splice(this.selectedStepEvent.index, 1);
    }
    this.selectedStepEvent = null;
    this.selectedStepEventChange.emit(null);
  }
}
