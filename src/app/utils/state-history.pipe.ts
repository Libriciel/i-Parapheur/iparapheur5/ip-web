/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { Task } from '../models/task';
import { Action, State, ExternalState } from '@libriciel/iparapheur-internal';


@Pipe({
  name: 'stateHistory'
})
export class StateHistoryPipe implements PipeTransform {


  public static compute(task: Task): string {

    if (!task) {
      return '';
    }

    switch (task.state) {
      case State.Bypassed: { return 'Contourné'; }
      case State.Rejected: { return 'Rejeté'; }
      case State.Seconded: { return 'Envoyé en avis complémentaire'; }
      case State.Transferred: { return 'Transféré'; }
      case State.Validated: {
        switch (task.action) {
          case Action.AskSecondOpinion: { return 'Envoyé en avis complémentaire'; }
          case Action.Archive: { return 'Envoyé à la corbeille'; }
          case Action.Bypass: { return 'Contourné'; }
          case Action.Chain: { return 'Enchaîné'; }
          case Action.Create: { return 'Créé'; }
          case Action.Delete: { return 'Supprimé'; }
          case Action.ExternalSignature: {
            return task.externalState === ExternalState.Active
              ? 'Retour de signature externe'
              : 'Envoyé en signature externe';
          }
          case Action.Ipng: { return 'Transféré'; }
          case Action.IpngReturn: { return 'Retourné'; }
          case Action.PaperSignature: { return 'Signé'; }
          case Action.Read: { return 'Lu'; }
          case Action.Recycle: { return 'Recyclé'; }
          case Action.Reject: { return 'Rejeté'; }
          case Action.Seal: { return 'Cacheté'; }
          case Action.SecondOpinion: { return 'Validé'; }
          case Action.SecureMail: {
            return task.externalState === ExternalState.Active
              ? 'Retour de mail sécurisé'
              : 'Envoyé en mail sécurisé';
          }
          case Action.Start: { return 'Envoyé dans le circuit'; }
          case Action.Signature: { return 'Signé'; }
          case Action.Transfer: { return 'Transféré'; }
          case Action.Update: { return 'Modifié'; }
          case Action.Undo: { return 'Annulé'; }
          case Action.Visa: { return 'Visé'; }
          default: { return ''; }
        }
      }
      default: { return ''; }
    }
  }


  transform(value: Task, ..._args: unknown[]): string {
    return StateHistoryPipe.compute(value);
  }


}
