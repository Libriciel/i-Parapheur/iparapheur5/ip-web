/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPasswordDoubledInputComponent } from './user-password-doubled-input.component';
import { User } from '../../../../../models/auth/user';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';


describe('UserPasswordDoubledInputComponent', () => {


  let component: UserPasswordDoubledInputComponent;
  let fixture: ComponentFixture<UserPasswordDoubledInputComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        declarations: [UserPasswordDoubledInputComponent],
        imports: [HttpClientTestingModule],
        providers: [HttpClient, HttpHandler],
      })
      .compileComponents();
  });


  beforeEach(() => {

    fixture = TestBed.createComponent(UserPasswordDoubledInputComponent);
    component = fixture.componentInstance;

    component.user = new User();
    component.user["isLdapSynchronized" as any] = false;

    fixture.detectChanges();
  });


  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
  //
  //
  // it('should count special characters', () => {
  //   expect(component.countSpecialCharactersFn("")).toBe(0);
  //   expect(component.countSpecialCharactersFn("abcd")).toBe(0);
  //   expect(component.countSpecialCharactersFn("ab-cd")).toBe(1);
  //   expect(component.countSpecialCharactersFn("&~#'")).toBe(4);
  // });
  //
  //
  // it('should count uppercase', () => {
  //   expect(component.countUppercaseFn("")).toBe(0);
  //   expect(component.countUppercaseFn("abcd")).toBe(0);
  //   expect(component.countUppercaseFn("ab-CD")).toBe(2);
  //   expect(component.countUppercaseFn("&~ABCD#'")).toBe(4);
  // });
  //
  //
  // it('should count lowercase', () => {
  //   expect(component.countLowercaseFn("")).toBe(0);
  //   expect(component.countLowercaseFn("ABCD")).toBe(0);
  //   expect(component.countLowercaseFn("ab-CD")).toBe(2);
  //   expect(component.countLowercaseFn("&~abcd#'")).toBe(4);
  // });
  //
  //
  // it('should count digits', () => {
  //   expect(component.countDigitsFn("")).toBe(0);
  //   expect(component.countDigitsFn("ABCD")).toBe(0);
  //   expect(component.countDigitsFn("ab00CD")).toBe(2);
  //   expect(component.countDigitsFn("&~1234#'")).toBe(4);
  // });


});
