iparapheur Web
===============

[![pipeline status](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-web/badges/develop/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-web/commits/develop) [![coverage report](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-web/badges/develop/coverage.svg)](https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-web/commits/develop) [![Lignes de code](https://sonarqube.libriciel.fr/api/project_badges/measure?project=ip-web&metric=ncloc)](https://sonarqube.libriciel.fr/dashboard?id=ip-web)   
[![quality_gate](https://sonarqube.libriciel.fr/api/project_badges/measure?project=ip-web&metric=alert_status)](https://sonarqube.libriciel.fr/dashboard?id=ip-web) [![maintainability](https://sonarqube.libriciel.fr/api/project_badges/measure?project=ip-web&metric=sqale_rating)](https://sonarqube.libriciel.fr/dashboard?id=ip-web) [![reliability](https://sonarqube.libriciel.fr/api/project_badges/measure?project=ip-web&metric=reliability_rating)](https://sonarqube.libriciel.fr/dashboard?id=ip-web) [![security](https://sonarqube.libriciel.fr/api/project_badges/measure?project=ip-web&metric=security_rating)](https://sonarqube.libriciel.fr/dashboard?id=ip-web)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Development server

Run `ng serve` for a dev server.  
Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build 

Run `ng build` to build the project.  
The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
