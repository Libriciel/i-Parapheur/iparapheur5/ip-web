/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, combineLatest } from 'rxjs';
import { Action } from '@libriciel/iparapheur-legacy';
import { MailParams, SecureMailService as StandardSecureMailService } from '@libriciel/iparapheur-standard';
import { FolderService as InternalFolderService } from '@libriciel/iparapheur-internal';
import { CommonIcons } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { ActionPopupMessages } from '../../action-popup-messages';
import { NotificationsService } from '../../../../../../services/notifications.service';
import { Folder } from '../../../../../../models/folder/folder';
import { FolderUtils } from '../../../../../../utils/folder-utils';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-send-by-mail-popup',
  templateUrl: './send-by-mail-popup.component.html',
  styleUrls: ['./send-by-mail-popup.component.scss']
})
export class SendByMailPopupComponent implements OnInit {


  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_DESK_ID_KEY = 'deskId';
  public static readonly INJECTABLE_FOLDERS_KEY = 'folders';
  public static readonly INJECTABLE_CURRENT_ACTION_KEY = 'currentAction';

  readonly actionEnum = Action;
  readonly commonIcons = CommonIcons;
  readonly messages = ActionPopupMessages;
  readonly commonMessages = CommonMessages;


  isProcessing = false;
  mailToList: string[] = [];
  mailCarbonCopyList: string[] = [];
  mailBlindCarbonCopyList: string[] = [];
  mailObject: string = '';
  mailBody: string = '';
  includeDocket: boolean = false;
  validationMetadataFormValid: boolean;



  // <editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              private internalFolderService: InternalFolderService,
              private standardSecureMailService: StandardSecureMailService,
              private notificationService: NotificationsService,
              @Inject(SendByMailPopupComponent.INJECTABLE_FOLDERS_KEY) public folders: Folder[],
              @Inject(SendByMailPopupComponent.INJECTABLE_DESK_ID_KEY) public deskId: string,
              @Inject(SendByMailPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(SendByMailPopupComponent.INJECTABLE_CURRENT_ACTION_KEY) public currentAction: Action) {

    if (!FolderUtils.MAIL_ACTIONS.includes(currentAction)) {
      throw new Error('SendByMailPopupComponent currentAction not relevant ' + currentAction);
    }
  }


  ngOnInit(): void {
    this.validationMetadataFormValid = this.currentAction !== Action.SecureMail;
  }


  // </editor-fold desc="LifeCycle">


  isFormValid(form: HTMLFormElement): boolean {
    return form.checkValidity() && this.validationMetadataFormValid;
  }


  onSendButtonClicked() {
    this.isProcessing = true;

    const mailParams = {} as MailParams;
    mailParams.to = this.mailToList.map(mail => mail.trim());
    mailParams.cc = this.mailCarbonCopyList.map(mail => mail.trim());
    mailParams.bcc = this.mailBlindCarbonCopyList.map(mail => mail.trim());
    mailParams.object = this.mailObject;
    mailParams.message = this.mailBody;
    mailParams.includeDocket = this.includeDocket;

    const observableList: Observable<void>[] = [];
    this.folders.forEach(folder => observableList.push(this.getMailRequest(folder, mailParams)));

    combineLatest(observableList)
      .subscribe({
        next: () => {
          this.notificationService.showSuccessMessage(ActionPopupMessages.actionSuccessMessage(this.currentAction));
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error: e => this.notificationService.showErrorMessage(ActionPopupMessages.actionErrorMessage(this.currentAction), e.message)
      })
      .add(() => this.isProcessing = false);
  }


  getMailRequest(folder: Folder, mailParams: MailParams): Observable<void> {
    mailParams.metadata = folder.populatedMandatoryStepMetadata;
    return (this.currentAction === Action.SecureMail)
      ? this.standardSecureMailService
        .requestSecureMail(this.tenantId, this.deskId, folder.id, folder.stepList[0].id, mailParams)
      : this.internalFolderService
        .sendFolderByMail(this.tenantId, folder.id, mailParams)
        .pipe(catchError(this.notificationService.handleHttpError('sendFolderByMail')));
  }


}
