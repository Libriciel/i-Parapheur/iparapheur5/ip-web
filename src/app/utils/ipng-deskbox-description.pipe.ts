/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { Deskbox } from '../models/ipng/deskbox';
import { CommonMessages } from '../shared/common-messages';

@Pipe({
  name: 'ipngDeskboxDescription'
})
export class IpngDeskboxDescriptionPipe implements PipeTransform {

  static getPreferredDescriptionForDeskbox(value: Deskbox): string {
    if (!value) {
      return CommonMessages.NO_ELEMENT;
    }

    if (value.description) {

      // TODO have a real language selection logic

      if (value.description['fr']) {
        return value.description['fr'];
      }

      if (value.description['fr_FR']) {
        return value.description['fr_FR'];
      }
    }

    return value.id;
  }

  transform(value: Deskbox, ..._args: unknown[]): unknown {
    return IpngDeskboxDescriptionPipe.getPreferredDescriptionForDeskbox(value);
  }
}
