/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { DetachedSignature } from './signature/detached-signature';
import { CustomNumberMap } from '../shared/models/custom-types';
import { PdfSignaturePosition, SignaturePlacement } from '@libriciel/iparapheur-legacy';
import { ValidatedSignatureInformation } from '@libriciel/iparapheur-internal/model/validatedSignatureInformation';

export class Document {

  id: string;
  name: string;
  index: number;
  size: number;
  isMainDocument: boolean;
  signatureProof: boolean;
  deletable: boolean;
  mediaType: string;
  pdfVisualId: string;
  pageCount: number;
  detachedSignatures: DetachedSignature[];
  embeddedSignatureInfos: ValidatedSignatureInformation[];
  sealTags: CustomNumberMap<PdfSignaturePosition>;
  signatureTags: CustomNumberMap<PdfSignaturePosition>;
  signaturePlacementAnnotations: SignaturePlacement[];

}
