/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { RouterStateSnapshot, ActivatedRouteSnapshot, ResolveFn } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CommonMessages } from '../../shared/common-messages';
import { TenantService, TenantDto } from '@libriciel/iparapheur-legacy';
import { NotificationsService } from '../notifications.service';
import { inject } from '@angular/core';

export const TenantResolver: ResolveFn<TenantDto> = (
  route: ActivatedRouteSnapshot,
  _state: RouterStateSnapshot,
  notificationsService: NotificationsService = inject(NotificationsService),
  tenantService: TenantService = inject(TenantService)
): Observable<TenantDto> => {

  const tenantId = route?.paramMap?.get('tenantId');

  if (!tenantId) {
    return of({id: null, name: null});
  }

  return tenantService
    .getTenant(tenantId)
    .pipe(catchError(notificationsService.handleHttpError('getTenant')))
    .pipe(catchError(() => of({id: tenantId, name: CommonMessages.UNREACHABLE_TENANT})));
};


