/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, AfterViewInit, Injector } from '@angular/core';
import { faCaretUp, faCaretDown, faDownload, IconDefinition, faCog } from '@fortawesome/free-solid-svg-icons';
import { Folder } from '../../../models/folder/folder';
import { NotificationsService } from '../../../services/notifications.service';
import { compareById, isCurrentVersion52OrAbove } from '../../../utils/string-utils';
import { ActivatedRoute } from '@angular/router';
import { CommonMessages } from '../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { GlobalPopupService } from '../../../shared/service/global-popup.service';
import { LegacyUserService } from '../../../services/ip-core/legacy-user.service';
import { AdminTenantService, TenantSortBy, UserDto } from '@libriciel/iparapheur-provisioning';
import { TenantService, ArchiveViewColumn, UserPreferencesDto, PageTenantRepresentation, TaskViewColumn } from '@libriciel/iparapheur-legacy';
import { AdminTrashBinService as InternalAdminTrashBinService, PageFolderListableDto, FolderListableDto, TableName, TableLayoutDto, LabelledColumnType, LabelledColumn } from '@libriciel/iparapheur-internal';
import { AdminTrashBinService as StandardAdminTrashBinService, FolderSortBy, TenantRepresentation } from '@libriciel/iparapheur-standard';
import { catchError } from 'rxjs/operators';
import { TasksMessages } from '../tasks/tasks-messages';
import { FileService } from '../../../services/file.service';
import { CrudOperation } from '../../../services/crud-operation';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectedUserPreferencesService } from '../../../services/selected-user-preferences.service';
import { TableLayoutPopupComponent } from '../tasks/table-layout-popup/table-layout-popup.component';
import { defaultArchiveListLabelledColumnList, defaultArchiveListColumnList } from '../../../services/resolvers/user-preferences.resolver';

@Component({
  selector: 'app-trash-bin-list',
  templateUrl: './trash-bin-list.component.html',
  styleUrls: ['./trash-bin-list.component.scss']
})
export class TrashBinListComponent implements AfterViewInit {

  readonly show52Features: boolean = isCurrentVersion52OrAbove();
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly pageSizes: number[] = [10, 15, 20, 50, 100];
  readonly downloadIcon: IconDefinition = faDownload;
  readonly caretUp: IconDefinition = faCaretUp;
  readonly caretDown: IconDefinition = faCaretDown;
  readonly sortByEnum = FolderSortBy;
  readonly compareByIdFn = compareById;
  readonly columnsEnum = ArchiveViewColumn;
  readonly cogIcon = faCog;
  readonly columnTypeEnum = LabelledColumnType;


  tenantList: Array<TenantRepresentation> = [];
  selectedTenant: TenantRepresentation;

  deskId: string;
  state: string;
  sortBy: FolderSortBy = FolderSortBy.FolderName;
  asc: boolean = true;

  folderList: Folder[] = [];
  page: number = 1;
  pageSizeIndex: number = 1;
  total: number = 0;

  userPreferences: UserPreferencesDto;
  loading: boolean = false;
  colspan: number;
  currentUserAdmin: boolean;
  currentUser: UserDto;

  currentTableLayout: TableLayoutDto;
  usedColumns: LabelledColumn[];


  // <editor-fold desc="LifeCycle">


  constructor(private adminTenantService: AdminTenantService,
              private fileService: FileService,
              public notificationsService: NotificationsService,
              public modalService: NgbModal,
              public legacyUserService: LegacyUserService,
              public route: ActivatedRoute,
              public globalPopupService: GlobalPopupService,
              private internalAdminTrashBinService: InternalAdminTrashBinService,
              private selectedUserPreferencesService: SelectedUserPreferencesService,
              private standardAdminTrashBinService: StandardAdminTrashBinService,
              private tenantService: TenantService) {}


  ngAfterViewInit() {
    this.selectedUserPreferencesService.currentUserPreferences$.subscribe(userPreferences => {
      this.userPreferences = userPreferences;

      this.initArchiveListAttributes();
      this.initArchiveListVariables();
    });
  }

  initArchiveListAttributes() {
    this.currentUser = this.route.snapshot.data["currentUser"];

    this.currentUserAdmin = this.legacyUserService.isCurrentUserAdmin();

    const sortBy: string[] = [TenantSortBy.Name + ',ASC'];
    const tenantRetriever$ = this.legacyUserService.isCurrentUserSuperAdmin()
      ? this.adminTenantService.listTenants(0, 10000, sortBy)
      : this.tenantService.listTenantsForUser(0, 10000, sortBy, true);

    tenantRetriever$
      .pipe(catchError(this.notificationsService.handleHttpError('getTenants')))
      .subscribe((tenantsRetrieved: PageTenantRepresentation) => {
        this.tenantList = tenantsRetrieved.content;
        this.selectedTenant = this.tenantList.length > 0 ? this.tenantList[0] : undefined;
        this.refreshFolderList();
      });
  }


  initArchiveListVariables(tableLayoutDto?: TableLayoutDto) {
    this.currentTableLayout = tableLayoutDto ?? this.userPreferences.tableLayoutList.find((list: TableLayoutDto): boolean => list.tableName === TableName.ArchiveList);

    this.usedColumns = this.currentTableLayout.labelledColumnList;

    this.colspan = this.usedColumns.length;
    this.pageSizeIndex = this.pageSizes.findIndex(item => item === this.userPreferences.taskViewDefaultPageSize) | 1;
  }


  // </editor-fold desc="LifeCycle">


  refreshFolderList() {

    if (!this.selectedTenant) {
      return;
    }

    this.loading = true;
    const direction: string = (this.asc ? ",ASC" : ",DESC");
    const sortBy: string[] = [
      this.sortBy + direction,
      FolderSortBy.FolderName + direction,
      FolderSortBy.FolderId + direction,
    ];

    this.internalAdminTrashBinService
      .listTrashBinListableFolders(this.selectedTenant.id, this.page - 1, this.getPageSize(this.pageSizeIndex), sortBy)
      .pipe(catchError(this.notificationsService.handleHttpError('listTrashBinFolders')))
      .subscribe({
        next: (foldersRetrieved: PageFolderListableDto) => {
          this.folderList = foldersRetrieved.content.map((representation: FolderListableDto) => Object.assign(new Folder(), representation));
          this.total = foldersRetrieved.totalElements;
        },
        error: e => this.notificationsService.showErrorMessage(TasksMessages.ERROR_RETRIEVING_FOLDERS, e)
      })
      .add((): boolean => this.loading = false);
  }


  onDownloadButtonClicked(folder: Folder) {
    // noinspection JSDeprecatedSymbols
    this.fileService
      .downloadFileWithObservable(
        this.standardAdminTrashBinService
          .downloadTrashBinFolderZip(this.selectedTenant.id, folder.id, 'body', true, {httpHeaderAccept: 'application/octet-stream'})
          .pipe(catchError(this.notificationsService.handleHttpError('downloadTrashBinFolderZip'))),
        folder.name + '.zip'
      )
      .subscribe({
        next: () => { /* Nothing to do */ },
        error: e => this.notificationsService.showErrorMessage(TasksMessages.ERROR_DOWNLOADING_FOLDER, e)
      });
  }


  isIdColumn(column: LabelledColumn): boolean {
    return column.id == TaskViewColumn.TaskId.toString() || column == TaskViewColumn.FolderId.toString();
  }


  showIdColumn(column: LabelledColumn): boolean {
    return !(this.isIdColumn(column) && !this.userPreferences.showAdminIds);
  }


  getPageSize(pageIndex: number): number {
    return this.pageSizes[pageIndex - 1];
  }


  mapColumnToSortBy(column: LabelledColumn): FolderSortBy {
    switch (column.id as ArchiveViewColumn) {
      case ArchiveViewColumn.Name:
        return FolderSortBy.FolderName;
      case ArchiveViewColumn.Id:
        return FolderSortBy.FolderId;
      case ArchiveViewColumn.CreationDate:
        return FolderSortBy.CreationDate;
      default:
        return null;
    }
  }


  // <editor-fold desc="UI callbacks">


  onDeleteButtonClicked(folder: Folder) {
    // noinspection JSDeprecatedSymbols
    this.globalPopupService
      .showDeleteValidationPopup(CommonMessages.foldersValidationPopupLabel([folder]), CommonMessages.foldersValidationPopupTitle([folder]))
      .then(
        () => this.standardAdminTrashBinService
          .deleteTrashBinFolder(this.selectedTenant.id, folder.id)
          .pipe(catchError(this.notificationsService.handleHttpError('deleteTrashBinFolder')))
          .subscribe({
            next: () => {
              this.notificationsService.showCrudMessage(CrudOperation.Delete, folder);
              this.refreshFolderList();
            },
            error: e => this.notificationsService.showCrudMessage(CrudOperation.Delete, folder, e.message, false)
          }),
        () => {/* Dismissed */}
      );
  }


  onRowOrderClicked(column: LabelledColumn) {
    const row: FolderSortBy = this.mapColumnToSortBy(column);
    if (!!row) {
      this.asc = (row === this.sortBy) ? !this.asc : true;
      this.sortBy = row;
      this.refreshFolderList();
    }
  }


  public openTableLayoutModal() {
    this.modalService
      .open(
        TableLayoutPopupComponent, {
          injector: Injector.create({
            providers: [
              {provide: TableLayoutPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.selectedTenant.id},
              {provide: TableLayoutPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: null},
              {provide: TableLayoutPopupComponent.INJECTABLE_TABLE_LAYOUT_KEY, useValue: this.currentTableLayout},
              {provide: TableLayoutPopupComponent.INJECTABLE_USER_PREFERENCES_KEY, useValue: this.userPreferences},
              {provide: TableLayoutPopupComponent.INJECTABLE_CURRENT_USER_KEY, useValue: this.currentUser}
            ]
          }),
          size: 'lg'
        }
      )
      .result
      .then(
        result => {
          if (result.reset) {
            this.currentTableLayout.id = null;
            this.currentTableLayout.columnList = defaultArchiveListColumnList;
            this.currentTableLayout["labelledColumnList" as any] = defaultArchiveListLabelledColumnList;
            this.notificationsService.showSuccessMessage("La configuration du bureau a bien remise à zéro, la configuration par défaut a donc été appliquée");
          } else {
            this.currentTableLayout = result.tableLayout;

            const editedTableLayout: TableLayoutDto = this.userPreferences.tableLayoutList
              .find((tableLayout: TableLayoutDto): boolean => tableLayout.id === result?.tableLayout?.id);
            if (!editedTableLayout) {
              this.userPreferences.tableLayoutList.push(this.currentTableLayout);
            } else {
              editedTableLayout.columnList = result?.tableLayout?.columnList ?? defaultArchiveListColumnList;
              editedTableLayout["labelledColumnList" as any] = result?.tableLayout?.labelledColumnList ?? defaultArchiveListLabelledColumnList;
            }
            this.notificationsService.showSuccessMessage("La configuration du bureau a bien été enregistrée");
          }

          this.initArchiveListVariables(this.currentTableLayout);
          this.refreshFolderList();
        },
        () => { /* Dismissed */ }
      );
  }


  // </editor-fold desc="UI callbacks">


}
