/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Output, EventEmitter, Input } from '@angular/core';
import { CommonMessages } from '../../../../../../../shared/common-messages';
import { TenantMessages } from '../../../tenant-messages';
import { UserPreferencesDto, TenantDto } from '@libriciel/iparapheur-legacy';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-tenant-popup-general',
  templateUrl: './admin-tenant-popup-general.component.html',
  styleUrls: ['./admin-tenant-popup-general.component.scss']
})
export class AdminTenantPopupGeneralComponent {

  readonly commonMessages = CommonMessages;
  readonly messages = TenantMessages;

  generalForm: UntypedFormGroup;

  @Output() valid = new EventEmitter<boolean>();
  @Input() userPreferences: UserPreferencesDto;
  @Input() pendingTenant: TenantDto;


  ngOnInit() {
    this.generalForm = new UntypedFormGroup({
      nameInput: new UntypedFormControl(this.pendingTenant.name, [
        Validators.required,
        Validators.maxLength(255)
      ]),
      zipCodeInput: new UntypedFormControl(this.pendingTenant.zipCode, [
        Validators.maxLength(16)
      ])
    });

    this.generalForm.get('nameInput')?.valueChanges.subscribe(value => this.pendingTenant.name = value);
    this.generalForm.get('zipCodeInput')?.valueChanges.subscribe(value => this.pendingTenant.zipCode = value);

    this.generalForm.statusChanges.subscribe(() => this.onChange());
  }

  onChange() {
    this.valid.emit(this.generalForm.valid);
  }

}
