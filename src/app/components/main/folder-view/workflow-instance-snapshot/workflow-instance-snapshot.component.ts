/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Injector } from '@angular/core';
import { WorkflowActor } from '@libriciel/ls-workflow';
import { IpStepInstance } from '../../../../models/workflows/ip-step-instance';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonIcons } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../shared/common-messages';
import { FolderViewMessages } from '../folder-view-messages';
import { faMinusSquare, faPlusSquare } from '@fortawesome/free-regular-svg-icons';
import { DeskListPopupComponent } from '../../../../shared/components/desk-list-popup/desk-list-popup.component';
import { FolderUtils } from '../../../../utils/folder-utils';
import { Action, State, FolderViewBlock, DeskRepresentation, UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';
import { StepValidationMode } from '../../../../models/workflows/step-validation-mode';
import { IpWorkflowInstance } from '../../../../models/workflows/ip-workflow-instance';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { StepToActionPipe } from '../../../../shared/utils/step-to-action.pipe';

@Component({
  selector: 'app-workflow-instance-snapshot',
  templateUrl: './workflow-instance-snapshot.component.html',
  styleUrls: ['./workflow-instance-snapshot.component.scss']
})
export class WorkflowInstanceSnapshotComponent {

  readonly messages = FolderViewMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly stateEnum = State;
  readonly actionEnum = Action;
  readonly secondaryActionEnum = SecondaryAction;
  readonly maxDisplayedValidators = 8;
  readonly validationModeEnum = StepValidationMode;
  readonly minusSquareIcon: IconDefinition = faMinusSquare;
  readonly plusSquareIcon: IconDefinition = faPlusSquare;
  readonly folderUtils = FolderUtils;
  readonly folderViewBlockEnum = FolderViewBlock;


  @Input() userPreferences: UserPreferencesDto;

  internalWorkflowInstance: IpWorkflowInstance;
  steps: IpStepInstance[][] = [];
  isCollapsed: boolean = false;


  // <editor-fold desc="LifeCycle">


  constructor(public modalService: NgbModal) {}


  @Input()
  get workflowInstance() {return this.internalWorkflowInstance;}


  set workflowInstance(newWorkflowInstance: IpWorkflowInstance) {
    this.internalWorkflowInstance = newWorkflowInstance;
    this.steps = this.groupSteps();
    console.log("WorkflowInstanceSnapshotComponent - regrouped steps : ", this.steps);
  }


  // </editor-fold desc="LifeCycle">


  isRejected(step: IpStepInstance): boolean {
    return step.state.toUpperCase() === State.Rejected.toUpperCase();
  }


  areParallel(steps: IpStepInstance[]): boolean {

    const validatedCount: number = steps
      .filter(step => step.state.toUpperCase() === State.Validated.toUpperCase())
      .length;

    const unPerformedCount: number = steps
      .filter(step => !FolderUtils.isPassed(step))
      .length;

    const addedTasks: number = steps
      .filter(step =>
        [State.Seconded.toUpperCase(), State.Transferred.toUpperCase()].includes(step.state.toUpperCase())
        || [Action.SecondOpinion.toUpperCase(), Action.Transfer.toUpperCase()].includes(step.type.toUpperCase())
      ).length;

    return (validatedCount + unPerformedCount - addedTasks) > 1;
  }


  countPending(steps: IpStepInstance[]): number {
    return steps
      .filter(step => FolderUtils.isActive(step))
      .length;
  }


  flatMapStepValidators(steps: IpStepInstance[]): WorkflowActor[] {
    return steps.flatMap(s => s.validators);
  }


  showValidators(validators: WorkflowActor[] | DeskRepresentation[]) {
    this.modalService
      .open(
        DeskListPopupComponent, {
          injector: Injector.create({
            providers: [
              {provide: DeskListPopupComponent.INJECTABLE_DESK_LIST_KEY, useValue: validators},
              {provide: DeskListPopupComponent.INJECTABLE_USER_PREFERENCES, useValue: this.userPreferences},
            ]
          }),
          size: 'lg'
        }
      )
      .result
      .then(
        result => console.log(result),
        () => {/* dismissed */}
      );
  }


  /**
   * The creation workflow index is always 1.
   * The validation workflow is 2 or more (+1 for every chain action).
   */
  hasCreationWorkflow(): boolean {
    return this.steps.some(l => l.some(s => s.workflowIndex == 1));
  }


  /**
   * By convenience, we display the draft step in the creation workflow.
   * Similarly, we display the final step as part of the validation workflow.
   * @param step
   */
  isWorkflowFirstStep(step: IpStepInstance): boolean {
    const isFirstStep = step?.stepIndex === 0;
    const isBeforeCreationWorkflow = step?.workflowIndex == 0;
    const isFirstOfValidationWorkflow = step?.workflowIndex == 2;
    return isFirstStep && (isBeforeCreationWorkflow || isFirstOfValidationWorkflow);
  }


  groupSteps(): IpStepInstance[][] {

    const result: IpStepInstance[][] = [];
    for (const step of this.workflowInstance.steps as IpStepInstance[]) {

      const lastExistingTask = result[result.length - 1]?.[0];
      const isSameWorkflow = !!step.workflowIndex && (lastExistingTask?.workflowIndex === step.workflowIndex);
      const isSameIndex = step.stepIndex != null && (lastExistingTask?.stepIndex === step.stepIndex);

      if (isSameWorkflow && isSameIndex) {
        result[result.length - 1].push(step);
      } else {
        result.push([step]);
      }
    }

    return result;
  }


}
