/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { GenericDeskNamePipe } from './generic-desk-name.pipe';
import { WorkflowUtils } from './workflow-utils';

describe('GenericDeskNamePipe', () => {


  it('create an instance', () => {
    const pipe = new GenericDeskNamePipe();
    expect(pipe).toBeTruthy();
  });


  it('return a template string on generic desk input', () => {
    const dummyEmitterDesk = {
      id: WorkflowUtils.GENERIC_DESK_IDS.EMITTER_ID,
      name: 'should not appear',
      shortName: 'should not appear',
      ownerIds: [],
      associatedDeskIds: [],
      filterableMetadataIds: [],
      supervisorIds: [],
      delegationManagerIds: []
    };
    expect(GenericDeskNamePipe.compute(dummyEmitterDesk)).toBe(WorkflowUtils.GENERIC_DESK_NAMES.EMITTER);

    const dummyVariableDesk = {
      id: WorkflowUtils.GENERIC_DESK_IDS.VARIABLE_DESK_ID,
      name: 'should not appear',
      shortName: 'should not appear',
      ownerIds: [],
      associatedDeskIds: [],
      filterableMetadataIds: [],
      supervisorIds: [],
      delegationManagerIds: []
    };
    expect(GenericDeskNamePipe.compute(dummyVariableDesk)).toBe(WorkflowUtils.GENERIC_DESK_NAMES.VARIABLE_DESK);

    const dummyBossOfDesk = {
      id: WorkflowUtils.GENERIC_DESK_IDS.BOSS_OF_ID,
      name: 'should not appear',
      shortName: 'should not appear',
      ownerIds: [],
      associatedDeskIds: [],
      filterableMetadataIds: [],
      supervisorIds: [],
      delegationManagerIds: []
    };
    expect(GenericDeskNamePipe.compute(dummyBossOfDesk)).toBe(WorkflowUtils.GENERIC_DESK_NAMES.BOSS_OF);
  });


  it('return given desk name on regular input', () => {
    expect(GenericDeskNamePipe.compute({
      id: 'desk01',
      shortName: 'Desk 01',
      name: 'Desk 01',
      ownerIds: [],
      associatedDeskIds: [],
      filterableMetadataIds: [],
      supervisorIds: [],
      delegationManagerIds: []
    })).toBe('Desk 01');
    expect(GenericDeskNamePipe.compute({
      id: 'desk01',
      shortName: '',
      name: '',
      ownerIds: [],
      associatedDeskIds: [],
      filterableMetadataIds: [],
      supervisorIds: [],
      delegationManagerIds: []
    })).toBe('');
    expect(GenericDeskNamePipe.compute({
      id: 'desk01',
      shortName: undefined,
      name: undefined,
      ownerIds: null,
      associatedDeskIds: [],
      filterableMetadataIds: [],
      supervisorIds: [],
      delegationManagerIds: []
    })).toBe(undefined);
    expect(GenericDeskNamePipe.compute({
      id: 'desk01',
      shortName: null,
      name: null,
      ownerIds: null,
      associatedDeskIds: [],
      filterableMetadataIds: [],
      supervisorIds: [],
      delegationManagerIds: []
    })).toBe(null);
  });


  it('return undefined on invalid input', () => {
    expect(GenericDeskNamePipe.compute(undefined)).toBe(undefined);
    expect(GenericDeskNamePipe.compute(null)).toBe(undefined);
  });


});
