/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input } from '@angular/core';
import { Folder } from '../../../../models/folder/folder';
import { CommonIcons } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../shared/common-messages';
import { FolderViewMessages } from '../folder-view-messages';
import { faMinusSquare, faPlusSquare } from '@fortawesome/free-regular-svg-icons';
import { Task } from '../../../../models/task';
import { FolderUtils } from '../../../../utils/folder-utils';
import { State, FolderViewBlock } from '@libriciel/iparapheur-legacy';

@Component({
  selector: 'app-annotation-display',
  templateUrl: './annotation-display.component.html',
  styleUrls: ['./annotation-display.component.scss']
})
export class AnnotationDisplayComponent implements OnInit {

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = FolderViewMessages;
  readonly minusSquareIcon = faMinusSquare;
  readonly plusSquareIcon = faPlusSquare;

  @Input() folder: Folder;
  @Input() currentFolderViewBlock: FolderViewBlock;

  isPrivateAnnotation: boolean;
  isCollapsed = false;
  // TODO : merge these two, and have a proper behavior on single-tasks
  publicAnnotationTaskList: Task[];
  privateAnnotationTask: Task;


  // <editor-fold desc="LifeCycle">


  ngOnInit(): void {

    this.isPrivateAnnotation = this.currentFolderViewBlock === FolderViewBlock.PrivateAnnotation;

    if (this.isPrivateAnnotation) {
      this.privateAnnotationTask = this.getPrivateAnnotation();
    } else {
      this.publicAnnotationTaskList = this.getPublicAnnotationList();
    }
  }


  // </editor-fold desc="LifeCycle">


  private getPublicAnnotationList(): Task[] {
    return this.folder?.stepList?.filter(step => [State.Validated, State.Rejected, State.Transferred, State.Seconded].includes(step.state))
      ?.filter(step => step.publicAnnotation);
  }


  private getPrivateAnnotation(): Task {
    const filteredList = this.folder.stepList?.filter(
      step => [State.Validated, State.Rejected, State.Transferred, State.Seconded, State.Current].includes(step.state)
    );

    const indexOfPossiblePrivateAnnotationTask = filteredList.findIndex(step => FolderUtils.isActive(step)) - 1;
    const possiblePrivateAnnotationTask = filteredList[indexOfPossiblePrivateAnnotationTask];
    return possiblePrivateAnnotationTask?.privateAnnotation ? possiblePrivateAnnotationTask : null;
  }


}
