/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { WorkflowDefinition, WorkflowStep, WorkflowActor } from '@libriciel/ls-workflow';
import { WorkflowDefinitionDto, StepDefinitionDto, DeskRepresentation } from '@libriciel/iparapheur-provisioning';
import { SubtypeDto } from '@libriciel/iparapheur-provisioning';
import { metadataValueIsNotEmpty } from '../../utils/string-utils';

export class WorkflowUtils {
  static readonly GENERIC_DESK_IDS = {
    EMITTER_ID: 'i_Parapheur_reserved_emitter',
    VARIABLE_DESK_ID: 'i_Parapheur_reserved_variable_desk',
    BOSS_OF_ID: 'i_Parapheur_reserved_boss_of'
  };

  static readonly GENERIC_DESK_NAMES = {
    EMITTER: 'Émetteur',
    VARIABLE_DESK: 'Bureau Variable',
    BOSS_OF: 'Chef de'
  };


  static readonly GENERIC_WORKFLOW_ACTORS = {
    EMITTER: {id: this.GENERIC_DESK_IDS.EMITTER_ID, name: this.GENERIC_DESK_NAMES.EMITTER},
    VARIABLE_DESK: {id: this.GENERIC_DESK_IDS.VARIABLE_DESK_ID, name: this.GENERIC_DESK_NAMES.VARIABLE_DESK},
    BOSS_OF: {id: this.GENERIC_DESK_IDS.BOSS_OF_ID, name: this.GENERIC_DESK_NAMES.BOSS_OF}
  };

  static countVariableDeskStep(workflowDefinition: WorkflowDefinition): number {
    return workflowDefinition
      .steps
      .flatMap((step: WorkflowStep) => step.validators)
      .filter((validator: WorkflowActor): boolean => validator.id === this.GENERIC_DESK_IDS.VARIABLE_DESK_ID)
      .length;
  }

  static countVariableDeskStepDto(workflowDefinition: WorkflowDefinitionDto): number {
    return workflowDefinition
      .steps
      .flatMap((step: StepDefinitionDto) => step.validatingDesks)
      .filter((validator: DeskRepresentation): boolean => validator.id === this.GENERIC_DESK_IDS.VARIABLE_DESK_ID)
      .length;
  }

  static  hasSelectionScript(selectedSubtype: SubtypeDto): boolean {
    return !selectedSubtype?.validationWorkflowId
      && !!selectedSubtype?.workflowSelectionScript;
  }

  static everyMandatoryMetadataIsSet(selectedSubtype: SubtypeDto, metadata: {}): boolean {
    return selectedSubtype.subtypeMetadataList
      .filter(subtypeMetadata => subtypeMetadata.mandatory)
      .map(subtypeMetadata => metadata[subtypeMetadata.metadata.key])
      .every(metadataValue => metadataValueIsNotEmpty(metadataValue));
  }
}
