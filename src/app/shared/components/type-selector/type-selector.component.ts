/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { compareById } from '../../../utils/string-utils';
import { CommonMessages } from '../../common-messages';
import { NotificationsService } from '../../../services/notifications.service';
import { Observable } from 'rxjs';
import { TypeRepresentation, StandardApiPageImplTypeRepresentation } from '@libriciel/iparapheur-standard';
import { PageTypeRepresentation } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-type-selector',
  templateUrl: './type-selector.component.html',
  styleUrls: ['./type-selector.component.scss']
})
export class TypeSelectorComponent implements OnInit, OnChanges {


  readonly commonMessages = CommonMessages;
  readonly compareByIdFn = compareById;

  /**
   * Optional parameter, that will be used to reset the field on value change.
   */
  @Input() tenantId: string;

  @Input() mandatory = false;
  @Input() disabled = false;
  @Input() placeholder = CommonMessages.NO_VALUE;
  @Input() type: TypeRepresentation;
  @Input() retrieveTypesFunction: (page: number, pageSize: number) => Observable<PageTypeRepresentation | StandardApiPageImplTypeRepresentation>;

  @Output() typeChange: EventEmitter<TypeRepresentation> = new EventEmitter<TypeRepresentation>();

  typeList: TypeRepresentation[];


  // <editor-fold desc="LifeCycle">


  constructor(private notificationsService: NotificationsService) { }


  ngOnInit(): void {
    this.refreshTypeList();
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (changes.tenantId?.currentValue !== changes.tenantId?.previousValue) {
      this.type = null;
      this.typeList = null;
      this.refreshTypeList();
    }
  }


  // </editor-fold desc="LifeCycle">


  refreshTypeList() {
    if (!this.retrieveTypesFunction) {
      console.error('Missing mandatory parameter retrieveTypeFunction');
      return;
    }

    this.retrieveTypesFunction(0, 999)
      .subscribe({
        next: result => {
          this.typeList = result.content;

          const hasSingleResult = (result.content.length === 1);
          const hasEmptyOrBrokenSelection = (!this.type || !compareById(this.type, result.content[0]));
          if (this.mandatory && hasSingleResult && hasEmptyOrBrokenSelection) {
            this.type = result.content[0];
            this.onTypeSelectionChanged();
          }
        },
        error: e => this.notificationsService.showErrorMessage(CommonMessages.ERROR_RETRIEVING_TYPES, e.message)
      });
  }


  onTypeSelectionChanged() {
    this.typeChange.emit(this.type);
  }


}
