/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { combineLatest } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AbsencesMessages } from '../absences-messages';
import { LegacyUserService } from '../../../../../services/ip-core/legacy-user.service';

@Component({
  selector: 'app-admin-absences-menu',
  templateUrl: './admin-absences-menu.component.html',
  styleUrls: ['./admin-absences-menu.component.scss']
})
export class AdminAbsencesMenuComponent implements OnInit {

  readonly messages = AbsencesMessages;

  tenantId: string;
  asAdmin = false;
  asFunctionalAdmin = false;


  // <editor-fold desc="LifeCycle">


  constructor(private route: ActivatedRoute,
              private legacyUserService: LegacyUserService) {}


  ngOnInit(): void {
    combineLatest([this.route.params, this.route.data])
      .subscribe(([params, data]) => {
        this.tenantId = params['tenantId'];
        this.asAdmin = data.asAdmin;
        this.asFunctionalAdmin = this.asAdmin && this.legacyUserService.isCurrentUserOnlyFunctionalAdminOfThisTenant(this.tenantId);
      });
  }


  // </editor-fold desc="LifeCycle">


}
