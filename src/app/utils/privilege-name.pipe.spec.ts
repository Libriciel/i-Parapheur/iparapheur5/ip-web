/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { PrivilegeNamePipe } from './privilege-name.pipe';
import { UserPrivilege } from '@libriciel/iparapheur-legacy';


describe('PrivilegeNamePipe', () => {


  it('create an instance', () => {
    const pipe = new PrivilegeNamePipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on valid input', () => {
    Object
      .keys(UserPrivilege)
      .map(k => UserPrivilege[k])
      .forEach(p => expect(PrivilegeNamePipe.compute(p)).toBeTruthy());
  });


  it('return default value on invalid input', () => {
    const defaultValue = PrivilegeNamePipe.compute(UserPrivilege.None);
    expect(PrivilegeNamePipe.compute(undefined)).toBe(defaultValue);
    expect(PrivilegeNamePipe.compute(null)).toBe(defaultValue);
  });


});
