/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { faCheck, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { CommonIcons, Style } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../shared/common-messages';
import { FolderViewMessages } from '../folder-view-messages';
import { FormGroup, FormControl } from '@angular/forms';
import { AnnotationForm, PartialAnnotationDataChange } from '../../../../shared/models/custom-types';
import { UserPreferencesDto, ExternalSignatureProvider } from '@libriciel/iparapheur-legacy';
import { TemplateType, SignaturePlacement, Action, StickyNote } from '@libriciel/iparapheur-internal';
import { TemplateUtils } from '../../../../utils/template-utils';
import { TemplateService } from '@libriciel/iparapheur-internal';
import { catchError } from 'rxjs/operators';
import { NotificationsService } from '../../../../services/notifications.service';

@Component({
  selector: 'app-annotation-popup',
  templateUrl: './annotation-popup.component.html',
  styleUrls: ['./annotation-popup.component.scss']
})
export class AnnotationPopupComponent implements OnInit {


  public static readonly injectablePendingActionListKey: string = 'pending_action_list';
  public static readonly injectableSignaturePlacementKey: string = 'signature_placement';
  public static readonly injectableLastTemplateTypeKey: string = 'last_template_type';
  public static readonly injectableUserPreferencesKey: string = 'user_preferences';
  public static readonly injectableStickyNoteKey: string = 'sticky_note';
  public static readonly injectableTypeKey: string = 'type';
  public static readonly injectableTenantIdKey: string = 'tenant_id';
  public static readonly injectableExternalActionProviderKey: string = 'external_action_provider';
  public static readonly injectableReadOnlyKey: string = 'read_only';


  readonly messages = FolderViewMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly style = Style;
  readonly checkIcon: IconDefinition = faCheck;


  templateTypes: TemplateType[];
  annotationFormGroup: FormGroup<AnnotationForm>;
  lastModificationDate: string = Date.now().toString();
  templateSrc: Blob;

  // <editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              private notificationsService: NotificationsService,
              private templateService: TemplateService,
              @Inject(AnnotationPopupComponent.injectablePendingActionListKey) public pendingActionList: Action[],
              @Inject(AnnotationPopupComponent.injectableSignaturePlacementKey) public signaturePlacement: SignaturePlacement,
              @Inject(AnnotationPopupComponent.injectableUserPreferencesKey) public userPreferences: UserPreferencesDto,
              @Inject(AnnotationPopupComponent.injectableStickyNoteKey) public stickyNote: StickyNote,
              @Inject(AnnotationPopupComponent.injectableTypeKey) public type: string,
              @Inject(AnnotationPopupComponent.injectableTenantIdKey) public tenantId: string,
              @Inject(AnnotationPopupComponent.injectableLastTemplateTypeKey) public lastTemplateType?: TemplateType,
              @Inject(AnnotationPopupComponent.injectableExternalActionProviderKey) public externalActionProvider?: ExternalSignatureProvider,
              @Inject(AnnotationPopupComponent.injectableReadOnlyKey) public readOnly: boolean = false) {

    const firstSignificantAction: Action = this.pendingActionList
      .find(action => [Action.Seal, Action.Signature, Action.ExternalSignature].includes(action));

    if (!!firstSignificantAction) {
      this.templateTypes = firstSignificantAction === Action.Seal
        ? TemplateUtils.SEAL_TEMPLATE_TYPES
        : TemplateUtils.SIGNATURE_TEMPLATE_TYPES;

      if (!!this.signaturePlacement && !this.signaturePlacement?.id) {
        this.signaturePlacement.templateType = firstSignificantAction === Action.Seal
          ? this.userPreferences.defaultSealTemplate || TemplateType.SealMedium
          : this.userPreferences.defaultSignatureTemplate || TemplateType.SignatureMedium;
      }
    }


    if (!!this.signaturePlacement && !!this.lastTemplateType) {
      this.signaturePlacement.templateType = this.lastTemplateType;
    }


    this.annotationFormGroup = new FormGroup<AnnotationForm>({
      content: new FormControl<string>(this.stickyNote?.content),
      signatureTemplate: new FormControl<TemplateType>(this.signaturePlacement?.templateType)
    });

    this.changeExamplePdf();

    this.annotationFormGroup.valueChanges.subscribe((changedData: PartialAnnotationDataChange) => {
      if (!!changedData.signatureTemplate) {
        this.signaturePlacement.templateType = changedData.signatureTemplate;
      }
      if (!!changedData.content) {
        this.stickyNote.content = changedData.content;
      }
      this.changeExamplePdf();
    });
  }


  ngOnInit() {
    if (!!this.stickyNote) {
      this.stickyNote.content = this.stickyNote.content || "";
    }
    if (!!this.signaturePlacement) {
      this.signaturePlacement.signatureNumber = this.signaturePlacement.signatureNumber || 0;
    }
  }


  // </editor-fold desc="LifeCycle">


  changeExamplePdf() {
    if (!this.isSignaturePlacement()) {
      return;
    }

    this.templateService
      .testSignatureTemplate(
        this.tenantId,
        this.signaturePlacement.templateType,
        true,
        null,
        'body',
        true,
        {httpHeaderAccept: 'application/pdf'}
      )
      .pipe(catchError(this.notificationsService.handleHttpError('createSecureMailServer')))
      .subscribe((blob: Blob) => this.templateSrc = blob);
  }


  perform() {
    this.activeModal.close({
      value: 'success',
      annotation: !!this.stickyNote ? this.stickyNote : this.signaturePlacement
    });
  }


  getTitle(): string {
    return this.type === 'signaturePlacementCreated' || this.type === 'signaturePlacementClick'
      ? FolderViewMessages.ANNOTATION_POPUP_TITLE_SIGNATURE_PLACEMENT
      : FolderViewMessages.ANNOTATION_POPUP_TITLE_ANNOTATION;
  }


  isSignaturePlacement(): boolean {
    return !!this.signaturePlacement;
  }


}
