/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { UserEditSubPanelsMessages } from '../../user-edit-page-subpanels/user-edit-sub-panels-messages';
import { LabelledColumn, TableName, TableLayoutDto } from '@libriciel/iparapheur-internal';
import { UserDto } from '@libriciel/iparapheur-provisioning';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { ProfileUpdateService } from '../../../../../shared/service/profile-update.service';
import { ActivatedRoute } from '@angular/router';
import { SelectedUserPreferencesService } from '../../../../../services/selected-user-preferences.service';

@Component({
  selector: 'app-user-settings-trash-bin',
  templateUrl: './user-settings-trash-bin.component.html',
  styleUrls: ['./user-settings-trash-bin.component.scss']
})
export class UserSettingsTrashBinComponent implements OnInit {

  readonly messages = UserEditSubPanelsMessages;
  readonly tableNameEnum = TableName;

  currentUser: UserDto;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  isProcessing: boolean = false;

  constructor(private profileUpdateService: ProfileUpdateService,
              private route: ActivatedRoute,
              private selectedUserPreferencesService: SelectedUserPreferencesService) {}


  ngOnInit(): void {
    this.currentUser = this.route.parent?.snapshot.data["currentUser"];

    this.selectedUserPreferencesService.currentUserPreferences$.subscribe(userPreferences => {
      this.userPreferences = userPreferences;

      this.initUserPreferences();
    });
  }


  initUserPreferences(): void {
    console.log('profilePage - initUserPreferences');
    this.userPreferences?.tableLayoutList?.forEach((tableLayout: TableLayoutDto) => {
      if (!!tableLayout) {
        tableLayout.columnList = tableLayout.labelledColumnList.map(c => c.id);
      }
    });
  }


  updateColumnList(labelledColumnList: LabelledColumn[], tableName: TableName) {
    const tableLayout: TableLayoutDto = this.userPreferences.tableLayoutList
      .find(tl => tl.tableName === tableName && tl.deskId == null);
    if (!!tableLayout) {
      tableLayout.columnList = labelledColumnList.map(c => c.id);
    } else {
      console.error("Could not retrieve the table layout associated with the tableName :" + tableName);
    }
  }


  saveForm() {
    this.userPreferences.tableLayoutList.forEach(tableLayout => {
      (tableLayout as any).labelledColumnList = tableLayout.labelledColumnList.filter(elem => tableLayout.columnList?.includes(elem.id));
    });

    this.isProcessing = true;

    this.profileUpdateService.updateUserPreferences(this.userPreferences)
      .then(r => this.isProcessing = false);
  }
}
