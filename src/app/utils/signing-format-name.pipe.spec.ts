/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { SigningFormatNamePipe } from './signing-format-name.pipe';
import { SignatureFormat } from '@libriciel/iparapheur-legacy';


describe('SigningFormatNamePipe', () => {


  it('create an instance', () => {
    const pipe = new SigningFormatNamePipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on valid input', () => {
    Object
      .keys(SignatureFormat)
      .map(k => SignatureFormat[k])
      .forEach(e => expect(SigningFormatNamePipe.compute(e)).toBeTruthy());
  });


  it('return the given value on invalid input', () => {
    expect(SigningFormatNamePipe.compute(undefined)).toBeUndefined();
    expect(SigningFormatNamePipe.compute(null)).toBeNull();
  });


});
