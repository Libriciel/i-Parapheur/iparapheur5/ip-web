/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { faGripHorizontal, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { UserEditSubPanelsMessages } from '../user-edit-sub-panels-messages';
import { UserPreferencesDto, FolderViewBlock } from '@libriciel/iparapheur-legacy';
import { SelectedUserPreferencesService } from '../../../../../services/selected-user-preferences.service';
import { ProfileUpdateService } from '../../../../../shared/service/profile-update.service';

@Component({
  selector: 'app-folder-view-preferences',
  templateUrl: './folder-view-preferences.component.html',
  styleUrls: ['./folder-view-preferences.component.scss']
})
export class FolderViewPreferencesComponent implements OnInit {


  readonly gripHorizontalIcon: IconDefinition = faGripHorizontal;
  readonly messages = UserEditSubPanelsMessages;

  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  isProcessing: boolean = false;

  // <editor-fold desc="LifeCycle">


  bodyElement: HTMLElement = document.body;
  currentBlockList: FolderViewBlock[];


  constructor(private profileUpdateService: ProfileUpdateService,
              private selectedUserPreferencesService: SelectedUserPreferencesService) {}


  ngOnInit(): void {
    this.selectedUserPreferencesService.currentUserPreferences$.subscribe(userPreferences => {
      this.userPreferences = userPreferences;
    });

    this.currentBlockList = this.userPreferences.folderViewBlockList;
  }


  // </editor-fold desc="LifeCycle">


  drop(event: CdkDragDrop<{ name: string, key: string, order: number }>): void {
    this.currentBlockList.splice(event.currentIndex, 0, this.currentBlockList.splice(event.previousIndex, 1)[0]);
    this.bodyElement.classList.remove('is-dragging');
  }


  // TODO not needed with angular 12+, where the dragPreview can be placed inside the parent element
  dragStart() {
    this.bodyElement.classList.add("is-dragging");
  }


  saveForm() {
    this.isProcessing = true;

    this.profileUpdateService.updateUserPreferences(this.userPreferences)
      .then(r => this.isProcessing = false);
  }
}
