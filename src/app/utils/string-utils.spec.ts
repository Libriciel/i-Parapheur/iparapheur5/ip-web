/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { compareById, integerRegex, emailRfc5322Regex, floatRegex, urlRfc3986Regex } from './string-utils';
import { HasId } from '../models/has-id';


describe('StringUtils', () => {


  it('integerRegex', () => {

    expect(integerRegex.test('123')).toBeTrue();
    expect(integerRegex.test('-1')).toBeTrue();

    expect(integerRegex.test('1.5')).toBeFalse();
    expect(integerRegex.test('1,5')).toBeFalse();
    expect(integerRegex.test('-1,5')).toBeFalse();
    expect(integerRegex.test('1,5f')).toBeFalse();
    expect(integerRegex.test('1.5.0')).toBeFalse();
    expect(integerRegex.test('test')).toBeFalse();
    expect(integerRegex.test('')).toBeFalse();
    expect(integerRegex.test(undefined)).toBeFalse();
    expect(integerRegex.test(null)).toBeFalse();
  });


  it('floatRegex', () => {

    expect(floatRegex.test('123')).toBeTrue();
    expect(floatRegex.test('-1')).toBeTrue();
    expect(floatRegex.test('1.5')).toBeTrue();
    expect(floatRegex.test('1,5')).toBeTrue();
    expect(floatRegex.test('-1,5')).toBeTrue();

    expect(floatRegex.test('1,5f')).toBeFalse();
    expect(floatRegex.test('1.5.0')).toBeFalse();
    expect(floatRegex.test('test')).toBeFalse();
    expect(floatRegex.test('')).toBeFalse();
    expect(floatRegex.test(undefined)).toBeFalse();
    expect(floatRegex.test(null)).toBeFalse();
  });


  it('rfc5322Regex', () => {

    expect(emailRfc5322Regex.test('test@dom.local')).toBeTrue();

    expect(emailRfc5322Regex.test('123')).toBeFalse();
    expect(emailRfc5322Regex.test('')).toBeFalse();
    expect(emailRfc5322Regex.test(undefined)).toBeFalse();
    expect(emailRfc5322Regex.test(null)).toBeFalse();
  });


  it('isValidUrl', () => {

    expect(urlRfc3986Regex.test('https://libriciel.fr')).toBeTrue();
    expect(urlRfc3986Regex.test('https://libriciel.fr/')).toBeTrue();

    expect(urlRfc3986Regex.test('test@dom.local')).toBeFalse();
    expect(urlRfc3986Regex.test('123')).toBeFalse();
    expect(urlRfc3986Regex.test('')).toBeFalse();
    expect(urlRfc3986Regex.test(undefined)).toBeFalse();
    expect(urlRfc3986Regex.test(null)).toBeFalse();
  });


  it('compareById', () => {

    const dummyClass1 = new class implements HasId {id = '1';};
    const dummyClass2 = new class implements HasId {id = '2';};
    const dummyClassNull = new class implements HasId {id = null;};
    const dummyClassUndefined = new class implements HasId {id = undefined;};

    expect(compareById(dummyClass1, dummyClass1)).toBe(true);
    expect(compareById(dummyClass1, dummyClass2)).toBe(false);
    expect(compareById(dummyClass2, dummyClass1)).toBe(false);
    expect(compareById(dummyClass1, dummyClassNull)).toBe(false);
    expect(compareById(dummyClass1, dummyClassUndefined)).toBe(false);
    expect(compareById(dummyClassNull, dummyClassUndefined)).toBe(false);
  });


});
