/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { faPlug, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SecureMailServer } from '../../../../../../../../../models/securemail/secure-mail-server';
import { AdminSecureMailService } from '../../../../../../../../../services/ip-core/admin-secure-mail.service';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { CrudOperation } from '../../../../../../../../../services/crud-operation';
import { ConnectorMessages } from '../../../connector-messages';
import { CommonIcons, Style } from '@libriciel/ls-elements';
import { SecureMailEntity } from '../../../../../../../../../models/securemail/secure-mail-entity';
import { Observable } from 'rxjs';
import { urlRfc3986Regex } from '../../../../../../../../../utils/string-utils';
import { UntypedFormGroup, UntypedFormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-add-secure-mail-connector',
  templateUrl: './add-secure-mail-connector.component.html',
  styleUrls: ['./add-secure-mail-connector.component.scss']
})
export class AddSecureMailConnectorComponent {

  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_EDIT_MODE_KEY = 'editMode';
  public static readonly INJECTABLE_CURRENT_SECURE_MAIL_SERVER_KEY = 'currentServer';

  readonly messages = ConnectorMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly plugIcon = faPlug;
  readonly eyeIcon = faEye;
  readonly eyeSlashIcon = faEyeSlash;
  readonly style = Style;

  connected: boolean = false;
  connectionError: boolean = false;
  isProcessing: boolean = false;
  hidePassword: boolean = true;
  entities: SecureMailEntity[] = [];

  // <editor-fold desc="LifeCycle">

  urlValidator: ValidatorFn = (control: AbstractControl) => {
    const malformedUrlError: ValidationErrors = {malformedUrlError: true};
    return urlRfc3986Regex.test(control.value) ? null : malformedUrlError;
  };

  secureMailServerForm: UntypedFormGroup = new UntypedFormGroup({
    name: new UntypedFormControl(this.currentServer?.name, [Validators.required, Validators.minLength(2)]),
    url: new UntypedFormControl(this.currentServer?.url, [Validators.required, Validators.minLength(2), this.urlValidator]),
    login: new UntypedFormControl(this.currentServer?.login, [Validators.required, Validators.minLength(2)]),
    password: new UntypedFormControl(this.currentServer?.password, [Validators.required, Validators.minLength(2)]),
    entity: new UntypedFormControl(this.currentServer?.entity, [])
  });

  constructor(public notificationService: NotificationsService,
              public adminSecureMailService: AdminSecureMailService,
              public activeModal: NgbActiveModal,
              @Inject(AddSecureMailConnectorComponent.INJECTABLE_TENANT_ID_KEY)
              public tenantId: string,
              @Inject(AddSecureMailConnectorComponent.INJECTABLE_EDIT_MODE_KEY)
              public editMode: boolean,
              @Inject(AddSecureMailConnectorComponent.INJECTABLE_CURRENT_SECURE_MAIL_SERVER_KEY)
              public currentServer: SecureMailServer) {
  }


  // </editor-fold desc="LifeCycle">

  public connect(): void {
    if (this.connected) {
      this.connected = false;
      this.connectionError = false;
      this.entities = [];
      return;
    }

    this.testSecureMailServerAndFetchEntities();
  }

  testSecureMailServerAndFetchEntities(): void {

    this.isProcessing = true;

    this.adminSecureMailService
      .testSecureMailServer(this.tenantId, this.currentServer)
      .subscribe({
        next: isConnected => {
          this.connected = isConnected;
          this.connectionError = !this.connected;

          if (this.connected) {
            this.fetchEntities();
          } else {
            this.isProcessing = false;
          }
        },
        error: () => {
          this.connected = false;
          this.connectionError = true;
          this.isProcessing = false;
        }
      });
  }

  fetchEntities(): void {
    this.isProcessing = true;
    this.adminSecureMailService
      .findSecureMailEntities(this.tenantId, this.currentServer)
      .subscribe({
        next: data => this.entities = data,
        error: e => this.notificationService.showCrudMessage(CrudOperation.Read, SecureMailEntity, e, false)
      })
      .add(() => this.isProcessing = false);
  }

  public getConnectionMessage(): string {
    if (this.connected && !this.connectionError) return ConnectorMessages.CONNECTED;
    if (!this.connected && this.connectionError) return ConnectorMessages.INVALID_LOG_INFO;
    return "";
  }

  executeForm(): void {
    if (!this.secureMailServerForm.valid) return;

    this.currentServer.type = 'mailsec';
    this.currentServer.tenantId = this.tenantId;

    this.isProcessing = true;

    const request$: Observable<any> = this.editMode
      ? this.adminSecureMailService.updateSecureMailServer(this.tenantId, this.currentServer)
      : this.adminSecureMailService.createSecureMailServer(this.tenantId, this.currentServer);

    request$
      .subscribe({
        next: () => {
          this.notificationService.showCrudMessage(CrudOperation.Create, this.currentServer);
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error: e => this.notificationService.showCrudMessage(
          this.editMode ? CrudOperation.Update : CrudOperation.Create, SecureMailServer, e, false
        )
      })
      .add(() => this.isProcessing = false);
  }


}
