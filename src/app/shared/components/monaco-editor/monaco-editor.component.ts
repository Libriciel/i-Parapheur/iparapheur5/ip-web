/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, AfterViewInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-monaco-editor',
  templateUrl: './monaco-editor.component.html',
  styleUrls: ['./monaco-editor.component.scss'],
})
export class MonacoEditorComponent implements AfterViewInit {

  @Input() language: string;

  @Input() code: string = '';
  @Output() codeChange: EventEmitter<string> = new EventEmitter<string>();

  initialized: boolean = false;
  editorOptions: any;

  ngAfterViewInit(): void {
    this.editorOptions = {
      theme: 'vs-dark',
      language: this.language
    };

    if (!this.initialized) {
      setTimeout(
        (): boolean => this.initialized = true,
        200
      );
    }
  }

  onChange($event: string) {
    this.codeChange.emit($event);
  }

}
