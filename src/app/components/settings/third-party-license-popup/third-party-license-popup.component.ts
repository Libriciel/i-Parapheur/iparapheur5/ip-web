/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonIcons } from '@libriciel/ls-elements';
import { SettingsMessages } from '../settings-messages';
import { CommonMessages } from '../../../shared/common-messages';

@Component({
  selector: 'app-third-party-license-popup',
  templateUrl: './third-party-license-popup.component.html',
  styleUrls: ['./third-party-license-popup.component.scss']
})
export class ThirdPartyLicensePopupComponent {

  public static readonly INJECTABLE_LICENSE_TXT = 'license';

  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly messages = SettingsMessages;


  constructor(public activeModal: NgbActiveModal,
              @Inject(ThirdPartyLicensePopupComponent.INJECTABLE_LICENSE_TXT) public license: string) {}


}
