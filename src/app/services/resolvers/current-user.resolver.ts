/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { inject } from '@angular/core';
import { RouterStateSnapshot, ActivatedRouteSnapshot, ResolveFn } from '@angular/router';
import { Observable, of, take } from 'rxjs';
import { catchError, map, filter } from 'rxjs/operators';
import { User } from '../../models/auth/user';
import { CurrentUserService } from '@libriciel/iparapheur-legacy';
import { NotificationsService } from '../notifications.service';
import { ResolverMessages } from './resolver-messages';
import { SelectedUserService } from '../selected-user.service';

export const CurrentUserResolver: ResolveFn<User> = (
  _route: ActivatedRouteSnapshot,
  _state: RouterStateSnapshot,
  notificationService: NotificationsService = inject(NotificationsService),
  selectedUserService: SelectedUserService = inject(SelectedUserService),
  currentUserService: CurrentUserService = inject(CurrentUserService)
): Observable<User> => currentUserService
  .getCurrentUser()
  .pipe(
    catchError(() => {
      notificationService.setGlobalError(ResolverMessages.ERROR_FETCHING_USER);
      return of(new User());
    }),
    map(userDto => {
      const result: User = Object.assign(new User(), userDto);
      selectedUserService.update(result);
      return result;
    })
  )
  .pipe(
    filter<User>((user: User) => !!user),
    take(1)
  );
