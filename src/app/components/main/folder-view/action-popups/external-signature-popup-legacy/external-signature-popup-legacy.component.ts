/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, OnInit } from '@angular/core';
import { CommonIcons, Style } from '@libriciel/ls-elements';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonMessages } from '../../../../../shared/common-messages';
import { ExternalState } from '../../../../../models/external-state.enum';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { Folder } from '../../../../../models/folder/folder';
import { NotificationsService } from '../../../../../services/notifications.service';
import { Observable, combineLatest } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Action, ExternalSignatureProcedure, WorkflowService, ExternalSignatureParams, SignRequestMember } from '@libriciel/iparapheur-legacy';
import { WorkflowService as LegacyWorkflowService } from '../../../../../services/ip-core/workflow.service';
import { ActionPopupMessages } from '../action-popup-messages';
import { ExternalSignatureMessages } from '../external-signature-popup/external-signature-messages';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-external-signature-popup-legacy',
  templateUrl: './external-signature-popup-legacy.component.html',
  styleUrls: ['./external-signature-popup-legacy.component.scss']
})
export class ExternalSignaturePopupLegacyComponent implements OnInit {


  static readonly RESERVED_EXT_SIG_METADATA_FIRSTNAME_KEY = 'i_Parapheur_reserved_ext_sig_firstname';
  static readonly RESERVED_EXT_SIG_METADATA_LASTNAME_KEY = 'i_Parapheur_reserved_ext_sig_lastname';
  static readonly RESERVED_EXT_SIG_METADATA_MAIL_KEY = 'i_Parapheur_reserved_ext_sig_mail';
  static readonly RESERVED_EXT_SIG_METADATA_PHONE_KEY = 'i_Parapheur_reserved_ext_sig_phone';

  static readonly INJECTABLE_FOLDERS_KEY: string = 'folders';
  static readonly INJECTABLE_DESK_ID_KEY: string = 'deskId';
  static readonly INJECTABLE_PERFORMED_ACTION_KEY: string = 'performedAction';
  static readonly INJECTABLE_TENANT_ID_KEY = 'tenant_id';
  static readonly INJECTABLE_EXTERNAL_SIGNATURE_PROCEDURE_KEY = 'externalSignatureProcedure';

  readonly messages = ExternalSignatureMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly actionEnum = Action;
  readonly styleEnum = Style;
  protected readonly infoIcon = faInfoCircle;

  isProcessing: boolean = false;
  validationMetadataFormValid: boolean = false;
  externalSignatureForm: UntypedFormGroup;


  // <editor-fold desc="LifeCycle">


  constructor(@Inject(ExternalSignaturePopupLegacyComponent.INJECTABLE_FOLDERS_KEY) public folders: Folder[],
              @Inject(ExternalSignaturePopupLegacyComponent.INJECTABLE_DESK_ID_KEY) public deskId: string,
              @Inject(ExternalSignaturePopupLegacyComponent.INJECTABLE_PERFORMED_ACTION_KEY) public performedAction: Action,
              @Inject(ExternalSignaturePopupLegacyComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(ExternalSignaturePopupLegacyComponent.INJECTABLE_EXTERNAL_SIGNATURE_PROCEDURE_KEY) public procedure: ExternalSignatureProcedure,
              private workflowService: WorkflowService,
              private legacyWorkflowService: LegacyWorkflowService,
              private notificationsService: NotificationsService,
              public activeModal: NgbActiveModal) {

    this.externalSignatureForm = new UntypedFormGroup({
      firstname: new UntypedFormControl(
        this.procedure?.members[0]?.firstName ?? this.folders[0]?.metadata[ExternalSignaturePopupLegacyComponent.RESERVED_EXT_SIG_METADATA_FIRSTNAME_KEY],
        [Validators.required]
      ),
      lastname: new UntypedFormControl(
        this.procedure?.members[0]?.lastName ?? this.folders[0]?.metadata[ExternalSignaturePopupLegacyComponent.RESERVED_EXT_SIG_METADATA_LASTNAME_KEY],
        [Validators.required]
      ),
      mail: new UntypedFormControl(
        this.procedure?.members[0]?.email ?? this.folders[0]?.metadata[ExternalSignaturePopupLegacyComponent.RESERVED_EXT_SIG_METADATA_MAIL_KEY],
        [Validators.required, Validators.email]
      ),
      phone: new UntypedFormControl(
        this.procedure?.members[0]?.phone ?? this.folders[0]?.metadata[ExternalSignaturePopupLegacyComponent.RESERVED_EXT_SIG_METADATA_PHONE_KEY],
        [Validators.required]
      )
    });
  }

  ngOnInit(): void {
    if (!this.areAllFoldersInFormState()) {
      this.disableForm();
    }
  }


  // </editor-fold desc="LifeCycle">


  disableForm(): void {
    this.externalSignatureForm.disable();
  }


  areAllFoldersInFormState(): boolean {
    return this.folders
      .map(folder => folder.stepList[0].externalState)
      .every(externalState => externalState === ExternalState.Form);
  }


  areAllFoldersInSignedState(): boolean {
    return this.folders
      .map(folder => folder.stepList[0].externalState)
      .every(externalState => externalState === ExternalState.Signed);
  }


  submit(): void {
    if (this.areAllFoldersInSignedState()) {
      this.forceExternalSignature();
    } else {
      this.requestExternalSignature();
    }
  }


  forceExternalSignature(): void {
    this.isProcessing = true;

    const observableList: Observable<void>[] = [];

    this.folders.forEach(folder => {
      observableList.push(
        this.workflowService.finalizeExternalSignature(
          this.tenantId,
          this.deskId,
          folder.id,
          folder.stepList[0].id,
          folder.subtype.externalSignatureConfig.id
        ).pipe(catchError(error => new Observable<void>(observer => observer.next(error))))
      );
    });

    combineLatest(observableList)
      .subscribe(data => {
          this.isProcessing = false;
          this.showNotifications(data);
          this.finalizeAction();
        },
      );
  }


  requestExternalSignature(): void {
    this.isProcessing = true;

    const member: SignRequestMember = {} as SignRequestMember;
    member.email = this.externalSignatureForm.get('mail').value.trim();
    member.firstname = this.externalSignatureForm.get('firstname').value.trim();
    member.lastname = this.externalSignatureForm.get('lastname').value.trim();
    member.phone = this.externalSignatureForm.get('phone').value.trim();

    const params: ExternalSignatureParams = {} as ExternalSignatureParams;
    params.name = 'name';
    params.members = [];
    params.members.push(member);

    const observableList: Observable<void>[] = [];
    this.folders.forEach(folder => {
      params.metadata = folder.populatedMandatoryStepMetadata;
      observableList.push(
        this.legacyWorkflowService.requestExternalProcedure(params, this.tenantId, this.deskId, folder.id, folder.stepList[0])
          .pipe(catchError(error => new Observable<void>(observer => observer.next(error)))));
    });

    combineLatest(observableList)
      .subscribe(data => {
          this.isProcessing = false;
          this.showNotifications(data);
          this.finalizeAction();
        },
      );
  }


  showNotifications(results: any[]): void {
    const errors: Error[] = results.filter(res => !!res);

    if (errors.length === 0) {
      this.notificationsService.showSuccessMessage(
        results.length === 1
          ? ActionPopupMessages.actionSuccessMessage(Action.ExternalSignature)
          : ActionPopupMessages.ALL_TASKS_SUCCESSFUL
      );
    } else {
      this.notificationsService.showErrorMessage(ActionPopupMessages.parseMultipleActionsErrorMessage(Action.ExternalSignature, errors));
    }
  }


  finalizeAction(): void {
    console.log('Finalize External Signature');
    this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
  }


}
