/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { UserEditSubPanelsMessages } from '../../user-edit-page-subpanels/user-edit-sub-panels-messages';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { ProfileUpdateService } from '../../../../../shared/service/profile-update.service';
import { SelectedUserPreferencesService } from '../../../../../services/selected-user-preferences.service';

@Component({
  selector: 'app-user-settings-absence',
  templateUrl: './user-settings-absence.component.html',
  styleUrls: ['./user-settings-absence.component.scss']
})
export class UserSettingsAbsenceComponent implements OnInit {

  protected readonly messages = UserEditSubPanelsMessages;

  isProcessing: boolean = false;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;

  constructor(private profileUpdateService: ProfileUpdateService,
              private selectedUserPreferencesService: SelectedUserPreferencesService) { }


  ngOnInit(): void {
    this.selectedUserPreferencesService.currentUserPreferences$.subscribe(userPreferences => {
      this.userPreferences = userPreferences;
    });
  }


  saveForm() {
    this.isProcessing = true;

    this.profileUpdateService.updateUserPreferences(this.userPreferences)
      .then(r => this.isProcessing = false);
  }
}
