/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class ConnectorMessages {

  static readonly CONNECTOR_CREATE: string = 'Créer une configuration';

  static readonly EXT_SIG_TITLE: string = 'Gestion des configurations de signatures externes';
  static readonly MAIL_SEC_TITLE: string = 'Gestion des configurations de mail sécurisé';

  static readonly SERVICE: string = 'Service';
  static readonly SIGNATURE_LEVEL: string = 'Niveau de signature';
  static readonly URL: string = 'Url';
  static readonly LOGIN: string = 'Identifiant';
  static readonly TOKEN: string = 'Token';

  static readonly CONNECT: string = 'Connecter';
  static readonly DISCONNECT: string = 'Déconnecter';
  static readonly CONNECTED: string = 'Connecté';

  static readonly INVALID_LOG_INFO: string = 'Utilisateur ou mot de passe invalide';
  static readonly CHOOSE_AN_ENTITY: string = 'Choisissez une entité';

  static readonly ERROR_CREATING_CONFIGURATION: string = 'Une erreur est survenue durant la création de la configuration';
  static readonly ERROR_EDITING_CONFIGURATION: string = `Une erreur est survenue durant l'édition de la configuration`;
  static readonly ERROR_DELETING_CONFIGURATION: string = 'Une erreur est survenue durant la suppression de la configuration';
  static readonly SUCCESS_CREATING_CONFIGURATION: string = 'La configuration a été créée avec succès';
  static readonly SUCCESS_EDITING_CONFIGURATION: string = 'La configuration a été éditée avec succès';
  static readonly SUCCESS_DELETING_CONFIGURATION: string = 'La configuration a été supprimée avec succès';

  static readonly PASSWORD_EDIT_PLACEHOLDER: string = 'Laisser le champ vide pour garder le MdP existant';
  static readonly TOKEN_EDIT_PLACEHOLDER: string = 'Laisser le champ vide pour garder le token existant';

  static getConnectorEditOrCreatePopupTitle = (editMode: boolean, name?: string): string => editMode
      ? `Modification du connecteur "${name}"`
      : `Ajout d'un nouveau connecteur`;

}

