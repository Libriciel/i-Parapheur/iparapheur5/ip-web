/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TypeProtocolComponent } from './type-protocol.component';
import { SignatureProtocolNamePipe } from '../../../../../../utils/signature-protocol-name.pipe';
import { SigningFormatNamePipe } from '../../../../../../utils/signing-format-name.pipe';
import { SignatureProtocol } from '@libriciel/iparapheur-legacy';
import { TenantDto, TypeDto } from '@libriciel/iparapheur-provisioning';


describe('TypeProtocolComponent', () => {


  let component: TypeProtocolComponent;
  let fixture: ComponentFixture<TypeProtocolComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        declarations: [TypeProtocolComponent, SignatureProtocolNamePipe, SigningFormatNamePipe]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(TypeProtocolComponent);
    component = fixture.componentInstance;

    component.tenant = {id: '01', name: 'testTenant',} as TenantDto;
    component.type = {name: 'testType', protocol: SignatureProtocol.None} as TypeDto;

    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
