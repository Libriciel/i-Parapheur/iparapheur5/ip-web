/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { faGripHorizontal, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { CommonIcons } from '@libriciel/ls-elements';
import { UserEditSubPanelsMessages } from '../user-edit-sub-panels-messages';
import { CommonMessages } from '../../../../../shared/common-messages';
import { FolderFilter } from '../../../../../models/folder/folder-filter';
import { LegacyUserService } from '../../../../../services/ip-core/legacy-user.service';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CrudOperation } from '../../../../../services/crud-operation';
import { LoggableUser } from '../../../../../models/auth/loggable-user';
import { compareById, isCurrentVersion52OrAbove } from '../../../../../utils/string-utils';
import { FolderSortBy, TaskViewColumn, UserPreferencesDto, MetadataService, ArchiveViewColumn } from '@libriciel/iparapheur-legacy';
import { TableName, TableLayoutDto, LabelledColumn, LabelledColumnType } from '@libriciel/iparapheur-internal';
import { UserDto, MetadataRepresentation, MetadataSortBy } from '@libriciel/iparapheur-provisioning';
import { TypologyMessages } from '../../../../admin/admin-sub-panels/typology/typology-messages';
import { catchError } from 'rxjs/operators';
import { MetadataMessages } from '../../../../admin/admin-sub-panels/advanced-admin/advanced-admin-sub-panels/advanced-config/sub-panels/metadata/metadata-messages';

@Component({
  selector: 'app-table-layout-editor',
  templateUrl: './table-layout-editor.component.html',
  styleUrls: ['./table-layout-editor.component.scss']
})
export class TableLayoutEditorComponent implements OnInit {

  readonly show52Features: boolean = isCurrentVersion52OrAbove();

  readonly messages = UserEditSubPanelsMessages;
  readonly metadataMessages = MetadataMessages;
  readonly typologyMessages = TypologyMessages;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly gripHorizontalIcon: IconDefinition = faGripHorizontal;
  readonly possibleResultsPerPage: number[] = [10, 15, 20, 50, 100];
  readonly taskViewColumEnum = TaskViewColumn;
  readonly compareByIdFn = compareById;
  readonly columnTypeEnum = LabelledColumnType;
  readonly MAX_COLUMNS_IN_DASHBOARD: number = 14;


  @Input() tableName: TableName;
  @Input() modifiedUser: UserDto;
  @Input() userPreferences: UserPreferencesDto;
  @Input() currentTenantId?: string;
  @Input() deskId: string = null;
  @Input() showTableLayoutOnly: boolean = true;

  @Output() columnListChange: EventEmitter<LabelledColumn[]> = new EventEmitter<LabelledColumn[]>();

  filters: FolderFilter[] = [];
  columns: LabelledColumn[];
  availableColumns: LabelledColumn[];
  currentTableLayout: TableLayoutDto;
  metadataList: MetadataRepresentation[];
  filteredMetadataList: MetadataRepresentation[] = [];
  selectedMetadata: MetadataRepresentation;
  usedColumns: LabelledColumn[];
  uniqueTenantId?: string;


  // <editor-fold desc="LifeCycle">


  constructor(private legacyUserService: LegacyUserService,
              private notificationsService: NotificationsService,
              private metadataService: MetadataService) { }


  ngOnInit(): void {

    if (this.show52Features && (this.currentTenantId || this.modifiedUser.associatedTenants.length === 1)) {
      this.uniqueTenantId = this.currentTenantId || this.modifiedUser.associatedTenants[0].id;
      this.metadataService
        .listMetadata(this.uniqueTenantId, 0, 500, [MetadataSortBy.Index + ",ASC"])
        .pipe(catchError(this.notificationsService.handleHttpError('listMetadata')))
        .subscribe(metadataRetrieved => {
          this.metadataList = metadataRetrieved.content;
          this.filterAlreadyLinkedMetadataFromList();
        });
    }

    this.legacyUserService
      .getCurrentUserFolderFilters()
      .subscribe({
        next: filters => {
          this.filters = filters.map(f => f as FolderFilter);
          this.userPreferences.currentFilter = filters.find(filter => filter.id === this.userPreferences.currentFilter?.id);
        },
        error: e => this.notificationsService.showCrudMessage(CrudOperation.Read, new LoggableUser(this.modifiedUser), e.message, false)
      });
    this.initTableData();
    this.currentTableLayout = this.userPreferences.tableLayoutList
      .find((tableLayoutDto: TableLayoutDto): boolean => tableLayoutDto.tableName === this.tableName && tableLayoutDto.deskId == this.deskId);

    if (!this.currentTableLayout) {

      this.currentTableLayout = structuredClone(this.userPreferences.tableLayoutList
        .find((tableLayoutDto: TableLayoutDto): boolean => tableLayoutDto.tableName === this.tableName && !tableLayoutDto.deskId));

      this.currentTableLayout.id = null;
      this.currentTableLayout.deskId = this.deskId;
    }

    this.usedColumns = this.currentTableLayout.labelledColumnList;
    this.availableColumns = Object.values(this.columns)
      .filter(item => !this.usedColumns.map(c => c.id).includes(item.id));
  }

  initTableData() {
    switch (this.tableName) {
      case TableName.TaskList:
        this.columns = Object.values(TaskViewColumn).map(value => {
            return {
              id: value,
              type: LabelledColumnType.ClassAttribute
            };
          }
        );
        break;
      case TableName.ArchiveList:
        this.columns = Object.values(ArchiveViewColumn).map(value => {
            return {
              id: value,
              type: LabelledColumnType.ClassAttribute
            };
          }
        );
        break;
      case TableName.AdminFolderList:
        this.columns = Object.values({...TaskViewColumn, LastModificationDate: "LAST_MODIFICATION_DATE"}).map(value => {
            return {
              id: value,
              type: LabelledColumnType.ClassAttribute
            };
          }
        );
        break;
      case TableName.FolderMainLayout:
    }
  }


  // </editor-fold desc="LifeCycle">


  drop(event: CdkDragDrop<FolderSortBy>): void {
    this.usedColumns
      .splice(
        event.currentIndex,
        0,
        this.usedColumns.splice(event.previousIndex, 1)[0]
      );
    this.columnListChange.emit(this.usedColumns);
  }


  removeFromUsedColumns(column: LabelledColumn) {
    if (!this.isColumnDeletable(column)) {
      return;
    }

    this.usedColumns = this.usedColumns.filter(col => col.id !== column.id);

    if (column.type === LabelledColumnType.Metadata) {
      this.filterAlreadyLinkedMetadataFromList();
    } else {
      this.availableColumns.push(column);
    }

    this.columnListChange.emit(this.usedColumns);
  }

  isColumnDeletable(column: LabelledColumn): boolean {
    return (this.tableName === TableName.TaskList && column.id !== TaskViewColumn.FolderName)
      || (this.tableName === TableName.ArchiveList && column.id !== ArchiveViewColumn.Name)
      || (this.tableName === TableName.AdminFolderList && column.id !== TaskViewColumn.FolderName);
  }

  addToUsedColumns(index: number) {
    if (this.usedColumns.length >= this.MAX_COLUMNS_IN_DASHBOARD) {
      console.log('cannot add another column, maximum reached');
      return;
    }

    this.usedColumns.push({
      id: this.availableColumns.splice(index, 1)[0].id,
      type: LabelledColumnType.ClassAttribute
    });
    this.columnListChange.emit(this.usedColumns);
  }

  filterAlreadyLinkedMetadataFromList() {
    this.filteredMetadataList = this.metadataList?.filter(m => !this.usedColumns.some(data => data.id === m.id));
  }

  searchMetadata = (term: string, item: MetadataRepresentation) => item.name.includes(term) || item.key.includes(term);

  onAddMetadataButtonClicked() {
    if (this.usedColumns.length >= this.MAX_COLUMNS_IN_DASHBOARD) {
      console.log('cannot add another column, maximum reached');
      return;
    }

    this.usedColumns.push({
      type: LabelledColumnType.Metadata,
      id: this.selectedMetadata.id,
      metadata: this.selectedMetadata
    });
    this.selectedMetadata = null;
    this.filterAlreadyLinkedMetadataFromList();
    this.columnListChange.emit(this.usedColumns);
  }

  showResultsPerPage(): boolean {
    return !this.showTableLayoutOnly && this.tableName === TableName.TaskList;
  }

  showSortBy(): boolean {
    return !this.showTableLayoutOnly && this.tableName === TableName.TaskList;
  }

  showAsc(): boolean {
    return !this.showTableLayoutOnly && this.tableName === TableName.TaskList;
  }

  showFilter(): boolean {
    return !this.showTableLayoutOnly && (this.tableName === TableName.TaskList || this.tableName === TableName.ArchiveList);
  }
}
