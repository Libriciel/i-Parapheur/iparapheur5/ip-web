/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, OnInit } from '@angular/core';
import { DeskMessages } from '../../desk-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { faInfoCircle, faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { Metadata } from '../../../../../../models/metadata';
import { NotificationsService } from '../../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { AdminMetadataService, MetadataSortBy, MetadataRepresentation } from '@libriciel/iparapheur-provisioning';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-desk-metadata',
  templateUrl: './desk-metadata.component.html',
  styleUrls: ['./desk-metadata.component.scss']
})
export class DeskMetadataComponent implements OnInit {


  readonly messages = DeskMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly infoIcon = faInfoCircle;
  readonly chevronLeftIcon = faChevronLeft;
  readonly chevronRightIcon = faChevronRight;

  @Input() tenantId: string;
  @Input() linkedMetadata: MetadataRepresentation[] = [];

  metadata: MetadataRepresentation[] = [];
  metadataPage = 1;
  metadataPageSize = 10;
  metadataPageCount: number;
  metadataCount: number;


  // <editor-fold desc="LifeCycle">


  constructor(private adminMetadataService: AdminMetadataService,
              public notificationsService: NotificationsService) {}


  ngOnInit(): void {
    this.requestMetadata(true);
  }


  // </editor-fold desc="LifeCycle">


  requestMetadata(newRequest: boolean) {

    if (newRequest) {
      this.metadataPage = 1;
    }

    if (this.tenantId) {
      const metadataSortBy = [MetadataSortBy.Index + ',ASC', MetadataSortBy.Name + ',ASC'];
      this.adminMetadataService
        .listMetadata(this.tenantId, false, null, this.metadataPage - 1, this.metadataPageSize, metadataSortBy)
        .pipe(catchError(this.notificationsService.handleHttpError('listMetadata')))
        .subscribe({
          next: paginatedResult => {
            this.metadata = paginatedResult.content.map(dto => Object.assign(new Metadata(), dto));
            this.metadataCount = paginatedResult.totalElements;
            this.metadataPageCount = Math.ceil(this.metadataCount / this.metadataPageSize);
          },
          error: e => this.notificationsService.showErrorMessage(this.messages.ERROR_FETCHING_METADATA, e.message)
        });
    }
  }


  select(metadata: MetadataRepresentation) {
    if (!this.linkedMetadata.find(e => e.id === metadata.id)) {
      this.linkedMetadata.push(metadata);
    }
  }


  removeSelection(metadata: MetadataRepresentation) {
    this.linkedMetadata.forEach((e, index) => {
      if (e.id === metadata.id) {
        this.linkedMetadata.splice(index, 1);
      }
    });
  }


  selected(metadata: MetadataRepresentation): boolean {
    return !!this.linkedMetadata.find(e => e.id === metadata.id);
  }


  previous() {
    if (this.hasPrevious()) {
      this.metadataPage--;
      this.requestMetadata(false);
    }
  }


  next() {
    if (this.hasNext()) {
      this.metadataPage++;
      this.requestMetadata(false);
    }
  }


  hasPrevious() {
    return this.metadataPage > 1;
  }


  hasNext() {
    return this.metadataPage < Math.ceil(this.metadataCount / this.metadataPageSize);
  }


}
