/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Injector, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { SendByMailPopupComponent } from '../../folder-view/action-popups/mail/send-by-mail-popup/send-by-mail-popup.component';
import { SimpleActionPopupComponent } from '../../folder-view/action-popups/simple-action-popup/simple-action-popup.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { CommonMessages } from '../../../../shared/common-messages';
import { Folder } from '../../../../models/folder/folder';
import { ExternalSignaturePopupLegacyComponent } from '../../folder-view/action-popups/external-signature-popup-legacy/external-signature-popup-legacy.component';
import { GlobalPopupService } from '../../../../shared/service/global-popup.service';
import { combineLatest, Observable, forkJoin, lastValueFrom } from 'rxjs';
import { NotificationsService } from '../../../../services/notifications.service';
import { TargetDeskActionPopupComponent } from '../../folder-view/action-popups/target-desk-action-popup/target-desk-action-popup.component';
import { Action, State, UserPreferencesDto, TenantDto } from '@libriciel/iparapheur-legacy';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';
import { TasksMessages } from '../tasks-messages';
import { FolderUtils } from '../../../../utils/folder-utils';
import { FolderService as LegacyFolderService } from '@libriciel/iparapheur-legacy';
import { FolderService as StandardFolderService } from '@libriciel/iparapheur-standard';
import { ExternalSignaturePopupComponent } from '../../folder-view/action-popups/external-signature-popup/external-signature-popup.component';
import { map } from 'rxjs/operators';
import { isCurrentVersion52OrAbove } from '../../../../utils/string-utils';

@Component({
  selector: 'app-multiple-action-list',
  templateUrl: './multiple-action-list.component.html',
  styleUrls: ['./multiple-action-list.component.scss']
})
export class MultipleActionListComponent implements OnChanges {


  public static readonly forbiddenActions: (Action | SecondaryAction)[] = [Action.Chain, SecondaryAction.Print];

  readonly show52Features: boolean = isCurrentVersion52OrAbove();

  public readonly actionEnum = Action;
  public readonly secondaryActionEnum = SecondaryAction;
  public readonly messages = TasksMessages;

  @Input() folders: Folder[];
  @Input() actions: (Action | SecondaryAction)[];
  @Input() deskId: string;
  @Input() asDelegate: boolean = false;
  @Input() tenant: TenantDto;
  @Input() userPreferences: UserPreferencesDto;
  @Output() actionPerformed: EventEmitter<Action | SecondaryAction> = new EventEmitter<Action | SecondaryAction>();

  isMandatoryReadMissing: boolean = false;


  // <editor-fold desc="LifeCycle">


  constructor(public modalService: NgbModal,
              private legacyFolderService: LegacyFolderService,
              private standardFolderService: StandardFolderService,
              public globalPopupService: GlobalPopupService,
              public notificationsService: NotificationsService,
              public router: Router) {}


  ngOnChanges(_: SimpleChanges): void {
    this.computeMandatoryReadingForSignatureActions();
  }


  // </editor-fold desc="LifeCycle">


  public openModal(action: Action | SecondaryAction): void {

    if (MultipleActionListComponent.forbiddenActions.includes(action)) {
      return;
    }

    if (action === Action.Delete) {
      this.switchModal(this.folders.map(folder => Object.assign({}, folder)), action);
      return;
    }

    const folderRequestList: Observable<Folder>[] = this.folders
      .map(folder => this.legacyFolderService
        .getFolder(
          this.tenant.id,
          folder.id,
          this.asDelegate
            ? folder.stepList.find(step => FolderUtils.isActive(step))?.desks[0].id
            : this.deskId,
          true
        )
        .pipe(map(folder => Folder.fromDto(folder)))
      );

    forkJoin(folderRequestList)
      .subscribe({
        next: (populatedFolders: Folder[]) => this.switchModal(populatedFolders, action),
        error: e => this.notificationsService.showErrorMessage(this.messages.ERROR_RETRIEVING_FOLDERS, e)
      });
  }


  computeMandatoryReadingForSignatureActions(): boolean {
    return this.folders
      .filter(folder => folder.selected)
      .some(folder => this.doesFolderHaveToBeRead(folder));
  }


  doesFolderHaveToBeRead(folder: Folder) {
    const isReadingMandatory: boolean = folder?.subtype?.readingMandatory;
    const isNotReadYet: boolean = !folder?.readByCurrentUser;
    const isASignature: boolean = folder?.stepList[0]?.action === Action.Signature;
    this.isMandatoryReadMissing = isReadingMandatory && isNotReadYet && isASignature;
  }


  private switchModal(folders: Folder[], action: Action | SecondaryAction) {
    folders.forEach(folder => folder.stepList = folder.stepList.filter(step => step.state === State.Current));

    let modalResult: Promise<any>;
    const modalSize: string = FolderUtils.getModalSize(action, folders);

    switch (action) {

      case Action.Delete:
        modalResult = this.globalPopupService
          .showDeleteValidationPopup(
            CommonMessages.foldersValidationPopupLabel(folders),
            CommonMessages.foldersValidationPopupTitle(folders)
          )
          .then(
            () => lastValueFrom(combineLatest(
              folders
                .map(d => d.id)
                .map(id => this.standardFolderService.deleteFolder(this.tenant.id, this.deskId, id))
            )),
            () => { /* Dismissed */ }
          );
        break;

      case SecondaryAction.Mail:
      case Action.SecureMail:
        modalResult = this.modalService
          .open(SendByMailPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: SendByMailPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.deskId},
                {provide: SendByMailPopupComponent.INJECTABLE_CURRENT_ACTION_KEY, useValue: action},
                {provide: SendByMailPopupComponent.INJECTABLE_FOLDERS_KEY, useValue: folders},
                {provide: SendByMailPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenant.id}
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case Action.ExternalSignature:
        if (this.show52Features) {
          modalResult = this.modalService
            .open(ExternalSignaturePopupComponent, {
              injector: Injector.create({
                providers: [
                  {provide: ExternalSignaturePopupComponent.INJECTABLE_FOLDERS_KEY, useValue: folders},
                  {provide: ExternalSignaturePopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.deskId},
                  {provide: ExternalSignaturePopupComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: action},
                  {provide: ExternalSignaturePopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenant.id},
                  {provide: ExternalSignaturePopupComponent.INJECTABLE_EXTERNAL_SIGNATURE_PROCEDURE_KEY, useValue: null}
                ]
              }),
              size: modalSize,
              backdrop: 'static'
            })
            .result;
        } else {
          modalResult = this.modalService
            .open(ExternalSignaturePopupLegacyComponent, {
              injector: Injector.create({
                providers: [
                  {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_FOLDERS_KEY, useValue: folders},
                  {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_DESK_ID_KEY, useValue: this.deskId},
                  {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: action},
                  {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenant.id},
                  {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_EXTERNAL_SIGNATURE_PROCEDURE_KEY, useValue: null}
                ]
              }),
              size: modalSize,
              backdrop: 'static'
            })
            .result;
        }


        break;

      case Action.Transfer:
      case Action.AskSecondOpinion:
        modalResult = this.modalService
          .open(TargetDeskActionPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: TargetDeskActionPopupComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: action},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_FOLDERS_KEY, useValue: folders},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.deskId},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenant.id},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_AS_ADMIN_KEY, useValue: false},
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      default:
        modalResult = this.modalService
          .open(SimpleActionPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: SimpleActionPopupComponent.injectableFoldersKey, useValue: folders},
                {provide: SimpleActionPopupComponent.injectableDeskIdKey, useValue: this.deskId},
                {provide: SimpleActionPopupComponent.injectablePerformedActionKey, useValue: action},
                {provide: SimpleActionPopupComponent.injectableTenantKey, useValue: this.tenant},
                {provide: SimpleActionPopupComponent.injectableUserPreferencesKey, useValue: this.userPreferences}
              ]
            }),
            size: modalSize
          })
          .result;
        break;
    }

    modalResult
      .then(result => {
        // Delete was dismissed
        if (result == undefined) {
          return;
        }

        // Delete was successful
        if (Array.isArray(result) && result.every(res => res == null)) {
          this.notificationsService.showSuccessMessage(CommonMessages.getMultipleDeleteCreationMessage(folders));
          this.actionPerformed.emit(action);
        }

        if ((result === CommonMessages.ACTION_RESULT_OK) || (result.value === CommonMessages.ACTION_RESULT_OK)) {
          this.actionPerformed.emit(action);
        }
      });
  }


}
