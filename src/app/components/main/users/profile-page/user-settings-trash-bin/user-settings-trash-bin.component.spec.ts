/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSettingsTrashBinComponent } from './user-settings-trash-bin.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ToastrModule } from 'ngx-toastr';
import { ProfileUpdateService } from '../../../../../shared/service/profile-update.service';
import { SelectedUserPreferencesService } from '../../../../../services/selected-user-preferences.service';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

describe('UserSettingsTrashBinComponent', () => {
  let component: UserSettingsTrashBinComponent;
  let fixture: ComponentFixture<UserSettingsTrashBinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserSettingsTrashBinComponent ],
      imports: [
        HttpClientTestingModule,
        ToastrModule.forRoot()
      ],
      providers: [
        ProfileUpdateService,
        SelectedUserPreferencesService,
        {provide: ActivatedRoute, useValue: {data: of({result: 'test'})}}],
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserSettingsTrashBinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
