/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AbsencePopupComponent } from './absence-popup.component';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { AdminMainLayoutComponent } from '../../../admin-main-layout/admin-main-layout.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DeskDto } from '@libriciel/iparapheur-legacy';

describe('AbsencePopupComponent', () => {

  let component: AbsencePopupComponent;
  let fixture: ComponentFixture<AbsencePopupComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [RouterModule.forRoot([]), ToastrModule.forRoot()],
        providers: [
          HttpClient,
          HttpHandler,
          NgbActiveModal,
          {provide: AbsencePopupComponent.INJECTABLE_DESK_KEY, useValue: {} as DeskDto},
          {provide: AbsencePopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: 'tenantId'},
          {provide: AbsencePopupComponent.INJECTABLE_AS_ADMIN_KEY, useValue: false},
        ],
        declarations: [AdminMainLayoutComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(AbsencePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
