/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { CrudOperation } from '../../../../../services/crud-operation';
import { LoggableSignatureImage } from '../../../../../models/auth/loggable-signature-image';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { Observable } from 'rxjs';
import { UserEditSubPanelsMessages } from '../../user-edit-page-subpanels/user-edit-sub-panels-messages';
import { NotificationsService } from '../../../../../services/notifications.service';
import { LegacyUserService } from '../../../../../services/ip-core/legacy-user.service';
import { SelectedUserPreferencesService } from '../../../../../services/selected-user-preferences.service';
import { ActivatedRoute } from '@angular/router';
import { UserDto } from '@libriciel/iparapheur-provisioning';
import { ProfileUpdateService } from '../../../../../shared/service/profile-update.service';
import { CONFIG } from '../../../../../shared/config/config';

@Component({
  selector: 'app-user-settings-signature',
  templateUrl: './user-settings-signature.component.html',
  styleUrls: ['./user-settings-signature.component.scss']
})
export class UserSettingsSignatureComponent implements OnInit {

  protected readonly messages = UserEditSubPanelsMessages;

  readonly show51features: boolean = !CONFIG?.VERSION?.startsWith('5.0.');

  currentUser: UserDto;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  isProcessing: boolean = false;

  signatureImageBlob: Blob;
  signatureImageSrc: string;
  startedSessionWithASignatureImage: boolean = false;
  signatureImageWasModified: boolean = false;


  constructor(private legacyUserService: LegacyUserService,
              private notificationService: NotificationsService,
              private profileUpdateService: ProfileUpdateService,
              private route: ActivatedRoute,
              private selectedUserPreferencesService: SelectedUserPreferencesService) { }


  ngOnInit(): void {
    this.currentUser = this.route.parent?.snapshot.data["currentUser"];

    this.selectedUserPreferencesService.currentUserPreferences$.subscribe(userPreferences => {
      this.userPreferences = userPreferences;

      this.initSignatureImage();
    });
  }


  signatureImageChanged(file: Blob) {
    this.signatureImageBlob = file;
    this.signatureImageWasModified = true;
  }


  signatureImageDeleted() {
    this.signatureImageSrc = null;
    this.signatureImageBlob = null;
    this.signatureImageWasModified = true;
  }


  initSignatureImage() {
    this.signatureImageBlob = null;
    if (!!this.userPreferences?.signatureImageContentId) {
      this.startedSessionWithASignatureImage = true;
      this.signatureImageSrc = this.legacyUserService.signatureImageUrl(Date.now().toString());
    } else {
      this.startedSessionWithASignatureImage = false;
      this.signatureImageSrc = null;
    }
    this.signatureImageWasModified = false;
  }


  handleSignatureImage() {
    if (!this.signatureImageWasModified) {
      return;
    }

    if (this.startedSessionWithASignatureImage && !this.signatureImageBlob) {
      this.legacyUserService
        .deleteSignatureImage()
        .pipe(catchError(this.notificationService.handleHttpError('deleteSignatureImage')))
        .subscribe({
          next: () => {
            this.notificationService.showCrudMessage(CrudOperation.Delete, new LoggableSignatureImage());
            // That's a strange syntax, but we actually reset the readonly property,
            // to avoid a useless request to the server
            this.userPreferences['signatureImageContentId' as UserPreferencesDto['signatureImageContentId']] = null;
            this.initSignatureImage();
          },
          error: e => this.notificationService.showCrudMessage(CrudOperation.Delete, new LoggableSignatureImage(), e.message, false)
        });
      return;
    }

    const $request: Observable<any> = this.startedSessionWithASignatureImage
      ? this.legacyUserService.updateSignatureImage(this.signatureImageBlob)
      : this.legacyUserService.setSignatureImage(this.signatureImageBlob);

    const operation: CrudOperation = this.startedSessionWithASignatureImage ? CrudOperation.Update : CrudOperation.Create;

    $request
      .subscribe({
        next: signatureContentId => {
          if (!!signatureContentId) {
            // That's a strange syntax, but we actually reset the readonly property,
            // to avoid a useless request to the server
            this.userPreferences['signatureImageContentId' as UserPreferencesDto['signatureImageContentId']] = signatureContentId;
          }

          this.notificationService.showCrudMessage(operation, new LoggableSignatureImage());
          this.initSignatureImage();
        },
        error: e => this.notificationService.showCrudMessage(operation, new LoggableSignatureImage(), e.message, false)
      });
  }


  saveForm() {
    this.isProcessing = true;

    this.handleSignatureImage();

    this.profileUpdateService.updateUserPreferences(this.userPreferences)
      .then(r => this.isProcessing = false);
  }
}



