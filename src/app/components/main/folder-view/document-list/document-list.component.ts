/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectorRef, AfterViewInit, OnInit } from '@angular/core';
import { Document } from '../../../../models/document';
import { SignatureProof } from '../../../../models/signatureProof';
import { faChevronRight, faEllipsisV, faDownload, faSyncAlt } from '@fortawesome/free-solid-svg-icons';
import { faPlusSquare, faMinusSquare } from '@fortawesome/free-regular-svg-icons';
import { Folder } from '../../../../models/folder/folder';
import { DocumentService } from '../../../../services/ip-core/document.service';
import { NotificationsService } from '../../../../services/notifications.service';
import { FolderService } from '../../../../services/ip-core/folder.service';
import { ActivatedRoute } from '@angular/router';
import { CommonIcons } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../shared/common-messages';
import { FolderViewMessages } from '../folder-view-messages';
import { GlobalPopupService } from '../../../../shared/service/global-popup.service';
import { DetachedSignature } from '../../../../models/signature/detached-signature';
import { FolderUtils } from '../../../../utils/folder-utils';
import { MIME_TYPES_DETACHED_SIGNATURES, isCurrentVersion52OrAbove } from '../../../../utils/string-utils';
import { TypologyUtils } from '../../../../utils/typology-utils';
import { FileService } from '../../../../services/file.service';
import { FolderViewBlock, UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { DeskDto } from '@libriciel/iparapheur-provisioning';
import { TypologyService, ServerInfoDto } from '@libriciel/iparapheur-internal';
import { FolderService as InternalFolderService } from '@libriciel/iparapheur-internal';
import { ValidatedSignatureInformation } from '@libriciel/iparapheur-internal/model/validatedSignatureInformation';


@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit, AfterViewInit {


  readonly show52Features: boolean = isCurrentVersion52OrAbove();
  readonly mimeTypesDetachedSignature = MIME_TYPES_DETACHED_SIGNATURES;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = FolderViewMessages;
  readonly faSyncAlt = faSyncAlt;
  readonly rightChevronIcon = faChevronRight;
  readonly faEllipsisV = faEllipsisV;
  readonly faDownload = faDownload;
  readonly minusSquareIcon = faMinusSquare;
  readonly plusSquareIcon = faPlusSquare;
  readonly isInValidationWorkflowFn = FolderUtils.isInValidationWorkflow;
  readonly doesContainEmbeddedSignatureFn = FolderUtils.doesContainEmbeddedSignature;
  readonly doesContainDetachedSignatureFn = FolderUtils.doesContainDetachedSignature;
  readonly doesContainSignatureFn = FolderUtils.doesContainSignature;
  readonly timestampToDateFn = FolderUtils.timestampToDate;
  readonly getSignatureProofErrorFn = FolderUtils.getSignatureProofError;
  readonly selectableMediaTypes: string[] = [
    'application/pdf', 'text/pdf',
    'application/vnd.oasis.opendocument.text', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  ];
  protected readonly FolderViewMessages = FolderViewMessages;
  protected readonly Object = Object;

  @Input() folder: Folder;
  @Input() currentDesk: DeskDto;
  @Input() currentDocument: Document;
  @Input() isEditable: boolean = false;
  @Input() currentFolderViewBlock: FolderViewBlock = FolderViewBlock.AnnexeDocuments;
  @Input() userPreferences: UserPreferencesDto;
  @Input() serverInfo: ServerInfoDto;
  @Input() isSignatureReport: boolean = false;
  @Output() documentSelected = new EventEmitter<Document>();

  tenantId: string;
  isCollapsed: boolean;
  doesTypologyAllowDetachedSignatureAdding: boolean;
  isInValidationWorkflow: boolean;
  canAddDocument: boolean = false;
  colspan: number;
  showDetachedSignatures: boolean;
  displayMainDocuments: boolean = false;


  // <editor-fold desc="LifeCycle">


  constructor(private documentService: DocumentService,
              private fileService: FileService,
              private folderService: FolderService,
              private globalPopupService: GlobalPopupService,
              private internalFolderService: InternalFolderService,
              private notificationsService: NotificationsService,
              private typologyService: TypologyService,
              public route: ActivatedRoute,
              public cdr: ChangeDetectorRef) { }


  ngOnInit(): void {
    this.displayMainDocuments = this.currentFolderViewBlock === FolderViewBlock.MainDocuments;
    this.isInValidationWorkflow = FolderUtils.isInValidationWorkflow(this.folder);

    const isAnnexe: boolean = !this.displayMainDocuments;
    const isSignatureProof: boolean = this.isSignatureReport;
    const isMultiMainDocument = this.folder?.subtype?.multiDocuments;
    const isMainActionEnabled = this.currentDesk.actionAllowed && !!this.folder?.type && !!this.folder.subtype;
    this.canAddDocument = isMainActionEnabled && (isAnnexe || isMultiMainDocument) && !isSignatureProof;
  }


  // TODO : check if some of this can be done earlier, in the ngOnInit
  ngAfterViewInit(): void {
    this.tenantId = this.route.snapshot.paramMap.get('tenantId');
    this.colspan = 4 + (this.userPreferences.showAdminIds ? 1 : 0);

    this.setDetachedSignatureRights();
  }


  // </editor-fold desc="LifeCycle">


  setDetachedSignatureRights(): void {
    const isEnvelopedParsedSignatureFormat: boolean = TypologyUtils.isEnvelopedSignatureFormat(this.folder?.type?.signatureFormat);

    // If no detached signature were added before validation workflow, we hide the menu.
    if (this.isInValidationWorkflow && isEnvelopedParsedSignatureFormat) {
      this.showDetachedSignatures = false;
      return;
    }

    // FIXME fetch the unparsed signature format from getFolder
    // If not, the not parsed signature format might be "AUTO"
    this.typologyService.getType(this.tenantId, this.folder?.type?.id)
      .subscribe(type => {
        const isEnvelopedSignatureFormat = TypologyUtils.isEnvelopedSignatureFormat(type.signatureFormat);
        this.showDetachedSignatures = !isEnvelopedSignatureFormat;
        this.doesTypologyAllowDetachedSignatureAdding = !isEnvelopedSignatureFormat && !this.isInValidationWorkflow;
      });
  }


  getDocumentList(): (Document)[] {
    if (!this.folder) {
      return [];
    }

    let filteredList: (Document)[];
    if (this.displayMainDocuments) {
      filteredList = this.folder.documentList.filter(doc => doc.isMainDocument);
      filteredList.sort((a: Document, b: Document): number => a.index - b.index);
    } else if (!this.isSignatureReport) {
      filteredList = this.folder.documentList.filter(doc => !doc.isMainDocument);
      filteredList.sort((a: Document, b: Document): number => a.index - b.index);
      if (!this.show52Features) {
        filteredList = filteredList.concat(this.getSignatureProofList());
      }
    } else {
      filteredList = this.getSignatureProofList();
    }

    return filteredList;
  }


  getSignatureProofList(): SignatureProof[] {
    if (!this.folder || !this.folder.signatureProofList) {
      return [];
    }
    let proofList: SignatureProof[] = this.folder.signatureProofList.filter(doc => doc.internal && !doc.errorMessage);
    proofList = proofList.concat(this.folder.signatureProofList.filter(doc => !doc.internal));

    return proofList;
  }


  containsInternalSignatureProof(): boolean {
    if (!this.folder || !this.folder.signatureProofList) {
      return false;
    }
    return this.folder.signatureProofList.filter(doc => doc.internal && !doc.errorMessage).length > 0;
  }


  generateDocumentSignatureVerification(): void {
    this.internalFolderService.generateDocumentSignatureVerification(this.tenantId, this.folder.id, 'body', true)
      .subscribe({
        next: updatedFolderDto => {
          FolderUtils.showSignatureProofSuccessOrErrorMessage(updatedFolderDto, this.notificationsService, this.messages);
          this.refreshDocumentList();
        },
        error: e => this.notificationsService.showErrorMessage(this.messages.GENERATE_SIGNATURE_VALIDATION_ERROR, e.message)
      });
  }


  onRowClicked(document: Document) {
    if (this.currentDocument?.id !== document.id && this.canBeOpenedInPdfViewer(document)) {
      this.documentSelected.emit(document);
    }
  }


  canBeOpenedInPdfViewer(document: Document): boolean {
    return this.selectableMediaTypes.includes(document.mediaType);
  }


  addDocument(event: any) {
    for (const file of event.target.files) {
      this.documentService
        .addDocument(this.tenantId, this.folder, this.displayMainDocuments, file.name, file)
        .subscribe({
          next: () => {
            this.notificationsService.showSuccessMessage(FolderViewMessages.ADD_DOCUMENT_SUCCESS);
            this.refreshDocumentList();
          },
          error: e => this.notificationsService.showErrorMessage(FolderViewMessages.ADD_DOCUMENT_ERROR, e.message)
        });
    }
  }


  replaceDocument(document: Document, event: any) {
    for (const file of event.target.files) {
      this.documentService
        .updateDocument(this.tenantId, this.folder, document, file.name, file)
        .subscribe({
          next: () => {
            this.notificationsService.showSuccessMessage(FolderViewMessages.REPLACE_DOCUMENT_SUCCESS);
            // TODO : a soft refresh
            location.reload();
          },
          error: e => this.notificationsService.showErrorMessage(FolderViewMessages.REPLACE_DOCUMENT_ERROR, e.message)
        });
    }
  }


  addDetachedSignature(event: any, document: Document) {
    for (const file of event.target.files) {
      this.documentService
        .addDetachedSignature(this.tenantId, this.folder.id, document.id, file, this.currentDesk)
        .subscribe({
          next: () => {
            this.notificationsService.showSuccessMessage(FolderViewMessages.ADD_DETACHED_SIGNATURE_SUCCESS);
            this.refreshDocumentList();
          },
          error: e => this.notificationsService.showErrorMessage(FolderViewMessages.ADD_DETACHED_SIGNATURE_ERROR, e.message)
        });
    }
  }


  deleteDetachedSignature(event: any, documentId: string, detachedSignature: DetachedSignature) {
    event.stopPropagation();
    this.globalPopupService
      .showDeleteValidationPopup(FolderViewMessages.detachedSignatureDeleteValidationPopup(detachedSignature.name))
      .then(
        () => this.doDeleteDetachedSignature(documentId, detachedSignature.id),
        () => {/* Dismissed */}
      );
  }


  downloadFile(doc: Document, detachedSignature?: DetachedSignature): void {
    const documentName = detachedSignature ? detachedSignature.name : doc.name;

    const url: string = detachedSignature
      ? this.documentService.detachedSignatureUrl(this.tenantId, this.folder.id, doc.id, detachedSignature.id)
      : this.documentService.documentUrl(this.tenantId, this.folder.id, doc.id);

    this.fileService.downloadFileWithUrl(url, documentName)
      .subscribe({
        next: () => {/* nothing here */},
        error: e => this.notificationsService.showErrorMessage(FolderViewMessages.errorDownloadingFile(!!detachedSignature, documentName), e)
      });
  }


  doDeleteDetachedSignature(documentId: string, detachedSignatureId: string): void {
    this.documentService
      .deleteDetachedSignature(this.tenantId, this.folder.id, documentId, detachedSignatureId)
      .subscribe({
        next: () => {
          this.notificationsService.showSuccessMessage(FolderViewMessages.REMOVE_DETACHED_SIGNATURE_SUCCESS);
          this.refreshDocumentList();
        },
        error: e => this.notificationsService.showErrorMessage(FolderViewMessages.REMOVE_DETACHED_SIGNATURE_ERROR, e.message)
      });
  }


  canDeleteDocument(document: Document): boolean {
    return !!document.deletable || !this.isInValidationWorkflow;
  }


  deleteDocument(event: any, document: Document) {
    event.stopPropagation();

    if (this.folder.documentList.length <= 1) {
      this.notificationsService.showErrorMessage(FolderViewMessages.CANNOT_DELETE_THE_LAST_DOCUMENT);
      return;
    }

    this.globalPopupService
      .showDeleteValidationPopup(FolderViewMessages.deleteDocumentValidationPopup(document.name))
      .then(
        () => this.doDeleteDocument(document),
        () => {/* Dismissed */}
      );
  }


  doDeleteDocument(document: Document): void {
    this.documentService
      .deleteDocument(this.tenantId, this.folder, document)
      .subscribe({
        next: () => {
          this.notificationsService.showSuccessMessage(FolderViewMessages.DELETE_DOCUMENT_SUCCESS);
          this.refreshDocumentList();
        },
        error: e => this.notificationsService.showErrorMessage(FolderViewMessages.DELETE_DOCUMENT_ERROR, e.message)
      });
  }


  refreshDocumentList() {
    this.folderService
      .getFolder(this.tenantId, this.folder.id)
      .subscribe({
        next: folderRetrieved => {
          this.folder.documentList = folderRetrieved.documentList;
          this.folder.signatureProofList = folderRetrieved.signatureProofList;

          // Refreshing selection state, and swapping to the first doc,
          // if the previous selected one is now missing
          if (!this.folder.documentList.includes(this.currentDocument) && this.displayMainDocuments === true) {
            this.onRowClicked(this.folder.documentList[0]);
          }
          this.cdr.detectChanges();
        },
        error: e => this.notificationsService.showErrorMessage(FolderViewMessages.ERROR_LOADING_WORKFLOW, e)
      });
  }


  getSignatureInfo(document) : {embedded: any[], detached: any[]} {

    const info = {
      embedded: [],
      detached: []
    };

    document.embeddedSignatureInfos?.forEach((signatureInfo: ValidatedSignatureInformation) => {
      info.embedded.push({
        signatory: signatureInfo.eidasLevel != null ? signatureInfo.principalIssuer : null,
        certificate: signatureInfo.eidasLevel != null ? signatureInfo.principalSubjectIssuer : null,
        signatureDateTime: signatureInfo.signatureDateTime,
        certificateBeginDate: signatureInfo.certificateBeginDate,
        certificateEndDate: signatureInfo.certificateEndDate,
        eidasLevel: signatureInfo.eidasLevel,
        error: signatureInfo.error
      });
    });

    document.detachedSignatures?.forEach((detachedSignature: DetachedSignature) => {
      info.detached.push({
        signatory: detachedSignature.signatureInfo.eidasLevel != null ? detachedSignature.signatureInfo.principalIssuer : null,
        certificate: detachedSignature.signatureInfo.eidasLevel != null ? detachedSignature.signatureInfo.principalSubjectIssuer : null,
        signatureDateTime: detachedSignature.signatureInfo.signatureDateTime,
        certificateBeginDate: detachedSignature.signatureInfo.certificateBeginDate,
        certificateEndDate: detachedSignature.signatureInfo.certificateEndDate,
        eidasLevel: detachedSignature.signatureInfo.eidasLevel,
        error: detachedSignature.signatureInfo.error
      });
    });

    return info;
  }


  isSignatureValidAndNotNull(signatureInfo : ValidatedSignatureInformation) : boolean {
    if (signatureInfo.error !== null) {
      return false;
    }
    return signatureInfo.isSignatureValid !== undefined ? true : null;
  }


}
