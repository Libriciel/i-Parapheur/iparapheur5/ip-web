/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class ExternalSignatureMessages {

  static readonly SIGNER_LIST_TITLE: string = `Liste des signataires`;

  static readonly EMPTY_SIGNER_LIST_PLACEHOLDER: string = `Aucun signataire n'a été ajouté`;
  static readonly EVERY_SIGNER_HAS_SIGNED: string = `Toutes les signatures ont été réalisées dans l'outil de signature externe.`;
  static readonly TIME_THRESHOLD_INFO: string = `Par défaut, le iparapheur récupère les documents signés toutes les 20 minutes.`;
  static readonly FINISH_STEP_INFO: string = `L'étape sera alors automatiquement terminée, et le dossier avancera à l'étape suivante.`;

  static readonly EXTERNAL_SIGNATURE_FORM_LABEL_FIRSTNAME: string = 'Prénom';
  static readonly EXTERNAL_SIGNATURE_FORM_LABEL_LASTNAME: string = 'Nom';
  static readonly EXTERNAL_SIGNATURE_FORM_LABEL_MAIL_ADDRESS: string = 'Adresse e-mail';
  static readonly EXTERNAL_SIGNATURE_FORM_LABEL_PHONE: string = 'Numéro de téléphone portable';

  static readonly ADD_SIGNER: string = 'Ajouter un signataire';
  static readonly BAD_SIGNER_FORM_DATA: string = 'Une ou plusieurs informations de ce signataire sont incorrectes ou manquantes';
}
