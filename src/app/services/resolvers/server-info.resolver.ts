/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ResolveFn, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NotificationsService } from '../notifications.service';
import { inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ServerInfoService, ServerInfoDto } from '@libriciel/iparapheur-internal';
import { catchError, map } from 'rxjs/operators';
import { ResolverMessages } from './resolver-messages';

export const ServerInfoResolver: ResolveFn<ServerInfoDto> = (
  _route: ActivatedRouteSnapshot,
  _state: RouterStateSnapshot,
  notificationService: NotificationsService = inject(NotificationsService),
  serverInfoService: ServerInfoService = inject(ServerInfoService),
): Observable<ServerInfoDto> => serverInfoService
  .getServerInfo()
  .pipe(
    catchError(() => {
      notificationService.setGlobalError(ResolverMessages.ERROR_FETCHING_SERVER_INFO);
      return of();
    }),
    map(ServerInfoDto => ServerInfoDto)
);