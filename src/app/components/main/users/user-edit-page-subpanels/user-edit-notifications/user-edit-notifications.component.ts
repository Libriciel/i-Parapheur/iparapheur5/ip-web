/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input } from '@angular/core';
import { UserEditSubPanelsMessages } from '../user-edit-sub-panels-messages';
import { Options } from "@libriciel/ls-elements";
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { ProfileUpdateService } from '../../../../../shared/service/profile-update.service';
import { UserDto } from '@libriciel/iparapheur-provisioning';
import { ActivatedRoute } from '@angular/router';
import { SelectedUserPreferencesService } from '../../../../../services/selected-user-preferences.service';

@Component({
  selector: 'app-user-edit-notifications',
  templateUrl: './user-edit-notifications.component.html',
  styleUrls: ['./user-edit-notifications.component.scss']
})
export class UserEditNotificationsComponent implements OnInit {


  static readonly SINGLE_NOTIFICATION = 'single_notifications';
  static readonly NONE_NOTIFICATION = 'none';

  readonly messages = UserEditSubPanelsMessages;
  readonly regexHourly = new RegExp(/0 0 \*(?:\/\d+)? \* \* \*/);
  readonly regexDaily = new RegExp(/0 0 \d+ \* \* \*/);
  readonly regexWeekly = new RegExp(/0 0 \d+ \* \* \d+/);

  @Input() currentUser: UserDto;
  @Input() userPreferences: UserPreferencesDto = {} as UserPreferencesDto;

  notificationOptions: Options;
  isProcessing: boolean = false;

  // <editor-fold desc="LifeCycle">

  constructor(private profileUpdateService: ProfileUpdateService,
              private route: ActivatedRoute,
              private selectedUserPreferencesService: SelectedUserPreferencesService) {}


  ngOnInit(): void {
    this.currentUser ??= this.route.parent?.snapshot.data["currentUser"];
    this.selectedUserPreferencesService.currentUserPreferences$.subscribe(userPreferences => {
      this.userPreferences = userPreferences;
    });

    this.notificationOptions = {
      name: this.messages.EDIT_SPECIFIC_NOTIFICATIONS_TITLE,
      values: [
        {name: this.messages.EDIT_SPECIFIC_NOTIFICATIONS_NEW_FOLDER_INVOLVING_ME, active: this.userPreferences.notifiedOnConfidentialFolders},
        {name: this.messages.EDIT_SPECIFIC_NOTIFICATIONS_ACTION_ON_FOLLOWED_FOLDER, active: this.userPreferences.notifiedOnFollowedFolders},
        {name: this.messages.EDIT_SPECIFIC_NOTIFICATIONS_LATE_FOLDER_INVOLVING_ME, active: this.userPreferences.notifiedOnLateFolders}
      ]
    };
  }


  // </editor-fold desc="LifeCycle">


  isNotificationSingle(): boolean {
    return this.userPreferences.notificationsCronFrequency == UserEditNotificationsComponent.SINGLE_NOTIFICATION;
  }


  isNotificationHourly(): boolean {
    return this.regexHourly.test(this.userPreferences.notificationsCronFrequency);
  }


  isNotificationDaily(): boolean {
    return this.regexDaily.test(this.userPreferences.notificationsCronFrequency);
  }


  isNotificationWeekly(): boolean {
    return this.regexWeekly.test(this.userPreferences.notificationsCronFrequency);
  }


  isNotificationDisabled(): boolean {
    return !this.userPreferences.notificationsCronFrequency
      || this.userPreferences.notificationsCronFrequency == UserEditNotificationsComponent.NONE_NOTIFICATION;
  }


  onSpecificNotificationValueChange() {

    this.userPreferences.notifiedOnConfidentialFolders = this.notificationOptions.values
      .find(option => option.name === UserEditSubPanelsMessages.EDIT_SPECIFIC_NOTIFICATIONS_NEW_FOLDER_INVOLVING_ME)
      .active;

    this.userPreferences.notifiedOnFollowedFolders = this.notificationOptions.values
      .find(option => option.name === UserEditSubPanelsMessages.EDIT_SPECIFIC_NOTIFICATIONS_ACTION_ON_FOLLOWED_FOLDER)
      .active;

    this.userPreferences.notifiedOnLateFolders = this.notificationOptions.values
      .find(option => option.name === UserEditSubPanelsMessages.EDIT_SPECIFIC_NOTIFICATIONS_LATE_FOLDER_INVOLVING_ME)
      .active;
  }


  saveForm() {
    this.isProcessing = true;

    this.profileUpdateService.updateUserPreferences(this.userPreferences)
      .then(r => this.isProcessing = false);
  }
}
