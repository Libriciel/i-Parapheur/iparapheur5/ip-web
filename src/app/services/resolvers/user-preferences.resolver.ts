/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, ResolveFn } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { FilterService } from '../filter.service';
import { NotificationsService } from '../notifications.service';
import { ResolverMessages } from './resolver-messages';
import { CurrentUserService, UserPreferencesDto, ArchiveViewColumn, FolderViewBlock, TaskViewColumn } from '@libriciel/iparapheur-legacy';
import { TableName } from '@libriciel/iparapheur-internal';
import { FolderFilter } from '../../models/folder/folder-filter';
import { SelectedUserPreferencesService } from '../selected-user-preferences.service';
import { TableLayoutDto, LabelledColumnType } from '@libriciel/iparapheur-internal';

export const defaultTaskListColumnList = [
  TaskViewColumn.FolderName,
  TaskViewColumn.State,
  TaskViewColumn.Type,
  TaskViewColumn.Subtype,
  TaskViewColumn.CurrentDesk,
  TaskViewColumn.LimitDate,
  TaskViewColumn.CreationDate,
  TaskViewColumn.OriginDesk
];
export const defaultTaskListLabelledColumnList = defaultTaskListColumnList.map(column => {
  return {id: column, type: LabelledColumnType.ClassAttribute};
});

export const defaultFolderMainLayoutColumnList = [
  FolderViewBlock.Actions,
  FolderViewBlock.Draft,
  FolderViewBlock.Metadata,
  FolderViewBlock.Workflow,
  FolderViewBlock.MainDocuments,
  FolderViewBlock.AnnexeDocuments
];
export const defaultFolderMainLayoutLabelledColumnList = defaultFolderMainLayoutColumnList.map(column => {
  return {id: column, type: LabelledColumnType.ClassAttribute};
});

export const defaultAdminFolderListColumnList = [
  TaskViewColumn.FolderId,
  TaskViewColumn.FolderName,
  TaskViewColumn.CurrentDesk,
  TaskViewColumn.CreationDate,
  "LAST_MODIFICATION_DATE",
  TaskViewColumn.Type,
  TaskViewColumn.Subtype,
  TaskViewColumn.State
];
export const defaultAdminFolderListLabelledColumnList = defaultAdminFolderListColumnList.map(column => {
  return {id: column, type: LabelledColumnType.ClassAttribute};
});

export const defaultArchiveListColumnList = [
  ArchiveViewColumn.Id,
  ArchiveViewColumn.Name,
  ArchiveViewColumn.OriginDesk,
  ArchiveViewColumn.TypeSubtype,
  ArchiveViewColumn.CreationDate,
  ArchiveViewColumn.Actions
];
export const defaultArchiveListLabelledColumnList = defaultArchiveListColumnList.map(column => {
  return {id: column, type: LabelledColumnType.ClassAttribute};
});


export const defaultTableLayerList = [
  {
    tableName: TableName.TaskList,
    defaultAsc: true,
    defaultSortBy: TaskViewColumn.FolderName,
    columnList: defaultTaskListColumnList,
    labelledColumnList: defaultTaskListLabelledColumnList
  },
  {
    tableName: TableName.FolderMainLayout,
    defaultAsc: true,
    defaultSortBy: null,
    columnList: defaultFolderMainLayoutColumnList,
    labelledColumnList: defaultFolderMainLayoutLabelledColumnList
  },
  {
    tableName: TableName.AdminFolderList,
    defaultAsc: true,
    defaultSortBy: null,
    columnList: defaultAdminFolderListColumnList,
    labelledColumnList: defaultAdminFolderListLabelledColumnList
  },
  {
    tableName: TableName.ArchiveList,
    defaultAsc: true,
    defaultSortBy: ArchiveViewColumn.Name,
    columnList: defaultArchiveListColumnList,
    labelledColumnList: defaultArchiveListLabelledColumnList
  }
];

export const UserPreferencesResolver: ResolveFn<UserPreferencesDto> = (
  _route: ActivatedRouteSnapshot,
  _state: RouterStateSnapshot,
  notificationService: NotificationsService = inject(NotificationsService),
  currentUserService: CurrentUserService = inject(CurrentUserService),
  selectedUserPreferencesService: SelectedUserPreferencesService = inject(SelectedUserPreferencesService),
  filterService: FilterService = inject(FilterService)
): Observable<UserPreferencesDto> => currentUserService
  .getPreferences()
  .pipe(
    catchError(() => {
      notificationService.setGlobalError(ResolverMessages.ERROR_FETCHING_USER_PREF);
      return of({
        taskViewDefaultPageSize: 10,
        archiveViewDefaultPageSize: 10,
        showAdminIds: false,
        currentFilter: null,
        currentFilterId: null,
        tableLayoutList: defaultTableLayerList,
        favoriteDesks: [],
        favoriteDeskIds: [],
        notificationsRedirectionMail: null,
        notificationsCronFrequency: null,
        notifiedOnConfidentialFolders: false,
        notifiedOnFollowedFolders: false,
        notifiedOnLateFolders: false,
        signatureImageContentId: null,
      } as UserPreferencesDto);
    }),
    map(userPreferencesDto => {

      const trashBinTableLayout: TableLayoutDto | undefined = userPreferencesDto.tableLayoutList.find(t => t.tableName === TableName.ArchiveList && !t.deskId);
      if (trashBinTableLayout?.labelledColumnList.length === 0) {
        userPreferencesDto.tableLayoutList.splice(userPreferencesDto.tableLayoutList.indexOf(trashBinTableLayout), 1);
        userPreferencesDto.tableLayoutList.push({
          id: trashBinTableLayout.id,
          deskId: null,
          tableName: TableName.ArchiveList,
          defaultAsc: trashBinTableLayout.defaultAsc,
          defaultSortBy: trashBinTableLayout.defaultSortBy,
          labelledColumnList: defaultArchiveListLabelledColumnList
        });
      }
      if (!trashBinTableLayout) {
        userPreferencesDto.tableLayoutList.push({
          tableName: TableName.ArchiveList,
          defaultAsc: true,
          defaultSortBy: TaskViewColumn.FolderName,
          labelledColumnList: defaultArchiveListLabelledColumnList
        });
      }

      const folderMainLayoutTableLayout: TableLayoutDto | undefined = userPreferencesDto.tableLayoutList.find(t => t.tableName === TableName.FolderMainLayout && !t.deskId);
      if (folderMainLayoutTableLayout && folderMainLayoutTableLayout.labelledColumnList?.length === 0) {
        userPreferencesDto.tableLayoutList.splice(userPreferencesDto.tableLayoutList.indexOf(folderMainLayoutTableLayout), 1);
        userPreferencesDto.tableLayoutList.push({
          id: folderMainLayoutTableLayout.id,
          deskId: null,
          tableName: TableName.FolderMainLayout,
          defaultAsc: folderMainLayoutTableLayout.defaultAsc,
          defaultSortBy: folderMainLayoutTableLayout.defaultSortBy,
          labelledColumnList: defaultFolderMainLayoutLabelledColumnList
        });
      }
      if (!folderMainLayoutTableLayout) {
        userPreferencesDto.tableLayoutList.push({
          tableName: TableName.FolderMainLayout,
          defaultAsc: true,
          defaultSortBy: null,
          labelledColumnList: defaultFolderMainLayoutLabelledColumnList
        });
      }

      const taskListTableLayout: TableLayoutDto | undefined = userPreferencesDto.tableLayoutList.find(t => t.tableName === TableName.TaskList && !t.deskId);
      if (taskListTableLayout && taskListTableLayout.labelledColumnList?.length === 0) {
        userPreferencesDto.tableLayoutList.splice(userPreferencesDto.tableLayoutList.indexOf(taskListTableLayout), 1);
        userPreferencesDto.tableLayoutList.push({
          id: taskListTableLayout.id,
          deskId: null,
          tableName: TableName.TaskList,
          defaultAsc: taskListTableLayout.defaultAsc,
          defaultSortBy: taskListTableLayout.defaultSortBy,
          labelledColumnList: defaultTaskListLabelledColumnList
        });
      }
      if (!taskListTableLayout) {
        userPreferencesDto.tableLayoutList.push({
          tableName: TableName.TaskList,
          defaultAsc: true,
          defaultSortBy: TaskViewColumn.FolderName,
          labelledColumnList: defaultTaskListLabelledColumnList
        });
      }

      const adminFolderListTableLayout: TableLayoutDto | undefined = userPreferencesDto.tableLayoutList.find(t => t.tableName === TableName.AdminFolderList && !t.deskId);
      if (adminFolderListTableLayout && adminFolderListTableLayout.labelledColumnList?.length === 0) {
        userPreferencesDto.tableLayoutList.splice(userPreferencesDto.tableLayoutList.indexOf(adminFolderListTableLayout), 1);
        userPreferencesDto.tableLayoutList.push({
          id: adminFolderListTableLayout.id,
          deskId: null,
          tableName: TableName.AdminFolderList,
          defaultAsc: adminFolderListTableLayout.defaultAsc,
          defaultSortBy: adminFolderListTableLayout.defaultSortBy,
          labelledColumnList: defaultAdminFolderListLabelledColumnList
        });
      }
      if (!adminFolderListTableLayout) {
        userPreferencesDto.tableLayoutList.push({
          tableName: TableName.AdminFolderList,
          defaultAsc: true,
          defaultSortBy: null,
          labelledColumnList: defaultAdminFolderListLabelledColumnList
        });
      }

      if (!!userPreferencesDto.currentFilter) {
        filterService.setDefaultFilter(Object.assign(new FolderFilter(), userPreferencesDto.currentFilter));
      }

      // Sending back result

      selectedUserPreferencesService.update(userPreferencesDto);
      return userPreferencesDto;
    })
  );

