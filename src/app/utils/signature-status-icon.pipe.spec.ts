/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { SignatureStatusIconPipe } from './signature-status-icon.pipe';
import { lsSignatureValid, lsSignatureInvalid, lsSignatureUnknown } from '../shared/models/icons';


describe('SignatureStatusIconPipe', () => {


  it('create an instance', () => {
    const pipe = new SignatureStatusIconPipe();
    expect(pipe).toBeTruthy();
  });


  it('return expected values', () => {
    expect(SignatureStatusIconPipe.compute(true)).toBe(lsSignatureValid);
    expect(SignatureStatusIconPipe.compute(false)).toBe(lsSignatureInvalid);
    expect(SignatureStatusIconPipe.compute(null)).toBe(lsSignatureUnknown);
    expect(SignatureStatusIconPipe.compute(undefined)).toBe(lsSignatureUnknown);
  });


});
