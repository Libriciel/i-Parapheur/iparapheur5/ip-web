/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class SealCertificateMessages {

  static readonly SEAL_TITLE = 'Gestion des certificats de cachet serveur';
  static readonly SEAL_EXP_DATE = 'Exp. date';
  static readonly SEAL_EDIT_CERT_IMAGE = 'Image de certificat';
  static readonly ADD_SEAL_CERTIFICATE_TITLE = `Ajout d'un certificat de cachet serveur`;
  static readonly EDIT_SEAL_CERTIFICATE_TITLE = `Édition du certificat de cachet serveur`;
  static readonly SEAL_IMPORT = 'Importer un nouveau certificat';
  static readonly FILE_SELECTION = 'Sélectionner le fichier';
  static readonly FILE_REPLACE = 'Remplacer le fichier';
  static readonly P12_FILE = `Fichier .p12`;

  static readonly ISSUER = `Autorité`;
  static readonly SUBJECT = `Sujet`;

  static readonly ERROR_CREATING_SEAL_CERTIFICATE = 'Une erreur est survenue durant la création du certificat.';
  static readonly ERROR_RETRIEVING_SEAL_CERTIFICATE = 'Erreur à la récupération du certificat de cachet serveur';
  static readonly ERROR_EDITING_SEAL_CERTIFICATE = `Une erreur est survenue durant l'édition du certificat.`;
  static readonly ERROR_DELETING_SEAL_CERTIFICATE = 'Une erreur est survenue durant la suppression du certificat.';
  static readonly ERROR_RETRIEVING_SEAL_CERTIFICATES = 'Erreur à la récupération des certificats de cachet serveur';
  static readonly SUCCESS_CREATING_SEAL_CERTIFICATE = 'Le certificat de cachet serveur a été créé avec succès';
  static readonly SUCCESS_EDITING_SEAL_CERTIFICATE = 'Le certificat de cachet serveur a été édité avec succès';
  static readonly SUCCESS_DELETING_SEAL_CERTIFICATE = 'Le certificat de cachet serveur a été supprimé avec succès';

  static associateCertificate = (data: number) => `Le certificat est associé à ${data} sous-type${data == 1 ? "" : "s"}.`;

}
