/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { getOrDefault } from '../../../../../utils/string-utils';
import { Folder } from '../../../../../models/folder/folder';
import { CommonIcons } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../../shared/common-messages';
import { Task } from 'src/app/models/task';
import { WorkflowService } from 'src/app/services/ip-core/workflow.service';
import { ActionPopupMessages } from '../action-popup-messages';
import { NotificationsService } from '../../../../../services/notifications.service';
import { Observable, of } from 'rxjs';
import { WorkflowService as LegacyWorkflowService, SignatureFormat, Action, CreateFolderRequest } from '@libriciel/iparapheur-legacy';
import { TypologyService as StandardTypologyService, StandardApiPageImplTypeRepresentation, TypeRepresentation } from '@libriciel/iparapheur-standard';
import { WorkflowService as InternalWorkflowService, TypologyService as InternalTypologyService } from '@libriciel/iparapheur-internal';
import { SubtypeDto, TypeDto, DeskDto, PageSubtypeRepresentation, WorkflowDefinitionDto } from '@libriciel/iparapheur-provisioning';
import { WorkflowUtils } from '../../../../../shared/utils/workflow-utils';
import { DeskService } from '../../../../../services/ip-core/desk.service';
import { CustomMap } from '../../../../../shared/models/custom-types';
import { DataConversionService } from '../../../../../services/data-conversion.service';
import { FolderCreationMessages } from '../../../folder-creation/folder-creation-messages';
import { switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-chain-popup',
  templateUrl: './chain-popup.component.html',
  styleUrls: ['./chain-popup.component.scss']
})
export class ChainPopupComponent {

  public static readonly INJECTABLE_FOLDER_KEY: string = 'folder';
  public static readonly INJECTABLE_DESK_KEY: string = 'desk';
  public static readonly INJECTABLE_TENANT_ID_KEY: string = 'tenantId';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = ActionPopupMessages;
  readonly getOrDefaultFn = getOrDefault;
  readonly actionEnum = Action;

  isTypologyCompatible: boolean = true;
  isProcessing: boolean = false;
  task: Task;
  selectedType: TypeDto;
  selectedSubtype: SubtypeDto;
  workflowDefinition: WorkflowDefinitionDto;
  variableDesksIds: CustomMap = {};
  variableDeskStepCount: number = 0;
  metadata: CustomMap = {};

  retrieveTypesFn = (page: number, pageSize: number) => this.retrieveTypes(page, pageSize);
  retrieveSubtypesFn = (page: number, pageSize: number) => this.retrieveSubtypes(page, pageSize);


  // </editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              private dataConversionService: DataConversionService,
              private internalWorkflowService: InternalWorkflowService,
              private standardTypologyService: StandardTypologyService,
              private legacyWorkflowService: LegacyWorkflowService,
              private internalTypologyService: InternalTypologyService,
              private workflowService: WorkflowService,
              public deskService: DeskService,
              public notificationService: NotificationsService,
              @Inject(ChainPopupComponent.INJECTABLE_FOLDER_KEY) public folder: Folder,
              @Inject(ChainPopupComponent.INJECTABLE_DESK_KEY) public desk: DeskDto,
              @Inject(ChainPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string) {
    this.task = this.folder.stepList.find(t => t.action == Action.Archive || t.action == Action.Chain);
  }


  // </editor-fold desc="LifeCycle">


  onTypeSelectionChanged(newType: TypeRepresentation) {
    this.workflowDefinition = null;
    this.selectedSubtype = null;


    this.internalTypologyService.getType(this.tenantId, newType.id)
      .subscribe({
        next: type => {
          this.selectedType = type;
          const newSignatureFormat = this.selectedType?.signatureFormat;
          const isSameFormat = this.folder.type.signatureFormat === newSignatureFormat;
          const isWildcardFormat = [SignatureFormat.Auto, SignatureFormat.XadesDetached, SignatureFormat.Pkcs7].includes(newSignatureFormat);
          this.isTypologyCompatible = isSameFormat || isWildcardFormat;
        },
        error: e => this.notificationService.showErrorMessage(`Erreur lors de la récupération du type ${newType.name}`, e.message)
      });
  }


  onSubtypeSelectionChanged(partialSubtype: SubtypeDto) {
    this.workflowDefinition = null;
    this.selectedSubtype = null;
    this.variableDesksIds = {};
    this.variableDeskStepCount = 0;
    this.metadata = {};
    this.internalTypologyService.getSubtype(this.tenantId, this.selectedType.id, partialSubtype.id)
      .pipe(
        switchMap(subtype => {
          this.selectedSubtype = subtype as SubtypeDto;
          if (this.selectedSubtype.validationWorkflowId) {
            return this.internalWorkflowService.getWorkflowDefinition(this.tenantId, this.desk.id, this.selectedSubtype.validationWorkflowId);
          }
          return of(null);
        })
      )
      .subscribe({
              next: workflowDefinition => {
                this.workflowDefinition = workflowDefinition;
                this.variableDeskStepCount = WorkflowUtils.countVariableDeskStepDto(workflowDefinition);
              },
              error: e => this.notificationService.showErrorMessage(this.messages.CANNOT_RETRIEVE_SUBTYPE, e.message)
      });
  }


  perform() {

    this.isProcessing = true;
    const request: CreateFolderRequest = {
      typeId: this.selectedType.id,
      subtypeId: this.selectedSubtype.id,
      metadata: this.metadata,
      name: this.folder.name,
      variableDesksIds: this.variableDesksIds
    } as CreateFolderRequest;

    this.selectedSubtype.subtypeMetadataList
      .forEach(subtypeMetadata => request.metadata[subtypeMetadata.metadata.id] = this.folder.metadata[subtypeMetadata.metadata.id]);

    return this.legacyWorkflowService
      .chain(this.tenantId, this.desk.id, this.folder.id, this.task.id, request)
      .subscribe({
        next: () => {
          this.notificationService.showSuccessMessage(ActionPopupMessages.actionFolderSuccessMessage(Action.Chain, this.folder));
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error: e => this.notificationService.showErrorMessage(ActionPopupMessages.actionFolderErrorMessage(Action.Chain, this.folder), e.message)

      })
      .add(() => this.isProcessing = false);
  }


  isFormValid(): boolean {
    return !!this.selectedSubtype
      && this.isTypologyCompatible
      && Object.values(this.variableDesksIds).filter(elem => !!elem).length === this.variableDeskStepCount;

  }


  retrieveTypes(page: number, pageSize: number): Observable<StandardApiPageImplTypeRepresentation> {
    console.debug(`retrieve creation allowed types : page: ${page}, pageSize: ${pageSize}`);
    return this.standardTypologyService.listCreationAllowedTypes(this.tenantId, this.desk.id, page, pageSize);
  }


  retrieveSubtypes(page: number, pageSize: number): Observable<PageSubtypeRepresentation> {
    console.debug(`retrieve creation allowed subtypes : page: ${page}, pageSize: ${pageSize}`);
    return this.standardTypologyService.listCreationAllowedSubtypes(this.tenantId, this.desk.id, this.selectedType?.id, page, pageSize);
  }


  metadataWasUpdated() {
    if (WorkflowUtils.hasSelectionScript(this.selectedSubtype)) {
      this.workflowDefinition = null;

      this.workflowService.getValidationWorkflowFromSelectionScript(
        this.metadata,
        this.selectedType,
        this.selectedSubtype,
        this.internalWorkflowService,
        this.tenantId,
        this.desk
      ).subscribe({
        next: result => {
          this.workflowDefinition = result;
          this.variableDeskStepCount = WorkflowUtils.countVariableDeskStepDto(this.workflowDefinition);
        },
        error: e => this.notificationService.showErrorMessage(FolderCreationMessages.GET_SUBTYPE_INFO_ERROR, e.message)
      });
    }
  }
}
