/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Folder } from '../models/folder/folder';
import { Task } from '../models/task';
import { WorkflowStep } from '@libriciel/ls-workflow';
import { SignatureProtocol, Action, State, DeskCount, TypeDto, SubtypeDto, Task as CoreTask, FolderDto } from '@libriciel/iparapheur-legacy';
import { SecondaryAction } from '../shared/models/secondary-action.enum';
import { Document } from '../models/document';
import { IpStepInstance } from '../models/workflows/ip-step-instance';
import { CONFIG } from '../shared/config/config';
import { SignatureProof } from '@libriciel/iparapheur-internal';
import { NotificationsService } from '../services/notifications.service';
import { FolderViewMessages } from '../components/main/folder-view/folder-view-messages';


export class FolderUtils {


  static AS_DESK_QUERY_PARAM_NAME = 'asDeskId';

  static SIMPLE_ACTIONS: (Action | SecondaryAction)[] = [Action.Visa, Action.Signature, Action.Seal];
  static MAIL_ACTIONS: (Action | SecondaryAction)[] = [Action.SecureMail, SecondaryAction.Mail];
  static EXTERNAL_ACTIONS: (Action | SecondaryAction)[] = [Action.ExternalSignature, Action.SecureMail, Action.Ipng, Action.IpngReturn];
  static TRANSFERABLE_ACTIONS: (Action | SecondaryAction)[] = [Action.SecondOpinion, Action.Visa, Action.Seal, Action.Signature];
  static CRYPTOGRAPHIC_ACTIONS: (Action | SecondaryAction)[] = [Action.Signature, Action.ExternalSignature, Action.Seal];
  static STACKABLE_ACTIONS: (Action | SecondaryAction)[] = [Action.Signature, Action.Seal, Action.Visa, Action.SecondOpinion];


  static getMaxDocumentsCount(type?: TypeDto, subtype?: SubtypeDto): number {

    if (type && (type.protocol !== SignatureProtocol.None)) {
      return 1;
    }

    if (subtype) {
      return subtype.maxMainDocuments;
    }

    return 1;
  }


  static computeDelegatedTotal(deskCount: DeskCount): number {
    return deskCount?.delegatingDesks.reduce((sum, current) => sum + current.pendingFoldersCount, 0);
  }


  static isInValidationWorkflow(folder: Folder): boolean {
    return folder.stepList
      .filter(step => step.workflowIndex > 1)
      .some(step => !!step.beginDate);
  }


  static doesContainEmbeddedSignature(document: Document): boolean {
    return (document?.embeddedSignatureInfos?.length ?? 0) > 0;
  }


  static doesContainDetachedSignature(document: Document): boolean {
    return (document?.detachedSignatures?.length ?? 0) > 0;
  }


  static doesContainSignature(document: Document): boolean {
    return FolderUtils.doesContainEmbeddedSignature(document) || FolderUtils.doesContainDetachedSignature(document);
  }


  static isActive(step: WorkflowStep | CoreTask | Task): boolean {
    return (step.state === State.Current) || (step.state == State.Pending);
  }


  static isCurrentOrUpcoming(step: Task): boolean {
    return step.state === State.Current || step.state === State.Upcoming || step.state === State.Pending;
  }


  static isSealOrSign(step: Task): boolean {
    return FolderUtils.CRYPTOGRAPHIC_ACTIONS.includes(step.action as Action);
  }


  static isFinal(task: CoreTask | Task): boolean {
    return (task.action === Action.Archive) && (task.state == State.Finished);
  }


  static isDraft(task: CoreTask | Task): boolean {
    return (task.action === Action.Start) && (task.state == State.Draft);
  }


  static isRetrievable(task: CoreTask | Task): boolean {
    return (task.action === Action.Undo) && (task.state == State.Retrievable);
  }


  static isValidCurrentTask(task: CoreTask | Task): boolean {
    return this.isActive(task) || this.isFinal(task) || this.isDraft(task) || this.isRetrievable(task);
  }


  static areActive(steps: (Task | CoreTask | WorkflowStep)[]): boolean {
    return steps.some(step => FolderUtils.isActive(step));
  }


  static isPassed(step: WorkflowStep | CoreTask | IpStepInstance | Task): boolean {
    return (step.state !== State.Current) && (step.state !== State.Pending) && (step.state !== State.Upcoming);
  }


  static arePassed(steps: (WorkflowStep | CoreTask | IpStepInstance | Task)[]): boolean {
    return steps.every(step => FolderUtils.isPassed(step));
  }


  static getHigherStackableAction(selectedActions: (Action | SecondaryAction)[]): Action {
    return this.STACKABLE_ACTIONS
      .filter((value, index) => selectedActions?.includes(this.STACKABLE_ACTIONS[index]))[0] as Action ?? null;
  }


  static getModalSize(action: Action | SecondaryAction, folders: Folder[]): string {
    switch (action) {

      case SecondaryAction.StackedValidation:
        const actions = folders.map(folder => folder.stepList[0].action);
        return FolderUtils.getHigherStackableAction(actions) === Action.Signature ? 'xl' : 'md';

      case SecondaryAction.Mail:
      case SecondaryAction.HistoryTasks:
      case Action.Signature:
      case Action.Chain:
      case SecondaryAction.IpngShowProof:
        return 'xl';

      case Action.ExternalSignature:
        if (CONFIG?.VERSION?.startsWith('5.2.')) {
          return 'xl';
        } else {
          return 'md';
        }

      case SecondaryAction.AddDesksToNotify:
        return 'lg';

      default :
        return 'md';
    }
  }


  static timestampToDate(timestamp?: number): Date {
    return (!timestamp)
      ? null
      : new Date(timestamp);
  }


  static getSignatureProofError(folder: Folder): string {
    return folder?.signatureProofList?.find(doc => doc.internal)?.errorMessage || null;
  }


  static showSignatureProofSuccessOrErrorMessage(signatureProof: SignatureProof,
                                                 notificationsService: NotificationsService,
                                                 messages = FolderViewMessages): void {

    if (signatureProof.errorMessage) {
      notificationsService.showErrorMessage(signatureProof.errorMessage);
    } else {
      notificationsService.showSuccessMessage(messages.GENERATE_SIGNATURE_VALIDATION_SUCCESS);
    }
  }


}
