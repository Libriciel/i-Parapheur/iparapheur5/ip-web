/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonIcons } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../../shared/common-messages';
import { LoggableUser } from '../../../../../models/auth/loggable-user';
import { CrudOperation } from '../../../../../services/crud-operation';
import { UserPopupMessages } from '../../user-popup/user-popup-messages';
import { AdminAllUsersService } from '@libriciel/iparapheur-provisioning';
import { UserDto } from '@libriciel/iparapheur-provisioning/model/userDto';
import { catchError } from 'rxjs/operators';
import { AuthUtils } from '../../../../../utils/auth-utils';
import { ServerInfoDto } from '@libriciel/iparapheur-internal';


@Component({
  selector: 'app-all-users-create-popup',
  templateUrl: './all-users-create-popup.component.html',
  styleUrls: ['./all-users-create-popup.component.scss']
})
export class AllUsersCreatePopupComponent {

  readonly messages = UserPopupMessages;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;

  public static readonly INJECTABLE_SERVER_INFO_KEY: string = 'serverInfo';

  @Input() tenantId: string;

  modifiedUser: UserDto = AuthUtils.createNewUserDto();
  password: string;
  isGeneralFormValid: boolean = false;
  isProcessing = false;


  constructor(private notificationsService: NotificationsService,
              private adminAllUserService: AdminAllUsersService,
              public activeModal: NgbActiveModal,
              @Inject(AllUsersCreatePopupComponent.INJECTABLE_SERVER_INFO_KEY) public serverInfo: ServerInfoDto) {
  }


  onSaveButtonClicked() {

    if (!this.password || !this.isGeneralFormValid) {
      return;
    }

    this.modifiedUser.password = this.password;
    this.modifiedUser.administeredDeskIds = this.modifiedUser.administeredDesks.map(desk => desk.id);

    this.isProcessing = true;
    this.adminAllUserService
      .createSuperAdminAsSuperAdmin(this.modifiedUser)
      .pipe(catchError(this.notificationsService.handleHttpError('createUser')))
      .subscribe({
        next: () => {
          this.notificationsService.showCrudMessage(CrudOperation.Create, new LoggableUser(this.modifiedUser));
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error: e => {
          this.notificationsService.showCrudMessage(CrudOperation.Create, new LoggableUser(this.modifiedUser), e.message, false);
        }
      })
      .add(() => this.isProcessing = false);
  }
}
