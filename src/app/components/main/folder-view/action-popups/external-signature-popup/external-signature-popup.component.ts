/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, OnInit } from '@angular/core';
import { CommonIcons, Style } from '@libriciel/ls-elements';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonMessages } from '../../../../../shared/common-messages';
import { ExternalSignatureMessages } from './external-signature-messages';
import { ExternalState } from '../../../../../models/external-state.enum';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Folder } from '../../../../../models/folder/folder';
import { NotificationsService } from '../../../../../services/notifications.service';
import { Observable, combineLatest } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Action, ExternalSignatureProcedure, WorkflowService, ExternalSignatureParams, ExternalSignatureMember, UserRepresentation } from '@libriciel/iparapheur-legacy';
import { WorkflowService as LegacyWorkflowService } from '../../../../../services/ip-core/workflow.service';
import { ActionPopupMessages } from '../action-popup-messages';
import { ExternalSignatureMemberForm, CustomMap, IndexedExternalSignatureMemberAttribute } from '../../../../../shared/models/custom-types';
import { faInfoCircle, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { emailRfc5322Regex, prettyPrintUser } from '../../../../../utils/string-utils';

@Component({
  selector: 'app-external-signature-popup',
  templateUrl: './external-signature-popup.component.html',
  styleUrls: ['./external-signature-popup.component.scss']
})
export class ExternalSignaturePopupComponent implements OnInit {


  static readonly FIRSTNAME_KEY: string = 'firstname';
  static readonly LASTNAME_KEY: string = 'lastname';
  static readonly MAIL_KEY: string = 'mail';
  static readonly PHONE_KEY: string = 'phone';

  static readonly INJECTABLE_FOLDERS_KEY: string = 'folders';
  static readonly INJECTABLE_DESK_ID_KEY: string = 'deskId';
  static readonly INJECTABLE_PERFORMED_ACTION_KEY: string = 'performedAction';
  static readonly INJECTABLE_TENANT_ID_KEY: string = 'tenant_id';
  static readonly INJECTABLE_EXTERNAL_SIGNATURE_PROCEDURE_KEY: string = 'externalSignatureProcedure';
  static readonly EXTERNAL_SIGNER_GROUPING_REGEX: RegExp = new RegExp("(^i_Parapheur_reserved_ext_sig_)(?<key>.*?)(?=_)(.*_)(?<index>\\d)");

  readonly messages = ExternalSignatureMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly actionEnum = Action;
  readonly styleEnum = Style;
  readonly infoIcon: IconDefinition = faInfoCircle;

  isProcessing: boolean = false;
  validationMetadataFormValid: boolean = false;

  currentMemberIndex: number = 0;
  signerList: ExternalSignatureMember[] = [];

  externalSignatureForm: FormGroup<ExternalSignatureMemberForm> = new FormGroup<ExternalSignatureMemberForm>({
    firstname: new FormControl<string>("", [Validators.required, Validators.minLength(2)]),
    lastname: new FormControl<string>("", [Validators.required, Validators.minLength(2)]),
    mail: new FormControl<string>("", [Validators.required, Validators.email]),
    phone: new FormControl<string>("", [Validators.required])
  });

  areAllSignersValid: boolean = false;
  areAllFoldersInFormState: boolean = false;
  areAllFoldersInSignedState: boolean = false;

  // <editor-fold desc="LifeCycle">


  constructor(@Inject(ExternalSignaturePopupComponent.INJECTABLE_FOLDERS_KEY) public folders: Folder[],
              @Inject(ExternalSignaturePopupComponent.INJECTABLE_DESK_ID_KEY) public deskId: string,
              @Inject(ExternalSignaturePopupComponent.INJECTABLE_PERFORMED_ACTION_KEY) public performedAction: Action,
              @Inject(ExternalSignaturePopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(ExternalSignaturePopupComponent.INJECTABLE_EXTERNAL_SIGNATURE_PROCEDURE_KEY) public procedure: ExternalSignatureProcedure,
              private workflowService: WorkflowService,
              private legacyWorkflowService: LegacyWorkflowService,
              private notificationsService: NotificationsService,
              public activeModal: NgbActiveModal) {}


  ngOnInit(): void {

    this.updateFolderStateStatus();

    this.signerList = this.procedure?.members ?? [];


    if (!this.areAllFoldersInFormState) {
      this.externalSignatureForm.disable();
      this.currentMemberIndex = 0;
    } else {
      this.createMetadataSigners(this.folders[0]);
    }

    if (this.signerList.length === 0) {
      this.addSigner();
    }


    this.patchFormValues(this.currentMemberIndex);

    this.externalSignatureForm.valueChanges.subscribe(value => {
      const signer: ExternalSignatureMember = this.signerList[this.currentMemberIndex];
      signer.firstName = value.firstname;
      signer.lastName = value.lastname;
      signer.email = value.mail;
      signer.phone = value.phone;

      this.updateSignersValidityStatus();
    });
  }

  updateFolderStateStatus(): void {
    this.areAllFoldersInFormState = this.folders
      .map(folder => folder.stepList[0].externalState)
      .every(externalState => externalState === ExternalState.Form);

    this.areAllFoldersInSignedState = this.folders
      .map(folder => folder.stepList[0].externalState)
      .every(externalState => externalState === ExternalState.Signed);
  }


  // </editor-fold desc="LifeCycle">

  createMetadataSigners(folder: Folder) {
    if (!folder) {
      return;
    }

    const sortedSignersData: IndexedExternalSignatureMemberAttribute[] = Object.entries(folder.metadata)
      .map(entry => {
        const key: string = entry[0];
        const value: string = entry[1];
        const groups: CustomMap = ExternalSignaturePopupComponent.EXTERNAL_SIGNER_GROUPING_REGEX.exec(key)?.groups;
        if (!groups) {
          return null;
        }
        return {
          index: parseInt(groups.index),
          key: groups.key,
          value: value
        };
      })
      .filter(data => !!data)
      .sort(({index: a}, {index: b}) => a - b);

    const signersCount: number = Math.max(...sortedSignersData.map(data => data.index));

    for (let i = 1 ; i <= signersCount ; i++) {
      const currentSignerData: IndexedExternalSignatureMemberAttribute[] = sortedSignersData.filter(data => data.index === i);
      this.signerList.push({
        firstName: currentSignerData.find(data => data.key === ExternalSignaturePopupComponent.FIRSTNAME_KEY)?.value || '',
        lastName: currentSignerData.find(data => data.key === ExternalSignaturePopupComponent.LASTNAME_KEY)?.value || '',
        email: currentSignerData.find(data => data.key === ExternalSignaturePopupComponent.MAIL_KEY)?.value || '',
        phone: currentSignerData.find(data => data.key === ExternalSignaturePopupComponent.PHONE_KEY)?.value || ''
      });
    }

  }


  patchFormValues(index: number): void {
    this.externalSignatureForm.patchValue({
      firstname: this.signerList[index]?.firstName,
      lastname: this.signerList[index]?.lastName,
      mail: this.signerList[index]?.email,
      phone: this.signerList[index]?.phone
    });
  }

  addSigner() {
    this.signerList.push({
      firstName: '',
      lastName: '',
      email: '',
      phone: ''
    });

    this.currentMemberIndex = this.signerList.length - 1;
    this.patchFormValues(this.currentMemberIndex);
  }

  editSigner(index: number) {
    this.currentMemberIndex = index;
    this.patchFormValues(this.currentMemberIndex);
  }

  removeSigner(index: number) {
    this.currentMemberIndex = -1;
    this.signerList.splice(index, 1);
  }


  submit(): void {
    if (this.areAllFoldersInSignedState) {
      this.forceExternalSignature();
    } else {
      this.requestExternalSignature();
    }
  }


  forceExternalSignature(): void {
    this.isProcessing = true;

    const observableList: Observable<void>[] = this.folders.map(f => this.workflowService.finalizeExternalSignature(
        this.tenantId,
        this.deskId,
        f.id,
        f.stepList[0].id,
        f.subtype.externalSignatureConfig.id
      ).pipe(catchError(error => new Observable<void>(observer => observer.next(error))))
    );

    combineLatest(observableList)
      .subscribe(data => {
        this.isProcessing = false;
        this.showNotifications(data);
        this.finalizeAction(data);
      });
  }


  requestExternalSignature(): void {
    this.isProcessing = true;
    const params: ExternalSignatureParams = {
      name: 'name',
      members: this.signerList.map(m => {
        return {
          firstname: m.firstName,
          lastname: m.lastName,
          email: m.email,
          phone: m.phone
        };
      }),
      metadata: {}
    };

    const observableList: Observable<void>[] = this.folders.map(f => {
      params.metadata = f.populatedMandatoryStepMetadata;
      return this.legacyWorkflowService.requestExternalProcedure(params, this.tenantId, this.deskId, f.id, f.stepList[0])
        .pipe(catchError(error => new Observable<void>(observer => observer.next(error))));
    });

    combineLatest(observableList)
      .subscribe(data => {
        this.isProcessing = false;
        this.showNotifications(data);
        this.finalizeAction(data);
      });
  }


  showNotifications(results: any[]): void {
    const errors: Error[] = results.filter(res => !!res);

    if (errors.length === 0) {
      this.notificationsService.showSuccessMessage(
        results.length === 1
          ? ActionPopupMessages.actionSuccessMessage(Action.ExternalSignature)
          : ActionPopupMessages.ALL_TASKS_SUCCESSFUL
      );
    } else {
      this.notificationsService.showErrorMessage(ActionPopupMessages.parseMultipleActionsErrorMessage(Action.ExternalSignature, errors));
    }
  }


  updateSignersValidityStatus(): void {
    if (this.signerList.length === 0) {
      this.areAllSignersValid = false;
      return;
    }

    const areMembersFieldsValid: boolean = this.signerList.every(signer => this.checkSignerValidity(signer));

    this.areAllSignersValid = areMembersFieldsValid;
  }


  checkSignerValidity(signer: ExternalSignatureMember): boolean {
    return emailRfc5322Regex.exec(signer?.email)
      && signer?.firstName?.length >= 1
      && signer?.lastName?.length >= 1
      && signer?.phone?.length >= 1;
  }


  finalizeAction(results: any[]): void {
    const errors: Error[] = results.filter(res => !!res);

    if (errors.length > 0) {
      return;
    }

    console.log('Finalize External Signature');
    this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
  }

  getSignerText(signer: ExternalSignatureMember): string {
    if (signer?.firstName?.length === 0 && signer?.lastName?.length === 0) {
      return '(Aucun nom)';
    } else {
      return prettyPrintUser(signer as UserRepresentation);
    }
  }

}
