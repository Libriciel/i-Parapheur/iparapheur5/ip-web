/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, OnInit, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { PdfJsParameters } from '../../../models/annotation/pdf-js-parameters';
import { Config } from '../../../config';
import { PdfJsViewerComponent } from 'ng2-pdfjs-viewer';

@Component({
  selector: 'app-pdf-example',
  templateUrl: './pdf-example.component.html',
  styleUrls: ['./pdf-example.component.scss']
})
export class PdfExampleComponent implements OnInit, OnChanges {

  @ViewChild('pdfJsViewer') pdfViewer: PdfJsViewerComponent;

  @Input() pdfSource: string | Blob | Uint8Array;
  @Input() viewerId: string;
  @Input() lastModificationDate: string;
  @Input() zoom: string = '100';
  @Input() height: string = '70vh';
  @Input() width: string = '100%';
  @Input() hideToolbar: boolean = true;

  pdfJsParameters: PdfJsParameters = {
    annotationButtonsParameters: {
      hideToolbar: true,
      hideAnnotation: true,
      hideSignaturePlacement: true,
      hideDraw: true,
      hideTextAnnotation: true
    },
    signaturePlacementAnnotations: [],
    annotationOptions: {
      width: Config.STAMP_WIDTH,
      height: Config.STAMP_HEIGHT,
      rectangleOrigin: 'BOTTOM_LEFT'
    }
  };


  // <editor-fold desc="LifeCycle">


  ngOnInit(): void {
    this.pdfJsParametersToLocalStorage();
  }

  ngOnChanges(_: SimpleChanges): void {
    setTimeout(
      () => this.pdfViewer?.refresh()
    );
  }


  // </editor-fold desc="LifeCycle">


  pdfJsParametersToLocalStorage(): void {
    localStorage.setItem("signaturePlacementAnnotations", JSON.stringify(this.pdfJsParameters.signaturePlacementAnnotations));
    localStorage.setItem("annotationButtonsParameters", JSON.stringify(this.pdfJsParameters.annotationButtonsParameters));
  }

}
