/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { DeskDto } from '@libriciel/iparapheur-provisioning';
import { UserRepresentation } from '@libriciel/iparapheur-provisioning/model/userRepresentation';
import { DeskRepresentation } from '@libriciel/iparapheur-provisioning/model/deskRepresentation';
import { MetadataRepresentation } from '@libriciel/iparapheur-provisioning/model/metadataRepresentation';


export class MutableDesk implements DeskDto {
  name: string;
  ownerIds: Array<string>;
  shortName: string;
  owners: Array<UserRepresentation>;
  supervisors: Array<UserRepresentation>;
  delegationManagers: Array<UserRepresentation>;
  associatedDesks: Array<DeskRepresentation>;
  filterableMetadata: Array<MetadataRepresentation>;
  associatedDeskIds: string[];
  filterableMetadataIds: string[];
  supervisorIds: string[];
  delegationManagerIds: string[];
}
