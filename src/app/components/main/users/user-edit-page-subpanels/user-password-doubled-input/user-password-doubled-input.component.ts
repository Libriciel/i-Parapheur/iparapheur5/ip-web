/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, AfterViewInit, ViewChild, ElementRef, Output, EventEmitter, Input, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { faEye, faEyeSlash, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { UserEditSubPanelsMessages } from '../user-edit-sub-panels-messages';
import { PasswordPolicies } from '@libriciel/iparapheur-internal/model/passwordPolicies';
import { CommonIcons } from '@libriciel/ls-elements';
import { UserDto } from '@libriciel/iparapheur-provisioning';
import { CommonMessages } from '../../../../../shared/common-messages';
import { ServerInfoDto } from '@libriciel/iparapheur-internal';


/**
 * TODO: this component sends a request on every opening, to get the policies. That's not the best idea ever.
 *  This should be deleted anyway, since no password change should be allowed in the app.
 */
@Component({
  selector: 'app-user-password-doubled-input',
  templateUrl: './user-password-doubled-input.component.html',
  styleUrls: ['./user-password-doubled-input.component.scss']
})
export class UserPasswordDoubledInputComponent implements OnInit, AfterViewInit {

  readonly eyeIcon: IconDefinition = faEye;
  readonly eyeSlashIcon: IconDefinition = faEyeSlash;
  readonly validatedIcon: IconDefinition = CommonIcons.VALIDATED_ICON;
  readonly rejectedIcon: IconDefinition = CommonIcons.REJECTED_ICON;
  readonly messages = UserEditSubPanelsMessages;
  readonly commonMessages = CommonMessages;

  readonly countSpecialCharactersFn = (value: string): number => value.replace(/[0-9a-zA-Z]/g, "").length;
  readonly countUppercaseFn = (value: string): number => value.replace(/[^A-Z]/g, "").length;
  readonly countLowercaseFn = (value: string): number => value.replace(/[^a-z]/g, "").length;
  readonly countDigitsFn = (value: string): number => value.replace(/[^0-9]/g, "").length;


  @Input() user: UserDto;
  @Input() serverInfo: ServerInfoDto;
  @Output() passwordChanged: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('passwordInput') passwordInput: ElementRef<HTMLInputElement>;
  @ViewChild('passwordConfirmationInput') passwordConfirmationInput: ElementRef<HTMLInputElement>;

  hidePassword: boolean = true;
  passwordPolicies: PasswordPolicies;
  currentCompiledRegex: RegExp = null;


  // <editor-fold desc="LifeCycle">


  ngOnInit(): void {
    this.passwordPolicies = this.serverInfo.passwordPolicies;
    this.currentCompiledRegex = !!this.passwordPolicies.regexPattern
      ? new RegExp(this.passwordPolicies.regexPattern)
      : null;
  }


  ngAfterViewInit(): void {
    this.registerKeyupOnInput(this.passwordInput.nativeElement);
    this.registerKeyupOnInput(this.passwordConfirmationInput.nativeElement);
  }


  // </editor-fold desc="LifeCycle">


  registerKeyupOnInput(inputElement: HTMLInputElement) {
    fromEvent(inputElement, 'keyup')
      .pipe(
        map((event: any) => event.target.value),
        debounceTime(150),
        distinctUntilChanged()
      )
      .subscribe(() => this.passwordInputChanged());
  }


  private passwordInputChanged() {
    const valuesMatch: boolean = this.passwordInput.nativeElement.value === this.passwordConfirmationInput.nativeElement.value;
    const valueIsEmpty: boolean = !this.passwordInput.nativeElement.value;

    this.passwordChanged.emit(!valuesMatch || valueIsEmpty ? null : this.passwordInput.nativeElement.value);
  }


}
