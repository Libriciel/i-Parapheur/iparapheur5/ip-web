/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faCaretUp, faCaretDown, faEye, faEyeSlash, faCog } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DeskService as LegacyDeskService } from '../../../../services/ip-core/desk.service';
import { ExternalState } from '../../../../models/external-state.enum';
import { CommonIcons } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../shared/common-messages';
import { Folder } from '../../../../models/folder/folder';
import { MultipleActionListComponent } from '../multiple-action-list/multiple-action-list.component';
import { LegacyUserService } from '../../../../services/ip-core/legacy-user.service';
import { FolderFilter } from '../../../../models/folder/folder-filter';
import { NamePipe } from '../../../../shared/utils/name.pipe';
import { TasksMessages } from '../tasks-messages';
import { FilterPanelIcons } from '../../../../shared/components/folder-filter-panel/filter-panel-icons';
import { of } from 'rxjs';
import { NotificationsService } from '../../../../services/notifications.service';
import { Action, State, FolderSortBy, DeskDto, TaskViewColumn, UserPreferencesDto, FolderFilterDto, TenantDto } from '@libriciel/iparapheur-legacy';
import { FolderService, TableLayoutDto, TableName, LabelledColumnType, LabelledColumn, ColumnedTaskListRequest } from '@libriciel/iparapheur-internal';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';
import { ColumToFolderSortByPipe } from '../../../../utils/colum-to-folder-sort-by.pipe';
import { FolderUtils } from '../../../../utils/folder-utils';
import { SelectedDeskService } from '../../../../services/resolvers/desk/selected-desk.service';
import { tap, mergeMap } from 'rxjs/operators';
import { Task } from '../../../../models/task';
import { UserDto } from '@libriciel/iparapheur-provisioning';
import { Title } from '@angular/platform-browser';
import { SelectedUserPreferencesService } from '../../../../services/selected-user-preferences.service';
import { TableLayoutPopupComponent } from '../table-layout-popup/table-layout-popup.component';
import { prettyPrintUser, isCurrentVersion52OrAbove } from '../../../../utils/string-utils';


@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  readonly show52Features: boolean = isCurrentVersion52OrAbove();
  readonly pageSizes: number[] = [10, 15, 20, 50, 100];
  readonly commonMessages = CommonMessages;
  readonly messages = TasksMessages;
  readonly commonIcons = CommonIcons;
  readonly icons = FilterPanelIcons;
  readonly taskViewColumnEnum = TaskViewColumn;
  readonly caretUp = faCaretUp;
  readonly caretDown = faCaretDown;
  readonly eyeIcon = faEye;
  readonly eyeSlashIcon = faEyeSlash;
  readonly cogIcon = faCog;
  readonly actionEnum = Action;
  readonly sortByEnum = FolderSortBy;
  readonly stateEnum = State;
  readonly columnTypeEnum = LabelledColumnType;
  readonly prettyPrintUserFn = prettyPrintUser;

  deskId: string;
  tenant: TenantDto;
  userPreferences: UserPreferencesDto;
  currentUser: UserDto;
  desk: DeskDto;
  folders: Folder[];
  loading: boolean = false;
  allFoldersSelected: boolean = false;
  showFilterPanel: boolean = false;
  actionsToDisplay: (Action | SecondaryAction)[] = [];
  selectedFolders: Folder[] = [];
  sortBy: TaskViewColumn | string = FolderSortBy.CreationDate;
  asc: boolean = true;
  page: number = 1;
  pageSizeIndex: number = 1;
  total: number = 0;
  selectedFilterId: string;
  userFolderFilters: FolderFilterDto[];
  colspan: number;
  folderState: State;
  filterFromNavigation: FolderFilterDto;
  filterIdFromNavigation: string;

  private _appliedFilter: FolderFilterDto;
  // Original filter is used to manage url changing depending on state folder in order to keep trace of the original search and be able to come back
  originalFilter: FolderFilterDto;
  currentTableLayout: TableLayoutDto;
  usedColumns: LabelledColumn[];


  doesFolderHaveToBeRead(folder: Folder): boolean {
    return folder.subtype?.readingMandatory && !folder.readByCurrentUser && folder.stepList[0].action === Action.Signature;
  }


  selectedFilterChange(filterId: string) {
    this.filterChanged({folderFilter: this.getFolderFilter(filterId), reloadFilters: false, changeOriginalFilter: true});
  }


  get appliedFilter(): FolderFilterDto {
    return this._appliedFilter;
  }


  set appliedFilter(value: FolderFilterDto) {
    value.state = value.state || this.folderState;
    this._appliedFilter = value;
  }


  // <editor-fold desc="LifeCycle">


  constructor(private folderService: FolderService,
              private legacyUserService: LegacyUserService,
              private modalService: NgbModal,
              private notificationsService: NotificationsService,
              private route: ActivatedRoute,
              private router: Router,
              private selectedDeskService: SelectedDeskService,
              private selectedUserPreferencesService: SelectedUserPreferencesService,
              private titleService: Title,
              public legacyDeskService: LegacyDeskService) {

    this.selectedUserPreferencesService.currentUserPreferences$.subscribe(userPreferences => {
      this.userPreferences = userPreferences;

      this.initTaskListAttributes();

      this.folderState = this.route.snapshot.url.pop()?.toString().toUpperCase() as State;

      this.filterFromNavigation = this.router.getCurrentNavigation()?.extras?.state?.filter;
      this.filterIdFromNavigation = this.router.getCurrentNavigation()?.extras?.state?.filterId;

      this.initTaskListVariables();
    });
  }


  initTaskListAttributes() {
    this.deskId = this.route.snapshot.paramMap.get('deskId');
    this.tenant = this.route.snapshot.data['tenant'];
    this.showFilterPanel = this.router.getCurrentNavigation()?.extras?.state?.panelOpened;
    this.currentUser = this.route.snapshot.data["currentUser"];
  }


  isIdColumn(column: LabelledColumn): boolean {
    return column.id == TaskViewColumn.TaskId.toString() || column == TaskViewColumn.FolderId.toString();
  }


  shouldShowIdColumn(): boolean {
    return this.userPreferences.showAdminIds || this.show52Features;
  }


  initTaskListVariables(tableLayoutDto?: TableLayoutDto) {
    const taskListTableLayouts: TableLayoutDto[] = this.userPreferences
      .tableLayoutList
      .filter((list: TableLayoutDto): boolean => list.tableName === TableName.TaskList);

    const currentDeskTableLayout: TableLayoutDto = taskListTableLayouts
      .find(tableLayout => tableLayout.deskId === this.deskId);

    const defaultTableLayout: TableLayoutDto = taskListTableLayouts
      .find(tableLayout => !tableLayout.deskId);

    this.currentTableLayout = tableLayoutDto ?? currentDeskTableLayout ?? defaultTableLayout;
    this.usedColumns = this.currentTableLayout.labelledColumnList;

    this.colspan = 1 + this.usedColumns.length;

    this.asc = this.currentTableLayout.defaultAsc;
    this.sortBy = this.currentTableLayout.defaultSortBy;

    const currentNavigationIndex = this.router.getCurrentNavigation()?.extras?.state?.pageSizeIndex;
    const userPreferencesDefaultIndex = this.pageSizes.findIndex(item => item === this.userPreferences.taskViewDefaultPageSize) + 1;
    this.pageSizeIndex = currentNavigationIndex || userPreferencesDefaultIndex || 1;

    this.originalFilter = this.router.getCurrentNavigation()?.extras?.state?.originalFilter;
  }


  ngOnInit(): void {
    this.selectedDeskService.selectedDesk
      .pipe(
        tap(selectedDesk => this.desk = selectedDesk),
        mergeMap(() => this.legacyUserService.getCurrentUserFolderFilters())
      )
      .subscribe({
        next: filters => {
          this.titleService.setTitle(this.desk?.shortName);
          this.userFolderFilters = filters.map(f => f as FolderFilter);
          this.appliedFilter = this.filterFromNavigation ?? this.getFolderFilter(this.filterIdFromNavigation);
          this.selectedFilterId = this.appliedFilter.id;
          this.originalFilter = this.originalFilter ?? FolderFilter.getFolderFilterForState(this.folderState);
          this.requestFolders();
        },
        error: e => this.notificationsService.showErrorMessage(TasksMessages.ERROR_RETRIEVING_FILTERS, e.message)
      });
  }


  // </editor-fold desc="LifeCycle">


  public getCurrentDeskNameIfAppropriate(folder: Folder): string {
    const task: Task = folder.stepList[0];

    if (!task) {
      return;
    }

    // Nothing set yet for a delegation coming from multiple desks.
    // This is a very special case, probably impossible to reach.
    // We won't display anything in that case.
    const isOriginDeskSingle = (task.desks.length === 1);

    return (isOriginDeskSingle) ? NamePipe.compute(task.desks[0].name) : '';
  }


  public getTargetDeskId(folder: Folder): string {
    const possibleTargetDeskIds: string[] = [];
    folder.stepList.forEach(task => {
      possibleTargetDeskIds.push(...task.desks.map(d => d.id));
    });

    if (possibleTargetDeskIds.includes(this.deskId) || possibleTargetDeskIds.length === 0) {
      return this.deskId;
    }

    return possibleTargetDeskIds.pop();
  }


  public isDateBeforeNow(date: Date): boolean {
    return (date != null) && (new Date(date).getTime() < Date.now());
  }


  public isCreateFolderAvailable() {

    const isTenantSet: boolean = !!this.desk?.tenantId;
    const isAllowed: boolean = this.desk?.folderCreationAllowed;
    const isCurrentUserOwner: boolean = this.legacyUserService.isCurrentUserDeskOwner(this.tenant.id, this.deskId);

    return isTenantSet && isAllowed && isCurrentUserOwner;
  }


  onMultipleActionSuccess(action: Action | SecondaryAction) {
    console.debug(`Success on action ${action}`);

    this.requestFolders();
  }


  // <editor-fold desc="UI callbacks">


  public filterChanged(event: { folderFilter: FolderFilterDto; reloadFilters: boolean; changeOriginalFilter: boolean }): void {
    const folderFilter: FolderFilterDto = event.folderFilter || FolderFilter.getFolderFilterForState(this.folderState);
    const reloadFilters: boolean = event.reloadFilters;
    const changeOriginalFilter: boolean = event.changeOriginalFilter;

    if (changeOriginalFilter) {
      this.originalFilter = Object.assign(new FolderFilter(), folderFilter);
    }

    if (folderFilter.state !== this.folderState) {
      this.router
        .navigate(
          ['tenant', this.tenant.id, 'desk', this.deskId, folderFilter.state.toString().toLowerCase()],
          {state: {filter: folderFilter, originalFilter: this.originalFilter, panelOpened: true, pageSizeIndex: this.pageSizeIndex}}
        )
        .then();
    } else {
      const call = reloadFilters
        ? this.legacyUserService.getCurrentUserFolderFilters().subscribe(filters => this.userFolderFilters = filters)
        : of().subscribe();

      call.add(
        () => {
          folderFilter.typeId = (folderFilter as FolderFilterDto).type?.id;
          folderFilter.subtypeId = (folderFilter as FolderFilterDto).subtype?.id;
          this.appliedFilter = folderFilter;
          this.selectedFilterId = folderFilter.id;
          this.requestFolders();
        }
      );
    }
  }


  public onRowOrderClicked(column: TaskViewColumn | string): void {

    if (!this.isSortableRow(column)) {
      return;
    }

    const parsedSortBy: FolderSortBy | string = ColumToFolderSortByPipe.compute(column);

    this.asc = (this.sortBy === parsedSortBy) ? !this.asc : true;
    this.sortBy = parsedSortBy;
    this.requestFolders();
  }


  // </editor-fold desc="UI callbacks">


  public isSortableRow(rowId: TaskViewColumn | string): boolean {

    const isSortableBefore52 = [
      TaskViewColumn.CreationDate, TaskViewColumn.CurrentDesk, TaskViewColumn.FolderId, TaskViewColumn.FolderName,
      TaskViewColumn.LimitDate, TaskViewColumn.OriginDesk, TaskViewColumn.TaskId
    ].map(value => value.valueOf())
      .includes(rowId);

    return this.show52Features || isSortableBefore52;
  }


  /**
   * Re-fetching the task list, updating the UI.
   */
  public requestFolders(): void {
    this.uncheckAll();

    if (!this.show52Features && this.sortBy == "ORIGIN_USER") {
      this.sortBy = "FOLDER_NAME";
    }

    const requestSortBy: string[] = [this.sortBy + (this.asc ? ",ASC" : ",DESC")];
    this.loading = true;

    if (this.show52Features) {

      // These dates seem to actually be strings, we have to parse these...
      const parsedFromDate = !!this.appliedFilter?.from ? Date.parse("" + this.appliedFilter.from) : null;
      const parsedToDate = !!this.appliedFilter?.to ? Date.parse("" + this.appliedFilter.to) : null;

      this.folderService
        .listFolderWithColumns(
          this.tenant.id,
          this.deskId,
          this.appliedFilter.state,
          {
            searchTerm: this.appliedFilter.searchData,
            typeId: this.appliedFilter.typeId,
            subtypeId: this.appliedFilter.subtypeId,
            createdAfter: parsedFromDate,
            createdBefore: parsedToDate,
            stillSinceTime: null,
            includeMetadata: this.currentTableLayout.labelledColumnList
                .filter(column => column.type == LabelledColumnType.Metadata)
                .map(column => column.id)
              ?? [],
            metadataValueFilterMap: {},
            state: this.appliedFilter.state,
            fromDeskIds: [this.deskId]
          } as ColumnedTaskListRequest,
          this.page - 1,
          this.getPageSize(this.pageSizeIndex),
          requestSortBy
        )
        .subscribe({
          next: foldersRetrieved => {
            this.folders = foldersRetrieved.content.map(dto => Object.assign(new Folder(), dto));
            this.total = foldersRetrieved.totalElements;
          },
          error: e => this.notificationsService.showErrorMessage(TasksMessages.ERROR_RETRIEVING_FOLDERS, e.message)
        })
        .add(() => this.loading = false);

    } else {

      this.folderService
        .listFolders(
          this.tenant.id,
          this.deskId,
          this.appliedFilter.state,
          this.appliedFilter,
          this.page - 1,
          this.getPageSize(this.pageSizeIndex),
          requestSortBy
        )
        .subscribe({
          next: foldersRetrieved => {
            this.folders = foldersRetrieved.content.map(dto => Object.assign(new Folder(), dto));
            this.total = foldersRetrieved.totalElements;
          },
          error: e => this.notificationsService.showErrorMessage(TasksMessages.ERROR_RETRIEVING_FOLDERS, e.message)
        })
        .add(() => this.loading = false);

    }
  }


  pageChange(newPage: number): void {
    this.page = newPage;
    this.requestFolders();
  }


  pageSizeChange(newPageSizeIndex: number): void {
    this.pageSizeIndex = newPageSizeIndex;
    this.requestFolders();
  }


  public getPageSize(pageIndex: number): number {
    return this.pageSizes[pageIndex - 1];
  }


  public openTableLayoutModal() {
    this.modalService
      .open(
        TableLayoutPopupComponent, {
          injector: Injector.create({
            providers: [
              {provide: TableLayoutPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenant.id},
              {provide: TableLayoutPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
              {provide: TableLayoutPopupComponent.INJECTABLE_TABLE_LAYOUT_KEY, useValue: this.currentTableLayout},
              {provide: TableLayoutPopupComponent.INJECTABLE_USER_PREFERENCES_KEY, useValue: this.userPreferences},
              {provide: TableLayoutPopupComponent.INJECTABLE_CURRENT_USER_KEY, useValue: this.currentUser}
            ]
          }),
          size: 'lg'
        }
      )
      .result
      .then(
        result => {
          const defaultTableLayout: TableLayoutDto = this.userPreferences.tableLayoutList
            .find((tableLayout: TableLayoutDto): boolean => tableLayout.tableName === TableName.TaskList && !tableLayout.deskId);

          if (result.reset) {
            this.userPreferences.tableLayoutList = this.userPreferences.tableLayoutList
              .filter((tableLayout: TableLayoutDto): boolean => tableLayout.id !== result.deletedId);

            this.currentTableLayout = defaultTableLayout;

            this.notificationsService.showSuccessMessage("La configuration du bureau a bien remise à zéro, la configuration par défaut a donc été appliquée");
          } else {
            this.currentTableLayout = result.tableLayout;

            const editedTableLayout: TableLayoutDto = this.userPreferences.tableLayoutList
              .find((tableLayout: TableLayoutDto): boolean => tableLayout.id === result?.tableLayout?.id);

            if (!editedTableLayout) {
              this.userPreferences.tableLayoutList.push(this.currentTableLayout);
            } else {
              editedTableLayout.columnList = result?.tableLayout?.columnList ?? defaultTableLayout.columnList;
              editedTableLayout["labelledColumnList" as any] = result?.tableLayout?.labelledColumnList ?? defaultTableLayout.labelledColumnList;
            }

            this.notificationsService.showSuccessMessage("La configuration du bureau a bien été enregistrée");
          }

          this.initTaskListVariables(this.currentTableLayout);
          this.requestFolders();
        },
        () => { /* Dismissed */ }
      );
  }


  // TODO : unit-test that !
  public setActionsToDisplay(): void {
    this.actionsToDisplay = [];

    const selectedFolders: Folder[] = this.folders.filter(f => f.selected === true);
    const oneFolderIsUnsafe: boolean = selectedFolders.filter(f => !f.type || !f.subtype).length > 0;
    this.allFoldersSelected = this.folders.length > 0 && this.folders.length === selectedFolders.length;

    if (selectedFolders.length === 0 || oneFolderIsUnsafe) {
      this.actionsToDisplay = [];
      this.selectedFolders = [];
      return;
    }

    const foldersActions = selectedFolders.map(folder => folder.stepList[0].action);
    const uniqueAction = foldersActions[0];

    const deskHasArchivingRights: boolean = this.desk?.archivingAllowed;
    const userHasActionRightOnDesk: boolean = this.desk?.actionAllowed;
    const inDownstreamState: boolean = this.appliedFilter?.state === State.Downstream;
    const inRetrievableState: boolean = this.appliedFilter?.state === State.Retrievable;

    const mainActionsAllowedInCurrentState: boolean = !inDownstreamState && !inRetrievableState && userHasActionRightOnDesk;
    const userHasActionRightOnFolders: boolean = userHasActionRightOnDesk && !inDownstreamState;
    const actionValidInState: boolean = !(uniqueAction === Action.Undo && !inRetrievableState) && !(uniqueAction === Action.Archive && !deskHasArchivingRights);

    const isThereOnlyStackableActions: boolean = foldersActions
      .every(action => FolderUtils.STACKABLE_ACTIONS.includes(action));

    const isThereMultipleStackableActions: boolean = foldersActions
      .filter(action => FolderUtils.STACKABLE_ACTIONS.includes(action))
      .filter((value, index, self) => self.indexOf(value) === index) // == distinct
      .length >= 2;

    const areAllActionsTheSame: boolean = foldersActions
      .every(action => action === foldersActions[0]);

    const areAllActionsRelevant: boolean = selectedFolders
      .filter(folder => !!folder.stepList[0].externalState)
      .filter(folder => folder.stepList[0].externalState !== ExternalState.Form)
      .length === 0;

    //
    // Main action selection
    //
    if (isThereOnlyStackableActions && isThereMultipleStackableActions && userHasActionRightOnFolders) {
      // Several different "stackable"-only  actions
      this.actionsToDisplay = [SecondaryAction.StackedValidation];
    } else if (areAllActionsTheSame
      && areAllActionsRelevant
      && actionValidInState
      && userHasActionRightOnFolders) {
      // all actions are the same action type, and action is allowed
      this.actionsToDisplay = [uniqueAction];
    }

    if (areAllActionsTheSame && areAllActionsRelevant && userHasActionRightOnFolders && uniqueAction === Action.Start) {
      this.actionsToDisplay.push(Action.Delete);
    }

    if (areAllActionsTheSame
      && areAllActionsRelevant
      && uniqueAction === Action.Delete
      && mainActionsAllowedInCurrentState
      && deskHasArchivingRights) {
      this.actionsToDisplay.push(Action.Archive);
    }

    if (!foldersActions.includes(Action.SecondOpinion)
      && !foldersActions.includes(Action.Delete)
      && !foldersActions.includes(Action.Start)
      && !foldersActions.includes(Action.Archive)
      && mainActionsAllowedInCurrentState) {
      this.actionsToDisplay.push(Action.Reject);
    }

    if (areAllActionsTheSame && uniqueAction === Action.Visa && mainActionsAllowedInCurrentState) {
      this.actionsToDisplay.push(Action.Transfer);
    }

    const areAllTasksExternal: boolean = foldersActions.every(action => FolderUtils.EXTERNAL_ACTIONS.includes(action));
    if (areAllTasksExternal && mainActionsAllowedInCurrentState) {
      this.actionsToDisplay.push(Action.Bypass);
    }

    if (this.legacyUserService.isCurrentUserDeskOwner(this.tenant.id, this.deskId)) {
      this.actionsToDisplay.push(SecondaryAction.Mail);
    }

    this.actionsToDisplay = this.actionsToDisplay.filter(action => !MultipleActionListComponent.forbiddenActions.includes(action));

    const oneFolderCannotPerformMainAction = selectedFolders.some(f => this.isMainActionUnavailable(f));
    if (oneFolderCannotPerformMainAction) {
      this.actionsToDisplay.splice(0, 1);
    }

    this.selectedFolders = this.folders.filter(folder => folder.selected === true);
  }


  private isMainActionUnavailable(folder: Folder): boolean {
    return !folder.type || !folder.subtype;
  }


  public handleCheckOrUncheckAll(checkEvent: boolean): void {
    if (checkEvent) {
      this.checkAll();
    } else {
      this.uncheckAll();
    }
  }


  public checkAll(): void {
    if (!this.folders) {
      return;
    }

    this.folders.forEach(folder => folder.selected = true);
    this.setActionsToDisplay();
  }


  uncheckAll(): void {
    if (!this.folders) {
      return;
    }

    this.folders.forEach(folder => folder.selected = false);
    this.setActionsToDisplay();
  }


  public toggleFilterPanel(): void {
    this.showFilterPanel = !this.showFilterPanel;
  }


  public viewingDelegatedFolders(): boolean {
    return this.folderState == 'DELEGATED';
  }


  private getFolderFilter(folderFilterId?: string): FolderFilterDto {
    return this.userFolderFilters.find(filter => filter.id === folderFilterId) || FolderFilter.getFolderFilterForState(this.folderState);
  }


}
