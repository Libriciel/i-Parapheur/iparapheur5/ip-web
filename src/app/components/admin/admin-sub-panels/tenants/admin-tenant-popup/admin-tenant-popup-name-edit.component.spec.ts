/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { AdminTenantPopupNameEditComponent } from './admin-tenant-popup-name-edit.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';

describe('AdminTenantPopupNameEditComponent', () => {

    let component: AdminTenantPopupNameEditComponent;
    let fixture: ComponentFixture<AdminTenantPopupNameEditComponent>;


    beforeEach(async () => {
      await TestBed
        .configureTestingModule({
          imports: [ToastrModule.forRoot()],
          providers: [
            NgbActiveModal, HttpClient, HttpHandler,
            {provide: AdminTenantPopupNameEditComponent.injectableTenantKey, useValue: {id: 'tenant01', name: 'Tenant 01'}},
            {provide: AdminTenantPopupNameEditComponent.injectableUserPreferencesKey, useValue: {} as UserPreferencesDto},
          ]
        })
        .compileComponents();
    });


    beforeEach(() => {
      fixture = TestBed.createComponent(AdminTenantPopupNameEditComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });


    it('should create', () => {
      expect(component).toBeTruthy();
    });


  });
