/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFavoriteDesksComponent } from './user-favorite-desks.component';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { of } from 'rxjs';
import { SelectedUserPreferencesService } from '../../../../../services/selected-user-preferences.service';


describe('UserFavoriteDesksComponent', () => {


  let component: UserFavoriteDesksComponent;
  let fixture: ComponentFixture<UserFavoriteDesksComponent>;

  const mockSelectedUserPreferencesService = {
    currentUserPreferences$: of({ favoriteDeskIds: [], favoriteDesks: [] }),
  };

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [HttpClient, HttpHandler,
        { provide: SelectedUserPreferencesService, useValue: mockSelectedUserPreferencesService }],
        declarations: [UserFavoriteDesksComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(UserFavoriteDesksComponent);
    component = fixture.componentInstance;
    component.userPreferences.favoriteDeskIds = [];
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
