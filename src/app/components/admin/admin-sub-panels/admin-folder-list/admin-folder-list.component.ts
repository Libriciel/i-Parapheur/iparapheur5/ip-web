/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Injector } from '@angular/core';
import { Observable, of } from 'rxjs';
import { faCaretUp, faCaretDown, faEyeSlash, faEye } from '@fortawesome/free-solid-svg-icons';
import { Folder } from '../../../../models/folder/folder';
import { NotificationsService } from '../../../../services/notifications.service';
import { CommonIcons, Style } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../shared/common-messages';
import { AdminMessages } from '../admin-messages';
import { TypologyEntity } from '../../../../models/typology-entity';
import { CrudOperation } from '../../../../services/crud-operation';
import { isNotEmpty, isEmpty, isCurrentVersion52OrAbove } from '../../../../utils/string-utils';
import { ActivatedRoute } from '@angular/router';
import { GlobalPopupService } from '../../../../shared/service/global-popup.service';
import { faMinusSquare, faPlusSquare } from '@fortawesome/free-regular-svg-icons';
import { HistoryPopupComponent } from '../../../main/history-popup/history-popup.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TargetDeskActionPopupComponent } from '../../../main/folder-view/action-popups/target-desk-action-popup/target-desk-action-popup.component';
import { Task } from '../../../../models/task';
import { FolderUtils } from '../../../../utils/folder-utils';
import { LegacyUserService } from '../../../../services/ip-core/legacy-user.service';
import { Action, State, FolderSortBy, PageDeskRepresentation, TaskViewColumn, CurrentUserService, UserPreferencesDto, SubtypeDto } from '@libriciel/iparapheur-legacy';
import { AdminTypologyService, PageSubtypeRepresentation, AdminDeskService, PageTypeRepresentation, DeskRepresentation, UserDto, AdminFolderService as ProvisioningAdminFolderService } from '@libriciel/iparapheur-provisioning';
import { TableLayoutDto, LabelledColumnType, LabelledColumn, TableName, AdminFolderService as InternalAdminFolderService, ColumnedTaskListRequest } from '@libriciel/iparapheur-internal';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';
import { TasksMessages } from '../../../main/tasks/tasks-messages';
import { SelectedUserPreferencesService } from '../../../../services/selected-user-preferences.service';
import { defaultTaskListColumnList } from '../../../../services/resolvers/user-preferences.resolver';


@Component({
  selector: 'app-admin-folder-list',
  templateUrl: './admin-folder-list.component.html',
  styleUrls: ['./admin-folder-list.component.scss']
})
export class AdminFolderListComponent implements OnInit {

  readonly show52Features: boolean = isCurrentVersion52OrAbove();
  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly caretUp = faCaretUp;
  readonly caretDown = faCaretDown;
  readonly sortByEnum = FolderSortBy;
  readonly stateEnum = State;
  readonly actionsEnum = Action;
  readonly secondaryActionsEnum = SecondaryAction;
  readonly messages = AdminMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly columnsEnum = TaskViewColumn;
  readonly styles = Style;
  readonly minusSquareIcon = faMinusSquare;
  readonly plusSquareIcon = faPlusSquare;
  readonly columnTypeEnum = LabelledColumnType;
  readonly eyeSlashIcon = faEyeSlash;
  readonly eyeIcon = faEye;

  tenantId: string;
  asFunctionalAdmin: boolean = false;

  selectedType: TypologyEntity;
  selectedSubtype: SubtypeDto;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;

  folders: Folder[] = [];
  deskList: DeskRepresentation[] = [];

  emitBefore: string = null;
  stillSince: string = null;
  stateFilter: State = null;

  showFolderSearchBar: boolean = true;
  isCollapsed: boolean = false;

  page = 1;
  pageSizeIndex = 1;
  total = 0;
  sortBy: FolderSortBy = FolderSortBy.FolderName;
  asc = true;
  currentSearchTerm = '';
  loading: boolean = false;
  colspan: number;
  currentUser: UserDto;
  currentTableLayout: TableLayoutDto;
  usedColumns: LabelledColumn[];

  retrieveTypesAsAdminFn = (page: number, pageSize: number) => this.retrieveTypesAsAdmin(page, pageSize);
  retrieveSubtypesAsAdminFn = (page: number, pageSize: number) => this.retrieveSubtypesAsAdmin(page, pageSize);


  // <editor-fold desc="LifeCycle">


  constructor(private adminDeskService: AdminDeskService,
              private provisioningAdminFolderService: ProvisioningAdminFolderService,
              private internalAdminFolderService: InternalAdminFolderService,
              private adminTypologyService: AdminTypologyService,
              private currentUserService: CurrentUserService,
              private globalPopupService: GlobalPopupService,
              private legacyUserService: LegacyUserService,
              private modalService: NgbModal,
              private notificationService: NotificationsService,
              private route: ActivatedRoute,
              private selectedUserPreferencesService: SelectedUserPreferencesService) { }


  ngOnInit(): void {
    this.selectedUserPreferencesService.currentUserPreferences$.subscribe(userPreferences => {
      this.userPreferences = userPreferences;

      this.initTaskListAttributes();
      this.initTaskListVariables();
      this.refreshFolderList();
    });
  }


  initTaskListAttributes() {
    this.tenantId = this.route.parent?.snapshot.params['tenantId'];
    this.currentUser = this.route.parent?.parent?.snapshot.data["currentUser"];
  }


  initTaskListVariables(columns?: LabelledColumn[]) {
    this.currentTableLayout = this.userPreferences?.tableLayoutList.find((list: TableLayoutDto): boolean => list.tableName === TableName.AdminFolderList);

    this.usedColumns = !!columns ? columns : this.currentTableLayout.labelledColumnList;

    this.colspan = 1 + this.usedColumns.length;
    this.asFunctionalAdmin = this.legacyUserService.isCurrentUserOnlyFunctionalAdminOfThisTenant(this.tenantId);
  }


  public isDateBeforeNow(date: Date): boolean {
    return (date != null) && (new Date(date).getTime() < Date.now());
  }


  // </editor-fold desc="LifeCycle">


  isIdColumn(column: LabelledColumn): boolean {
    return column.id == TaskViewColumn.TaskId.toString() || column == TaskViewColumn.FolderId.toString();
  }


  showIdColumn(column: LabelledColumn): boolean {
    return !(this.isIdColumn(column) && !this.userPreferences.showAdminIds);
  }


  transfer(folder: Folder): void {

    const taskToDo: Task = Object.assign(new Task(), folder.stepList[0]);

    this.modalService
      .open(TargetDeskActionPopupComponent, {
        injector: Injector.create({
          providers: [
            {provide: TargetDeskActionPopupComponent.INJECTABLE_FOLDERS_KEY, useValue: [folder]},
            {provide: TargetDeskActionPopupComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: Action.Transfer},
            {provide: TargetDeskActionPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: taskToDo.desks[0].id},
            {provide: TargetDeskActionPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
            {provide: TargetDeskActionPopupComponent.INJECTABLE_AS_ADMIN_KEY, useValue: true}
          ]
        }),
        size: 'md'
      })
      .result
      .then(
        result => {
          if (result.value === CommonMessages.ACTION_RESULT_OK) {
            this.refreshFolderList();
          }
        },
        () => {/* Not used */}
      );
  }


  retrieveDesksAsAdminFn = (page: number, pageSize: number, searchTerm): Observable<PageDeskRepresentation> => {

    if (!this.tenantId) {
      return of({});
    }

    const search = isNotEmpty(searchTerm) ? searchTerm : null;
    return this.asFunctionalAdmin
      ? this.currentUserService.getAdministeredDesksForTenant(this.tenantId, page, pageSize, [], search)
      : this.adminDeskService.listDesks(this.tenantId, page, pageSize, [], search);
  };


  onTypeSelectionChanged(): void {
    this.selectedSubtype = null;
  }


  onRowOrderClicked(column: LabelledColumn): void {
    const row: FolderSortBy = this.mapColumnToSortBy(column);
    if (!!row) {
      this.asc = (row === this.sortBy) ? !this.asc : true;
      this.sortBy = row;
      this.refreshFolderList();
    }
  }


  updateSearchTerm(newTerm: string): void {
    this.currentSearchTerm = newTerm;
    this.page = 1;
  }


  refreshFolderList(): void {

    this.loading = true;

    const searchTerm: string = this.currentSearchTerm.length > 0 ? this.currentSearchTerm : null;
    const emitBeforeDateTime: number = isNotEmpty(this.emitBefore) ? new Date(this.emitBefore.valueOf()).getTime() : null;
    const stillSinceDateTime: number = isNotEmpty(this.stillSince) ? new Date(this.stillSince.valueOf()).getTime() : null;
    const fromDeskIds: string[] = this.deskList?.length > 0 ? [this.deskList[0]?.id] : [];

    const requestSortBy: string[] = [this.sortBy + (this.asc ? ',ASC' : ',DESC')];

    const request: ColumnedTaskListRequest = {
      searchTerm: searchTerm,

      typeId: this.selectedType?.id,
      subtypeId: this.selectedSubtype?.id,
      legacyId: null,
      fromDeskIds: fromDeskIds,

      createdAfter: null,
      createdBefore: emitBeforeDateTime,
      stillSinceTime: stillSinceDateTime,

      includeMetadata: defaultTaskListColumnList,
      metadataValueFilterMap: {},
      state: this.stateFilter
    };

    this.internalAdminFolderService
      .listFoldersAsAdmin(
        this.tenantId,
        request,
        this.page - 1,
        this.getPageSize(this.pageSizeIndex),
        requestSortBy
      )
      .subscribe({
        next: foldersRetrieved => {
          this.folders = foldersRetrieved.content.map(f => {
            const mappedFolder = Object.assign(new Folder(), f);
            // FIXME we should probably add the state property to the folderDto on backend, and compute it there, it would make more sense
            mappedFolder.state = f.stepList.length > 0 ? f.stepList[0].state : null;
            return mappedFolder;
          });

          this.total = foldersRetrieved.totalElements;
        },
        error: e => this.notificationService.showErrorMessage(TasksMessages.ERROR_RETRIEVING_FOLDERS, e)
      })
      .add(() => this.loading = false);
  }


  onHistoryButtonClicked(folder: Folder) {
    this.modalService
      .open(HistoryPopupComponent, {
        injector: Injector.create({
          providers: [
            {provide: HistoryPopupComponent.INJECTABLE_FOLDER_KEY, useValue: folder},
            {provide: HistoryPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: null},
            {provide: HistoryPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId}
          ]
        }),
        size: 'xl'
      })
      .result
      .then(
        () => {/* Not used */},
        () => {/* Dismissed */}
      );
  }


  isTransferAvailable(folder: Folder): boolean {
    if (isEmpty(folder.stepList as [])) {
      return false;
    }

    const currentTask = folder.stepList[0];
    return currentTask.desks.length === 1
      && FolderUtils.isActive(currentTask)
      && FolderUtils.TRANSFERABLE_ACTIONS.includes(currentTask.action);
  }


  onDeleteButtonClicked(folder: Folder) {
    this.globalPopupService
      .showDeleteValidationPopup(CommonMessages.foldersValidationPopupLabel([folder]), CommonMessages.foldersValidationPopupTitle([folder]))
      .then(
        () => this.deleteFolder(folder),
        () => {/* Dismissed */}
      );
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  resetSearch(): void {
    this.page = 1;
    this.total = 0;
    this.sortBy = FolderSortBy.FolderName;
    this.asc = true;
    this.currentSearchTerm = "";
    this.selectedType = null;
    this.selectedSubtype = null;
    this.emitBefore = null;
    this.stillSince = null;
    this.stateFilter = null;
    this.deskList = [];
    this.showFolderSearchBar = false;
    setTimeout(() => this.showFolderSearchBar = true);
    this.refreshFolderList();
  }


  mapColumnToSortBy(column: LabelledColumn): FolderSortBy {

    // FIXME: Add it to the lib, and integrate this into the switch case
    if (column.id === 'LAST_MODIFICATION_DATE') {
      return FolderSortBy.StillSinceDate;
    }

    switch (column.id as TaskViewColumn) {
      case TaskViewColumn.FolderName: { return FolderSortBy.FolderName; }
      case TaskViewColumn.LimitDate: { return FolderSortBy.LateDate; }
      case TaskViewColumn.CreationDate: { return FolderSortBy.CreationDate; }
      case TaskViewColumn.TaskId: { return FolderSortBy.TaskId; }
      case TaskViewColumn.FolderId: { return FolderSortBy.FolderId; }
      default: { return null; }
    }
  }


  retrieveTypesAsAdmin(page: number, pageSize: number): Observable<PageTypeRepresentation> {
    return this.adminTypologyService.listTypes(this.tenantId, page, pageSize);
  }


  retrieveSubtypesAsAdmin(page: number, pageSize: number): Observable<PageSubtypeRepresentation> {
    return this.adminTypologyService.listSubtypes(this.tenantId, this.selectedType.id, page, pageSize);
  }


  private deleteFolder(folder: Folder) {
    this.provisioningAdminFolderService
      .deleteFolderAsAdmin(this.tenantId, folder.id)
      .subscribe({
        next: () => {
          this.refreshFolderList();
          this.notificationService.showCrudMessage(CrudOperation.Delete, folder);
        },
        error: e => this.notificationService.showCrudMessage(CrudOperation.Delete, folder, e.message, false)
      });
  }


}
