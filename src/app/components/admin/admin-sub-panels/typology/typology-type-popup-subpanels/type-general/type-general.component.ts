/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { TypologyMessages } from '../../typology-messages';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { TypeDto } from '@libriciel/iparapheur-legacy';

@Component({
  selector: 'app-type-general',
  templateUrl: './type-general.component.html',
  styleUrls: ['./type-general.component.scss']
})
export class TypeGeneralComponent implements AfterViewInit {

  readonly commonMessages = CommonMessages;
  readonly messages = TypologyMessages;
  readonly nameMinLength = 2;
  readonly nameMaxLength = 255;
  readonly descriptionMinLength = 3;
  readonly descriptionMaxLength = 255;

  @Input() type: TypeDto;
  @Output() valid = new EventEmitter<boolean>();

  generalForm = new UntypedFormGroup({
    nameInput: new UntypedFormControl(null, [
      Validators.required,
      Validators.minLength(this.nameMinLength),
      Validators.maxLength(this.nameMaxLength)
    ]),
    descriptionInput: new UntypedFormControl(null, [
      Validators.required,
      Validators.minLength(this.descriptionMinLength),
      Validators.maxLength(this.descriptionMaxLength)
    ]),
  });


  // <editor-fold desc="LifeCycle">


  ngAfterViewInit(): void {
    this.onChange();
  }


  // </editor-fold desc="LifeCycle">


  onChange() {
    this.valid.emit(this.generalForm.valid);
  }


}
