/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


export class WorkflowEditorMessages {

  static readonly WORKFLOW_NAME_LABEL: string = 'Nom du circuit :';

  static readonly CREATE_WORKFLOW_TITLE: string = 'Création du circuit';
  static readonly EDIT_WORKFLOW_TITLE: string = 'Modification du circuit';
  static readonly CREATE_WORKFLOW: string = 'Créer le circuit';
  static readonly SAVE_WORKFLOW: string = 'Enregistrer les modifications';
  static readonly SAVE_STEP: string = `Enregistrer l'étape`;

  static readonly ERROR_CREATING_WORKFLOW_DEFINITION: string = 'Une erreur est survenue durant la création du circuit';
  static readonly ERROR_EDITING_WORKFLOW_DEFINITION: string = `Une erreur est survenue durant l'édition du circuit`;
  static readonly ERROR_DELETING_WORKFLOW_DEFINITION: string = 'Une erreur est survenue durant la suppression du circuit';
  static readonly ERROR_RETRIEVING_WORKFLOW_DEFINITIONS: string = `Erreur à la récupération des circuits`;
  static readonly SUCCESS_CREATING_WORKFLOW_DEFINITION: string = 'Le circuit a été créé avec succès';
  static readonly SUCCESS_EDITING_WORKFLOW_DEFINITION: string = 'Le circuit a été édité avec succès';
  static readonly SUCCESS_DELETING_WORKFLOW_DEFINITION: string = 'Le circuit a été supprimé avec succès';

  static readonly COPY_TXT: string = ' (copie)';

  static readonly SELECT_METADATA_PLACEHOLDER: string = 'Choisissez une métadonnée...';

  static readonly STEP_TYPE_LABEL: string = `Type d'étape`;
  static readonly VALIDATION_MODE_LABEL: string = 'Mode de validation';
  static readonly NOTIFIED_DESKS_LABEL: string = 'Bureau(x) notifié(s)';
  static readonly MANDATORY_METADATA_VALIDATION_LABEL: string = `Métadonnée(s) obligatoire(s)`;
  static readonly MANDATORY_METADATA_REJECTION_LABEL: string = `Métadonnée(s) de rejet obligatoire(s)`;

  static readonly SECURE_MAIL_WARNING_MESSAGE: string = `
    Le mail sécurisé Pastell est une fonctionnalité en voie de dépréciation.  
  
    Il est préférable de revoir le circuit documentaire, pour effectuer cette étape directement dans Pastell.
    
    Ce type d'étape sera supprimé du iparapheur, dans une future version encore indéterminée.
  `;

}
