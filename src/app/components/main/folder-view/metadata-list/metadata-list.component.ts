/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, EventEmitter, Output } from '@angular/core';
import { MetadataDto, SubtypeDto, SubtypeMetadataDto } from '@libriciel/iparapheur-legacy';
import { metadataValueIsNotEmpty } from '../../../../utils/string-utils';


export type MetadataEvent = {
  fullMetadataMap: Map<string, string>,
  modifiedMetadataList: MetadataDto[]
}

@Component({
  selector: 'app-metadata-list',
  templateUrl: './metadata-list.component.html',
  styleUrls: ['./metadata-list.component.scss']
})
export class MetadataListComponent {

  @Input() valuedMetadata: Map<string, string> = new Map<string, string>();
  @Input() subtype: SubtypeDto;
  @Input() tenantId: string;
  @Input() isFolderEditable: boolean = false;

  @Output() valuedMetadataChanged: EventEmitter<MetadataEvent> = new EventEmitter<MetadataEvent>();

  isProcessing = false;
  modifiedSubtypeMetadataSet: Set<SubtypeMetadataDto> = new Set();
  isCollapsed = false;
  isSaveBtnDisabled = false;


  onValueChanged(subtypeMetadata: SubtypeMetadataDto) {
    this.modifiedSubtypeMetadataSet.add(subtypeMetadata);
    this.isSaveBtnDisabled = this.someMandatoryValueIsEmpty(Array.from(this.modifiedSubtypeMetadataSet));
  }

  someMandatoryValueIsEmpty(modifiedSubtypeMetadataList: SubtypeMetadataDto[]): boolean {
    return modifiedSubtypeMetadataList
      .filter(s => s.mandatory)
      .map(s => this.valuedMetadata[s.metadata.key])
      .some(metadataValue => !metadataValueIsNotEmpty(metadataValue));
  }

  onSaveButtonClicked() {

    const modifiedSubtypeMetadataList = Array.from(this.modifiedSubtypeMetadataSet);

    const someMandatoryValueIsEmpty: boolean = this.someMandatoryValueIsEmpty(modifiedSubtypeMetadataList);
    if (someMandatoryValueIsEmpty) {
      console.warn('Cannot set empty value on mandatory metadata');
      return;
    }

    const modifiedMetadataList: MetadataDto[] = modifiedSubtypeMetadataList.map(s => s.metadata);
    this.valuedMetadataChanged.emit({fullMetadataMap: this.valuedMetadata, modifiedMetadataList});
  }


}
