/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { SealCertificatePopupComponent } from './seal-certificate-popup.component';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ToastrModule } from 'ngx-toastr';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { FormUtils } from '../../../../../../../../../utils/form-utils';


describe('AddSealCertificatePopupComponent', () => {


  let component: SealCertificatePopupComponent;
  let fixture: ComponentFixture<SealCertificatePopupComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [
          NgbActiveModal, HttpClient, HttpHandler,
          {provide: SealCertificatePopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: 'tenant_01'},
          {provide: SealCertificatePopupComponent.INJECTABLE_SEAL_CERTIFICATE_KEY, useValue: FormUtils.generateBlankSealCertificateDto()},
        ],
        declarations: [SealCertificatePopupComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(SealCertificatePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
