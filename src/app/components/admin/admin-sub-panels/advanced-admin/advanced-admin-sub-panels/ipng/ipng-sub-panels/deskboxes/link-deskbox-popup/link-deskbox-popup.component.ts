/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { Deskbox } from '../../../../../../../../../models/ipng/deskbox';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IpngService } from '../../../../../../../../../services/ipng.service';
import { CommonIcons } from '@libriciel/ls-elements';
import { Observable, of } from 'rxjs';
import { isNotEmpty } from '../../../../../../../../../utils/string-utils';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { IpngMessage } from '../../../ipng-message';
import { WorkflowActor } from '@libriciel/ls-workflow';
import { AdminDeskService, PageDeskRepresentation, DeskRepresentation } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-link-deskbox-popup',
  templateUrl: './link-deskbox-popup.component.html',
  styleUrls: ['./link-deskbox-popup.component.scss']
})
export class LinkDeskboxPopupComponent {


  public static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  public static readonly INJECTABLE_IPNG_ENTITY_ID_KEY = 'ipngEntityId';
  public static readonly INJECTABLE_DESKBOX_KEY = 'deskbox';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly ipngMessage = IpngMessage;

  isProcessing = false;
  selectedDesk: DeskRepresentation;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationService: NotificationsService,
              public activeModal: NgbActiveModal,
              private ipngService: IpngService,
              private adminDeskService: AdminDeskService,
              @Inject(LinkDeskboxPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(LinkDeskboxPopupComponent.INJECTABLE_IPNG_ENTITY_ID_KEY) public ipngEntityId: string,
              @Inject(LinkDeskboxPopupComponent.INJECTABLE_DESKBOX_KEY) public deskbox: Deskbox) { }


  // </editor-fold desc="LifeCycle">


  retrieveDesksAsAdminFn = (page: number, pageSize: number, searchTerm): Observable<PageDeskRepresentation> => {
    return !this.tenantId
      ? this.adminDeskService.listDesks(this.tenantId, page, pageSize, isNotEmpty(searchTerm) ? searchTerm : null)
      : of();
  };


  updateDeskbox(newSelectedDesks: Array<DeskRepresentation | WorkflowActor>): void {
    this.selectedDesk = newSelectedDesks[0] as DeskRepresentation;
  }


  validate() {
    this.isProcessing = true;

    this.ipngService
      .linkDeskAndDeskbox(this.tenantId, this.ipngEntityId, this.selectedDesk.id, this.deskbox.id)
      .subscribe({
        next: () => {
          this.notificationService.showSuccessMessage(IpngMessage.LINKS_SUCCESSFULLY_CREATED);
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error: e => this.notificationService.showErrorMessage(IpngMessage.ERROR_LINKING_DESKBOXES, e.message)
      })
      .add(() => this.isProcessing = false);
  }


}
