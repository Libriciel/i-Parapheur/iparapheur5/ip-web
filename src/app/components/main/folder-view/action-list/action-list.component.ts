/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Injector, Output, EventEmitter, OnChanges } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, NavigationExtras } from '@angular/router';
import { Folder } from '../../../../models/folder/folder';
import { SimpleActionPopupComponent } from '../action-popups/simple-action-popup/simple-action-popup.component';
import { ExternalState } from '../../../../models/external-state.enum';
import { ChainPopupComponent } from '../action-popups/chain-popup/chain-popup.component';
import { SendByMailPopupComponent } from '../action-popups/mail/send-by-mail-popup/send-by-mail-popup.component';
import { PrintPopupComponent } from '../action-popups/print-popup/print-popup.component';
import { CommonMessages } from '../../../../shared/common-messages';
import { ExternalSignaturePopupLegacyComponent } from '../action-popups/external-signature-popup-legacy/external-signature-popup-legacy.component';
import { Task } from 'src/app/models/task';
import { FolderViewMessages } from '../folder-view-messages';
import { HistoryPopupComponent } from '../../history-popup/history-popup.component';
import { GlobalPopupService } from '../../../../shared/service/global-popup.service';
import { FolderService as StandardFolderService } from '@libriciel/iparapheur-standard';
import { NotificationsService } from '../../../../services/notifications.service';
import { faInfoCircle, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { SecureMailStatusPopupComponent } from '../action-popups/mail/secure-mail-status-popup/secure-mail-status-popup.component';
import { TargetDeskActionPopupComponent } from '../action-popups/target-desk-action-popup/target-desk-action-popup.component';
import { AddDesksToNotifyPopupComponent } from '../action-popups/add-desks-to-notify-popup/add-desks-to-notify-popup.component';
import { IpngProofDisplayPopupComponent } from '../../ipng/ipng-proof-display-popup/ipng-proof-display-popup.component';
import { Action, State, SignatureFormat, ExternalSignatureService, ExternalSignatureProcedure, UserPreferencesDto, TenantDto, FolderFilterDto, FolderDto } from '@libriciel/iparapheur-legacy';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';
import { FolderUtils } from '../../../../utils/folder-utils';
import { FileService } from '../../../../services/file.service';
import { DeskDto } from '@libriciel/iparapheur-provisioning';
import { FolderService as InternalFolderService, TableName, TableLayoutDto, ServerInfoDto } from '@libriciel/iparapheur-internal';
import { lastValueFrom } from 'rxjs';
import { DeskService } from '../../../../services/ip-core/desk.service';
import { ExternalSignaturePopupComponent } from '../action-popups/external-signature-popup/external-signature-popup.component';
import { isCurrentVersion52OrAbove } from '../../../../utils/string-utils';
import { LegacyUserService } from '../../../../services/ip-core/legacy-user.service';

@Component({
  selector: 'app-action-list',
  templateUrl: './action-list.component.html',
  styleUrls: ['./action-list.component.scss']
})
export class ActionListComponent implements OnChanges {


  readonly show52Features: boolean = isCurrentVersion52OrAbove();
  readonly actionEnum = Action;
  readonly secondaryActionEnum = SecondaryAction;
  readonly commonMessages = CommonMessages;
  readonly messages = FolderViewMessages;
  readonly infoIcon: IconDefinition = faInfoCircle;


  @Input() folder: Folder;
  @Input() desk!: DeskDto;
  @Input() userPreferences: UserPreferencesDto;
  @Input() serverInfo: ServerInfoDto;
  @Input() tenant!: TenantDto;
  @Output() actualizeFolderEvent: EventEmitter<void> = new EventEmitter<void>();

  secondaryActionList: (Action | SecondaryAction) [] = [
    SecondaryAction.Print,
    SecondaryAction.Mail,
    // TODO not implemented yet, better remove the button
    // SecondaryAction.AddDesksToNotify,
    Action.Transfer,
    Action.AskSecondOpinion,
    Action.Chain,
    SecondaryAction.HistoryTasks,
    SecondaryAction.Download,
    SecondaryAction.IpngShowProof,
  ].concat(this.show52Features ? [SecondaryAction.SignatureValidation] : []);

  alternativeActionList: SecondaryAction[] = [
    SecondaryAction.Print,
    SecondaryAction.Mail,
    SecondaryAction.HistoryTasks,
    SecondaryAction.Download,
    SecondaryAction.IpngShowProof
  ];

  stepList: Task[];
  externalSignatureProcedure: ExternalSignatureProcedure;
  isIpngProofAvailable: boolean = false;
  isMainActionEnabled: boolean = true;
  nextFolder: FolderDto;


  // <editor-fold desc="LifeCycle">


  constructor(public deskService: DeskService,
              public externalSignatureService: ExternalSignatureService,
              private legacyUserService: LegacyUserService,
              private fileService: FileService,
              public globalPopupService: GlobalPopupService,
              private internalFolderService: InternalFolderService,
              public notificationsService: NotificationsService,
              private standardFolderService: StandardFolderService,
              public modalService: NgbModal,
              public router: Router) {}


  ngOnChanges(): void {
    if (!!this.folder) {
      this.initWithFolder();
    }

    this.setNextFolder();
  }


  private initWithFolder(): void {
    // Filtering currently performable tasks
    this.stepList = !!this.folder?.stepList
      ? this.folder.stepList
        ?.filter(task => task.state === State.Current)
        ?.filter(task => !task.date)
      : [];
    this.isIpngProofAvailable = this.areSomeIPNGProofAvailable();
    this.isMainActionEnabled = this.desk?.actionAllowed && !!this.folder?.type && !!this.folder.subtype;

    this.populateExternalSignatureDataIfNeeded();
  }


  // </editor-fold desc="LifeCycle">


  populateExternalSignatureDataIfNeeded(): void {
    const currentStep: Task = this.stepList[0];

    if (currentStep?.action !== Action.ExternalSignature || currentStep?.externalState === ExternalState.Form) {
      return;
    }

    this.externalSignatureService.getProcedureData(
      this.tenant.id,
      this.desk.id,
      this.folder.subtype.externalSignatureConfig.id,
      currentStep.externalSignatureProcedureId
    ).subscribe(procedure => {
      this.externalSignatureProcedure = procedure;
    });

  }


  areSomeIPNGProofAvailable(): boolean {
    if (!this.folder?.stepList) {
      return false;
    }

    const ipngTasks: Task[] = this.folder.stepList
      .filter(task => task.action === Action.Ipng);

    const validatedIpngTasks: Task[] = ipngTasks.filter(task => task.state === State.Validated);
    const validatedIpngStepIndexes = validatedIpngTasks.map(t => t.stepIndex);

    const unfinishedIpngTasks: Task[] = ipngTasks
      .filter(task => validatedIpngStepIndexes.includes(task.stepIndex))
      .filter(task => task.state != State.Validated);

    return validatedIpngTasks.length > 0 && validatedIpngTasks.length > unfinishedIpngTasks.length;
  }


  // <editor-fold desc="UI callbacks">


  isPaperSignable(step: Task): boolean {
    return this.desk?.actionAllowed
      && this.folder.type?.signatureFormat === SignatureFormat.Pades
      && (step.action === Action.Signature)
      && !this.folder.subtype.digitalSignatureMandatory;
  }


  isRejectable(step: Task): boolean {

    const deskActionAllowed: boolean = this.desk?.actionAllowed;
    const isSimpleAction: boolean = FolderUtils.SIMPLE_ACTIONS.includes(step.action);
    const isExternalAction: boolean = FolderUtils.EXTERNAL_ACTIONS.includes(step.action);
    const isInFormState: boolean = !this.externalActionWaitingStatus(step);

    return deskActionAllowed && (isSimpleAction || (isExternalAction && isInFormState));
  }


  /**
   * Only rejected folders steps can be chained.
   * Final steps on rejected folders are the ones asking for an {@link Delete} action.
   * @param step
   */
  isRecyclable(step: Task): boolean {
    return this.desk.archivingAllowed
      && (step.action === Action.Delete);
  }


  /**
   * Drafts and archivable folders can be deleted.
   * @param step
   */
  isDeletableAsSecondaryAction(step: Task): boolean {
    const isDraft = (step.action === Action.Start);
    // const isDeleteAllowed = this.desk.archivingAllowed;
    // const isDeleteAvailable = (step.action === Action.Archive);
    // return isDraft || (isDeleteAllowed && isDeleteAvailable);
    return isDraft;
  }


  /**
   * Checking step and desk permissions.
   * @param step
   */
  isArchivableAsSecondaryAction(step: Task): boolean {
    const isArchiveAllowed = this.desk.archivingAllowed;
    const isArchiveAvailable = (step.action === Action.Delete);
    return isArchiveAllowed && isArchiveAvailable;
  }


  shouldHideArchiveAction(step: Task): boolean {
    const isArchiveAvailable = (step.action === Action.Archive);
    const isArchiveAllowed = this.desk.archivingAllowed;

    return isArchiveAvailable && !isArchiveAllowed;
  }

  /**
   * Only "external" steps can be bypassed.
   * @param step
   */
  isBypassable(step: Task): boolean {
    return this.desk?.actionAllowed
      && FolderUtils.EXTERNAL_ACTIONS.includes(step.action);
  }


  isUndoable(step: Task): boolean {
    switch (step.action) {
      case Action.SecureMail:
      case Action.ExternalSignature:
        return this.externalActionWaitingStatus(step);
      default:
        return false;
    }
  }


  isSecondaryActionAvailable(action: Action | SecondaryAction, step: Task): boolean {
    switch (action) {

      case SecondaryAction.SignatureValidation:
        return this.show52Features && this.serverInfo.signatureValidationServiceActivated;

      // Both are a kind of transfer,
      // and are subject to the same permissions
      case Action.Transfer:
      case Action.AskSecondOpinion:
        return this.desk?.actionAllowed
          && FolderUtils.TRANSFERABLE_ACTIONS.includes(step?.action);

      // Only "final" steps can be chained.
      // Final steps are the ones asking for an {@link Archive} action.
      case Action.Chain:
        return this.desk.chainAllowed && (step?.action == Action.Archive);

      case SecondaryAction.IpngShowProof:
        return this.isIpngProofAvailable;

      case SecondaryAction.Mail:
        if (!this.tenant?.id || !this.desk.id) {
          return false;
        }
        return this.legacyUserService.isCurrentUserDeskOwner(this.tenant.id, this.desk.id);

      // Every other action (history...) is allowed
      default:
        return true;
    }
  }


  isLinkEnabled(action: Action | SecondaryAction): boolean {
    switch (action) {

      case SecondaryAction.SignatureValidation:
        for (const document of this.folder.documentList) {
          const containSignaturesInfo: boolean = document.detachedSignatures.length > 0 || document.embeddedSignatureInfos?.length > 0;
          // TODO: Remove signature format condition when embedded xades and cades signature are recognized by chorus signature validation
          const hasSupportedChorusFormat: boolean = this.folder.type.signatureFormat != SignatureFormat.Pkcs7
            && this.folder.type.signatureFormat != SignatureFormat.PesV2;

          // TODO: Remove mediaType condition when embeddedSignatureInfos are extended to other mediaType
          if (containSignaturesInfo && hasSupportedChorusFormat) {
            return true;
          }
        }
        return false;

      default:
        return true;
    }
  }


  isSignedExternalSignature(step: Task): boolean {
    return step.action === Action.ExternalSignature && step.externalState === ExternalState.Signed;
  }


  externalActionWaitingStatus(task: Task) {
    const externalAction: boolean =
      task.action === Action.ExternalSignature
      || task.action === Action.Ipng
      || task.action === Action.SecureMail;

    return externalAction
      && !!task.externalState
      && (task.externalState !== ExternalState.Form);
  }


  // </editor-fold desc="UI callbacks">


  openModal(task: Task, executedAction: Action | SecondaryAction) {

    let modalResult: Promise<any>;
    const folderToDo = Object.assign(new Folder(), this.folder);
    const modalSize = FolderUtils.getModalSize(executedAction, [folderToDo]);
    const taskToDo = Object.assign(new Task(), task);
    taskToDo.action = executedAction;
    folderToDo.stepList = [taskToDo];

    switch (executedAction) {

      case SecondaryAction.AddDesksToNotify:
        modalResult = this.modalService
          .open(AddDesksToNotifyPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: AddDesksToNotifyPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenant.id},
                {provide: AddDesksToNotifyPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
                {provide: AddDesksToNotifyPopupComponent.INJECTABLE_TASK_KEY, useValue: this.folder.stepList.find(step => step.state === State.Current)}
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case Action.Delete:
        modalResult = this.globalPopupService
          .showDeleteValidationPopup(CommonMessages.foldersValidationPopupLabel([folderToDo]), CommonMessages.foldersValidationPopupTitle([folderToDo]))
          .then(
            () => lastValueFrom(this.standardFolderService.deleteFolder(this.tenant.id, this.desk.id, folderToDo.id)),
            () => { /* Dismissed */ }
          );
        break;

      case Action.Chain:
        modalResult = this.modalService
          .open(ChainPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: ChainPopupComponent.INJECTABLE_FOLDER_KEY, useValue: folderToDo},
                {provide: ChainPopupComponent.INJECTABLE_DESK_KEY, useValue: this.desk},
                {provide: ChainPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenant.id}
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case SecondaryAction.Print:
        modalResult = this.modalService
          .open(PrintPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: PrintPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenant.id},
                {provide: PrintPopupComponent.INJECTABLE_FOLDER_KEY, useValue: folderToDo},
                {provide: PrintPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case SecondaryAction.Mail:
      case Action.SecureMail: {
        const currentTaskId = this.stepList[0]?.id;

        if (executedAction === Action.SecureMail && this.externalActionWaitingStatus(this.stepList[0])) {
          modalResult = this.modalService
            .open(SecureMailStatusPopupComponent, {
              injector: Injector.create({
                providers: [
                  {provide: SecureMailStatusPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
                  {provide: SecureMailStatusPopupComponent.INJECTABLE_FOLDER_KEY, useValue: folderToDo},
                  {provide: SecureMailStatusPopupComponent.INJECTABLE_TENANT_KEY, useValue: this.tenant.id},
                  {provide: SecureMailStatusPopupComponent.INJECTABLE_TASK_KEY, useValue: currentTaskId}
                ]
              }),
              size: "xl"
            })
            .result;
        } else {
          modalResult = this.modalService
            .open(SendByMailPopupComponent, {
              injector: Injector.create({
                providers: [
                  {provide: SendByMailPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
                  {provide: SendByMailPopupComponent.INJECTABLE_CURRENT_ACTION_KEY, useValue: executedAction},
                  {provide: SendByMailPopupComponent.INJECTABLE_FOLDERS_KEY, useValue: [folderToDo]},
                  {provide: SendByMailPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenant.id}
                ]
              }),
              size: modalSize
            })
            .result;
        }
        break;
      }
      case Action.ExternalSignature:
        if (this.show52Features) {
          modalResult = this.modalService
            .open(ExternalSignaturePopupComponent, {
              injector: Injector.create({
                providers: [
                  {provide: ExternalSignaturePopupComponent.INJECTABLE_FOLDERS_KEY, useValue: [folderToDo]},
                  {provide: ExternalSignaturePopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
                  {provide: ExternalSignaturePopupComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: executedAction},
                  {provide: ExternalSignaturePopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenant.id},
                  {provide: ExternalSignaturePopupComponent.INJECTABLE_EXTERNAL_SIGNATURE_PROCEDURE_KEY, useValue: this.externalSignatureProcedure}
                ]
              }),
              size: modalSize,
              backdrop: 'static'
            })
            .result;
        } else {
          modalResult = this.modalService
            .open(ExternalSignaturePopupLegacyComponent, {
              injector: Injector.create({
                providers: [
                  {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_FOLDERS_KEY, useValue: [folderToDo]},
                  {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
                  {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: executedAction},
                  {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenant.id},
                  {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_EXTERNAL_SIGNATURE_PROCEDURE_KEY, useValue: this.externalSignatureProcedure}
                ]
              }),
              size: modalSize,
              backdrop: 'static'
            })
            .result;
        }


        break;

      case SecondaryAction.HistoryTasks:
        modalResult = this.modalService
          .open(HistoryPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: HistoryPopupComponent.INJECTABLE_FOLDER_KEY, useValue: folderToDo},
                {provide: HistoryPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
                {provide: HistoryPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenant.id}
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case SecondaryAction.IpngShowProof:
        modalResult = this.modalService
          .open(IpngProofDisplayPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: IpngProofDisplayPopupComponent.injectableFolderKey, useValue: folderToDo},
                {provide: IpngProofDisplayPopupComponent.injectableDeskIdKey, useValue: this.desk.id},
                {provide: IpngProofDisplayPopupComponent.injectableTenantIdKey, useValue: this.tenant.id}
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case Action.Transfer:
      case Action.AskSecondOpinion:
        modalResult = this.modalService
          .open(TargetDeskActionPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: TargetDeskActionPopupComponent.INJECTABLE_DESK_ID_KEY, useValue: this.desk.id},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenant.id},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: executedAction},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_FOLDERS_KEY, useValue: [folderToDo]},
                {provide: TargetDeskActionPopupComponent.INJECTABLE_AS_ADMIN_KEY, useValue: false},
              ]
            }),
            size: modalSize
          })
          .result;
        break;

      case SecondaryAction.Download:
        this.fileService
          .downloadFileWithObservable(
            this.standardFolderService.downloadFolderZip(
              this.tenant.id, this.desk.id, this.folder.id, 'body', true,
              {httpHeaderAccept: 'application/octet-stream'}
            ),
            this.folder.name + '.zip'
          )
          .subscribe({
            next: () => console.log(this.messages.DOWNLOAD_PRINT_DOC_SUCCESS),
            error: e => this.notificationsService.showErrorMessage(this.messages.DOWNLOAD_PRINT_DOC_ERROR, e)
          });
        break;

      case SecondaryAction.SignatureValidation:
        this.internalFolderService.generateDocumentSignatureVerification(this.tenant.id, this.folder.id)
          .subscribe({
            next: signatureProof => {
              FolderUtils.showSignatureProofSuccessOrErrorMessage(signatureProof, this.notificationsService, this.messages);
              this.actualizeFolderEvent.emit();
            },
            error: e => this.notificationsService.showErrorMessage(this.messages.GENERATE_SIGNATURE_VALIDATION_ERROR, e.message)
          });
        break;

      default:
        modalResult = this.modalService
          .open(SimpleActionPopupComponent, {
            injector: Injector.create({
              providers: [
                {provide: SimpleActionPopupComponent.injectableFoldersKey, useValue: [folderToDo]},
                {provide: SimpleActionPopupComponent.injectableDeskIdKey, useValue: this.desk.id},
                {provide: SimpleActionPopupComponent.injectablePerformedActionKey, useValue: executedAction},
                {provide: SimpleActionPopupComponent.injectableTenantKey, useValue: this.tenant},
                {provide: SimpleActionPopupComponent.injectableUserPreferencesKey, useValue: this.userPreferences}
              ]
            }),
            size: modalSize
          })
          .result;
        break;
    }

    if (executedAction === SecondaryAction.Print || executedAction === SecondaryAction.Download) {
      return;
    }

    modalResult.then(
      result => {
        // Delete was dismissed
        if (result === undefined) {
          return;
        }
        // Delete was successful
        if (result === null) {
          this.notificationsService.showSuccessMessage(CommonMessages.getDeleteCreationMessage(folderToDo));
        }
        // On success, go up in parent desk
        if ((result === null) || (result === CommonMessages.ACTION_RESULT_OK) || (result.value === CommonMessages.ACTION_RESULT_OK)) {

          if (executedAction === Action.Recycle) {
            setTimeout(() => this.actualizeFolderEvent.emit());
            return;
          }

          this.redirectToNextAction();
        }
      },
      () => { /* Dismissed */ }
    );
  }

  redirectToNextAction() {
    if (!!this.nextFolder) {
      const currentTask: Task = this.folder.stepList.filter(s => s.state === State.Current).shift();
      const isDelegated: boolean = !currentTask.desks.map(desk => desk.id).includes(this.desk.id);
      const nextFolderCurrentDeskId: string = this.nextFolder.stepList.flatMap(s => s.desks).map(d => d.id)[0];
      const requestExtras: NavigationExtras = {queryParams: {asDeskId: nextFolderCurrentDeskId}};
      const url: string = this.deskService.getFolderViewUrl(this.tenant.id, this.desk.id, this.nextFolder.id);

      if (isDelegated) {
        this.router.navigate([url], requestExtras).then(() => { /* Not used */ });
      } else {
        this.router.navigate([url]).then(() => { /* Not used */ });
      }
    } else {
      const state: State = this.getCurrentFolderState();

      this.router
        .navigate(['/tenant/' + this.tenant.id + '/desk/' + this.desk.id + '/' + state.toString().toLowerCase()])
        .then(() => { /* Not used */ });
    }
  }


  getCurrentFolderState(): State {
    const currentTask: Task = this.folder.stepList.filter(s => s.state === State.Current).shift();

    if (!currentTask) {
      return State.Pending;
    }

    const isDelegated: boolean = !currentTask.desks.map(desk => desk.id).includes(this.desk.id);

    let state: State = State.Pending;
    if (isDelegated) {
      state = State.Delegated;
    } else if (currentTask.action == Action.Archive) {
      state = State.Finished;
    } else if (currentTask.action == Action.Start) {
      state = State.Draft;
    } else if (currentTask.action == Action.Delete) {
      state = State.Rejected;
    }

    return state;
  }


  public setNextFolder(): void {
    const tableLayout: TableLayoutDto = this.userPreferences.tableLayoutList.find(tl => tl.tableName === TableName.TaskList);
    const sortBy: string = tableLayout?.defaultSortBy ?? this.userPreferences.taskViewDefaultSortBy;
    const asc: boolean = tableLayout?.defaultAsc ?? this.userPreferences.taskViewDefaultAsc;

    const filter: FolderFilterDto = {
      state: this.getCurrentFolderState()
    };

    this.internalFolderService
      .listFolders(
        this.tenant.id,
        this.desk.id,
        filter.state,
        filter,
        0,
        2,
        [sortBy + "," + (asc ? "ASC" : "DESC")]
      )
      .subscribe({
        next: foldersRetrieved => {
          const filteredFolders: FolderDto[] = foldersRetrieved.content.filter(f => f.id != this.folder.id);

          if (filteredFolders.length === 0) {
            return;
          }

          this.nextFolder = filteredFolders[0];
        }
      });
  }

}
