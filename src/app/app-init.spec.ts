/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { TestBed } from '@angular/core/testing';
import { parseVisibility } from './app-init';
import { FolderVisibility } from '@libriciel/iparapheur-legacy';


/**
 * Angular's global initialisation.
 */
describe('appInitializer', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: []
    });
  });

  it('should parse the folder visibility', () => {
    expect(parseVisibility("confidentiel")).toBe(FolderVisibility.Confidential);
  });

  it('should parse the folder visibility', () => {
    expect(parseVisibility("confidential")).toBe(FolderVisibility.Confidential);
  });

  it('should parse the folder visibility', () => {
    expect(parseVisibility("")).toBe(FolderVisibility.Confidential);
  });

  it('should parse the folder visibility', () => {
    expect(parseVisibility(null)).toBe(FolderVisibility.Confidential);
  });

  it('should parse the folder visibility', () => {
    expect(parseVisibility("public")).toBe(FolderVisibility.Public);
  });

});

