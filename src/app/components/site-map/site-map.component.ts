/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { SiteMapMessages } from './site-map-messages';
import { CommonMessages } from '../../shared/common-messages';
import { combineLatest, Subscription } from 'rxjs';
import { PageDeskRepresentation, PageDeskCount, TenantService, CurrentUserService, State, TenantRepresentation, DeskRepresentation, TenantSortBy } from '@libriciel/iparapheur-legacy';
import { FlatTreeNode, DeskWithCount } from '../../shared/models/custom-types';
import { FolderUtils } from '../../utils/folder-utils';
import { tap, catchError } from 'rxjs/operators';
import { NotificationsService } from '../../services/notifications.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { WebsocketService } from '../../services/websockets/websocket.service';
import { PageTenantRepresentation } from '@libriciel/iparapheur-legacy/model/pageTenantRepresentation';

@Component({
  selector: 'app-site-map',
  templateUrl: './site-map.component.html',
  styleUrls: ['./site-map.component.scss']
})
export class SiteMapComponent implements OnInit {

  readonly messages = SiteMapMessages;
  readonly commonMessages = CommonMessages;
  readonly stateEnum = State;
  readonly computeDelegatedTotalFn = FolderUtils.computeDelegatedTotal;

  flattenTreeStructureUnfiltered: FlatTreeNode[] = [];
  flattenTreeStructure: FlatTreeNode[] = [];
  tenants: TenantRepresentation[] = [];
  desks: DeskWithCount[] = [];
  folderCountsSubscriptions: Subscription[] = [];

  constructor(public tenantService: TenantService,
              public currentUserService: CurrentUserService,
              public notificationService: NotificationsService,
              public modalService: NgbModal,
              private websocketService: WebsocketService) {}


  ngOnInit() {
    combineLatest([
      this.tenantService.listTenantsForUser(0, 10000, [TenantSortBy.Name + ',ASC'], false),
      this.currentUserService.getDesks(null, null, null),
      this.currentUserService.getDesksFolderCount(null, null, null)
    ]).pipe(
      tap((result: [PageTenantRepresentation, PageDeskRepresentation, PageDeskCount]) => {
        const tenantsRetrieved: PageTenantRepresentation = result[0];
        const desksRetrieved: PageDeskRepresentation = result[1];
        const deskListWithCount: PageDeskCount = result[2];

        this.tenants = tenantsRetrieved.content;
        this.retrieveDesksAndCounts(desksRetrieved, deskListWithCount);
      }),
      catchError(this.notificationService.handleHttpError('getDesks'))
    ).subscribe(() => {
      this.buildFlattenTreeStructure();
      this.watchForDeskCountChanges();
    });
  }


  private retrieveDesksAndCounts(desksRetrieved: PageDeskRepresentation, deskListWithCount: PageDeskCount) {
    this.desks = desksRetrieved.content.map(desk => {
      return {desk: desk, count: null};
    });

    this.desks.forEach(deskWithCount => {
      const correspondingDeskCount = deskListWithCount.content.find(deskCount => deskWithCount.desk.id === deskCount.deskId);
      if (!!correspondingDeskCount) {
        deskWithCount.count = correspondingDeskCount;
      }
    });
  }


  private buildFlattenTreeStructure() {

    if (!this.desks || this.desks.length === 0) {
      this.flattenTreeStructure = null;
      return;
    }

    if (!this.tenants || this.tenants.length === 0) {
      this.flattenTreeStructureUnfiltered = [{tenant: null, deskWithCount: null, isDesk: false, expanded: true}];
      this.flattenTreeStructureUnfiltered.concat(this.desks.map(d => {
        return {tenant: null, deskWithCount: d, isDesk: true, expanded: true};
      }));
      return;
    }

    const desksWithoutTenant: DeskRepresentation[] = this.desks
      .filter(deskWithCount => !deskWithCount.desk.tenantId)
      .map(deskWithCount => deskWithCount.desk);
    if (desksWithoutTenant.length > 0) {
      console.warn('Desks without matched tenant : ', desksWithoutTenant);
    }
    const newTree: FlatTreeNode[] = [];
    this.tenants.forEach(tenant => {

      const tenantDesks: FlatTreeNode[] = this.desks
        .filter(deskWithCount => deskWithCount.desk.tenantId === tenant.id)
        .map(deskWithCount => {
          return {tenant: null, deskWithCount: deskWithCount, isDesk: true, expanded: true};
        });

      if (tenantDesks.length > 0) {
        newTree.push({tenant: tenant, deskWithCount: null, isDesk: false, expanded: true});
        newTree.push(...tenantDesks);
      }
    });

    this.flattenTreeStructureUnfiltered = newTree;
    this.flattenTreeStructure = this.flattenTreeStructureUnfiltered.filter(node => !node.isDesk || (node.isDesk && node.expanded));
  }


  private watchForDeskCountChanges() {
    this.websocketService.watchForDeskCountChanges(this.desks.map(data => data.desk))
      .subscribe(observables => {
        this.folderCountsSubscriptions = observables.map(observable => observable.subscribe(
          newDeskCount => {
            const index = this.desks.findIndex(existingDeskCount => existingDeskCount.desk?.id === newDeskCount.deskId);
            if (index === -1) {
              return;
            }
            console.debug(`Folder count updated, tenant : ${newDeskCount.tenantId}, desk : ${newDeskCount.deskId}`);
            this.desks[index].count = newDeskCount;
          }
        ));
      });
  }
}
