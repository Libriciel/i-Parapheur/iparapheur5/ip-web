/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class AdminMessages {

  static readonly ADMINISTRATION = 'Administration';
  static readonly TENANT_SELECTOR_LABEL = `Administrer l'entité\u00a0:`;

  static readonly SERVER_INFOS = 'Informations serveur';
  static readonly TENANTS = 'Entités';
  static readonly ALL_USERS = 'Tous les utilisateurs';

  static readonly USERS = 'Utilisateurs';
  static readonly DESKS = 'Bureaux';
  static readonly ABSENCES = 'Absences';
  static readonly LAST_MODIFICATION_DATE = 'Dernière modification';

  static readonly WORKFLOWS = 'Circuits';
  static readonly TYPOLOGY = 'Typologie des dossiers';
  static readonly FOLDERS = 'Dossiers';
  static readonly ADVANCED_ADMIN = 'Avancé';
  static readonly ERROR_REQUESTING_TENANTS = 'Erreur lors de la récupération des entités';

  static readonly NO_TENANT_FOR_CURRENT_USER = `Aucun tenant trouvé pour l'utilisateur actuellemnt connecté.
    Rechargez la page. si le problème persiste, contactez votre administrateur.`;

  static readonly REVISION = 'Révision';

  static readonly RECORDING_SUCCESSFUL = "Enregistrement réussi";

  // General advanced admin

  static readonly ADVANCED_ADMINISTRATION = 'Administration avancée';

  // Chorus pro
  static readonly ADMIN_SIGN_VALIDATION_TITLE = 'Validation de signature';
  static readonly ADMIN_SIGN_VALIDATION_IDS = 'Identifiants enregistrés';
  static readonly ADMIN_SIGN_VALIDATION_MODIFY_IDS = 'Modifier les identifiants';
  static readonly ADMIN_SIGN_VALIDATION_CLIENT_ID = 'Client ID';
  static readonly ADMIN_SIGN_VALIDATION_CLIENT_SECRET = 'Client secret';
  static readonly ADMIN_SIGN_VALIDATION_PRESENTATION = `
    <div>
      <p>iparapheur utilise l’outil de vérification de signature électronique mis à disposition par Chorus Pro via l'API E-signature disponible sur PISTE, 
      afin de générer un rapport de validation de signature lors de la gestion d'un dossier.</p>
      <p>Pour activer cette fonctionnalité, les identifiants de votre application PISTE (Oauth) sont à renseigner ci-dessous.<br>
      Pour obtenir ces identifiants, vous devez créer un compte Piste (service externe : <a href="https://piste.gouv.fr/">piste.gouv.fr</a>), puis une application,
      et demander l'accès à l'API E-signature de Chorus Pro pour cette application.<br>
      Ces actions sont indépendantes de Libriciel Scop. Pour plus de renseignement, veuillez consulter le centre d'aide de leur site internet.</p>
      <p>Les appels vers l'API E-signature sont limités à 20 interrogations par seconde et 1 million d’appels par jour.</p>
      <p>Remarque : Ce service de validation de signature n'a pas de lien avec la plateforme Chorus Pro pour la gestion des factures.</p>
    </div>
`;
  static readonly ADMIN_SIGN_VALIDATION_NON_ACTIVATED = ' Fonctionnalité inactive : identifiants manquants';
  static readonly ADMIN_SIGN_VALIDATION_VALID_IDENTIFICATION = ' Identifiant valide : ';
  static readonly ADMIN_SIGN_VALIDATION_DELETE_CONFIGURATION = ' Supprimer les identifiants ';

  // RGPD
  static readonly ADMIN_RGPD_TITLE = 'Protection des données';
  static readonly ADMIN_RGPD_ENTITY = 'Exploitation';
  static readonly ADMIN_NAME = 'Nom';
  static readonly ADMIN_ADDRESS = 'Adresse';
  static readonly ADMIN_SIRET = 'Siret';
  static readonly ADMIN_COMMENT = 'Commentaire';
  static readonly ADMIN_RGPD_ENTITY_APE_CODE = 'Code APE';
  static readonly ADMIN_RGPD_ENTITY_PHONE = 'N° de téléphone';
  static readonly ADMIN_MAIL = 'Email';
  static readonly ADMIN_RGPD_HOSTING = 'Hébergement';
  static readonly ADMIN_RGPD_RESPONSIBLE = 'Responsable';
  static readonly ADMIN_RGPD_RESP_TITLE = 'Titre';
  static readonly ADMIN_RGPD_DPO = `Délégué(e) à la protection des données de l'exploitant (DPD/DPO)`;
  static readonly ADMIN_RGPD_MAINTENANCE = 'Maintenance';
  static readonly ADMIN_HOST_SAME_CHECKBOX = `L'hébergement est assuré par l'exploitant`;
  static readonly ADMIN_MAINTENANCE_SAME_CHECKBOX = `La maintenance est assurée par l'hébergeur`;

  // Tenants

  static readonly TENANT_MANAGEMENT = 'Gestion des entités';
  static readonly CREATE_TENANT = 'Créer une nouvelle entité';
  static readonly SEARCH_TENANTS = 'Rechercher une entité';

  // Folders

  static readonly SEARCH_FOLDERS = 'Rechercher des dossiers';
  static readonly FOLDER_MANAGEMENT = 'Gestion des dossiers';
  static readonly START_DATE_BEFORE = 'Émis avant le';
  static readonly START_DATE_BEFORE_TOOLTIP = 'Dossier émis avant la date indiquée';
  static readonly STILL_SINCE = 'Immobile depuis le';
  static readonly STILL_SINCE_TOOLTIP = 'Dossier sur le même bureau depuis la date indiquée';
  static readonly FOLDER_TITLE = 'Titre de dossier';
  static readonly PENDING_FILTER_LABEL = 'Afficher seulement les dossiers';
  static readonly ONLY_PENDING_LABEL = 'À traiter';
  static readonly ONLY_LATE_LABEL = 'En retard';
  static readonly PENDING_FILTER_PLACEHOLDER = 'À traiter / En retard';
  static readonly RESET_SEARCH = 'Réinitialiser la recherche';
  static readonly SEARCH = 'Recherche';
  static readonly TRANSFER_DISABLED = `
    <div>Le dossier ne peut être transféré si :</div>
    <ul>
      <li>Il est en brouillon ou en fin de circuit.</li>
      <li>Il est dans une étape parallèle.</li>
      <li>Il n'est pas un Visa, un Cachet, une Signature ou un Avis complémentaire.</li>
    </ul>
`;

  // Workflows

  static readonly WORKFLOW_MANAGEMENT = 'Gestion des circuits';
  static readonly SEARCH_WORKFLOWS = 'Rechercher des circuits';
  static readonly CREATE_WORKFLOW = 'Créer un circuit';
  static readonly CLONE_WORKFLOW = 'Dupliquer un circuit';

  // Popups

  static tenantUserUnlinkValidationPopupLabel = (name: string) => `Êtes-vous sûr de vouloir dissocier l'utilisateur "${name}" de l'entité ?`;
  static workflowValidationPopupLabel = (name: string) => `Êtes-vous sûr de vouloir supprimer le circuit "${name}" ?`;
  static layerValidationPopupLabel = (name: string) => `Êtes-vous sûr de vouloir supprimer le calque "${name}" ?`;
  static metadataValidationPopupLabel = (name: string) => `Êtes-vous sûr de vouloir supprimer la métadonnée "${name}" ?`;
  static certificateValidationPopupLabel = (name: string) => `Êtes-vous sûr de vouloir supprimer le certificat "${name}" ?`;
  static externalSignatureConnectorPopupLabel = (name: string) => `Êtes-vous sûr de vouloir supprimer le connecteur "${name}" ?`;
  static securerMailConnectorPopupLabel = (name: string) => `Êtes-vous sûr de vouloir supprimer le connecteur "${name}" ?`;
  static deskPopupLabel = (name: string) => `Êtes-vous sûr de vouloir supprimer le bureau "${name}" ?`;
  static typologyPopupLabel = (name: string) => `Êtes-vous sûr de vouloir supprimer la typologie "${name}" ?`;

}
