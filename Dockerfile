#
# iparapheur Web
# Copyright (C) 2019-2025 Libriciel SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

# Using the stable version here, not the mainline.
# See http://nginx.org/en/download.html
FROM hubdocker.libriciel.fr/nginx:1.26.3-alpine3.20@sha256:d2c11a1e63f200585d8225996fd666436277a54e8c0ba728fa9afff28f075bd7

# Open Containers Initiative parameters
ARG CI_COMMIT_REF_NAME=""
ARG CI_COMMIT_SHA=""
ARG CI_PIPELINE_CREATED_AT=""
# Non-standard variables, that are still widely used.
# If it is already set in the FROM image, it has to be overridden.
LABEL maintainer="Libriciel SCOP"
LABEL org.label-schema.build-date="$CI_PIPELINE_CREATED_AT"
LABEL org.label-schema.name="ip-web"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.vendor="Libriciel SCOP"
# Open Containers Initiative's image specifications
LABEL org.opencontainers.image.authors="Libriciel SCOP"
LABEL org.opencontainers.image.created="$CI_PIPELINE_CREATED_AT"
LABEL org.opencontainers.image.description="iparapheur user interface"
LABEL org.opencontainers.image.licenses="GNU Affero GPL v3"
LABEL org.opencontainers.image.revision="$CI_COMMIT_SHA"
LABEL org.opencontainers.image.source="registry.libriciel.fr:443/public/signature/ip-web"
LABEL org.opencontainers.image.title="ip-web"
LABEL org.opencontainers.image.vendor="Libriciel SCOP"
LABEL org.opencontainers.image.version="$CI_COMMIT_REF_NAME"

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*
COPY dist/ip-web/ /usr/share/nginx/html/
COPY src/scripts/ /usr/share/scripts

RUN rm /etc/nginx/conf.d/default.conf
COPY docker/nginx/* /etc/nginx/conf.d/

CMD sh /usr/share/scripts/setenv.sh  /usr/share/nginx/html/assets && nginx -g 'daemon off;'
