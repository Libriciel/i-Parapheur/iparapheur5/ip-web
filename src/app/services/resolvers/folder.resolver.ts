/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { inject } from '@angular/core';
import { RouterStateSnapshot, ActivatedRouteSnapshot, ResolveFn } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { Folder } from '../../models/folder/folder';
import { FolderService } from '../ip-core/folder.service';
import { FolderUtils } from '../../utils/folder-utils';
import { catchError } from 'rxjs/operators';
import { NotificationsService } from '../notifications.service';
import { CommonMessages } from '../../shared/common-messages';


export const FolderResolver: ResolveFn<Folder> = (
  route: ActivatedRouteSnapshot,
  _state: RouterStateSnapshot,
  notificationsService: NotificationsService = inject(NotificationsService),
  folderService: FolderService = inject(FolderService),
): Observable<Folder> => {
  const tenantId: string = route?.paramMap?.get('tenantId');
  let folderId = route?.paramMap?.get('folderId');
  let trailingAsDeskId = null;

  // FIXME Somehow the queryParam mechanic is not working as expected sometimes,
  // and it is appended to the end param (here 'folderId') instead of being accessible through the queryParamMap.
  // this is a workaround
  if (folderId) {
    const idx = folderId.indexOf('?' + FolderUtils.AS_DESK_QUERY_PARAM_NAME);
    if (idx !== -1) {
      trailingAsDeskId = folderId.substring(idx + FolderUtils.AS_DESK_QUERY_PARAM_NAME.length + 2);
      folderId = folderId.substring(0, idx);
    }
  }

  let deskId: string = route?.queryParamMap?.get(FolderUtils.AS_DESK_QUERY_PARAM_NAME);
  if (!deskId) {
    deskId = trailingAsDeskId || route?.paramMap?.get('deskId');
  }

  if (folderId) {
    return folderService
      .getFolder(tenantId, folderId, deskId)
      .pipe(catchError((error: any): Observable<never> => {
        const errorDetails = error?.status === 403 ? CommonMessages.INSUFFICIENT_RIGHTS : '';
        notificationsService.showErrorMessage(CommonMessages.CANNOT_OPEN_FOLDER, errorDetails);
        return throwError(() => error.error);
      }));
  } else {
    return of(new Folder());
  }
};



