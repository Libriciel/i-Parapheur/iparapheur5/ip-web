/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


export class AllUsersMessages {


  static readonly ALL_USERS = 'Tous les utilisateurs';

  static readonly TENANT_SEARCH_USER = `Rechercher un utilisateur sur l'entité`;
  static readonly USER_DELETION = `Suppression de l'utilisateur`;

  static readonly LDAP_USER_TOOLTIP = `Cet utilisateur provient d'une synchronisation LDAP`;
  static readonly IDP_USER_TOOLTIP = `Cet utilisateur provient d'un fournisseur d'indentité externe (SSO)`;
  static readonly LINKED_TENANTS = 'Entités associées';

  static readonly WARNING_ABOUT_TO_DELETE_USER = 'Vous êtes sur le point de supprimer définitivement cet utilisateur.';
  static readonly THIS_USER_IS_LINKED_TO = 'Cet utilisateur est actuellement associé à';
  static readonly DELETION_IS_INSTANTANEOUS = 'La suppression sera immédiate.';
  static readonly USER_IS_ADMINISTRATOR_OF = `L'utilisateur est administrateur de `;
  static readonly CANNOT_DELETE_A_5_TENANT_ADMIN_TOOLTIP = `Action impossible, l'utilisateur est administrateur de plus de 5 entités.`;

  static readonly ERROR_RETRIEVING_USERS = 'Erreur à la récupération des utilisateurs';

  static readonly CREATE_SUPER_ADMIN_USER = 'Créer un super-administrateur';
}
