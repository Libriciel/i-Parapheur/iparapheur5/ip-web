/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Observable, BehaviorSubject, forkJoin } from 'rxjs';
import { PaginatedResult } from '../../models/paginated-result';
import { BatchResult } from '../../models/commons/batch-result';

export class IpService {


  /**
   * return an Observable that manage the deletion of all elements of type T for a tenant
   * using the retrieveFunction pagination function to get them
   * and deleteFunction to delete them.
   * deletions are made by batch of batchSize
   *
   * @template T
   * @param {string}  tenantId - concerned tenantId.
   * @param {number}  batchSize - how many deletions will be sent in a batch.
   * @param {(tenantId: string, page: number, batchSize: number) => Observable<PaginatedResult<T>>}  retrieveFunction - call function to get a batch for deletion.
   * @param {(tenantId: string, element: T) => Observable<void>} deleteFunction - delete function for unitary deletion
   * @return {string} This is the result
   */
  static deleteAllElements<T extends { id?: string | number }>(tenantId: string,
                                                               batchSize: number,
                                                               retrieveFunction: (tenantId: string,
                                                                                  page: number,
                                                                                  batchSize: number) => Observable<PaginatedResult<T>>,
                                                               deleteFunction: (tenantId: string, element: T) => Observable<void>): Observable<BatchResult> {

    return new Observable(observer => {
      const dataObservable: BehaviorSubject<T[]> = new BehaviorSubject<T[]>([]);

      // Retrieving a batch of elements to be deleted to initiate the process
      retrieveFunction(tenantId, 0, batchSize)
        .subscribe(
          elementsToBeDeleted => {
            dataObservable.next(elementsToBeDeleted.data);
            console.debug('Récupéré : ' + elementsToBeDeleted.data.map(element => element.id).join(', '));
            const total = Math.ceil(elementsToBeDeleted.total / batchSize);
            let completed = 0;
            dataObservable.subscribe({
              next: elements => {
                if (elements.length > 0) {
                  // Building a batch of deletion
                  const removeCalls = [];
                  elements.forEach(element => removeCalls.push(deleteFunction(tenantId, element)));
                  console.debug('Suppression : ' + elements.map(element => element.id).join(', '));

                  // Send a batch of deletions
                  forkJoin(removeCalls)
                    .subscribe({
                      next: () => {
                        // All deletions have succeeded
                        console.debug('Supprimés');
                        completed++;
                        // Send a status report
                        observer.next({completed: completed, total: total, done: false});
                        console.debug('Récupération');
                        // Retrieve other elements to be deleted
                        retrieveFunction(tenantId, 0, batchSize)
                          .subscribe(data => {
                            console.debug('Récupéré : ' + data.data.map(element => element.id).join(', '));
                            dataObservable.next(data.data);
                          });
                      },
                      error: e => observer.error(e)
                    });
                } else {
                  // No more elements of type T, deletion process is complete
                  observer.next({completed: completed, total: total, done: true});
                  observer.complete();
                }
              },
              error: e => console.log('Error retrieving data : ' + e.message)
            });
          }
        );
    });
  }


}
