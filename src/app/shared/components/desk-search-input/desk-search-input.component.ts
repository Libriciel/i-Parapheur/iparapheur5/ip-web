/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonMessages } from '../../common-messages';
import { compareById } from '../../../utils/string-utils';
import { PageDeskRepresentation, DeskRepresentation } from '@libriciel/iparapheur-legacy';
import { WorkflowUtils } from '../../utils/workflow-utils';

@Component({
  selector: 'app-desk-search-input',
  templateUrl: './desk-search-input.component.html',
  styleUrls: ['./desk-search-input.component.scss']
})
export class DeskSearchInputComponent implements OnInit, OnChanges {

  readonly commonMessages = CommonMessages;
  readonly genericWorkflowActors = WorkflowUtils.GENERIC_WORKFLOW_ACTORS;
  readonly pageSize = 10;
  readonly compareByIdFn = compareById;

  @Input() desks: DeskRepresentation[];
  @Output() desksChange: EventEmitter<DeskRepresentation[]> = new EventEmitter<DeskRepresentation[]>();

  /**
   * Optional parameter, that will be used to reset the field on value change.
   */
  @Input() tenantId: string;

  @Input() clearable: boolean;
  @Input() multiple: boolean;
  @Input() retrieveDeskFunction: (page: number, pageSize: number, searchTerm: string) => Observable<PageDeskRepresentation>;
  @Input() genericValidatorsAvailable = false;
  @Input() genericEmitterOnly = false;
  @Input() autoSelectIfSingle = false;
  @Input() disabled = false;


  currentSearchTerm: string;
  currentPage: number = 0;
  availableValidators: DeskRepresentation[] = [];
  totalValidators: number;


  // <editor-fold desc="LifeCycle">


  ngOnInit(): void {
    this.updateAvailableDesksList(false);
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (changes.tenantId?.currentValue !== changes.tenantId?.previousValue) {
      this.desks = [];
      this.availableValidators = [];
      this.totalValidators = 0;
      this.updateAvailableDesksList(false);
    }
  }


  // </editor-fold desc="LifeCycle">


  onDeskSelectionChanged(event: any, noReload: boolean = false) {
    // Wrapping single elements in an array, to ease factorization
    this.desks = this.multiple ? event : [event];
    this.desksChange.emit(this.desks);
    if (!noReload){
      this.updateDesksWithNewSearchTerm({term: '', items: []});
    }
  }


  updateDesksWithNewSearchTerm(searchEvt: { term: string, items: any[] }) {
    this.currentPage = 0;
    this.currentSearchTerm = searchEvt.term;
    this.updateAvailableDesksList(false);
  }


  onClear() {
    this.updateDesksWithNewSearchTerm({term: '', items: []});
  }


  loadNextPageOfDesks() {
    let availableValidatorsLength: number = this.availableValidators.length;
    if (this.genericEmitterOnly && this.genericValidatorsAvailable) {
      availableValidatorsLength -= 1;
    }
    if (this.genericValidatorsAvailable && !this.genericEmitterOnly) {
      availableValidatorsLength -= 3;
    }
    if (this.totalValidators > availableValidatorsLength) {
      this.currentPage += 1;
      this.updateAvailableDesksList(true);
    }
  }


  updateAvailableDesksList(appendToCurrentList: boolean) {
    this.retrieveDeskFunction(this.currentPage, this.pageSize, this.currentSearchTerm)
      .subscribe(paginatedDeskList => {
        this.totalValidators = paginatedDeskList.totalElements;

        let validators: DeskRepresentation[] = (appendToCurrentList ? this.availableValidators : []).concat(paginatedDeskList.content);

        if (!appendToCurrentList && this.autoSelectIfSingle && this.totalValidators === 1) {
          const onlyDesk: DeskRepresentation = validators[0];
          this.onDeskSelectionChanged(this.multiple ? [onlyDesk] : onlyDesk, true);
        }

        validators = validators.concat(this.desks);

        this.availableValidators = validators.filter((validator, index, self) =>
          index === self.findIndex((desk) => desk?.id === validator?.id)
        );
      });
  }

  filterDesks = (term: string, item: DeskRepresentation) => item.name.includes(term);


}
