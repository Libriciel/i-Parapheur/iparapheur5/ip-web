/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Injector, OnInit } from '@angular/core';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditMetadataPopupComponent } from '../edit-metadata-popup/edit-metadata-popup.component';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { CrudOperation } from '../../../../../../../../../services/crud-operation';
import { ActivatedRoute } from '@angular/router';
import { CommonIcons } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { MetadataMessages } from '../metadata-messages';
import { GlobalPopupService } from '../../../../../../../../../shared/service/global-popup.service';
import { AdminMessages } from '../../../../../../admin-messages';
import { of } from 'rxjs';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { AdminMetadataService, MetadataRepresentation, MetadataSortBy, } from '@libriciel/iparapheur-provisioning';
import { catchError } from 'rxjs/operators';
import { HasName } from '../../../../../../../../../models/has-name';
import { FormUtils } from '../../../../../../../../../utils/form-utils';

@Component({
  selector: 'app-admin-metadata-list',
  templateUrl: './admin-metadata-list.component.html',
  styleUrls: ['./admin-metadata-list.component.scss']
})
export class AdminMetadataListComponent implements OnInit {

  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly commonIcons = CommonIcons;
  readonly caretUpIcon = faCaretUp;
  readonly caretDownIcon = faCaretDown;
  readonly sortByEnum = MetadataSortBy;
  readonly commonMessages = CommonMessages;
  readonly messages = MetadataMessages;
  readonly adminMessages = AdminMessages;


  tenantId: string;
  metadataList: MetadataRepresentation[] = [];
  page = 1;
  pageSizeIndex = 1;
  total = 0;
  sortBy: MetadataSortBy = MetadataSortBy.Name;
  asc = true;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  loading: boolean = false;
  colspan: number;
  searchTerm: string;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public modalService: NgbModal,
              public globalPopupService: GlobalPopupService,
              public route: ActivatedRoute,
              public adminMetadataService: AdminMetadataService) { }


  ngOnInit() {
    this.route?.parent?.parent?.parent?.parent?.data?.subscribe(data => {
      this.userPreferences = data["userPreferences"];
      this.colspan = 4 + (this.userPreferences.showAdminIds ? 1 : 0);
    });
    this.route?.parent?.parent?.parent?.params?.subscribe(params => {
      this.tenantId = params['tenantId'];
      this.refreshMetadataList();
    });
  }


  // </editor-fold desc="LifeCycle">


  refreshMetadataList() {

    this.loading = true;

    const direction = (this.asc ? ",ASC" : ",DESC");
    const sortBy = [
      this.sortBy + direction,
      MetadataSortBy.Name + direction,
      MetadataSortBy.Key + direction,
      MetadataSortBy.Id + direction,
    ];
    this.adminMetadataService
      .listMetadata(this.tenantId, false, this.searchTerm, this.page - 1, this.getPageSize(this.pageSizeIndex), sortBy)
      .pipe(catchError(this.notificationsService.handleHttpError('listMetadata')))
      .subscribe({
        next: metadataPage => {
          this.metadataList = metadataPage.content;
          this.total = metadataPage.totalElements;
        },
        error: e => this.notificationsService.showErrorMessage(MetadataMessages.ERROR_RETRIEVING_METADATA, e)
      })
      .add(() => this.loading = false);
  }


  openEditMetadataPopup(targetMetadata?: MetadataRepresentation) {

    const getFullMetadata$ = targetMetadata?.id
      ? this.adminMetadataService.getMetadata(this.tenantId, targetMetadata.id)
      : of(FormUtils.generateBlankMetadataDto());

    getFullMetadata$
      .pipe(catchError(this.notificationsService.handleHttpError('getMetadataAsAdmin')))
      .subscribe({
        next: dto => {
          this.modalService
            .open(EditMetadataPopupComponent, {
                injector: Injector.create({
                  providers: [
                    {provide: EditMetadataPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                    {provide: EditMetadataPopupComponent.INJECTABLE_EXISTING_METADATA_KEY, useValue: dto}
                  ]
                }),
                size: 'lg',
                backdrop: 'static'
              }
            )
            .result
            .then(
              () => this.refreshMetadataList(),
              () => { /* Dismissed */ }
            );
        },
        error: e => this.notificationsService.showCrudMessage(CrudOperation.Read, targetMetadata as HasName, e.message, false)
      });
  }


  onDeleteButtonClicked(metadata: MetadataRepresentation) {
    this.globalPopupService
      .showDeleteValidationPopup(this.adminMessages.metadataValidationPopupLabel(metadata.name))
      .then(
        () => this.deleteMetadata(metadata),
        () => {/* Dismissed */}
      );
  }


  deleteMetadata(metadata: MetadataRepresentation): void {
    this.adminMetadataService
      .deleteMetadata(this.tenantId, metadata.id)
      .pipe(catchError(this.notificationsService.handleHttpError('deleteMetadata')))
      .subscribe({
        next: () => {
          this.notificationsService.showSuccessMessage(MetadataMessages.SUCCESS_DELETING_METADATA);
          this.refreshMetadataList();
        },
        error: e => this.notificationsService.showErrorMessage(MetadataMessages.ERROR_DELETING_METADATA, e.message)
      });
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  onRowOrderClicked(row: MetadataSortBy) {
    this.asc = (row === this.sortBy) ? !this.asc : true;
    this.sortBy = row;
    this.refreshMetadataList();
  }


  updateSearchTerms(searchTerm: string) {
    this.searchTerm = searchTerm;
    this.refreshMetadataList();
  }


}
