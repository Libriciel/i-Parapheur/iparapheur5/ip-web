/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { UserEditSubPanelsMessages } from '../user-edit-sub-panels-messages';
import { UserDto } from '@libriciel/iparapheur-provisioning';
import { catchError } from 'rxjs/operators';
import { CurrentUserService, ServerInfoDto } from '@libriciel/iparapheur-internal';
import { NotificationsService } from '../../../../../services/notifications.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-edit-password',
  templateUrl: './user-edit-password.component.html',
  styleUrls: ['./user-edit-password.component.scss']
})
export class UserEditPasswordComponent implements OnInit {

  readonly messages = UserEditSubPanelsMessages;

  currentUser: UserDto;
  isProcessing: boolean = false;
  newPassword: string;

  serverInfo: ServerInfoDto;

  @Output() passwordChangeEvent = new EventEmitter<string>();

  constructor(private currentUserInternalService: CurrentUserService,
              private notificationService: NotificationsService,
              private route: ActivatedRoute) {}


  ngOnInit(): void {
    this.currentUser = this.route.parent?.snapshot.data["currentUser"];
    this.route.parent?.data.subscribe(data => this.serverInfo = data['serverInfo']);
  }


  emitPassword(newPassword: string) {
    if (this.currentUser.isLdapSynchronized) return;
    this.passwordChangeEvent.emit(newPassword);
  }


  savePassword() {
    if (!this.newPassword) {
      return;
    }

    this.isProcessing = true;

    this.currentUserInternalService
      .updateCurrentUserPassword({password: this.newPassword} as UserDto)
      .pipe(catchError(this.notificationService.handleHttpError('savePassword')))
      .subscribe({
        next: () => this.notificationService.showSuccessMessage(UserEditSubPanelsMessages.PROFILE_SAVE_PASSWORD_SUCCESS),
        error: e => this.notificationService.showErrorMessage(UserEditSubPanelsMessages.PROFILE_SAVE_PASSWORD_ERROR, e.message)
      })
      .add(() => this.isProcessing = false);
  }
}

