/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { CurrentUserService, TableName } from '@libriciel/iparapheur-internal';
import { TableLayoutDto, LabelledColumn } from '@libriciel/iparapheur-internal';
import { CommonIcons, Style } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../shared/common-messages';
import { UserDto } from '@libriciel/iparapheur-provisioning';
import { TasksMessages } from '../tasks-messages';
import { NotificationsService } from '../../../../services/notifications.service';
import { catchError } from 'rxjs/operators';
import { defaultTableLayerList, defaultArchiveListColumnList, defaultFolderMainLayoutColumnList, defaultAdminFolderListColumnList } from '../../../../services/resolvers/user-preferences.resolver';
import { getFirstErrorMessage } from '../../../../utils/string-utils';

@Component({
  selector: 'app-table-layout-popup',
  templateUrl: './table-layout-popup.component.html',
  styleUrls: ['./table-layout-popup.component.scss']
})
export class TableLayoutPopupComponent {


  public static readonly INJECTABLE_TENANT_ID_KEY: string = 'tenant_id';
  public static readonly INJECTABLE_DESK_ID_KEY: string = 'desk_id';
  public static readonly INJECTABLE_TABLE_LAYOUT_KEY: string = 'table_layout';
  public static readonly INJECTABLE_USER_PREFERENCES_KEY: string = 'user_preferences';
  public static readonly INJECTABLE_CURRENT_USER_KEY: string = 'current_user';

  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly styleEnum = Style;


  tasks: Task[] = [];
  editedUserPreferences: UserPreferencesDto;
  editedTableLayout: TableLayoutDto;

  resettable: boolean = false;

  // <editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              private internalCurrentUserService: CurrentUserService,
              private notificationService: NotificationsService,
              @Inject(TableLayoutPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(TableLayoutPopupComponent.INJECTABLE_TABLE_LAYOUT_KEY) public tableLayout: TableLayoutDto,
              @Inject(TableLayoutPopupComponent.INJECTABLE_USER_PREFERENCES_KEY) public userPreferences: UserPreferencesDto,
              @Inject(TableLayoutPopupComponent.INJECTABLE_CURRENT_USER_KEY) public currentUser: UserDto,
              @Inject(TableLayoutPopupComponent.INJECTABLE_DESK_ID_KEY) public deskId: string) {

    this.setResettable();

    this.editedUserPreferences = structuredClone(this.userPreferences);
    this.editedTableLayout = structuredClone(this.tableLayout);
    this.editedTableLayout.columnList = this.editedTableLayout.labelledColumnList.map(labelledElement => labelledElement.id);

    if (tableLayout.tableName === TableName.TaskList && !!this.deskId && !this.editedTableLayout.deskId) {
      this.editedTableLayout.id = null;
      this.editedTableLayout.deskId = this.deskId;
      this.editedUserPreferences.tableLayoutList.push(this.editedTableLayout);
    }

  }


  // </editor-fold desc="LifeCycle">


  setResettable() {
    if (this.tableLayout.tableName === TableName.TaskList) {
      this.resettable = !!this.tableLayout.deskId;
      return;
    }

    let defaultColumns: any[];
    if (this.tableLayout.tableName === TableName.ArchiveList) {
      defaultColumns = defaultArchiveListColumnList;
    } else if (this.tableLayout.tableName === TableName.FolderMainLayout) {
      defaultColumns = defaultFolderMainLayoutColumnList;
    } else if (this.tableLayout.tableName === TableName.AdminFolderList) {
      defaultColumns = defaultAdminFolderListColumnList;
    }

    this.resettable = JSON.stringify(this.tableLayout.labelledColumnList.map(c => c.id)) !== JSON.stringify(defaultColumns);
  }


  updateColumnList(columnList: LabelledColumn[]) {
    this.editedTableLayout.columnList = columnList.map(labelledElement => labelledElement.id);
    this.editedTableLayout["labelledColumnList" as any] = columnList;
  }


  save() {
    this.internalCurrentUserService
      .updateTableLayout(this.editedTableLayout.tableName, this.editedTableLayout)
      .pipe(catchError(this.notificationService.handleHttpError('update table layout')))
      .subscribe({
        next: result => {
          this.editedTableLayout.id = result.id;
          this.editedTableLayout.deskId = result.deskId;

          this.activeModal.close({
            userPreferences: this.editedUserPreferences,
            tableLayout: this.editedTableLayout,
            action: CommonMessages.ACTION_RESULT_OK,
            reset: false,
            deletedId: null
          });
        },
        error: e => this.notificationService.showErrorMessage(TasksMessages.ERROR_UPDATING_TABLE_LAYOUT, e)
      });
  }

  onResetButtonClicked() {
    if (!this.tableLayout?.id) {
      return;
    }

    this.editedUserPreferences.tableLayoutList = defaultTableLayerList;
    this.internalCurrentUserService.deleteTableLayout(this.tableLayout.id)
      .subscribe({
        next: () => this.activeModal.close({
          userPreferences: this.editedUserPreferences,
          action: CommonMessages.ACTION_RESULT_OK,
          reset: true,
          deletedId: this.tableLayout.id
        }),
        error: e => this.notificationService.showErrorMessage(TasksMessages.ERROR_DELETING_TABLE_LAYOUT, getFirstErrorMessage(e))
      });
  }


}
