/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { RxStomp } from '@stomp/rx-stomp';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { DeskRepresentation, DeskCount } from '@libriciel/iparapheur-legacy';
import { CONFIG } from '../../shared/config/config';
import { websocketConfig } from './websocket-config';
import { KeycloakService } from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService extends RxStomp {

  applicationHost: string;
  applicationUrl: string;

  constructor(private keycloakService: KeycloakService) {
    super();

    this.applicationHost = CONFIG?.APPLICATION_URL?.startsWith('https')
      ? 'wss'
      : 'ws';

    this.applicationUrl = this.applicationHost === 'wss'
      ? CONFIG?.APPLICATION_URL?.substring(8)
      : CONFIG?.APPLICATION_URL?.substring(7);

    websocketConfig.brokerURL = `${this.applicationHost}://${this.applicationUrl}/api/websocket`;

    websocketConfig.beforeConnect = () => {
      return this.createAuthHeaderPromise();
    };

    this.configure(websocketConfig);

    this.activate();

  }

  watchForDeskCountChanges(deskList: DeskRepresentation[]): Observable<Observable<DeskCount>[]> {
    return this.ensureActive()
      .pipe(map(() => {
        const result: Observable<DeskCount>[] = [];
        deskList
          .filter(desk => !!desk)
          .forEach(desk => {
            result.push(this.watch({destination: `/websocket/tenant/${desk.tenantId}/desk/${desk.id}/folder/watch`})
              .pipe(map(message => JSON.parse(message.body) as DeskCount))
            );
          });
        return result;
      }));
  }

  ensureActive(): Observable<any> {
    if (this.active) {
      return of("empty_token");
    }

    return of({}).pipe(
      tap(() => this.activate())
    );
  }

  private createAuthHeaderPromise(): Promise<void> {

    return new Promise((resolve, reject) => {
      if (this.keycloakService.isTokenExpired()) {
        this.keycloakService.updateToken().then(
          res => this.fetchAuthTokenAndResolve(resolve, reject)
          , err => reject(err)
        );
      } else {
        this.fetchAuthTokenAndResolve(resolve, reject);
      }
    });
  }

  private fetchAuthTokenAndResolve(resolve: (value: (PromiseLike<void> | void)) => void, reject: (reason?: any) => void): void {
    this.keycloakService.getToken().then(
      token => {
        websocketConfig.connectHeaders = {
          "Authorization": 'Bearer ' + token
        };

        this.configure(websocketConfig);

        resolve();
      },
      err => reject(err)
    );
  }

}
