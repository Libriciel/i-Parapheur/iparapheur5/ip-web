/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { AdminMessages } from '../../../../admin-messages';
import { Style, CommonIcons } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../../../../shared/common-messages';
import { AdminAdvancedConfigService } from '@libriciel/iparapheur-internal';
import { faCircleExclamation, faCircleCheck } from '@fortawesome/free-solid-svg-icons';
import { Validators, UntypedFormGroup, UntypedFormControl } from '@angular/forms';
import { NotificationsService } from '../../../../../../../services/notifications.service';

@Component({
  selector: 'app-chorus-pro-main-layout',
  templateUrl: './chorus-pro-main-layout.component.html',
  styleUrls: ['./chorus-pro-main-layout.component.scss']
})
export class ChorusProMainLayoutComponent implements OnInit {

  readonly messages = AdminMessages;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly style = Style;
  readonly exclamationIcon = faCircleExclamation;
  readonly checkIcon = faCircleCheck;

  protected clientId;
  protected clientSecret;
  protected activatedFeature: boolean = false;
  protected myForm: UntypedFormGroup;
  isProcessing = false;


  constructor(
    public adminAdvancedConfigService: AdminAdvancedConfigService,
    private notificationsService: NotificationsService) {
    this.myForm = new UntypedFormGroup({
      clientId: new UntypedFormControl(this.clientId, [Validators.required]),
      clientSecret: new UntypedFormControl(null, [Validators.required])
    });
  }


  ngOnInit(): void {
    this.getChorusConfiguration();
  }


  getChorusConfiguration(): void {
    const response = this.adminAdvancedConfigService.getSignatureValidationServiceConfiguration();
    if (response) {
      response.subscribe(data => {
        this.clientId = data.clientId == null ? '' : data.clientId;
        this.activatedFeature = data.clientId != null;
      });
    }
  }


  onSaveButtonClicked() {
    this.isProcessing = true;
    this.adminAdvancedConfigService
      .updateSignatureValidationServiceConfiguration({
        "clientId": this.myForm.value.clientId,
        "clientSecret": this.myForm.value.clientSecret
      })
      .subscribe({
        next: () => {
          this.getChorusConfiguration();
          this.activatedFeature = true;
        },
        error: e => this.notificationsService.showErrorMessage(e.message)
      })
      .add(() => this.isProcessing = false);
  }


  onDeleteButtonClicked() {
    this.adminAdvancedConfigService
      .deleteSignatureValidationServiceConfiguration()
      .subscribe({
        next: () => this.activatedFeature = false,
        error: e => this.notificationsService.showErrorMessage(e.message)
      });
  }


}
