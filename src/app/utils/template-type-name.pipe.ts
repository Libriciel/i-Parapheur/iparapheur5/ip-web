/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { TemplateType } from '@libriciel/iparapheur-legacy';

@Pipe({
  name: 'templateTypeName'
})
export class TemplateTypeNamePipe implements PipeTransform {


  public static compute(templateType: TemplateType) {
    switch (templateType) {
      case TemplateType.MailNotificationSingle: { return 'Notification simple'; }
      case TemplateType.MailNotificationDigest : { return 'Notifications groupées'; }
      case TemplateType.MailActionSend : { return 'Action "Envoyer par mail"'; }
      case TemplateType.Docket : { return `Bordereau`; }

      case TemplateType.SealAutomatic : { return 'Cachet automatique'; }
      case TemplateType.SealMedium : { return 'Cachet moyen'; }
      case TemplateType.SealLarge : { return 'Cachet grand'; }
      case TemplateType.SealAlternate : { return 'Cachet alternatif'; }

      case TemplateType.SignatureSmall : { return 'Signature petite'; }
      case TemplateType.SignatureMedium : { return 'Signature moyenne'; }
      case TemplateType.SignatureLarge : { return 'Signature grande'; }
      case TemplateType.SignatureAlternate1 : { return 'Signature alternative 1'; }
      case TemplateType.SignatureAlternate2 : { return 'Signature alternative 2'; }

      default: { return templateType; }
    }
  }


  transform(value: TemplateType, _args?: any): any {
    return TemplateTypeNamePipe.compute(value);
  }


}
