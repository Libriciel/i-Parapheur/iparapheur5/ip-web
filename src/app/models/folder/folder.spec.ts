/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Folder } from './folder';

describe('Folder', () => {


  it('should create an instance', () => {
    expect(new Folder()).toBeTruthy();
  });


  it('should deserialize a full string', () => {
    const toParse: string = '' +
      '{' +
      '   "id":"aaaa-aaaa-aaaa",' +
      '   "type":{' +
      '      "id":"type_152079",' +
      '      "name":"CAdES",' +
      '      "description":"Gatling generated type with randomTypeValue:152079",' +
      '      "depth":null,' +
      '      "childrenCount":null,' +
      '      "signatureFormat":"PKCS7",' +
      '      "protocol":"NONE",' +
      '      "signatureVisible":true,' +
      '      "signaturePosition":null,' +
      '      "signatureLocation":null,' +
      '      "signatureZipCode":null' +
      '   },' +
      '   "subtype":{' +
      '      "id":"subtype_97645",' +
      '      "name":"SubType 97645",' +
      '      "description":"Gatling generated type with randomSubtypeValue:97645",' +
      '      "depth":null,' +
      '      "childrenCount":null,' +
      '      "creationWorkflowId":null,' +
      '      "validationWorkflowId":"simple_workflow_38738",' +
      '      "workflowSelectionScript":null,' +
      '      "secureMailServerId":null,' +
      '      "externalSignatureConfig":null,' +
      '      "creationPermittedRoles":[' +
      '         ' +
      '      ],' +
      '      "sealCertificateId":"5e0d9855-62e9-44e9-8b27-bf3a8178e5a4",' +
      '      "subtypeMetadataList":[' +
      '         {' +
      '            "id":"subtype_97645_4fc83a3b-754c-4d26-b53b-596ed8fd6c10",' +
      '            "metadata":{' +
      '               "id":"4fc83a3b-754c-4d26-b53b-596ed8fd6c10",' +
      '               "key":"test_boolean",' +
      '               "name":"Test boolean",' +
      '               "index":-4,' +
      '               "type":"BOOLEAN",' +
      '               "restrictedValues":[' +
      '                  ' +
      '               ]' +
      '            },' +
      '            "defaultValue":"",' +
      '            "mandatory":false,' +
      '            "editable":false' +
      '         },' +
      '         {' +
      '            "id":"subtype_97645_261f10a7-832b-48e6-a779-ce175a36cd5d",' +
      '            "metadata":{' +
      '               "id":"261f10a7-832b-48e6-a779-ce175a36cd5d",' +
      '               "key":"test_date",' +
      '               "name":"Test date",' +
      '               "index":0,' +
      '               "type":"DATE",' +
      '               "restrictedValues":[' +
      '                  "2021-03-02",' +
      '                  "2021-03-09"' +
      '               ]' +
      '            },' +
      '            "defaultValue":null,' +
      '            "mandatory":false,' +
      '            "editable":false' +
      '         }' +
      '      ],' +
      '      "digitalSignatureMandatory":true,' +
      '      "readingMandatory":false,' +
      '      "annexeIncluded":false,' +
      '      "multiDocuments":false' +
      '   },' +
      '   "originDesk":{' +
      '      "id":"dddd-dddd-dddd",' +
      '      "name":"Origin desk"' +
      '   },' +
      '   "finalDesk":{' +
      '      "id":"eeee-eeee-eeee",' +
      '      "name":"Final desk"' +
      '   },' +
      '   "dueDate":null,' +
      '   "metadata":{' +
      '      "workflow_internal_steps":"[]",' +
      '      "i_Parapheur_internal_emitter_id":"f383fd2d-7658-4175-a367-f62814ab93fb",' +
      '      "action":"i_Parapheur_internal_start",' +
      '      "workflow_internal_validation_start_date":"2021-05-28T09:41:09.954+0000"' +
      '   },' +
      '   "name":"Folder 01",' +
      '   "creationWorkflowDefinitionKey":null,' +
      '   "validationWorkflowDefinitionKey":null,' +
      '   "stepList":[' +
      '      {' +
      '         "id":"5c775f20-bffd-11eb-9ec8-0242ac12000f",' +
      '         "metadata":{' +
      '            "workflow_internal_origin_group_id":"f383fd2d-7658-4175-a367-f62814ab93fb",' +
      '            "workflow_internal_final_group_id":"f383fd2d-7658-4175-a367-f62814ab93fb",' +
      '            "workflow_internal_steps":"[]",' +
      '            "i_Parapheur_internal_emitter_id":"f383fd2d-7658-4175-a367-f62814ab93fb",' +
      '            "workflow_internal_validation_start_date":"2021-05-28T09:41:09.954+0000",' +
      '            "workflow_internal_validation_workflow_id":"simple_workflow_38738",' +
      '            "workflow_internal_workflow_index":"0"' +
      '         },' +
      '         "action":"START",' +
      '         "state":"VALIDATED",' +
      '         "desks":[' +
      '            {' +
      '               "id":"f383fd2d-7658-4175-a367-f62814ab93fb",' +
      '               "name":"Secrétariat du maire de Sajas",' +
      '               "shortName":null,' +
      '               "description":null,' +
      '               "tenantId":null,' +
      '               "level":null,' +
      '               "directChildrenCount":null,' +
      '               "parentDesk":null,' +
      '               "delegatingDesks":[' +
      '                  ' +
      '               ],' +
      '               "draftFoldersCount":-1,' +
      '               "rejectedFoldersCount":-1,' +
      '               "pendingFoldersCount":-1,' +
      '               "finishedFoldersCount":-1,' +
      '               "lateFoldersCount":-1,' +
      '               "isChecked":null' +
      '            }' +
      '         ],' +
      '         "user":{' +
      '            "id":"771aca98-5059-409c-8a16-6d273ea4c4c5",' +
      '            "userName":"user",' +
      '            "email":null,' +
      '            "firstName":null,' +
      '            "lastName":null,' +
      '            "notificationsCronFrequency":null,' +
      '            "notificationsRedirectionMail":null,' +
      '            "complementaryField":null,' +
      '            "signatureImageContentId":null,' +
      '            "privilege":null,' +
      '            "isChecked":null,' +
      '            "isLocked":null,' +
      '            "isLdapSynchronized":null,' +
      '            "rolesCount":null' +
      '         },' +
      '         "beginDate":"2021-05-28T19:40:46.389+00:00",' +
      '         "date":"2021-05-28T19:41:09.978+00:00",' +
      '         "readByCurrentUser":false' +
      '      },' +
      '      {' +
      '         "id":"6a8a6d28-bffd-11eb-9ec8-0242ac12000f",' +
      '         "metadata":{' +
      '            "workflow_internal_final_group_id":"f383fd2d-7658-4175-a367-f62814ab93fb",' +
      '            "workflow_internal_origin_group_id":"f383fd2d-7658-4175-a367-f62814ab93fb",' +
      '            "workflow_internal_validation_workflow_id":"simple_workflow_38738",' +
      '            "workflow_internal_workflow_index":"2",' +
      '            "workflow_internal_step_index":"0",' +
      '            "workflow_internal_current_candidate_groups":"0b4bd4ba-661e-4ff0-bedf-0042bd8e4c6c",' +
      '            "workflow_internal_previous_action":"i_Parapheur_internal_start",' +
      '            "workflow_internal_steps":"[]",' +
      '            "workflow_internal_previous_candidate_groups":"unknown",' +
      '            "i_Parapheur_internal_emitter_id":"f383fd2d-7658-4175-a367-f62814ab93fb",' +
      '            "action":"i_Parapheur_internal_start",' +
      '            "workflow_internal_validation_start_date":"2021-05-28T09:41:09.954+0000"' +
      '         },' +
      '         "action":"SEAL",' +
      '         "state":"CURRENT",' +
      '         "desks":[' +
      '            {' +
      '               "id":"0b4bd4ba-661e-4ff0-bedf-0042bd8e4c6c",' +
      '               "name":"Secrétariat à l\'adjoint aux festivités de Valframbert",' +
      '               "shortName":null,' +
      '               "description":null,' +
      '               "tenantId":null,' +
      '               "level":null,' +
      '               "directChildrenCount":null,' +
      '               "parentDesk":null,' +
      '               "delegatingDesks":[' +
      '                  ' +
      '               ],' +
      '               "draftFoldersCount":-1,' +
      '               "rejectedFoldersCount":-1,' +
      '               "pendingFoldersCount":-1,' +
      '               "finishedFoldersCount":-1,' +
      '               "lateFoldersCount":-1,' +
      '               "isChecked":null' +
      '            }' +
      '         ],' +
      '         "beginDate":"2021-05-28T19:41:10.000+00:00",' +
      '         "readByCurrentUser":false' +
      '      },' +
      '      {' +
      '         "metadata":{},' +
      '         "action":"ARCHIVE",' +
      '         "state":"PENDING",' +
      '         "desks":[' +
      '            {' +
      '               "id":"f383fd2d-7658-4175-a367-f62814ab93fb",' +
      '               "name":"Secrétariat du maire de Sajas",' +
      '               "shortName":null,' +
      '               "description":null,' +
      '               "tenantId":null,' +
      '               "level":null,' +
      '               "directChildrenCount":null,' +
      '               "parentDesk":null,' +
      '               "delegatingDesks":[' +
      '                  ' +
      '               ],' +
      '               "draftFoldersCount":-1,' +
      '               "rejectedFoldersCount":-1,' +
      '               "pendingFoldersCount":-1,' +
      '               "finishedFoldersCount":-1,' +
      '               "lateFoldersCount":-1,' +
      '               "isChecked":null' +
      '            }' +
      '         ],' +
      '         "readByCurrentUser":false' +
      '      }' +
      '   ],' +
      '   "documentList":[' +
      '      {' +
      '         "id":"59eded9e-6341-4063-aec8-bc171842f7d2",' +
      '         "name":"file",' +
      '         "index":0,' +
      '         "contentLength":207521,' +
      '         "mediaType":"application/pdf",' +
      '         "mediaVersion":null,' +
      '         "checksumAlgorithm":null,' +
      '         "checksumValue":null,' +
      '         "detachedSignatures":[' +
      '            ' +
      '         ],' +
      '         "isMainDocument":true' +
      '      }' +
      '   ],' +
      '   "draftCreationDate":"2021-05-28T19:40:46.387+00:00",' +
      '   "sentToArchivesDate":null,' +
      '   "state":null,' +
      '   "readByCurrentUser":false' +
      '}';

    const folder: Folder = JSON.parse(toParse);

    expect(folder).toBeTruthy();
    expect(folder.id).toBe('aaaa-aaaa-aaaa');
    expect(folder.name).toBe('Folder 01');
    expect(folder.originDesk.id).toBe('dddd-dddd-dddd');
    expect(folder.originDesk.name).toBe('Origin desk');
    expect(folder.finalDesk.id).toBe('eeee-eeee-eeee');
    expect(folder.finalDesk.name).toBe('Final desk');
    expect(folder.stepList.length).toBe(3);
    expect(new Date(folder.draftCreationDate).getTime()).toBe(1622230846387);
    expect(folder.sentToArchivesDate).toBeNull();
  });


});
