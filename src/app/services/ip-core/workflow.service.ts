/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from '../../models/task';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { DataConversionService } from '../data-conversion.service';
import { NotificationsService } from '../notifications.service';
import { CONFIG } from '../../shared/config/config';
import { ExternalSignatureParams, TypeDto, CreateFolderRequest } from '@libriciel/iparapheur-legacy';
import { Folder } from '../../models/folder/folder';
import { IpWorkflowInstance } from '../../models/workflows/ip-workflow-instance';
import { SubtypeDto, DeskDto, WorkflowDefinitionDto } from '@libriciel/iparapheur-provisioning';
import { WorkflowUtils } from '../../shared/utils/workflow-utils';
import { WorkflowService as internalWorkflowService } from '@libriciel/iparapheur-internal';

@Injectable({
  providedIn: 'root'
})
export class WorkflowService {


  constructor(public http: HttpClient,
              public dataConversionService: DataConversionService,
              public notificationsService: NotificationsService) { }


  requestExternalProcedure(params: ExternalSignatureParams, tenantId: string, deskId: string, folderId: string, task: Task): Observable<void> {
    return this.http
      .put<void>(`${CONFIG.BASE_API_URL}/tenant/${tenantId}/desk/${deskId}/folder/${folderId}/task/${task.id}/external_signature`, params)
      .pipe(catchError(this.notificationsService.handleHttpError('createExternalSignatureProcedure')));
  }

  getWorkflowInstance(folder: Folder): Observable<IpWorkflowInstance> {
    return of(this.dataConversionService.createWorkflowInstanceFromFolder(folder));
  }

  getValidationWorkflowFromSelectionScript(
    metadata: {},
    selectedType: TypeDto,
    selectedSubtype: SubtypeDto,
    internalWorkflowService: internalWorkflowService,
    tenantId: string,
    desk: DeskDto
  ): Observable<WorkflowDefinitionDto>  {

    if (!WorkflowUtils.everyMandatoryMetadataIsSet(selectedSubtype, metadata)) {
      console.log('updateValidationWorkflowFromSelectionScript - Missing some mandatory metadata here, skipping...');
      return of();
    }

    const folderRequest = {
      metadata: metadata,
      typeId: selectedType.id,
      subtypeId: selectedSubtype.id,
      name: ''
    } as CreateFolderRequest;

    return internalWorkflowService.evaluateWorkflowSelectionScript(tenantId, desk.id, folderRequest);
  }

}
