/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, AfterViewInit, Injector } from '@angular/core';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminTenantPopupComponent } from '../admin-tenant-popup/admin-tenant-popup-main-layout/admin-tenant-popup.component';
import { AdminTenantPopupNameEditComponent } from '../admin-tenant-popup/admin-tenant-popup-name-edit.component';
import { NotificationsService } from '../../../../../services/notifications.service';
import { AdminTenantDeletePopupComponent } from '../admin-tenant-delete-popup/admin-tenant-delete-popup.component';
import { CommonMessages } from '../../../../../shared/common-messages';
import { AdminMessages } from '../../admin-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { ActivatedRoute } from '@angular/router';
import { AdminTenantService, TenantSortBy, TenantRepresentation } from '@libriciel/iparapheur-provisioning';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { catchError } from 'rxjs/operators';
import { isCurrentVersion52OrAbove } from '../../../../../utils/string-utils';

@Component({
  selector: 'app-admin-tenant-list',
  templateUrl: './admin-tenant-list.component.html',
  styleUrls: ['./admin-tenant-list.component.scss']
})
export class AdminTenantListComponent implements AfterViewInit {

  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly caretUpIcon = faCaretUp;
  readonly caretDownIcon = faCaretDown;
  readonly sortByEnum = TenantSortBy;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = AdminMessages;
  readonly show52Features: boolean = isCurrentVersion52OrAbove();

  tenantList: TenantRepresentation[] = [];
  page = 1;
  pageSizeIndex = 1;
  total = 0;
  searchTerm = '';
  sortBy: TenantSortBy = TenantSortBy.Name;
  asc = true;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  loading = false;
  colspan: number;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public adminTenantService: AdminTenantService,
              private route: ActivatedRoute,
              public modalService: NgbModal) {}


  ngAfterViewInit() {
    this.route.parent.data.subscribe(data => this.userPreferences = data['userPreferences']);
    this.colspan = 2 + (this.userPreferences.showAdminIds ? 1 : 0);
    this.refreshTenantList(false);
  }


  // </editor-fold desc="LifeCycle">


  updateSearchTerm(searchTerm: string) {
    this.searchTerm = searchTerm;
    this.page = 1;
    this.refreshTenantList(false);
  }


  refreshTenantList(isNewPageSize: boolean) {

    const sortBy = [this.sortBy + (this.asc ? ',ASC' : ',DESC')];
    const searchTerm = this.searchTerm.length > 0 ? this.searchTerm : null;

    this.loading = true;
    if (isNewPageSize) {
      this.page = 1;
    }

    this.adminTenantService
      .listTenants(this.page - 1, this.getPageSize(this.pageSizeIndex), sortBy, searchTerm)
      .pipe(catchError(this.notificationsService.handleHttpError('listTenants')))
      .subscribe({
        next: tenantsRetrieved => {
          this.tenantList = tenantsRetrieved.content;
          this.total = tenantsRetrieved.totalElements;
        },
        error: e => this.notificationsService.showErrorMessage(AdminMessages.ERROR_REQUESTING_TENANTS, e)
      })
      .add(() => this.loading = false);
  }


  onCreateButtonClicked() {

    if (this.show52Features) {
      this.modalService
        .open(
          AdminTenantPopupComponent,
          {
            injector: Injector.create({
              providers: [
                {provide: AdminTenantPopupComponent.injectableTenantKey, useValue: null},
                {provide: AdminTenantPopupComponent.injectableUserPreferencesKey, useValue: this.userPreferences}
              ]
            })
          }
        )
        .result
        .then(
          () => this.refreshTenantList(false),
          () => { /* Dismissed */ }
        );
    } else {
      this.modalService
        .open(
          AdminTenantPopupNameEditComponent,
          {
            injector: Injector.create({
              providers: [
                {provide: AdminTenantPopupNameEditComponent.injectableTenantKey, useValue: null},
                {provide: AdminTenantPopupNameEditComponent.injectableUserPreferencesKey, useValue: this.userPreferences}
              ]
            })
          }
        )
        .result
        .then(
          () => this.refreshTenantList(false),
          () => { /* Dismissed */ }
        );
    }

  }


  onEditButtonClicked(tenant: TenantRepresentation) {
    if (this.show52Features) {

      this.loading = true;

      this.adminTenantService
        .getTenant(tenant.id)
        .pipe(catchError(this.notificationsService.handleHttpError('listTenants')))
        .subscribe({
          next: tenantRetrieved => {
            this.modalService
              .open(
                AdminTenantPopupComponent,
                {
                  injector: Injector.create({
                    providers: [
                      {provide: AdminTenantPopupComponent.injectableTenantKey, useValue: tenantRetrieved},
                      {provide: AdminTenantPopupComponent.injectableUserPreferencesKey, useValue: this.userPreferences}
                    ]
                  })
                }
              )
              .result
              .then(
                () => this.refreshTenantList(false),
                () => { /* Dismissed */ }
              );

          },
          error: e => this.notificationsService.showErrorMessage(AdminMessages.ERROR_REQUESTING_TENANTS, e)
        })
        .add(() => this.loading = false);

    } else {
      this.modalService
        .open(
          AdminTenantPopupNameEditComponent,
          {
            injector: Injector.create({
              providers: [
                {provide: AdminTenantPopupNameEditComponent.injectableTenantKey, useValue: tenant},
                {provide: AdminTenantPopupNameEditComponent.injectableUserPreferencesKey, useValue: this.userPreferences}
              ]
            })
          }
        )
        .result
        .then(
          () => this.refreshTenantList(false),
          () => { /* Dismissed */ }
        );
    }

  }


  onDeleteButtonClicked(tenant: TenantRepresentation) {
    this.modalService
      .open(
        AdminTenantDeletePopupComponent,
        {
          injector: Injector.create({providers: [{provide: AdminTenantDeletePopupComponent.injectableTenantKey, useValue: tenant}]}),
          size: 'lg'
        }
      )
      .result
      .then(
        () => this.refreshTenantList(false),
        () => { /* Dismissed */ }
      );
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  onRowOrderClicked(row: TenantSortBy) {
    this.asc = (row === this.sortBy) ? !this.asc : true;
    this.sortBy = row;
    this.refreshTenantList(false);
  }


}
