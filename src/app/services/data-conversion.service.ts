/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { StepValidationMode } from '../models/workflows/step-validation-mode';
import { StepDefinitionParallelType, StepDefinitionDto, WorkflowDefinitionDto } from '@libriciel/iparapheur-provisioning';
import { ActionNamePipe } from '../shared/utils/action-name.pipe';
import { WorkflowActor, WorkflowStep, WorkflowDefinition, WorkflowMetadata, WorkflowStepState } from '@libriciel/ls-workflow';
import { Action } from '@libriciel/iparapheur-internal';
import { WorkflowUtils } from '../shared/utils/workflow-utils';
import { Folder } from '../models/folder/folder';
import { IpWorkflowInstance } from '../models/workflows/ip-workflow-instance';
import { SecondaryAction } from '../shared/models/secondary-action.enum';
import { FolderUtils } from '../utils/folder-utils';
import { IpStepInstance } from '../models/workflows/ip-step-instance';
import { from, reduce } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IpWorkflowActor } from '../models/workflows/ip-workflow-actor';
import { Task } from '../models/task';

export enum StepType {
  Start = 'START',
  Visa = 'VISA',
  Signature = 'SIGNATURE',
  ExternalSignature = 'EXTERNAL_SIGNATURE',
  Ipng = 'IPNG',
  SecureMail = 'SECURE_MAIL',
  Seal = 'SEAL',
  SecondOpinion = 'SECOND_OPINION',
  End = 'END',
}

@Injectable({
  providedIn: 'root'
})
export class DataConversionService {

  public static workflowDefinitionToWorkflowDefinitionDto(workflowDefinition: WorkflowDefinition): WorkflowDefinitionDto {
    workflowDefinition.steps = workflowDefinition.steps.filter((step: WorkflowStep) => this.isStepValid(step));

    return {
      id: workflowDefinition.id,
      name: workflowDefinition.name,
      key: !!workflowDefinition.key ? workflowDefinition.key : DataConversionService.workflowNameToKey(workflowDefinition.name),
      steps: workflowDefinition.steps.map((step: WorkflowStep) => DataConversionService.workflowStepToStepDefinitionDto(step))
    };
  }

  private static isStepValid(step: WorkflowStep): boolean {
    return !!step.type && !!step.name && !!step.validators && step.validators.length > 0;
  }

  public static workflowStepToStepDefinitionDto(workflowStep: WorkflowStep): StepDefinitionDto {
    let validationMode: StepDefinitionParallelType = null;
    switch (workflowStep.validationMode) {
      case "OR":
        validationMode = StepDefinitionParallelType.Or;
        break;
      case "AND":
        validationMode = StepDefinitionParallelType.And;
        break;
    }

    return {
      type: workflowStep.type,
      validatingDeskIds: workflowStep.validators?.map((validator: WorkflowActor) => validator.id as string) || [],
      notifiedDeskIds: workflowStep.notifiedValidators?.map((validator: WorkflowActor) => validator.id as string) || [],
      mandatoryValidationMetadataIds: workflowStep.validationMetadata?.map((metadata: WorkflowMetadata) => metadata.id) || [],
      mandatoryRejectionMetadataIds: workflowStep.rejectionMetadata?.map((metadata: WorkflowMetadata) => metadata.id) || [],
      parallelType: validationMode
    };
  }


  public static workflowDefinitionDtoToWorkflowDefinition(workflowDefinitionDto: WorkflowDefinitionDto): WorkflowDefinition {
    console.debug('createWorkflowDefinitionFromDto, input : ', workflowDefinitionDto);

    return {
      id: workflowDefinitionDto.id,
      key: workflowDefinitionDto.key,
      name: workflowDefinitionDto.name,
      steps: workflowDefinitionDto.steps.map((step: StepDefinitionDto) => DataConversionService.stepDefinitionDtoToWorkflowStep(step))
    };
  }

  public static stepDefinitionDtoToWorkflowStep(stepDefinition: StepDefinitionDto): WorkflowStep {

    const result: WorkflowStep = {
      type: stepDefinition.type,
      name: stepDefinition.type === Action.Archive ? 'Fin de circuit' : ActionNamePipe.compute(stepDefinition.type),
      validationMode: (stepDefinition.parallelType == StepDefinitionParallelType.Or) ? StepValidationMode.Or : StepValidationMode.And,
      validators: !!stepDefinition.validatingDesks ? [...stepDefinition.validatingDesks] : [],
      notifiedValidators: !!stepDefinition.notifiedDesks ? [...stepDefinition.notifiedDesks] : [],
      validationMetadata: !!stepDefinition.mandatoryValidationMetadata ? [...stepDefinition.mandatoryValidationMetadata] : [],
      rejectionMetadata: !!stepDefinition.mandatoryRejectionMetadata ? [...stepDefinition.mandatoryRejectionMetadata] : []
    };

    if (result.validators?.length === 1) {
      result.validationMode = StepValidationMode.Simple;
    }

    result.validators.forEach((validator: WorkflowActor) => {
      switch (validator.id) {
        case WorkflowUtils.GENERIC_DESK_IDS.EMITTER_ID:
          validator.name = WorkflowUtils.GENERIC_DESK_NAMES.EMITTER;
          break;
        case WorkflowUtils.GENERIC_DESK_IDS.VARIABLE_DESK_ID:
          validator.name = WorkflowUtils.GENERIC_DESK_NAMES.VARIABLE_DESK;
          break;
        case WorkflowUtils.GENERIC_DESK_IDS.BOSS_OF_ID:
          validator.name = WorkflowUtils.GENERIC_DESK_NAMES.BOSS_OF;
          break;
      }
    });

    return result;
  }

  public createWorkflowInstanceFromFolder(folder: Folder): IpWorkflowInstance {
    console.log('createWorkflowInstanceFromFolder');

    let workflowInstance = null;
    const endActions: (Action | SecondaryAction)[] = [Action.Archive, Action.Delete, Action.Undo]; // TODO : Check why we have an undo here

    const instanceData: any = {};
    instanceData.id = folder.id;
    instanceData.name = folder.name;
    instanceData.finalDesk = folder.finalDesk;
    instanceData.isOver = folder.stepList
      .filter(t => !endActions.includes(t.action))
      .filter(t => !FolderUtils.isPassed(t))
      .length <= 0;

    if (!folder.stepList) {
      return instanceData;
    }

    const stepInstanceList: IpStepInstance[] = [];

    from(folder.stepList)
      .pipe(
        filter((task: Task) => Object.values(StepType).includes(task.action as unknown as StepType)),
        map((task: Task) => {
          const actor: IpWorkflowActor = new IpWorkflowActor(task.user);
          const stepInstance: IpStepInstance = new IpStepInstance({
            id: task.id,
            name: task.action,
            type: this.actionToStep(task.action),
            validators: task.desks as IpWorkflowActor[],
            delegatedByDesk: task.delegatedByDesk,
            actedUponBy: actor,
            state: task.state.toUpperCase() as WorkflowStepState,
            workflowIndex: task.workflowIndex,
            stepIndex: task.stepIndex
          });
          stepInstance.completedDate = task.date;
          return stepInstance;
        }),
        reduce(
          (acc: IpStepInstance[], val: IpStepInstance) => {
            acc.push(val);
            return acc;
          },
          stepInstanceList
        )
      )
      .subscribe((stepList: IpStepInstance[]) => {
        instanceData.steps = stepList;
        workflowInstance = new IpWorkflowInstance(instanceData);
      });

    return workflowInstance;
  }

  public actionToStep(action: Action | SecondaryAction): StepType {
    switch (action) {
      case SecondaryAction.End: { return StepType.End; }
      case Action.ExternalSignature: { return StepType.ExternalSignature; }
      case Action.Ipng: { return StepType.Ipng; }
      case Action.Seal: { return StepType.Seal; }
      case Action.SecondOpinion: { return StepType.SecondOpinion; }
      case Action.SecureMail: { return StepType.SecureMail; }
      case Action.Signature: { return StepType.Signature; }
      case Action.Start: { return StepType.Start; }
      case Action.Visa: { return StepType.Visa; }
      default: { return null; }
    }
  }

  public static workflowNameToKey(name: string): string {

    // Having a number as the first char is forbidden, we'll prefix it with a '_'
    if (/^\d/.test(name)) {
      name = '_' + name;
    }

    // Replace every character that is not alphanumeric, '_', '.' or '-'
    return name.replace(/[^a-z\d_.-]/gi, '_').toLowerCase();
  }


}
