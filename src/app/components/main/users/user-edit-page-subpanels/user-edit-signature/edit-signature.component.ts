/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CommonIcons } from '@libriciel/ls-elements';
import { UserEditSubPanelsMessages } from '../user-edit-sub-panels-messages';
import { CommonMessages } from '../../../../../shared/common-messages';
import { DomSanitizer } from '@angular/platform-browser';
import { UserDto } from '@libriciel/iparapheur-provisioning/model/userDto';

@Component({
  selector: 'app-edit-signature',
  templateUrl: './edit-signature.component.html',
  styleUrls: ['./edit-signature.component.scss']
})
export class EditSignatureComponent {


  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = UserEditSubPanelsMessages;

  @Input() isAdmin: boolean;
  @Input() signatureImageSrc: string | ArrayBuffer;
  @Output() signatureImageSrcChange: EventEmitter<string | ArrayBuffer> = new EventEmitter<string | ArrayBuffer>();

  @Output() signatureChangedEvent: EventEmitter<File> = new EventEmitter<File>();
  @Output() signatureDeleteEvent: EventEmitter<UserDto> = new EventEmitter<UserDto>();


  // <editor-fold desc="LifeCycle">


  constructor(public sanitizer: DomSanitizer) { }


  // </editor-fold desc="LifeCycle">


  // <editor-fold desc="UI Callbacks">


  onAddImageButtonClicked(event: Event) {

    const selectedFiles: FileList = (event.target as HTMLInputElement).files;
    const file: File = selectedFiles.item(0);

    const reader: FileReader = new FileReader();
    reader.onloadend = (event: ProgressEvent<FileReader>) => {
      this.signatureImageSrc = event.target.result;
      this.signatureImageSrcChange.emit(this.signatureImageSrc);
    };

    reader.readAsDataURL(file);

    this.signatureChangedEvent.emit(file);
  }


  onDeleteImageButtonClicked() {
    this.signatureDeleteEvent.emit();
  }


  // </editor-fold desc="UI Callbacks">


}
