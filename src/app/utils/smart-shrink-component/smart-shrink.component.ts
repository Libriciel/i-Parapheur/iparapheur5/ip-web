/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ChangeDetectionStrategy, Input } from "@angular/core";
import { Component } from "@angular/core";


/**
 * @author Original code from bennadel.com, under an MIT licence
 */
@Component({
  selector: "app-smart-shrink",
  host: {
    "[title]": "fullText"
  },
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: [ "./smart-shrink.component.scss" ],
  templateUrl: './smart-shrink.component.html',
})
export class SmartShrinkComponent {

  public fullText: string;
  public leftText: string;
  public rightText: string;


  constructor() {

    this.leftText = "";
    this.rightText = "";

  }

  @Input()
  set text( value: string ) {

    // We're going to split the string towards the end. This is just a judgment call.
    // Since we can't dynamically change the split as the container changes size (at
    // least, not with a lot more work), we have to pick a location that scales the
    // ellipsis well.
    const splitIndex = Math.round(value.length * 0.75);

    this.fullText = value;
    this.leftText = value.slice( 0, splitIndex );
    this.rightText = value.slice( splitIndex );

  }

}


