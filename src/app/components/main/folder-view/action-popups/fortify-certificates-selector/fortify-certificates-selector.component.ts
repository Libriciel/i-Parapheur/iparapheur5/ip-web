/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, AfterViewInit, Output, EventEmitter, Input, ViewChild, ElementRef, OnChanges } from '@angular/core';
import { PeculiarFortifyCertificatesCustomEvent } from '@peculiar/fortify-webcomponents';
import { ISelectionSuccessEvent } from '@peculiar/fortify-webcomponents/dist/types/components/fortify-certificates/fortify-certificates';
import { CryptoKey } from 'webcrypto-core';
import { SocketCrypto } from '@webcrypto-local/client';
import { from, combineLatest, Observable, of } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';
import { Convert } from 'pvtsutils';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonMessages } from '../../../../../shared/common-messages';
import { FortifyCertificateData } from '../../../../../models/fortify-certificate-data';
import { NotificationsService } from '../../../../../services/notifications.service';
import { FortifyMessages } from './fortify-messages';
import { NgChanges } from '../../../../../shared/models/custom-types';

@Component({
  selector: 'app-fortify-certificates-selector',
  templateUrl: './fortify-certificates-selector.component.html',
  styleUrls: ['./fortify-certificates-selector.component.scss']
})
export class FortifyCertificatesSelectorComponent implements AfterViewInit, OnChanges {
  @ViewChild('fortifyCertificatesContainer') fortifyCertificatesContainer: ElementRef;
  @Input() dataToSign: string[] = [];
  @Output() certChosen: EventEmitter<FortifyCertificateData> = new EventEmitter<FortifyCertificateData>();
  @Output() signatureResult: EventEmitter<string[]> = new EventEmitter<string[]>();

  readonly SIGNATURE_ALGORITHM: string = 'RSASSA-PKCS1-v1_5';
  readonly HASH_ALGORITHM: string = 'SHA-256';

  provider: SocketCrypto;
  privateKey: CryptoKey;

  constructor(private notificationsService: NotificationsService, private activeModal: NgbActiveModal) { }

  ngAfterViewInit(): void {
    this.configureFortifyHtmlElement();
  }


  ngOnChanges(changes: NgChanges<any>): void {
    if (changes.dataToSign?.currentValue) this.onDataToSignUpdate();
  }

  emitOnCertChosenEvent(event: PeculiarFortifyCertificatesCustomEvent<ISelectionSuccessEvent>): void {
    if (!event.detail.providerId)
      return this.notificationsService.showErrorMessage(FortifyMessages.NO_PROVIDER_ID_ERROR);
    if (!event.detail.certificateId)
      return this.notificationsService.showErrorMessage(FortifyMessages.NO_CERTIFICATE_ID_ERROR);
    if (!event.detail.privateKeyId)
      return this.notificationsService.showErrorMessage(FortifyMessages.NO_PRIVATE_KEY_ID_ERROR);

    from(event.detail.socketProvider.getCrypto(event.detail.providerId))
      .pipe(tap(provider => this.provider = provider))
      .pipe(mergeMap(provider => combineLatest([
        of(provider),
        from(provider.certStorage.getItem(event.detail.certificateId)),
        from(provider.keyStorage.getItem(event.detail.privateKeyId))
      ])))
      .pipe(tap((data: any[]) => this.privateKey = data[2]))
      .pipe(mergeMap(([provider, certificate]) => combineLatest([
        of(certificate),
        from(provider.certStorage.exportCert('pem', certificate))
      ])))
      .subscribe(([certificate, certBase64]) => this.certChosen.emit({
        certificate: certificate,
        certBase64: this.fixBase64Certificate(certBase64 as string)
      }));
  }


  onDataToSignUpdate(): void {
    combineLatest(this.dataToSign.map((data: string) => this.sign(data)))
      .subscribe({
        next: (data: string[]) => this.signatureResult.emit(data),
        error: e => this.notificationsService.showErrorMessage(e)
      });
  }


  sign(dataToSign: string): Observable<string> {
    return from(
      this.provider.subtle.sign(
        {
          name: this.SIGNATURE_ALGORITHM,
          hash: {name: this.HASH_ALGORITHM}
        },
        this.privateKey,
        Convert.FromBase64(dataToSign))
    ).pipe(map(signature => btoa(String.fromCharCode.apply(null, new Uint8Array(signature)))));
  }


  configureFortifyHtmlElement(): void {
    const peculiarFortifyCertificates: HTMLPeculiarFortifyCertificatesElement =
      document.createElement('peculiar-fortify-certificates') as HTMLPeculiarFortifyCertificatesElement;

    peculiarFortifyCertificates.hideFooter = true;
    peculiarFortifyCertificates.language = 'fr';

    peculiarFortifyCertificates.addEventListener(
      'selectionCancel',
      () => this.activeModal.dismiss(CommonMessages.ACTION_RESULT_CANCEL)
    );

    peculiarFortifyCertificates.addEventListener(
      'selectionSuccess',
      (event: PeculiarFortifyCertificatesCustomEvent<ISelectionSuccessEvent>) => this.emitOnCertChosenEvent(event)
    );

    this.fortifyCertificatesContainer.nativeElement.append(peculiarFortifyCertificates);
  }


  fixBase64Certificate(cert: string): string {
    return cert
      .replace('-----BEGIN CERTIFICATE-----', '')
      .replace('-----END CERTIFICATE-----', '')
      .replace(/(\r\n|\n|\r)/gm, "")
      .trim();
  }

}
