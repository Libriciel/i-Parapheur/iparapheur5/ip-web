/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Task } from '../task';
import { Document } from '../document';
import { SignatureProof } from '../signatureProof';
import { HasId } from '../has-id';
import { HasName } from '../has-name';
import { State, FolderVisibility, DeskRepresentation, TypeDto, SubtypeDto, FolderDto } from '@libriciel/iparapheur-legacy';
import { UserRepresentation } from '@libriciel/iparapheur-provisioning/model/userRepresentation';
import { User } from '../auth/user';


export class Folder implements HasId, HasName {

  id: string;
  name: string;
  stepList: Array<Task>;
  documentList: Array<Document>;
  signatureProofList: Array<SignatureProof>;
  metadata: Map<string, string>;
  state: State;
  originUser: UserRepresentation;
  originDesk: DeskRepresentation;
  finalDesk: DeskRepresentation;

  dueDate: Date;
  draftCreationDate: Date;
  sentToArchivesDate: Date;

  type: TypeDto;
  subtype: SubtypeDto;
  readByCurrentUser: boolean;
  selected: boolean = false;
  populatedMandatoryStepMetadata: { [p: string]: string } = {};
  visibility: FolderVisibility;


  /**
   * Deep parse, to get proper typing
   * @param folderDto
   */
  static fromDto(folderDto: FolderDto): Folder {
    const folder: Folder = Object.assign(new Folder(), folderDto);
    folder.stepList.forEach((t, i) => {
      folder.stepList[i] = Object.assign(new Task(), t);
      folder.stepList[i].user = Object.assign(new User(), t.user);
      folder.stepList[i].beginDate = !!t.beginDate ? new Date(t.beginDate) : null;
      folder.stepList[i].date = !!t.date ? new Date(t.date) : null;
    });
    return folder;
  }


}
