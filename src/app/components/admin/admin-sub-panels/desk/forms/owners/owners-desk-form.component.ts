/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, ViewChild, OnChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { DeskMessages } from '../../desk-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { DoubleListComponent } from '../../../../../../shared/components/double-list/double-list.component';
import { stringifyUser } from '../../../../../../utils/string-utils';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { DeskDto, AdminTenantUserService, PageListableUser, UserSortBy, UserRepresentation } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-owners-desk-form',
  templateUrl: './owners-desk-form.component.html',
  styleUrls: ['./owners-desk-form.component.scss']
})
export class OwnersDeskFormComponent implements OnChanges {

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = DeskMessages;
  readonly stringifyUserFn = stringifyUser;

  @Input() desk: DeskDto;
  @Input() tenantId: string;
  @Input() owners: UserRepresentation[] = [];
  @ViewChild('doubleListComponent') doubleListComponent: DoubleListComponent<UserRepresentation>;

  currentSearch: string = null;
  unmodifiableElements: UserRepresentation[] = [];


  // <editor-fold desc="LifeCycle">


  constructor(private adminTenantUserService: AdminTenantUserService) {}


  ngOnChanges(): void {
    // FIXME : Link this to the KEYCLOAK_CLIENT env variable
    this.unmodifiableElements = this.owners.filter(o => o.userName === "service-account-ipcore-api");
  }


  // </editor-fold desc="LifeCycle">


  onSearchKeyUp(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentSearch = newTerm;
    this.doubleListComponent.requestElements(true);
  }


  requestDeskUsersFn = (page: number, pageSize: number): Observable<PageListableUser> => {
    const searchTerm = this.currentSearch?.length > 0 ? this.currentSearch : null;
    const sortBy = [`${UserSortBy.Username.toString()},ASC`];

    return this.adminTenantUserService
      .listTenantUsers(this.tenantId, page, pageSize, sortBy, searchTerm, this.desk.id);
  };


}
