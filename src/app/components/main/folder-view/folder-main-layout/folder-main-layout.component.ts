/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, ViewChild, Injector, ElementRef } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Folder } from '../../../../models/folder/folder';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AnnotationPopupComponent } from '../annotation-popup/annotation-popup.component';
import { Document } from '../../../../models/document';
import { DocumentService } from '../../../../services/ip-core/document.service';
import { WorkflowService } from '../../../../services/ip-core/workflow.service';
import { Config } from '../../../../config';
import { Observable, forkJoin, concat } from 'rxjs';
import { PdfJsParameters } from '../../../../models/annotation/pdf-js-parameters';
import { FolderService } from '../../../../services/ip-core/folder.service';
import { CrudOperation } from '../../../../services/crud-operation';
import { MetadataEvent } from '../metadata-list/metadata-list.component';
import { NotificationsService } from '../../../../services/notifications.service';
import { FolderViewMessages } from '../folder-view-messages';
import { IpStepInstance } from '../../../../models/workflows/ip-step-instance';
import { Metadata } from '../../../../models/metadata';
import { CommonMessages } from '../../../../shared/common-messages';
import { faMinusSquare, faPlusSquare } from '@fortawesome/free-regular-svg-icons';
import { MetadataService, FolderViewBlock, UserPreferencesDto, ExternalSignatureProvider } from '@libriciel/iparapheur-legacy';
import { TenantDto, TemplateType, SignatureFormat, TemplateInfo, DeskDto } from '@libriciel/iparapheur-provisioning';
import { DocumentService as InternalDocumentService, StickyNote, State, Action, PdfSignaturePosition, SignaturePlacement, ServerInfoDto } from '@libriciel/iparapheur-internal';
import { FolderUtils } from '../../../../utils/folder-utils';
import { map, catchError } from 'rxjs/operators';
import { Task } from '../../../../models/task';
import { CustomNumberMap, CustomPdfJsEvent } from '../../../../shared/models/custom-types';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';
import { IpWorkflowInstance } from '../../../../models/workflows/ip-workflow-instance';
import { ExternalState } from '../../../../models/external-state.enum';
import { MetadataFormComponent } from '../metadata-form/metadata-form.component';
import { HttpClient } from '@angular/common/http';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import RectangleOriginEnum = SignaturePlacement.RectangleOriginEnum;
import { Title } from '@angular/platform-browser';
import { isCurrentVersion52OrAbove } from '../../../../utils/string-utils';
import { LegacyUserService } from '../../../../services/ip-core/legacy-user.service';

@Component({
  selector: 'app-folder-main-layout',
  templateUrl: './folder-main-layout.component.html',
  styleUrls: ['./folder-main-layout.component.scss']
})
export class FolderMainLayoutComponent implements OnInit {


  private currentWorkflowIndex: number = 0;
  private apiVersion: string = Config.API_VERSION;
  private pdfJsParameters: PdfJsParameters = {
    annotationButtonsParameters: {
      hideToolbar: false,
      hideAnnotation: false,
      hideSignaturePlacement: false,
      hideDraw: true,
      hideTextAnnotation: true
    },
    signaturePlacementAnnotations: [],
    annotationOptions: {
      width: Config.STAMP_WIDTH,
      height: Config.STAMP_HEIGHT,
      rectangleOrigin: 'CENTER'
    }
  };

  readonly show52Features: boolean = isCurrentVersion52OrAbove();
  readonly folderViewBlockEnum = FolderViewBlock;
  readonly messages = FolderViewMessages;
  readonly commonMessages = CommonMessages;
  readonly minusSquareIcon: IconDefinition = faMinusSquare;
  readonly plusSquareIcon: IconDefinition = faPlusSquare;


  @ViewChild('pdfJsViewer') pdfViewer;
  @ViewChild('pesViewerFrame') pesViewerFrame: ElementRef;

  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  serverInfo: ServerInfoDto;
  workflowInstance: IpWorkflowInstance = {} as IpWorkflowInstance;
  currentDocument: Document;
  folder: Folder;
  currentDesk: DeskDto;
  asOtherDeskId: string;
  stepMetadata: Metadata[] = [];

  isMetadataSectionCollapsed: boolean = false;
  annotationsLoaded: boolean = false;
  tenant: TenantDto;
  currentPdfSource: Blob;
  isPesViewerActive: boolean = false;
  isPesFolder: boolean = false;
  countMissingMetadata: number;
  showStepMetadata: boolean = false;

  defaultTemplateType: TemplateType = TemplateType.SignatureMedium;
  defaultTemplateInfo: TemplateInfo;

  isCurrentUserDeskOwner: boolean = false;

  modificationEnabled: boolean = false;

  // <editor-fold desc="LifeCycle">


  constructor(private workflowService: WorkflowService,
              private documentService: DocumentService,
              private legacyUserService: LegacyUserService,
              private internalDocumentService: InternalDocumentService,
              private metadataService: MetadataService,
              private folderService: FolderService,
              private notificationService: NotificationsService,
              private titleService: Title,
              public modalService: NgbModal,
              public http: HttpClient,
              public route: ActivatedRoute) {}


  ngOnInit(): void {
    this.userPreferences = this.route.snapshot.data["userPreferences"];
    this.serverInfo = this.route.snapshot.data["serverInfo"];

    const queryParamMap: ParamMap = this.route.snapshot.queryParamMap;
    this.asOtherDeskId = queryParamMap.get(FolderUtils.AS_DESK_QUERY_PARAM_NAME);
    this.route.data.subscribe(data => {
      this.folder = null;
      setTimeout(() => {
        this.tenant = data.tenant;
        this.currentDesk = data.desk;
        this.folder = data.folder;
        this.isCurrentUserDeskOwner = this.legacyUserService.isCurrentUserDeskOwner(this.tenant.id, this.currentDesk.id);
        this.titleService.setTitle(this.folder.name);
        this.initializeWithNewFolder();


        const firstSignificantTask: Action | SecondaryAction = this.folder.stepList
          .filter(step => FolderUtils.isCurrentOrUpcoming(step))
          .find(step => FolderUtils.isSealOrSign(step))?.action;

        if (!!firstSignificantTask) {
          this.defaultTemplateType = firstSignificantTask == Action.Seal
            ? this.userPreferences.defaultSealTemplate || TemplateType.SealMedium
            : this.userPreferences.defaultSignatureTemplate || TemplateType.SignatureMedium;

          this.defaultTemplateInfo = this.tenant.templateInfoMap[this.defaultTemplateType];
        }
      });
    });
  }


  private initializeWithNewFolder(): void {
    this.workflowService
      .getWorkflowInstance(this.folder)
      .subscribe({
        next: workflowInstance => {
          this.workflowInstance = workflowInstance as IpWorkflowInstance;
          const currentStep: IpStepInstance = this.workflowInstance.steps
            .filter(step => step.state.toUpperCase() === State.Current.toUpperCase())
            .map(s => s as IpStepInstance)
            .pop();
          this.currentWorkflowIndex = currentStep?.workflowIndex;

          this.parseMetadata();

          if (this.folder.documentList.length > 0) {
            this.onDocumentSelected(this.folder.documentList.filter(d => d.isMainDocument)[0]);
          }
          this.isPesFolder = this.folder.type.protocol === "HELIOS";
          this.modificationEnabled = (!this.isFolderStarted() || this.isInCreationWorkflow()) && this.isCurrentUserDeskOwner;
          this.initAnnotationButtons();
        },
        error: e => this.notificationService.showErrorMessage(this.messages.ERROR_LOADING_WORKFLOW, e?.message)
      });
  }


  refreshFolder() {
    const currentFolderId: string = this.folder.id;
    this.folder = null;

    this.folderService
      .getFolder(this.tenant.id, currentFolderId, this.currentDesk.id)
      .subscribe(newFolder => {
        this.folder = newFolder;
        this.initializeWithNewFolder();
      });
  }


  // </editor-fold desc="LifeCycle">


  // <editor-fold desc="Annotations">


  onAnnotationEvent(event: any): void {
    switch (event.type) {

      case 'pageChange':
      case 'annotationMode' :
      case 'signaturePlacementMode' :
        // Not used
        break;

      case 'annotationCreated' :
      case 'annotationClick' :
        const stickyNote: StickyNote = {} as StickyNote;
        this.fillAnnotation(stickyNote, event);
        stickyNote.content = (event as CustomPdfJsEvent).value.contentsObj?.str;
        this.onAnnotationSelected(stickyNote, event.type);
        break;

      case 'signaturePlacementCreated' :
      case 'signaturePlacementClick' :
        const signaturePlacement: SignaturePlacement = {} as SignaturePlacement;
        this.fillAnnotation(signaturePlacement, event);
        signaturePlacement.signatureNumber = event.value.signatureNumber;
        this.onAnnotationSelected(signaturePlacement, event.type);
        break;

      default:
        console.log('Unknown annotation eventType:' + event.type);
    }
  }


  fillAnnotation(annotation: any, event: any): void {
    const eventAnnotation = event.value.annotation || event.value;
    const rect = eventAnnotation.rect;

    annotation.id = eventAnnotation.identifierName;
    annotation.page = eventAnnotation.page;
    annotation.pageRotation = eventAnnotation.pageRotation;
    annotation.pageWidth = eventAnnotation.pageWidth;
    annotation.pageHeight = eventAnnotation.pageHeight;
    annotation.rectangleOrigin = eventAnnotation.rectangleOrigin;

    annotation.x = rect[0];
    annotation.y = rect[1];

    let width: number;
    let height: number;
    if (annotation.pageRotation === 0 || annotation.pageRotation === 180) {
      width = Math.abs(rect[2] - rect[0]);
      height = Math.abs(rect[3] - rect[1]);
    } else {
      width = Math.abs(rect[3] - rect[1]);
      height = Math.abs(rect[2] - rect[0]);
    }

    const annotationSize = this.getAnnotationSizeBasedOnExternalProviders(width, height);
    annotation.width = annotationSize.width;
    annotation.height = annotationSize.height;

    if (annotation.rectangleOrigin == 'BOTTOM_LEFT' || annotation.rectangleOrigin == RectangleOriginEnum.BottomLeft) {
      this.switchCoordinatesToCenterFromRect(annotation, rect);
      annotation.rectangleOrigin = 'CENTER';
    }

  }


  private onAnnotationSelected(annotation: any, type: string): void {
    if (this.modalService.hasOpenModals()) {
      return;
    }

    let annotationToOpen: any = annotation;
    const isSignaturePlacement: boolean = !Object.keys(annotation).includes("content");
    const actionList: (Action | SecondaryAction)[] = this.folder.stepList.map(task => task.action);
    const populatedSignatureAnnotation: SignaturePlacement = this.pdfJsParameters.signaturePlacementAnnotations.length > 0
      ? this.pdfJsParameters.signaturePlacementAnnotations[0]
      : null;

    if (type === "signaturePlacementClick") {
      annotationToOpen = populatedSignatureAnnotation;
    }

    const currentTasks: Task[] = this.folder.stepList.filter(step => step.state === State.Current);
    const currentTaskIsExternalSignature: boolean = currentTasks.every(task => task.action === Action.ExternalSignature);

    this.modalService
      .open(AnnotationPopupComponent, {
          injector: Injector.create({
            providers: [
              {provide: AnnotationPopupComponent.injectableSignaturePlacementKey, useValue: isSignaturePlacement ? annotationToOpen : null},
              {provide: AnnotationPopupComponent.injectableStickyNoteKey, useValue: isSignaturePlacement ? null : annotationToOpen},
              {provide: AnnotationPopupComponent.injectableUserPreferencesKey, useValue: this.userPreferences},
              {provide: AnnotationPopupComponent.injectablePendingActionListKey, useValue: actionList},
              {provide: AnnotationPopupComponent.injectableTypeKey, useValue: type},
              {provide: AnnotationPopupComponent.injectableTenantIdKey, useValue: this.tenant.id},
              {provide: AnnotationPopupComponent.injectableLastTemplateTypeKey, useValue: populatedSignatureAnnotation?.templateType},
              {
                provide: AnnotationPopupComponent.injectableReadOnlyKey,
                useValue: !this.legacyUserService.isCurrentUserDeskOwner(this.tenant.id, this.currentDesk.id)
              },
              {
                provide: AnnotationPopupComponent.injectableExternalActionProviderKey,
                useValue: currentTaskIsExternalSignature ? this.folder.subtype?.externalSignatureConfig?.serviceName : null
              }
            ]
          })
        }
      )
      .result
      .then(
        result => {
          switch (result.value) {
            case 'success' : {
              if (isSignaturePlacement) {
                this.transform(result.annotation);
              }

              const request$: Observable<void> = isSignaturePlacement
                ? this.internalDocumentService
                  .createSignaturePlacementAnnotation(
                    this.tenant.id,
                    this.folder.id,
                    this.currentDocument.id,
                    FolderMainLayoutComponent.createSignaturePlacementParameter(result.annotation)
                  )
                  .pipe(catchError(this.notificationService.handleHttpError('createSignaturePlacementAnnotation')))
                : this.internalDocumentService
                  .createAnnotation(
                    this.tenant.id,
                    this.folder.id,
                    this.currentDocument.id,
                    FolderMainLayoutComponent.createStickyNoteParameter(result.annotation)
                  )
                  .pipe(catchError(this.notificationService.handleHttpError('createAnnotation')));

              request$.subscribe(() => {
                this.refreshViewer();
                console.log('Annotation saved : ' + result.annotation.id);
              });
              break;
            }
            case  'delete': {
              this.internalDocumentService
                .deleteAnnotation(this.tenant.id, this.folder.id, this.currentDocument.id, result.annotation.id)
                .pipe(catchError(this.notificationService.handleHttpError('deleteAnnotation')))
                .subscribe(() => {
                  console.log('Annotation deleted : ' + result.annotation.id);
                  this.refreshViewer();
                });
              break;
            }

            default: {
              if (type === "annotationCreated" || type === "signaturePlacementCreated") {
                this.refreshViewer();
              }
            }
          }
        },
        () => {
          if (type === "annotationCreated" || type === "signaturePlacementCreated") {
            this.refreshViewer();
          }
        }
      );
  }


  private refreshViewer(): void {
    this.annotationsLoaded = false;

    const usePdfVisual: boolean = this.shouldUsePdfVisual();

    const retrieveDocument$: Observable<Blob> = usePdfVisual
      ? this.documentService.getPdfVisualForDocument(this.tenant.id, this.folder.id, this.currentDocument.id)
      : this.documentService.getDocument(this.tenant.id, this.folder.id, this.currentDocument.id);

    const requests$: Observable<any>[] = [
      retrieveDocument$
    ];

    const isMainDocument = this.currentDocument.isMainDocument;
    const hasVisualAnnotation = this.currentDocument.mediaType == "application/pdf" || this.currentDocument.mediaType == "application/octet-stream";
    if (isMainDocument && hasVisualAnnotation) {
      const retrieveSignaturePlacementAnnotations$ = this.internalDocumentService
        .getSignaturePlacementAnnotations(this.tenant.id, this.folder.id, this.currentDocument.id)
        .pipe(catchError(this.notificationService.handleHttpError('getSignaturePlacementAnnotations')));
      requests$.push(retrieveSignaturePlacementAnnotations$);
    }

    forkJoin(requests$)
      .subscribe({
        next: result => {
          const annotations = result[1];
          this.pdfJsParameters.signaturePlacementAnnotations = annotations || [];
          this.currentDocument.signaturePlacementAnnotations = annotations || [];

          this.refreshDocumentWithBlobData(this.currentDocument, result[0]);
        },
        error: () => console.debug("Error retrieving annotations.")
      })
      .add(() => {
        this.setSignatureAnnotationParameters(this.currentDocument);
        this.pdfJsParametersToLocalStorage();
        this.annotationsLoaded = true;
        this.pdfViewer?.refresh();
      });
  }


  private shouldUsePdfVisual(): boolean {
    if (!this.currentDocument.pdfVisualId) {
      return false;
    }

    if (this.currentDocument.isMainDocument && this.folder.type?.signatureFormat === SignatureFormat.Pades) {
      // if in PAdES and the validation workflow started, the 'original' doc is now a PDF, and we should not use pdf visual
      return !FolderUtils.isInValidationWorkflow(this.folder);
    }

    return true;
  }

  private initAnnotationButtons(): void {

    if (!this.currentDesk?.actionAllowed) {
      this.pdfJsParameters.annotationButtonsParameters.hideAnnotation = true;
      this.pdfJsParameters.annotationButtonsParameters.hideSignaturePlacement = true;
    } else {
      switch (this.folder.type?.signatureFormat ?? null) {

        case SignatureFormat.XadesDetached:
        case SignatureFormat.Pkcs7:
          this.pdfJsParameters.annotationButtonsParameters.hideAnnotation = true;
          this.pdfJsParameters.annotationButtonsParameters.hideSignaturePlacement = true;
          break;

        case SignatureFormat.Pades:
        case SignatureFormat.PesV2: // Useless case here, since we are going for the PES-Viewer, but we don't want to log the default case warning
          this.pdfJsParameters.annotationButtonsParameters.hideAnnotation = !this.folder.subtype?.annotationsAllowed;

          const currentTask: Task = this.folder.stepList.filter(step => step.state === State.Current).pop();
          this.pdfJsParameters.annotationButtonsParameters.hideSignaturePlacement =
            !currentTask
            || currentTask.externalState === ExternalState.Active;
          break;

        case SignatureFormat.Auto: // It shall be evaluated server-side, and never appear here
        default:
          console.log('initAnnotationButtons: something went wrong on signature format evaluation, disabling everything annotation-like...');
          this.pdfJsParameters.annotationButtonsParameters.hideAnnotation = true;
          this.pdfJsParameters.annotationButtonsParameters.hideSignaturePlacement = true;
          break;
      }
    }

    if (this.folder.stepList
      .filter(step => FolderUtils.CRYPTOGRAPHIC_ACTIONS.includes(step.action))
      .filter(step => !FolderUtils.isPassed(step))
      .length === 0) {
      this.pdfJsParameters.annotationButtonsParameters.hideSignaturePlacement = true;
    }
    this.pdfJsParametersToLocalStorage();
  }


  pdfJsParametersToLocalStorage(): void {
    this.pdfJsParameters.signaturePlacementAnnotations
      .filter(annotation => !!annotation?.id)
      .forEach(annotation => this.transform(annotation, true));

    localStorage.setItem("signaturePlacementAnnotations", JSON.stringify(this.pdfJsParameters.signaturePlacementAnnotations));
    localStorage.setItem("annotationButtonsParameters", JSON.stringify(this.pdfJsParameters.annotationButtonsParameters));
    localStorage.setItem("annotationOptions", JSON.stringify(this.pdfJsParameters.annotationOptions));
  }


  private static createSignaturePlacementParameter(annotation: any): SignaturePlacement {
    return {
      id: annotation.id,
      signatureNumber: annotation.signatureNumber,
      templateType: annotation.templateType,
      page: annotation.page,
      pageRotation: annotation.pageRotation,
      rectangleOrigin: annotation.rectangleOrigin || 'BOTTOM_LEFT',
      pageWidth: annotation.pageWidth,
      pageHeight: annotation.pageHeight,
      width: annotation.width,
      height: annotation.height,
      x: annotation.x,
      y: annotation.y,
    } as SignaturePlacement;
  }


  private static createStickyNoteParameter(annotation: any): StickyNote {
    return {
      id: annotation.id,
      content: annotation.content,
      page: annotation.page,
      pageRotation: annotation.pageRotation,
      rectangleOrigin: 'BOTTOM_LEFT',
      width: annotation.width,
      height: annotation.height,
      x: annotation.x,
      y: annotation.y,
    } as StickyNote;
  }


  // </editor-fold desc="Annotations">


  transform(annotation: SignaturePlacement, reverse: boolean = false) {

    if (annotation.pageRotation === 0) {
      return;
    }

    const pageIsLandscape: boolean = annotation.pageWidth > annotation.pageHeight;

    const centeredOriginX: number = pageIsLandscape && annotation.pageRotation != 180
      ? annotation.pageHeight / 2
      : annotation.pageWidth / 2;
    const centeredOriginY: number = pageIsLandscape && annotation.pageRotation != 180
      ? annotation.pageWidth / 2
      : annotation.pageHeight / 2;


    // Use center as position for the calculation
    if (annotation.rectangleOrigin == 'BOTTOM_LEFT' && reverse) {
      this.switchBackendCoordsToCentered(annotation);
      annotation.rectangleOrigin = 'CENTER';
    }

    const xTranslatedToCenteredOrigin: number = annotation.x - centeredOriginX;
    const yTranslatedToCenteredOrigin: number = annotation.y - centeredOriginY;

    let rotatedXTranslatedToOrigin: number;
    let rotatedYTranslatedToOrigin: number;

    let rotateBy = annotation.pageRotation;
    if (reverse) {
      switch (rotateBy) {
        case 90:
          rotateBy = 270;
          break;
        case 270:
          rotateBy = 90;
          break;
      }
    }

    switch (rotateBy) {
      case 90: {
        rotatedXTranslatedToOrigin = yTranslatedToCenteredOrigin;
        rotatedYTranslatedToOrigin = -xTranslatedToCenteredOrigin;
        break;
      }
      case 270: {
        rotatedXTranslatedToOrigin = -yTranslatedToCenteredOrigin;
        rotatedYTranslatedToOrigin = xTranslatedToCenteredOrigin;
        break;
      }
      case 180: {
        rotatedXTranslatedToOrigin = -xTranslatedToCenteredOrigin;
        rotatedYTranslatedToOrigin = -yTranslatedToCenteredOrigin;
        break;
      }
      default:
        console.warn("unexpected rotation : ", rotateBy);
        return;
    }

    const rotatedX: number = rotatedXTranslatedToOrigin + centeredOriginX;
    const rotatedY: number = rotatedYTranslatedToOrigin + centeredOriginY;

    annotation.x = rotatedX;
    annotation.y = rotatedY;
  }


  private switchBackendCoordsToCentered(annotation: SignaturePlacement) {

    annotation.x = annotation.x + annotation.width / 2;
    annotation.y = annotation.y + annotation.height / 2;

    const isTilted: boolean = annotation.pageRotation == 90 || annotation.pageRotation == 270;
    if (isTilted) {
      const pageWidth: number = annotation.pageWidth;
      const pageHeight: number = annotation.pageHeight;
      if (pageHeight > pageWidth) {
        annotation.x -= (pageHeight - pageWidth) / 2;
        annotation.y += (pageHeight - pageWidth) / 2;
      } else {
        if (annotation.pageRotation == 90) {
          annotation.x -= (pageWidth - pageHeight) / 2;
          annotation.y -= (pageWidth - pageHeight) / 2;
        } else {
          annotation.x += (pageWidth - pageHeight) / 2;
          annotation.y += (pageWidth - pageHeight) / 2;
        }
      }
    }
  }


  private switchCoordinatesToCenterFromRect(annotation: SignaturePlacement, rect: number[]) {
    switch (annotation.pageRotation) {
      case 90: {
        annotation.x = rect[0] - annotation.height / 2;
        annotation.y = rect[3] - annotation.width / 2;
        break;
      }
      case 270: {
        annotation.x = rect[2] - annotation.height / 2;
        annotation.y = rect[1] - annotation.width / 2;
        break;
      }
      case 180: {
        annotation.x = annotation.x - annotation.width / 2;
        annotation.y = annotation.y - annotation.height / 2;
        break;
      }
      default: {
        annotation.x = annotation.x + annotation.width / 2;
        annotation.y = annotation.y + annotation.height / 2;
      }
    }
  }


  // <editor-fold desc="UI callbacks">


  isInCreationWorkflow(): boolean {
    return this.currentWorkflowIndex === 1;
  }


  public isFolderStarted(): boolean {
    return this.folder.stepList
      .filter(t => t.action === Action.Start)
      .filter(t => t.state === State.Validated)
      .length > 0;
  }


  public onFolderUpdate(): void {
    // FIXME this is a workaround for an anguar-related bug, that return null instead of the real value of 'asDeskId' query param in some cases.
    const asDeskId = this.asOtherDeskId ?? this.currentDesk.id;
    this.folderService
      .getFolder(this.tenant.id, this.folder.id, asDeskId)
      .subscribe(updatedFolder => {
        this.folder = updatedFolder;
        this.initializeWithNewFolder();
      });
  }


  public onMetadataChanged(metadataUpdateEvt: MetadataEvent): void {
    const requests: Observable<any>[] = metadataUpdateEvt.modifiedMetadataList
      .map(m => this.folderService.setMetadata(this.tenant.id, this.folder, m.id, metadataUpdateEvt.fullMetadataMap[m.key]));

    console.log('onSaveButtonClicked - requests len : ', requests.length);
    // We cannot use combineLatest here because concurrent on the same folder update triggers a safety crash on workflow
    concat(...requests)
      .subscribe({
        next: () => {
          this.onFolderUpdate();
          this.notificationService.showCrudMessage(CrudOperation.Update, this.folder);
        },
        error: e => this.notificationService.showCrudMessage(CrudOperation.Update, this.folder, e.message, false)
      });
  }


  setSignatureAnnotationParameters(document?: Document) {
    if (this.isXmlOrDetachedSignatureFormat()) {
      return;
    }

    if (!document.isMainDocument) {
      this.pdfJsParameters.signaturePlacementAnnotations = [];
      return;
    }

    const documentToUse = document || this.currentDocument;

    const upcomingFilteredSteps: Array<Task> = this.folder.stepList
      .filter(step => FolderUtils.isCurrentOrUpcoming(step))
      .filter(step => FolderUtils.isSealOrSign(step));

    if (upcomingFilteredSteps.length === 0) {
      this.pdfJsParameters.signaturePlacementAnnotations = [];
      return;
    }

    if (documentToUse.signaturePlacementAnnotations?.length !== 0) {
      this.pdfJsParameters.signaturePlacementAnnotations = documentToUse.signaturePlacementAnnotations;
      return;
    }

    this.pdfJsParameters.signaturePlacementAnnotations = [this.getDefaultSignaturePosition()];

    switch (upcomingFilteredSteps[0].action) {
      case Action.Seal:
        this.setPdfJsParametersFromTagMap(documentToUse.sealTags ?? [], [Action.Seal]);
        break;
      case Action.Signature:
      case Action.ExternalSignature:
        this.setPdfJsParametersFromTagMap(documentToUse.signatureTags ?? [], [Action.Signature, Action.ExternalSignature]);
        break;
    }
  }


  setPdfJsParametersFromTagMap(tagMap: CustomNumberMap<PdfSignaturePosition>, actions: (Action | SecondaryAction)[]) {
    // default user pref-defined position
    const defaultSignaturePlacement: SignaturePlacement = this.getDefaultSignaturePosition();

    // default document-tags defined position
    let position: SignaturePlacement = Object.keys(tagMap).includes("0")
      ? {
        pageRotation: 0,
        signatureNumber: 0,
        rectangleOrigin: 'CENTER',
        height: defaultSignaturePlacement.height,
        width: defaultSignaturePlacement.width,
        page: tagMap[0].page,
        x: tagMap[0].x,
        y: tagMap[0].y,
        templateType: defaultSignaturePlacement.templateType
      }
      : defaultSignaturePlacement;

    const tagIndex: string = (this.folder.stepList
      .filter(step => actions.includes(step.action))
      .filter(step => step.state === State.Validated || step.state === State.Bypassed)
      .length + 1)
      .toString();

    if (Object.keys(tagMap).includes(tagIndex)) {
      position = {
        pageRotation: 0,
        signatureNumber: 0,
        rectangleOrigin: 'CENTER',
        height: defaultSignaturePlacement.height,
        width: defaultSignaturePlacement.width,
        page: tagMap[tagIndex].page,
        x: tagMap[tagIndex].x,
        y: tagMap[tagIndex].y,
        templateType: defaultSignaturePlacement.templateType
      };
    }

    this.pdfJsParameters.signaturePlacementAnnotations = [position];
  }


  public onDocumentSelected(document: Document): void {
    this.currentDocument = document;
    this.unloadPesViewer();
    this.setSignatureAnnotationParameters(document);

    if (document.mediaType === 'text/xml') {
      // Special case on PES documents
      this.isPesViewerActive = true;
      this.loadPesViewerForCurrentDocument();

    } else {
      // It may be 'application/pdf', 'application/octet-stream', or any weird things like that...
      // The default case is a good match.
      this.isPesViewerActive = false;
      this.refreshViewer();
    }
  }


  public shouldShowSignatureReports(): boolean {

    if (!this.show52Features) {
      return false;
    }

    // In any case, if we have a signature report, we show it
    if (this.folder.signatureProofList?.length > 0) {
      return true;
    }

    // The signature report does not work on these 2 types.
    // We filter them out.
    return ![SignatureFormat.PesV2, SignatureFormat.Pkcs7].includes(this.folder.type?.signatureFormat);
  }


  private refreshDocumentWithBlobData(document: Document, blobData: any) {
    const blob = new Blob([blobData], {type: document.mediaType});
    this.currentPdfSource = blob;

    if (this.pdfViewer) {
      this.pdfViewer.pdfSrc = this.currentPdfSource;
    }
  }


  private loadPesViewerForCurrentDocument(): void {
    const url: string = '/api/' + this.apiVersion + '/tenant/' + this.tenant.id + '/folder/' + this.folder.id + '/document/' + this.currentDocument.id + '/pes-viewer';

    this.documentService.loadPageFromUrl(url)
      .subscribe((response: HTMLDocument) => {
          if (this.pesViewerFrame) {
            this.pesViewerFrame.nativeElement.srcdoc = response.documentElement.innerHTML;
          }
        }
      );
  }


  private unloadPesViewer(): void {
    if (this.pesViewerFrame) {
      this.pesViewerFrame.nativeElement.srcdoc = '';
    }
  }


  private setParsedMetadata(metadataKey: string): Observable<Metadata>[] {
    const metadataObservables: Observable<Metadata>[] = [];
    const requestedMetadataId: string[] = [];
    this.folder
      .stepList
      .filter(step => !!step[metadataKey])
      .forEach(step => {
        step[metadataKey]
          .filter((id: string) => !requestedMetadataId.includes(id))
          .forEach((id: string) => {
            this.removeStepMetadataInSubtype(id);
            requestedMetadataId.push(id);
            metadataObservables.push(
              this.metadataService
                .getMetadata(this.tenant.id, id)
                .pipe(
                  map(metadataDto => Object.assign(new Metadata(), metadataDto)),
                  catchError(this.notificationService.handleHttpError('getMetadata'))
                ));
          });
      });
    return metadataObservables;
  }


  private removeStepMetadataInSubtype(id: string): void {
    const duplicateSubtypeExists: boolean = this.folder
      .subtype
      .subtypeMetadataList
      .filter(metadata => metadata.metadata.id === id)
      .length > 0;

    if (!duplicateSubtypeExists) {
      return;
    }

    const index: number = this.folder
      .subtype
      .subtypeMetadataList
      .findIndex(metadata => metadata.metadata.id === id);

    this.folder.subtype.subtypeMetadataList.splice(index, 1);
  }


  private parseMetadata(): void {
    forkJoin(
      [
        ...this.setParsedMetadata(MetadataFormComponent.MANDATORY_VALIDATION_KEY),
        ...this.setParsedMetadata(MetadataFormComponent.MANDATORY_REJECTION_KEY)
      ]
    ).subscribe({
      next: (result: Metadata[]) => {
        const distinctMetadata: Metadata[] = [];
        result.forEach(meta => {
          if (!distinctMetadata.map(meta => meta.id).includes(meta.id)) {
            distinctMetadata.push(meta);
          }
        });
        this.stepMetadata = distinctMetadata;
        this.countMissingMetadata = this.stepMetadata.filter(metadata => !!this.folder.metadata[metadata.key]).length;
        this.setShowStepMetadata();
      },
      error: e => this.notificationService.showCrudMessage(CrudOperation.Read, e)
    });
  }


  showSubTypeMetadata(): boolean {
    return this.folder?.subtype?.subtypeMetadataList?.length > 0;
  }


  setShowStepMetadata(): void {
    this.showStepMetadata = this.stepMetadata?.length > 0
      && this.countMissingMetadata > 0;
  }


  isXmlOrDetachedSignatureFormat(): boolean {
    return [SignatureFormat.XadesDetached, SignatureFormat.Pkcs7, SignatureFormat.PesV2]
      .includes(this.folder.type.signatureFormat);
  }


  // </editor-fold desc="UI callbacks">


  getDefaultSignaturePosition(): SignaturePlacement {
    let page: number = this.folder.type.signaturePosition?.page;
    if (page <= 0) {
      if (page < 0) {
        page += 1;
      }
      page = Math.max(this.currentDocument?.pageCount - Math.abs(page), 1);
    }

    let width: number = this.defaultTemplateInfo?.width || Config.STAMP_WIDTH;
    let height: number = this.defaultTemplateInfo?.height || Config.STAMP_HEIGHT;

    const annotationSize = this.getAnnotationSizeBasedOnExternalProviders(width, height);
    width = annotationSize.width;
    height = annotationSize.height;


    return {
      pageRotation: 0,
      signatureNumber: 0,
      rectangleOrigin: 'BOTTOM_LEFT',
      width: width,
      height: height,
      page: page,
      x: this.folder.type.signaturePosition?.x,
      y: this.folder.type.signaturePosition?.y,
      templateType: this.defaultTemplateType
    };
  }

  getAnnotationSizeBasedOnExternalProviders(width: number, height: number): { width: number, height: number } {
    const folderContainsExternalSignatureConfig: boolean = !!this.folder.subtype?.externalSignatureConfig?.serviceName;
    const currentTasks: Task[] = this.folder.stepList.filter(step => step.state === State.Current);
    const currentTaskIsExternalSignature: boolean = currentTasks.every(task => task.action === Action.ExternalSignature);

    if (!currentTaskIsExternalSignature || !folderContainsExternalSignatureConfig) {
      return {width: width, height: height};
    }

    switch (this.folder.subtype.externalSignatureConfig.serviceName) {
      case ExternalSignatureProvider.YousignV2:
        return {width: 120, height: 80};
      case ExternalSignatureProvider.YousignV3:
        return {width: 85, height: 37};
      case ExternalSignatureProvider.Docage:
        return {width: 100, height: 100};
      case ExternalSignatureProvider.Universign:
        return {width: 200, height: 50};
      // TODO - they add a page, there is no stamp to show
      case ExternalSignatureProvider.OodriveSign:
        return {width: 0, height: 0};
      default:
        console.warn('getAnnotationSizeBasedOnExternalProviders - external signature provider not handled! using default stamp size');
        return {width: width, height: height};
    }
  }


}
