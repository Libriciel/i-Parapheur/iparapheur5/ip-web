/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PaginatedResult } from '../../models/paginated-result';
import { NotificationsService } from '../notifications.service';
import { jsonHeader } from '../../utils/http-utils';
import { IpService } from '../../shared/service/ip-service';
import { CONFIG } from '../../shared/config/config';
import { Folder } from '../../models/folder/folder';
import { State, FolderSortBy, FolderDto } from '@libriciel/iparapheur-legacy';
import { SubtypeRepresentation, TypeRepresentation } from '@libriciel/iparapheur-provisioning';


@Injectable({
  providedIn: 'root'
})
export class AdminFolderService extends IpService {


  constructor(public http: HttpClient,
              public notificationsService: NotificationsService) {
    super();
  }


  getFolders(tenantId: string, page: number, pageSize: number, sortBy: FolderSortBy = FolderSortBy.FolderId,
             asc: boolean = true, deskId?: string, state?: State, type?: TypeRepresentation, subtype?: SubtypeRepresentation,
             searchTerm?: string, emitBefore?: Date, stillSince?: Date): Observable<PaginatedResult<FolderDto>> {

    let httpParams = new HttpParams()
      .set('page', String(page))
      .set('pageSize', String(pageSize))
      .set('sortBy', sortBy)
      .set('asc', String(asc));

    if (!!searchTerm) { httpParams = httpParams.set('searchTerm', searchTerm); }
    if (!!deskId) { httpParams = httpParams.set('deskId', deskId); }
    if (!!type?.id) { httpParams = httpParams.set('typeId', type.id); }
    if (!!subtype?.id) { httpParams = httpParams.set('subtypeId', subtype.id); }
    if (!!emitBefore) { httpParams = httpParams.set('emitBeforeTime', String(emitBefore.getTime())); }
    if (!!stillSince) { httpParams = httpParams.set('stillSinceTime', String(stillSince.getTime())); }
    if (!!state) { httpParams = httpParams.set('state', state); }

    return this.http
      .get<PaginatedResult<FolderDto>>(
        `${CONFIG.BASE_API_URL}/admin/tenant/${tenantId}/folder`,
        {
          headers: jsonHeader,
          params: httpParams
        }
      )
      .pipe(
        tap(value => value.data.forEach((f, i) => value.data[i] = Object.assign(new Folder(), f))),
        catchError(this.notificationsService.handleHttpError('getFoldersAsAdmin'))
      );
  }


}
