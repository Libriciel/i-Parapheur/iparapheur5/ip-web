/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { AreAllSignaturesValidPipe } from './are-all-signatures-valid.pipe';
import { ValidatedSignatureInformation } from '@libriciel/iparapheur-internal/model/validatedSignatureInformation';
import { Document } from '../models/document';


describe('SignatureStatusPipe', () => {


  it('create an instance', () => {
    const pipe = new AreAllSignaturesValidPipe();
    expect(pipe).toBeTruthy();
  });


  it('return true if there is only valid signatures', () => {

    const document = new Document();
    document.embeddedSignatureInfos = [
      {isSignatureValid: true} as ValidatedSignatureInformation,
      {isSignatureValid: true} as ValidatedSignatureInformation,
      {isSignatureValid: true} as ValidatedSignatureInformation,
    ];

    expect(AreAllSignaturesValidPipe.compute(document, false)).toBe(true);
  });


  it('return false if there is one invalid signature', () => {

    const document = new Document();
    document.embeddedSignatureInfos = [
      {isSignatureValid: true} as ValidatedSignatureInformation,
      {isSignatureValid: true} as ValidatedSignatureInformation,
      {isSignatureValid: false} as ValidatedSignatureInformation,
      {isSignatureValid: undefined} as ValidatedSignatureInformation,
    ];

    expect(AreAllSignaturesValidPipe.compute(document, false)).toBe(false);
  });


  it('return undefined if there is any undefined signature status', () => {

    const document = new Document();
    document.embeddedSignatureInfos = [
      {isSignatureValid: true} as ValidatedSignatureInformation,
      {isSignatureValid: undefined} as ValidatedSignatureInformation,
      {isSignatureValid: true} as ValidatedSignatureInformation,
    ];

    expect(AreAllSignaturesValidPipe.compute(document, false)).toBe(null);
  });


});
