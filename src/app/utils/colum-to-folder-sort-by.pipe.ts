/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { FolderSortBy, TaskViewColumn } from '@libriciel/iparapheur-legacy';

@Pipe({
  name: 'columToFolderSortBy'
})
export class ColumToFolderSortByPipe implements PipeTransform {


  public static compute(column: TaskViewColumn | string): FolderSortBy | string {
    switch (column) {
      case TaskViewColumn.CreationDate:
        return FolderSortBy.CreationDate;
      case TaskViewColumn.OriginDesk:
        return FolderSortBy.EmitterDeskName;
      case TaskViewColumn.OriginUser:
        return FolderSortBy.EmitterUserName;
      case TaskViewColumn.CurrentDesk:
        return FolderSortBy.CurrentDeskName;
      case TaskViewColumn.FolderName:
        return FolderSortBy.FolderName;
      case TaskViewColumn.FolderId:
        return FolderSortBy.FolderId;
      case TaskViewColumn.LimitDate:
        return FolderSortBy.LateDate;
      case TaskViewColumn.State:
        return FolderSortBy.ActionType;
      case TaskViewColumn.Subtype:
        return FolderSortBy.SubtypeName;
      case TaskViewColumn.TaskId:
        return FolderSortBy.TaskId;
      case TaskViewColumn.Type:
        return FolderSortBy.TypeName;
      default:
        return column;
    }
  }


  transform(value: TaskViewColumn | string, ..._args: unknown[]): unknown {
    return ColumToFolderSortByPipe.compute(value);
  }


}
