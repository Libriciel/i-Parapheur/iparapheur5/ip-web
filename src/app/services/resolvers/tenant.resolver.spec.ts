/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { TestBed } from '@angular/core/testing';
import { TenantResolver } from './tenant.resolver';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { of, Observable } from 'rxjs';
import { EnvironmentInjector } from '@angular/core';
import { TenantRepresentation } from '@libriciel/iparapheur-legacy';


describe('TenantResolver', () => {

  const mockRoute: ActivatedRouteSnapshot = {params: {id: 100}} as unknown as ActivatedRouteSnapshot;
  const mockResult: TenantRepresentation = {id: null, name: null};

  describe('TenantResolver', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [
          HttpClient, HttpHandler,
          {provide: TenantResolver, useValue: of(mockResult)},
        ],
      });
    });

    it('should return the requested tenant', () => {
      const result = TestBed.inject(EnvironmentInjector).runInContext(() =>
        TenantResolver(mockRoute, {} as RouterStateSnapshot)
      ) as Observable<TenantRepresentation>;
      result.subscribe(result => expect(result).toEqual(mockResult));
    });

  });
});
