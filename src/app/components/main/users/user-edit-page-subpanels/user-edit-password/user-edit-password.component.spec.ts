/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UserEditPasswordComponent } from './user-edit-password.component';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { User } from '../../../../../models/auth/user';
import { FilterService } from '../../../../../services/filter.service';
import { ProfileUpdateService } from '../../../../../shared/service/profile-update.service';
import { SelectedUserPreferencesService } from '../../../../../services/selected-user-preferences.service';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';


describe('UserEditPasswordComponent', () => {


  let component: UserEditPasswordComponent;
  let fixture: ComponentFixture<UserEditPasswordComponent>;


  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [
          HttpClient,
          HttpHandler,
          FilterService,
          ProfileUpdateService,
          SelectedUserPreferencesService,
          {provide: ActivatedRoute, useValue: {data: of({result: 'test'})}}],
        declarations: [UserEditPasswordComponent],
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(UserEditPasswordComponent);
    component = fixture.componentInstance;
    component.currentUser = new User();
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
