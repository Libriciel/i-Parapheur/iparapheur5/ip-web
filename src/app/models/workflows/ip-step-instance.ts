/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { DeskRepresentation } from '@libriciel/iparapheur-legacy';
import { IpWorkflowActor } from './ip-workflow-actor';

export class IpStepInstance {
  id: string;
  name: string;
  type: any;
  state: any;
  delegatedByDesk: DeskRepresentation;
  workflowIndex: number;
  stepIndex: number;
  actedUponBy?: IpWorkflowActor;
  comment?: string;
  actionId?: string;
  validatedBy: any[] = [];
  completedDate?: Date;
  validators: any[];


  constructor(data?: {
    id: string;
    name: string;
    type: any;
    validators: IpWorkflowActor[];
    delegatedByDesk: DeskRepresentation;
    state: any;
    actedUponBy?: IpWorkflowActor;
    comment?: string;
    actionId?: string;
    workflowIndex?: number;
    stepIndex?: number;
  }) {
    this.id = data.id;
    this.name = data.name;
    this.type = data.type;
    this.delegatedByDesk = data.delegatedByDesk;
    this.state = data.state;
    this.actedUponBy = data.actedUponBy;
    this.comment = data.comment;
    this.workflowIndex = data.workflowIndex;
    this.stepIndex = data.stepIndex;
    this.validators = data.validators ? [...data.validators] : [];
    this.validatedBy = data.actedUponBy ? [data.actedUponBy] : [];
    if (data.actionId) {
      this.id = data.actionId;
    }
  }
}
