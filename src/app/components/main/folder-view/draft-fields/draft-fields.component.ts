/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Folder } from '../../../../models/folder/folder';
import { NotificationsService } from '../../../../services/notifications.service';
import { FolderService } from '../../../../services/ip-core/folder.service';
import { CommonMessages } from '../../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { CrudOperation } from '../../../../services/crud-operation';
import { FolderViewMessages } from '../folder-view-messages';
import { faMinusSquare, faPlusSquare } from '@fortawesome/free-regular-svg-icons';
import { Action, State, FolderViewBlock } from '@libriciel/iparapheur-legacy';
import { DeskDto, SubtypeDto } from '@libriciel/iparapheur-provisioning';
import { Observable } from 'rxjs';
import { StandardApiPageImplSubtypeRepresentation, TypologyService as StandardTypologyService } from '@libriciel/iparapheur-standard';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { formatDate } from '@angular/common';
import { DraftForm } from '../../../../shared/models/custom-types';

@Component({
  selector: 'app-draft-fields',
  templateUrl: './draft-fields.component.html',
  styleUrls: ['./draft-fields.component.scss']
})
export class DraftFieldsComponent implements OnInit {


  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = FolderViewMessages;
  readonly folderViewBlock = FolderViewBlock;
  readonly minusSquareIcon: IconDefinition = faMinusSquare;
  readonly plusSquareIcon: IconDefinition = faPlusSquare;

  @Input() modificationsEnabled: boolean = true;
  @Input() tenantId: string;
  @Input() desk: DeskDto;
  @Input() folder: Folder;
  @Output() folderChange: EventEmitter<Folder> = new EventEmitter<Folder>();

  selectedSubtype: SubtypeDto;
  saveProcessing: boolean = false;
  isCollapsed: boolean = false;
  isDraft: boolean = false;

  draftForm: FormGroup<DraftForm>;


  // <editor-fold desc="LifeCycle">


  constructor(private folderService: FolderService,
              private standardTypologyService: StandardTypologyService,
              private notificationsService: NotificationsService) {}


  ngOnInit(): void {
    this.setupForm();

    this.isDraft = this.isInDraftState();

    if (this.isDraft) {
      this.selectedSubtype = this.folder.subtype;
    }

    if (!this.isDraft || !this.modificationsEnabled) {
      this.draftForm.controls.name.disable();
      this.draftForm.controls.dueDate.disable();
    }
  }


  // </editor-fold desc="LifeCycle">


  setupForm() {
    this.draftForm = new FormGroup({
      name: new FormControl<string>(
        "",
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(255)
        ]
      ),
      dueDate: new FormControl<string>(null, [Validators.required])
    });

    const dueDateValue: string = this.folder?.dueDate
      ? formatDate(this.folder.dueDate, 'yyyy-MM-dd', 'fr-FR')
      : null;

    this.draftForm.patchValue({
      name: this.folder.name,
      dueDate: dueDateValue
    });


    this.draftForm.valueChanges.subscribe(changes => {
      if (!!changes.name) {
        this.folder.name = changes.name;
      }

      if (!!changes.dueDate) {
        this.folder.dueDate = new Date(changes.dueDate);
      }
    });
  }

  isDateBeforeNow(date: Date): boolean {
    return (date != null) && (new Date(date).getTime() < Date.now());
  }


  isInDraftState(): boolean {
    return this.folder.stepList
      .filter(step => step.action == Action.Start)
      .filter(step => step.state == State.Current)
      .length >= 1;
  }


  retrieveSubtypesFn = (page: number, pageSize: number) => this.retrieveSubtypes(page, pageSize);


  onSubtypeSelectionChanged() {
    if (!this.selectedSubtype) {
      return;
    }

    this.folder.subtype = this.selectedSubtype;
  }

  // <editor-fold desc="UI callbacks">


  onSaveButtonClicked() {
    this.saveProcessing = true;

    this.folderService
      .editFolder(this.tenantId, this.folder)
      .subscribe({
        next: () => {
          this.folderChange.emit(this.folder);
          this.notificationsService.showCrudMessage(CrudOperation.Update, this.folder);
        },
        error: e => this.notificationsService.showCrudMessage(CrudOperation.Update, this.folder, e.message, false)
      })
      .add(() => this.saveProcessing = false);
  }


  // </editor-fold desc="UI callbacks">


  retrieveSubtypes(page: number, pageSize: number): Observable<StandardApiPageImplSubtypeRepresentation> {
    console.debug(`retrieve creation allowed subtypes : page: ${page}, pageSize: ${pageSize}`);
    return this.standardTypologyService.listCreationAllowedSubtypes(this.tenantId, this.desk.id, this.folder.type?.id, page, pageSize);
  }


}
