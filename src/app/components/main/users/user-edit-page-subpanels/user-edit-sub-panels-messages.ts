/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


export class UserEditSubPanelsMessages {

  static readonly NO_SIGNATURE_IMAGE = `Pas d'image`;
  static readonly ADD_SIGNATURE_IMAGE = 'Ajouter une image';
  static readonly REPLACE_SIGNATURE_IMAGE = `Remplacer l'image`;
  static readonly DELETE_SIGNATURE_IMAGE = `Supprimer l'image`;

  static readonly REDIRECTION_MAIL_LABEL = 'Courriel destinataire des notifications';
  static readonly NOTIFICATION_FREQUENCY_LABEL = 'Fréquence des notifications';
  static readonly NOTIFICATION_FREQUENCY_NONE = 'Aucune';
  static readonly NOTIFICATION_FREQUENCY_SINGLE = 'Unitaire';
  static readonly NOTIFICATION_FREQUENCY_HOURLY = 'Heure';
  static readonly NOTIFICATION_FREQUENCY_DAILY = 'Jour';
  static readonly NOTIFICATION_FREQUENCY_WEEKLY = 'Semaine';
  static readonly EVERY_HOURS = 'Toutes les ';
  static readonly HOUR_S = 'heure(s)';
  static readonly EVERY_DAYS = 'Tous les jours à ';
  static readonly HOURS = 'heures';
  static readonly EVERY_WEEK = 'Toutes les semaines, le';
  static readonly MONDAY = 'lundi';
  static readonly TUESDAY = 'mardi';
  static readonly WEDNESDAY = 'mercredi';
  static readonly THURSDAY = 'jeudi';
  static readonly FRIDAY = 'vendredi';
  static readonly SATURDAY = 'samedi';
  static readonly SUNDAY = 'dimanche';


  static readonly PROFILE_VALIDATE = 'Enregistrer';
  static readonly PROFILE_SAVE_USER_PREF_SUCCESS = `Préférences d'utilisateur éditées avec succès.`;
  static readonly PROFILE_SAVE_USER_PREF_ERROR = `Erreur lors de l'édition des préférences d'utilisateur`;
  static readonly PROFILE_SAVE_PASSWORD_SUCCESS = 'Mot de passe édité avec succès.';
  static readonly PROFILE_SAVE_PASSWORD_ERROR = `Erreur lors de l'édition du mot de passe`;
  static readonly FAVORITE_DESKS_PREFERENCES_TITLE = `Gestion des bureaux favoris`;
  static readonly ABSENCES_PREFERENCES_TITLE = `Gestion des absences`;
  static readonly TRASH_BIN_PREFERENCES_TITLE = `Gestion de la corbeille`;
  static readonly DEBUG_PREFERENCES_TITLE = `Gestion du débogage`;

  static readonly FOLDER_VIEW_PREFERENCES_TITLE = 'Personnalisation de la visualisation de dossier';
  static readonly FOLDER_VIEW_PREFERENCES_SUBTITLE = 'Ordre des blocs';

  static readonly DEFAULT_TEMPLATES = 'Modèles par défaut';
  static readonly SIGNATURE_TEMPLATES = 'Modèle de signature';
  static readonly SEAL_TEMPLATES = 'Modèle de cachet serveur';
  static readonly CANNOT_SHOW_TEMPLATE_PREVIEW_WITH_MULTIPLE_TENANTS = `La prévisualisation des modèles de signature n'est disponible que si vous n'avez qu'une seule entité associée.`;
  static readonly NEW_PASSWORD_LABEL = 'Nouveau mot de passe';
  static readonly CONFIRM_PASSWORD_LABEL = 'Confirmer le mot de passe';
  static readonly EDIT_PASSWORD_TITLE = 'Modification du mot de passe';
  static readonly SAVE_NEW_PASSWORD = 'Enregistrer le nouveau mot de passe';

  static readonly EDIT_NOTIFICATIONS_TITLE = 'Modifications des notifications';

  static readonly EDIT_SPECIFIC_NOTIFICATIONS_TITLE = 'Notifications qui me concernent';
  static readonly EDIT_SPECIFIC_NOTIFICATIONS_NEW_FOLDER_INVOLVING_ME = 'Un dossier est arrivé sur un bureau me concernant';
  static readonly EDIT_SPECIFIC_NOTIFICATIONS_ACTION_ON_FOLLOWED_FOLDER = 'Une action a eu lieu sur un dossier suivi';
  static readonly EDIT_SPECIFIC_NOTIFICATIONS_LATE_FOLDER_INVOLVING_ME = 'Un dossier me concernant est en retard';

  static readonly EDIT_DESK_ORDER_FAVORITE_DESKS = 'Bureaux favoris (max. 15)';
  static readonly EDIT_DESK_ORDER_ORDER = 'Ordre';

  static readonly CANNOT_RETRIEVE_DESKS = 'Erreur lors de la récupération des bureaux';
  static readonly FAVORITE_DESK_LIMIT_REACHED = `Impossible d'ajouter un bureau favori. La limite a été atteinte.`;

  static readonly DASHBOARD_TITLE = 'Modification du tableau de bord';
  static readonly DASHBOARD_RESULTS_PER_PAGE = 'Nombre de résultats à afficher par page';
  static readonly DASHBOARD_DEFAULT_SORT = 'Tri par défaut';
  static readonly DASHBOARD_ASC = 'Ascendant';
  static readonly DASHBOARD_DESC = 'Descendant';
  static readonly DASHBOARD_DEFAULT_FILTER = 'Filtre par défaut';
  static readonly DASHBOARD_SORT_ELEMENTS = 'Réorganiser les colonnes';
  static readonly DASHBOARD_AVAILABLE_COLUMNS = 'Colonnes disponibles';
  static readonly DASHBOARD_USED_COLUMNS = 'Colonnes affichées';
  static readonly DASHBOARD_ORDER = 'Ordre';

  static readonly SHOW_ADMIN_IDS = 'Afficher les identifiants';
  static readonly SHOW = 'Afficher';
  static readonly DO_NOT_SHOW = 'Ne pas afficher';

  static readonly PASSWORD_CHANGE_DISABLED_LDAP_TOOLTIP: string = `Le mot de passe ne peut pas être modifié sur un utilisateur provenant d'un LDAP`;

  static readonly PASSWORD_NOT_EMAIL_HINT: string = `Le mot de passe ne peut pas être l'adresse e-mail`;
  static readonly PASSWORD_NOT_USERNAME_HINT: string = `Le mot de passe ne peut pas être l'identifiant`;

  static readonly METADATA_DISABLED_WHEN_USER_ASSOCIATED_TO_MULTIPLE_TENANTS: string = `L'ajout de métadonnées au tableau de bord est désactivé car vous êtes associés à plusieurs entités.`;
  static readonly MAX_COLUMNS_IN_DASHBOARD_REACHED: string = `Nombre maximum de colonnes dans le tableau de bord atteint.`;
  static readonly SELECT_METADATA_TO_ADD: string = `Sélectionner la metadonnée à ajouter`;
  static readonly ADD_METADATA_TO_COLUMNS: string = `Ajouter la metadonnée`;


  /**
   * This is the "known" 80-entropy regex, set into Keycloak.
   * Any other request can be set, and a generic message will be displayed.
   * But if this regex shows up, we'll display our own message.
   */
  static readonly ENTROPY_80_REGEX: string = '^(?:(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{12,})|(?:(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{14,})|' +
    '(?:(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{13,})|(?:(?=.*[a-z])(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{13,})|(?:(?=.*[A-Z])(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{1' +
    '3,})|(?:(?=.*[a-z])(?=.*[A-Z]).{14,})|(?:(?=.*[a-z])(?=.*[0-9]).{16,})|(?:(?=.*[a-z])(?=.*[^a-zA-Z0-9]).{14,})|(?:(?=.*[A-Z])(?=.*[0-9]).{16,})|(?:(' +
    '?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{14,})|(?:(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{15,})|(?:[a-z]{17,})|(?:[A-Z]{17,})|(?:[0-9]{24,})|(?:[^a-zA-Z0-9]{16,})$';

  static readonly getPasswordSpecialCharacterCountHint = (min: number): string => (min > 1)
    ? `Le mot de passe doit contenir au moins ${min} caractères spéciaux`
    : 'Le mot de passe doit contenir au moins un caractère spécial';

  static readonly getPasswordUppercaseCountHint = (min: number): string => (min > 1)
    ? `Le mot de passe doit contenir au moins ${min} majuscules`
    : 'Le mot de passe doit contenir au moins une majuscule';

  static readonly getPasswordLowercaseCountHint = (min: number): string => (min > 1)
    ? `Le mot de passe doit contenir au moins ${min} minuscules`
    : 'Le mot de passe doit contenir au moins une minuscule';

  static readonly getPasswordDigitsCountHint = (min: number): string => (min > 1)
    ? `Le mot de passe doit contenir au moins ${min} chiffres`
    : 'Le mot de passe doit contenir au moins un chiffre';

  static readonly getPasswordMinLengthHint = (min: number): string => (min > 1)
    ? `Le mot de passe doit contenir au moins ${min} caractères`
    : 'Le mot de passe doit contenir au moins un caractère';

  static readonly getPasswordMaxLengthHint = (min: number): string => (min > 1)
    ? `Le mot de passe doit contenir au plus ${min} caractères`
    : 'Le mot de passe doit contenir au plus un caractère';

  static readonly getRegexHint = (regex: string): string => (regex == this.ENTROPY_80_REGEX)
    ? `Le mot de passe doit avoir une entropie de 80`
    : `Le mot de passe doit être validé par l'expression régulière définie par l'administrateur`;


}
