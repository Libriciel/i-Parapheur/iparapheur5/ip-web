/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { TemplatesMessages } from '../templates-messages';
import { TemplateUtils } from '../../../../../../../../../utils/template-utils';
import { TemplateType } from '@libriciel/iparapheur-legacy';

@Component({
  selector: 'app-signature-template-page',
  templateUrl: './signature-template-page.component.html',
  styleUrls: ['./signature-template-page.component.scss']
})
export class SignatureTemplatePageComponent {

  readonly messages = TemplatesMessages;
  readonly templates: TemplateType[] = TemplateUtils.ADMIN_SEAL_AND_SIGNATURE_TEMPLATE_TYPES;

}
