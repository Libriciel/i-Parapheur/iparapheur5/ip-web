/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { ExternalSignatureProvider } from '@libriciel/iparapheur-legacy';

@Pipe({
  name: 'externalSignatureProvider'
})
export class ExternalSignatureProviderPipe implements PipeTransform {

  public static compute(value: ExternalSignatureProvider): string {
    switch (value) {
      case ExternalSignatureProvider.Docage: {return 'Docage'; }
      case ExternalSignatureProvider.YousignV2: {return 'Yousign V2'; }
      case ExternalSignatureProvider.YousignV3: {return 'Yousign V3'; }
      case ExternalSignatureProvider.Universign: {return 'Universign'; }
      case ExternalSignatureProvider.OodriveSign: {return 'Oodrive Sign'; }
      default: { return value ?? null; }
    }
  }


  transform(value: ExternalSignatureProvider): string {
    return ExternalSignatureProviderPipe.compute(value);
  }

}
