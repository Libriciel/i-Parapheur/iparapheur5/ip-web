/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { WorkflowUtils } from './workflow-utils';
import { WorkflowDefinition, WorkflowStep, WorkflowActor } from '@libriciel/ls-workflow';
import { WorkflowDefinitionDto } from '@libriciel/iparapheur-provisioning';

describe('WorkflowUtils', () => {

  const variableActor: WorkflowActor = {
    id: WorkflowUtils.GENERIC_DESK_IDS.VARIABLE_DESK_ID,
    name: WorkflowUtils.GENERIC_DESK_NAMES.VARIABLE_DESK
  };


  it('should create an instance', () => {
    expect(new WorkflowUtils()).toBeTruthy();
  });


  it('should return 0', () => {
    const workflowDefinition: WorkflowDefinition = new WorkflowDefinition();
    workflowDefinition.steps = [new WorkflowStep()];
    expect(WorkflowUtils.countVariableDeskStep(workflowDefinition)).toBe(0);

    const workflowDefinitionDto: WorkflowDefinitionDto = {key: '', name: '', steps: []};
    expect(WorkflowUtils.countVariableDeskStepDto(workflowDefinitionDto)).toBe(0);
  });


  it('should return 1', () => {
    const workflowDefinition: WorkflowDefinition = new WorkflowDefinition();
    const step1: WorkflowStep = new WorkflowStep();
    step1.validators = [variableActor];
    workflowDefinition.steps = [step1];

    expect(WorkflowUtils.countVariableDeskStep(workflowDefinition)).toBe(1);

    const workflowDefinitionDto: WorkflowDefinitionDto = {
      key: '',
      name: '',
      steps: [
        {
          validatingDesks: [WorkflowUtils.GENERIC_WORKFLOW_ACTORS.VARIABLE_DESK]
        }
      ]
    };
    expect(WorkflowUtils.countVariableDeskStepDto(workflowDefinitionDto)).toBe(1);
  });


  it('should return 5', () => {
    const workflowDefinition: WorkflowDefinition = new WorkflowDefinition();
    const step1: WorkflowStep = new WorkflowStep();
    step1.validators = [variableActor];

    const step2: WorkflowStep = new WorkflowStep();
    step2.validators = [variableActor];

    const step3: WorkflowStep = new WorkflowStep();
    step3.validators = [variableActor];

    const step4: WorkflowStep = new WorkflowStep();
    step4.validators = [variableActor];

    const step5: WorkflowStep = new WorkflowStep();
    step5.validators = [variableActor];
    workflowDefinition.steps = [step1, step2, step3, step4, step5];

    expect(WorkflowUtils.countVariableDeskStep(workflowDefinition)).toBe(5);

    const workflowDefinitionDto: WorkflowDefinitionDto = {
      key: '',
      name: '',
      steps: [
        {validatingDesks: [WorkflowUtils.GENERIC_WORKFLOW_ACTORS.VARIABLE_DESK]},
        {validatingDesks: [WorkflowUtils.GENERIC_WORKFLOW_ACTORS.VARIABLE_DESK]},
        {validatingDesks: [WorkflowUtils.GENERIC_WORKFLOW_ACTORS.VARIABLE_DESK]},
        {validatingDesks: [WorkflowUtils.GENERIC_WORKFLOW_ACTORS.VARIABLE_DESK]},
        {validatingDesks: [WorkflowUtils.GENERIC_WORKFLOW_ACTORS.VARIABLE_DESK]}
      ]
    };
    expect(WorkflowUtils.countVariableDeskStepDto(workflowDefinitionDto)).toBe(5);
  });


});
