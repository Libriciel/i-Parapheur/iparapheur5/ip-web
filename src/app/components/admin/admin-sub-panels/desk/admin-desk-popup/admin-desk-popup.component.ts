/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DeskMessages } from '../desk-messages';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { CrudOperation } from '../../../../../services/crud-operation';
import { Observable } from 'rxjs';
import { AdminDeskService, DeskDto } from '@libriciel/iparapheur-provisioning';
import { ToastrService } from 'ngx-toastr';
import { MutableDesk } from '../../../../../models/auth/mutable-desk';
import { catchError } from 'rxjs/operators';
import { CoreApiError } from '../../../../../shared/models/custom-types';

@Component({
  selector: 'app-admin-desk-popup',
  templateUrl: './admin-desk-popup.component.html',
  styleUrls: ['./admin-desk-popup.component.scss']
})
export class AdminDeskPopupComponent {

  public static readonly INJECTABLE_DESK_KEY: string = 'desk';
  public static readonly INJECTABLE_TENANT_ID_KEY: string = 'tenantId';

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = DeskMessages;

  generalDeskFormValid: boolean = true;
  ownersDeskFormValid: boolean = true;
  isProcessing: boolean = false;
  deskNameBeforeEdition: string;

  // <editor-fold desc="LifeCycle">


  constructor(protected adminDeskService: AdminDeskService,
              protected notificationService: NotificationsService,
              protected toastr: ToastrService,
              public activeModal: NgbActiveModal,
              @Inject(AdminDeskPopupComponent.INJECTABLE_DESK_KEY) public desk: DeskDto,
              @Inject(AdminDeskPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string) {
    this.deskNameBeforeEdition = desk.name;

    // Ensure all the desk's element lists are properly initialised
    const modifiableDesk: MutableDesk = this.desk as MutableDesk;
    modifiableDesk.owners = this.desk.owners || [];
    modifiableDesk.supervisors = this.desk.supervisors || [];
    modifiableDesk.delegationManagers = this.desk.delegationManagers || [];
    modifiableDesk.associatedDesks = this.desk.associatedDesks || [];
    modifiableDesk.filterableMetadata = this.desk.filterableMetadata || [];
  }


  // </editor-fold desc="LifeCycle">


  validate(): void {

    if (!this.validForms()) {
      console.log('Invalid form, cancelling...');
      return;
    }

    this.isProcessing = true;
    const crudOperation: CrudOperation = this.desk.id ? CrudOperation.Update : CrudOperation.Create;

    const requestObservable$: Observable<any> = this.desk.id
      ? this.adminDeskService.editDesk(this.tenantId, this.desk.id, this.createDeskDtoFromCurrentDesk())
      : this.adminDeskService.createDesk(this.tenantId, this.createDeskDtoFromCurrentDesk());

    requestObservable$
      .pipe(catchError(this.notificationService.handleHttpError('create/update desk')))
      .subscribe({
        next: () => {
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
          this.notificationService.showSuccessMessage(
            `Le bureau ${this.desk.name} a été ${crudOperation === CrudOperation.Create ? 'créé' : 'modifié'} avec succès`
          );
        },
        error: (e: CoreApiError) => this.notificationService.showCoreApiErrorMessage(
          `Erreur lors de la ${crudOperation === CrudOperation.Create ? 'création' : 'modification'} du bureau ${this.deskNameBeforeEdition}`,
          e
        )
      })
      .add(() => this.isProcessing = false);
  }


  validForms(): boolean {
    return this.generalDeskFormValid && this.ownersDeskFormValid;
  }


  validateGeneralForm($event: boolean) {
    this.generalDeskFormValid = $event;
  }


  createDeskDtoFromCurrentDesk(): DeskDto {
    return {
      shortName: this.desk.shortName,
      name: this.desk.name,
      description: this.desk.description || null,
      parentDeskId: this.desk.parentDesk?.id || null,
      folderCreationAllowed: this.desk.folderCreationAllowed,
      actionAllowed: this.desk.actionAllowed,
      archivingAllowed: this.desk.archivingAllowed,
      chainAllowed: this.desk.chainAllowed,
      ownerIds: this.desk.owners.map(u => u.id),
      associatedDeskIds: this.desk.associatedDesks.map(d => d.id),
      filterableMetadataIds: this.desk.filterableMetadata.map(m => m.id),
      supervisorIds: this.desk.supervisors.map(u => u.id),
      delegationManagerIds: this.desk.delegationManagers.map(u => u.id),
    };
  }


}
