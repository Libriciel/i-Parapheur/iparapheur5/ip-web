/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { UserEditSubPanelsMessages } from '../../components/main/users/user-edit-page-subpanels/user-edit-sub-panels-messages';
import { NotificationsService } from '../../services/notifications.service';
import { CurrentUserService, UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { SelectedUserPreferencesService } from '../../services/selected-user-preferences.service';
import { LabelledColumn, TableName, TableLayoutDto } from '@libriciel/iparapheur-internal';

@Injectable({
  providedIn: 'root'
})
export class ProfileUpdateService {

  constructor(private notificationService: NotificationsService,
              private currentUserService: CurrentUserService,
              private selectedUserPreferencesService: SelectedUserPreferencesService) { }


  updateColumnList(labelledColumnList: LabelledColumn[], tableName: TableName, userPreferences : UserPreferencesDto) {
    const tableLayout: TableLayoutDto = userPreferences.tableLayoutList
      .find(tl => tl.tableName === tableName && tl.deskId == null);
    if (!!tableLayout) {
      tableLayout.columnList = labelledColumnList.map(c => c.id);
    } else {
      console.error("Could not retrieve the table layout associated with the tableName :" + tableName);
    }
  }

  async updateUserPreferences(userPreferences : UserPreferencesDto) {
    this.currentUserService
      .updatePreferences(userPreferences)
      .subscribe({
        next: () => {
          // FIXME return user preferences on Update or get it before update
          this.selectedUserPreferencesService.update(userPreferences);
          this.notificationService.showSuccessMessage(UserEditSubPanelsMessages.PROFILE_SAVE_USER_PREF_SUCCESS);
        },
        error: e => this.notificationService.showErrorMessage(UserEditSubPanelsMessages.PROFILE_SAVE_USER_PREF_ERROR, e.message)
      })
      .add();
  }
}
