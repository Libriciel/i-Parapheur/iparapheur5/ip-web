/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TenantMessages } from '../tenant-messages';
import { CommonMessages } from '../../../../../shared/common-messages';
import { CommonIcons, Style } from '@libriciel/ls-elements';
import { forkJoin, Observable, of, BehaviorSubject, throwError } from 'rxjs';
import { NotificationsService } from '../../../../../services/notifications.service';
import * as freeSolidIcons from '@fortawesome/free-solid-svg-icons';
import { faCheck, faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { tap, catchError, map, switchMap } from 'rxjs/operators';
import { BatchResult } from '../../../../../models/commons/batch-result';
import { CrudOperation } from '../../../../../services/crud-operation';
import { AdminSecureMailService } from '../../../../../services/ip-core/admin-secure-mail.service';
import { AdminMetadataService, AdminDeskService as ProvisioningAdminDeskService, AdminSealCertificateService, AdminWorkflowDefinitionService, AdminTypologyService, MetadataSortBy, AdminTenantUserService, AdminTenantService, AdminTemplateService, LayerSortBy, WorkflowDefinitionDto, AdminFolderService as ProvisioningAdminFolderService } from '@libriciel/iparapheur-provisioning';
import { AdminTrashBinService as StandardAdminTrashBinService, TypeRepresentation } from '@libriciel/iparapheur-standard';
import { FolderSortBy, ExternalSignatureConfigDto, AdminExternalSignatureService, UserRepresentation, TenantRepresentation, TemplateType, FolderDto } from '@libriciel/iparapheur-legacy';
import { AdminTrashBinService, AdminLayerService, AdminDeskService as InternalAdminDeskService, HierarchisedDeskRepresentation, AdminFolderService as InternalAdminFolderService } from '@libriciel/iparapheur-internal';
import { Metadata } from '../../../../../models/metadata';
import { PaginatedResult } from '../../../../../models/paginated-result';
import { IpService } from '../../../../../shared/service/ip-service';
import { Layer } from '../../../../../models/pdf-stamp/layer';
import { SealCertificate } from '../../../../../models/seal-certificate';
import { Folder } from '../../../../../models/folder/folder';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-admin-tenant-delete-popup',
  templateUrl: './admin-tenant-delete-popup.component.html',
  styleUrls: ['./admin-tenant-delete-popup.component.scss']
})
export class AdminTenantDeletePopupComponent implements OnInit {

  static readonly injectableTenantKey = 'tenant';

  readonly messages = TenantMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly processState = ProcessState;
  readonly styles = Style;
  readonly successIcon = faCheck;
  readonly toBeProcessIcon = faCaretRight;
  readonly failureIcon = CommonIcons.CLOSE_ICON;
  readonly batchSize = 10;

  state: ProcessState;
  isFormValid = false;
  completedCalls = 0;
  totalCalls = 0;
  confirmTenantName = '';

  items: TobeDeletedItem[] = [
    new TobeDeletedItem(
      CommonMessages.FOLDER_NAME,
      this.deleteAllFolders(this.tenant.id, this.batchSize),
      this.internalAdminFolderService.listFoldersAsAdmin(this.tenant.id, null, 0, 1).pipe(map(result => result.totalElements))
    ),
    new TobeDeletedItem(
      CommonMessages.TRASH_BIN_NAME,
      this.deleteAllTrashBinFolders(this.tenant.id, this.batchSize),
      this.adminTrashBinService.listTrashBinListableFolders(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))
    ),
    new TobeDeletedItem(
      CommonMessages.TYPE_AND_SUBTYPE_NAME,
      this.deleteAllTypes(this.tenant.id, this.batchSize),
      this.adminTypologyService.listTypes(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))
    ),
    new TobeDeletedItem(
      CommonMessages.SEAL_CERTIFICATE_NAME,
      this.deleteAllSealCertificates(this.tenant.id, this.batchSize),
      this.adminSealCertificateService.listSealCertificate(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))
    ),
    new TobeDeletedItem(
      CommonMessages.SECURE_MAIL_SERVER_NAME,
      this.adminSecureMailService.deleteSecureMailServers(this.tenant.id, this.batchSize),
      this.adminSecureMailService.getSecureMailServersCount(this.tenant.id)
    ),
    new TobeDeletedItem(
      CommonMessages.EXTERNAL_SIGNATURE_CONFIG_NAME,
      this.deleteAllExternalSignatureConfigs(this.tenant.id, this.batchSize),
      this.adminExternalSignatureService.listExternalSignatureConfigs(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))
    ),
    new TobeDeletedItem(
      CommonMessages.LAYER_NAME,
      this.deleteAllLayers(this.tenant.id, this.batchSize),
      this.adminLayerService.listLayers(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))
    ),
    new TobeDeletedItem(
      CommonMessages.WORKFLOW_NAME,
      this.deleteAllWorkflowDefinitions(this.tenant.id, this.batchSize),
      this.adminWorkflowDefinitionService.listWorkflowDefinitions(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))
    ),
    new TobeDeletedItem(
      CommonMessages.DESK,
      this.deleteAllDesks(this.tenant.id, this.batchSize),
      this.provisioningAdminDeskService.listDesks(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))
    ),
    new TobeDeletedItem(
      CommonMessages.METADATA_NAME,
      this.deleteAllMetadata(this.tenant.id, this.batchSize),
      this.adminMetadataService.listMetadata(this.tenant.id, false, null, 0, 1).pipe(map(result => result.totalElements))
    ),
    new TobeDeletedItem(
      CommonMessages.USER,
      this.deleteAllUsers(this.tenant.id, this.batchSize),
      this.adminTenantUserService.listTenantUsers(this.tenant.id, 0, 1).pipe(map(result => result.totalElements))
    ),
    new TobeDeletedItem(
      CommonMessages.TEMPLATE_NAME,
      this.deleteAllTemplates(this.tenant.id),
      this.getCustomTemplateCount(this.tenant.id)
    ),
  ];


  // <editor-fold desc="LifeCycle">


  constructor(private adminExternalSignatureService: AdminExternalSignatureService,
              private adminTrashBinService: AdminTrashBinService,
              private standardAdminTrashBinService: StandardAdminTrashBinService,
              private adminSecureMailService: AdminSecureMailService,
              private adminSealCertificateService: AdminSealCertificateService,
              private adminTenantService: AdminTenantService,
              private provisioningAdminFolderService: ProvisioningAdminFolderService,
              private internalAdminFolderService: InternalAdminFolderService,
              private adminLayerService: AdminLayerService,
              private adminTypologyService: AdminTypologyService,
              private adminMetadataService: AdminMetadataService,
              private adminWorkflowDefinitionService: AdminWorkflowDefinitionService,
              private provisioningAdminDeskService: ProvisioningAdminDeskService,
              private internalAdminDeskService: InternalAdminDeskService,
              private adminTenantUserService: AdminTenantUserService,
              private adminTemplatesService: AdminTemplateService,
              private notificationsService: NotificationsService,
              public activeModal: NgbActiveModal,
              @Inject(AdminTenantDeletePopupComponent.injectableTenantKey) public tenant?: TenantRepresentation) { }


  ngOnInit(): void {
    this.state = ProcessState.loading;
    const requests = [];
    this.items.forEach(item => requests.push(item.getCount$.pipe(tap(result => item.value = result))));

    forkJoin(requests)
      .subscribe({
        next: () => {
          this.totalCalls = this.items
            .map(item => Math.ceil(item.value / this.batchSize))
            .reduce((a, b) => a + b, 0);
          this.state = ProcessState.tobeProcessed;
        },
        error: e => this.notificationsService.showErrorMessage(this.messages.ErrorLoadingTenantOrDependencies(this.tenant.name), e.message)
      });
  }


  // <editor-fold desc="LifeCycle">


  deleteAllFolders(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<FolderDto>(
      tenantId,
      batchSize,
      (tid, p, b) =>
        this.internalAdminFolderService
          .listFoldersAsAdmin(tid, null, p, b)
          .pipe(map(page => {
              return {
                data: page.content,
                page: page.pageable.pageSize,
                pageSize: page.pageable.pageSize,
                total: page.totalElements
              };
            }
          )),
      (tid, f) => this.provisioningAdminFolderService.deleteFolderAsAdmin(tid, f.id)
    );
  }


  deleteAllTemplates(tenantId: string): Observable<BatchResult> {
    return this.getCustomTemplates(tenantId)
      .pipe(switchMap(
        templateTypes => {
          const deleteCalls = templateTypes
            .map(templateType => this.adminTemplatesService.deleteCustomTemplate(tenantId, templateType));
          return deleteCalls.length === 0
            ? of({completed: 0, total: 0, done: true})
            : forkJoin(deleteCalls).pipe(map(() => ({completed: 1, total: 1, done: true})));
        })
      );
  }


  getCustomTemplateCount(tenantId: string): Observable<number> {
    return this.getCustomTemplates(tenantId).pipe(map(result => result.length));
  }


  getCustomTemplates(tenantId: string): Observable<TemplateType[]> {

    const existenceCheckCalls = Object
      .values(TemplateType)
      .map(templateType => this.adminTemplatesService
        .getCustomTemplate(tenantId, templateType, 'body', false, {httpHeaderAccept: 'text/plain'})
        // On a 404 error, the tenant does not have any custom tenant defined.
        // FIXME : For some reason, the error returned when we set the 'text/plain' response is null.
        //  We can't filter the 404 error only.
        .pipe(
          map(() => templateType),
          catchError(() => of(null))
        ));

    return forkJoin(existenceCheckCalls)
      .pipe(
        switchMap((templates: TemplateType[]) => {
          // Null is returned if there is no custom templates
          // We have to filter null results to get the templates we have to delete
          const deletableTemplates: TemplateType[] = templates.filter(template => !!template);
          return of(deletableTemplates);
        })
      );
  }

  /**
   * Removing the last admin user of a tenant is forbidden and returns a 406 code.
   * But when we are in a tenant deletion process, we can let a last admin amongst tenant user.
   * It  won't affect the final tenant deletion.
   */
  deleteAllUsers(tenantId: string, batchSize: number): Observable<BatchResult> {

    // batch of size 1 will be a problem since we must let the last admin user
    batchSize = Math.max(batchSize, 2);

    return new Observable(observer => {
      const dataObservable: BehaviorSubject<UserRepresentation[]> = new BehaviorSubject<UserRepresentation[]>([]);
      // Retrieving a batch of users to be deleted to initiate the process
      this.adminTenantUserService.listTenantUsers(tenantId, 0, batchSize)
        .subscribe(data => {
          dataObservable.next(data.content);
          console.debug('Récupéré : ' + data.content.map(element => element.id).join(', '));
          const total = Math.ceil((data.totalElements - 1) / batchSize);
          let completed = 0;
          dataObservable.subscribe(users => {
            if (users.length > 1) {
              // Building a batch of deletion
              const removeCalls = [];
              users.forEach(user => removeCalls.push(this.adminTenantUserService.removeUser(tenantId, user.id)
                .pipe(catchError((err: HttpErrorResponse) => err.status === 406 ? of(null) : throwError(() => new Error(err.message)))),
              ));
              console.debug('Suppression : ' + users.map(user => user.id).join(', '));
              // Send a batch of deletions
              forkJoin(removeCalls)
                .subscribe({
                  next: () => {
                    // All deletions have succeeded
                    console.debug('Supprimés');
                    completed++;
                    // Send a status report
                    observer.next({completed: completed, total: total, done: false});
                    console.debug('Récupération');
                    // Retrieve other elements to be deleted
                    this.adminTenantUserService.listTenantUsers(tenantId, 0, batchSize)
                      .subscribe(data => {
                        console.debug('Récupéré : ' + data.content.map(user => user.id).join(', '));
                        dataObservable.next(data.content);
                      });
                  },
                  error: e => observer.error(e)
                });
            } else {
              // No more elements of type T, deletion process is complete
              observer.next({completed: completed, total: total, done: true});
              observer.complete();
            }
          });
        });
    });
  }


  deleteAllExternalSignatureConfigs(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<ExternalSignatureConfigDto>(
      tenantId,
      batchSize,
      (tid, p, b) => this.adminExternalSignatureService
        .listExternalSignatureConfigs(tid, p, b)
        .pipe(map(page => {
            return {
              data: page.content,
              page: page.pageable.pageNumber,
              pageSize: page.size,
              total: page.totalElements
            };
          }
        )),
      (tid, s) => this.adminExternalSignatureService.deleteExternalSignatureConfig(tid, s.id)
    );
  }


  deleteAllDesks(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<HierarchisedDeskRepresentation>(
      tenantId,
      batchSize,
      (t, p, b) => this.internalAdminDeskService.listHierarchicDesks(tenantId, false, [], p, b, ['NAME' + ',DESC'], null)
        .pipe(
          map(page => {
            const result: PaginatedResult<HierarchisedDeskRepresentation> = new PaginatedResult<HierarchisedDeskRepresentation>();
            result.data = page.content.filter(desk => desk.directChildrenCount === 0);
            result.page = page.number;
            result.total = page.totalElements;
            result.pageSize = page.size;
            return result;
          }),
          catchError(this.notificationsService.handleHttpError('listDesks'))
        ),
      (tid, d) => this.provisioningAdminDeskService.deleteDesk(tid, d.id)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteDesk')))
    );
  }


  deleteAllMetadata(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<Metadata>(
      tenantId,
      batchSize,
      (_, currentPage, __) => this.adminMetadataService
        .listMetadata(tenantId, false, null, currentPage, batchSize, [MetadataSortBy.Id + ',ASC'])
        .pipe(
          map(data => {
            const result = new PaginatedResult<Metadata>();
            result.data = data.content.map(dto => Object.assign(new Metadata(), dto));
            result.total = data.totalElements;
            return result;
          }),
          catchError(this.notificationsService.handleHttpError('listMetadataAsAdmin'))
        ),
      (_, metadata) => this.adminMetadataService
        .deleteMetadata(tenantId, metadata.id)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteMetadata')))
    );
  }


  deleteAllLayers(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<Layer>(
      tenantId,
      batchSize,
      (_, currentPage, __) => this.adminLayerService
        .listLayers(tenantId, currentPage, batchSize, [LayerSortBy.Id + ',ASC'])
        .pipe(
          map(data => {
            const result = new PaginatedResult<Layer>();
            result.data = data.content.map(dto => Object.assign(new Layer(), dto));
            result.total = data.totalElements;
            return result;
          }),
          catchError(this.notificationsService.handleHttpError('listMetadataAsAdmin'))
        ),
      (_, layer) => this.adminLayerService
        .deleteLayer(tenantId, layer.id)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteLayer')))
    );
  }


  deleteAllTrashBinFolders(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<Folder>(
      tenantId,
      batchSize,
      (_, currentPage, __) => this.adminTrashBinService
        .listTrashBinListableFolders(tenantId, currentPage, batchSize, [`${FolderSortBy.FolderId},ASC`])
        .pipe(
          map(data => {
            const result = new PaginatedResult<Folder>();
            result.data = data.content.map(dto => Object.assign(new Folder(), dto));
            result.total = data.totalElements;
            return result;
          }),
          catchError(this.notificationsService.handleHttpError('getTrashBinFolders'))
        ),
      (_, folder) => this.standardAdminTrashBinService
        .deleteTrashBinFolder(tenantId, folder.id)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteTrashBinFolder')))
    );
  }


  deleteAllTypes(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<TypeRepresentation>(
      tenantId,
      batchSize,
      (_, currentPage, __) => this.adminTypologyService.listTypes(tenantId, currentPage, batchSize)
        .pipe(
          map(page => {
            const result = new PaginatedResult<TypeRepresentation>();
            result.data = page.content;
            result.total = page.totalElements;
            return result;
          }),
          catchError(this.notificationsService.handleHttpError('listSealCertificateAsAdmin'))
        ),
      (_, type) => this.adminTypologyService
        .deleteType(tenantId, type.id)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteType')))
    );
  }


  deleteAllWorkflowDefinitions(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<WorkflowDefinitionDto>(
      tenantId,
      batchSize,
      (_, currentPage, __) => this.adminWorkflowDefinitionService
        .listWorkflowDefinitions(tenantId, currentPage, batchSize)
        .pipe(
          map(page => {
            const result = new PaginatedResult<WorkflowDefinitionDto>();
            result.data = page.content.map(dto => Object.assign({}, dto));
            result.total = page.totalElements;
            return result;
          }),
          catchError(this.notificationsService.handleHttpError('listWorkflowDefinitions'))
        ),
      (_, workflowDefinition) => this.adminWorkflowDefinitionService
        .deleteWorkflowDefinition(tenantId, workflowDefinition.key)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteWorkflowDefinition')))
    );
  }


  deleteAllSealCertificates(tenantId: string, batchSize: number): Observable<BatchResult> {
    return IpService.deleteAllElements<SealCertificate>(
      tenantId,
      batchSize,
      (_, currentPage, __) => this.adminSealCertificateService
        .listSealCertificate(tenantId, currentPage, batchSize)
        .pipe(
          map(page => {
            const result = new PaginatedResult<SealCertificate>();
            result.data = page.content.map(dto => Object.assign(new SealCertificate(), dto));
            result.total = page.totalElements;
            return result;
          }),
          catchError(this.notificationsService.handleHttpError('listSealCertificateAsAdmin'))
        ),
      (_, sealCertificate) => this.adminSealCertificateService
        .deleteSealCertificate(tenantId, sealCertificate.id)
        .pipe(catchError(this.notificationsService.handleHttpError('deleteSealCertificate')))
    );
  }


  onDeleteButtonClicked() {
    this.state = ProcessState.processing;
    const itemsToBeDeleted = this.items.slice();
    const deleteProcess: BehaviorSubject<TobeDeletedItem> = new BehaviorSubject<TobeDeletedItem>(itemsToBeDeleted.shift());
    new Observable(observer => deleteProcess
      .subscribe(item => {
        if (item) {
          console.debug('suppression des ' + item.name);
          if (item.value > 0) {
            item.state = ProcessState.processing;
            // For each batch, number of completed batch is incremented (for progress bars)
            item.deleteBatch$.subscribe({
              next: batchResult => {
                if (!batchResult.done) {
                  this.completedCalls++;
                  item.completed++;
                }
                console.debug('Lot terminé : ' + batchResult.completed + '/' + batchResult.total);
                if (batchResult.done) {
                  item.state = ProcessState.done;
                  deleteProcess.next(itemsToBeDeleted.shift());
                }
              },
              error: e => {
                item.state = ProcessState.failure;
                observer.error(e);
              }
            });
          } else {
            // Nothing to do with this item
            item.state = ProcessState.done;
            deleteProcess.next(itemsToBeDeleted.shift());
          }
        } else {
          // last item has been processed, all deletions have been done
          observer.next(null);
          observer.complete();
        }
      }))
      .subscribe({
        // All dependencies have been deleted, deletion of the tenant
        next: () => {
          this.adminTenantService
            .deleteTenant(this.tenant.id)
            .pipe(catchError(this.notificationsService.handleHttpError('delete tenant')))
            .subscribe({
              next: () => this.state = ProcessState.done,
              error: e => {
                this.state = ProcessState.tobeProcessed;
                this.notificationsService.showCrudMessage(CrudOperation.Delete, this.tenant, e.message, false);
              }
            });
        },
        // There was a problem during dependencies deletions
        error: e => {
          this.state = ProcessState.failure;
          this.notificationsService.showCrudMessage(CrudOperation.Delete, this.tenant, e.message, false);
        }
      });
  }


  onChange($event: string) {
    this.isFormValid = this.tenant.name.trim() === $event.trim();
  }


  getProgressPerCent(): number {
    return (this.totalCalls === 0 || this.state === ProcessState.done)
      ? 100
      : Math.round(this.completedCalls * 100 / this.totalCalls);
  }


  getProgressPerCentForItem(item: TobeDeletedItem) {
    return (item.value === 0 || item.state === ProcessState.done)
      ? 100
      : Math.round(item.completed * 100 / Math.ceil(item.value / this.batchSize));
  }


  getIcon(item: TobeDeletedItem): freeSolidIcons.IconDefinition {
    switch (item.state) {
      case ProcessState.tobeProcessed:
        return this.toBeProcessIcon;
      case ProcessState.processing:
        return CommonIcons.SPINNER_ICON;
      case ProcessState.done:
        return this.successIcon;
      case ProcessState.failure:
        return this.failureIcon;
    }
  }


}


class TobeDeletedItem {

  name: string;
  value: number;
  state: ProcessState;
  completed: number;
  getCount$: Observable<number>;
  deleteBatch$: Observable<BatchResult>;


  constructor(name: string, deleteBatch$: Observable<BatchResult>, getCount$?: Observable<number>) {
    this.name = name;
    this.state = ProcessState.tobeProcessed;
    this.value = 0;
    this.completed = 0;
    this.getCount$ = getCount$ ? getCount$ : of(undefined);
    this.deleteBatch$ = deleteBatch$;
  }

}


enum ProcessState {
  tobeProcessed = 'tobeProcessed', processing = 'Processing', done = 'Done', failure = 'Failure', loading = 'Loading'
}


