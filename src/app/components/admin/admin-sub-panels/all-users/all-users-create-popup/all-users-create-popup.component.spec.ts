/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { AllUsersCreatePopupComponent } from './all-users-create-popup.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { NgbActiveModal, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { ServerInfoDto } from '@libriciel/iparapheur-internal';
import { GdprInformationDetailsDto } from '@libriciel/iparapheur-internal/model/gdprInformationDetailsDto';
import { PasswordPolicies } from '@libriciel/iparapheur-internal/model/passwordPolicies';

describe('AllUsersCreatePopupComponent', () => {


  let component: AllUsersCreatePopupComponent;
  let fixture: ComponentFixture<AllUsersCreatePopupComponent>;

  const serverInfoMock: ServerInfoDto = {
    gdprInformationDetailsDto: {} as GdprInformationDetailsDto,
    passwordPolicies: {} as PasswordPolicies,
    signatureValidationServiceActivated: false
  };

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [ToastrModule.forRoot(), NgbNavModule],
        providers: [
          HttpClient,
          HttpHandler,
          NgbActiveModal,
          {provide: AllUsersCreatePopupComponent.INJECTABLE_SERVER_INFO_KEY, useValue: serverInfoMock}
        ],
        declarations: [AllUsersCreatePopupComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(AllUsersCreatePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
