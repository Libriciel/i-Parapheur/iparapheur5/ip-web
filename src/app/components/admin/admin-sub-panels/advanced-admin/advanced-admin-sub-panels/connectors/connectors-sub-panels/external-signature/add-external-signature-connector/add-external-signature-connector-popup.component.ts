/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { ConnectorMessages } from '../../../connector-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { Observable, of } from 'rxjs';
import { Validators, ValidatorFn, ValidationErrors, AbstractControl, FormGroup, FormControl } from '@angular/forms';
import { urlRfc3986Regex, isCurrentVersion52OrAbove } from '../../../../../../../../../utils/string-utils';
import { faEye, faEyeSlash, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { AdminExternalSignatureService } from '@libriciel/iparapheur-legacy';
import { AdminExternalSignatureService as InternalAdminExternalSignatureService, ExternalSignatureProvider, ExternalSignatureConfigDto } from '@libriciel/iparapheur-internal';
import { ExternalSignatureConnectorForm } from '../../../../../../../../../shared/models/custom-types';
import { switchMap } from 'rxjs/operators';
import { ExternalSignatureLevel } from '@libriciel/iparapheur-internal';

@Component({
  selector: 'app-add-external-signature-connector-popup',
  templateUrl: './add-external-signature-connector-popup.component.html',
  styleUrls: ['./add-external-signature-connector-popup.component.scss']
})
export class AddExternalSignatureConnectorPopupComponent implements OnInit {

  static readonly INJECTABLE_CURRENT_EXTERNAL_SIGNATURE_CONF_KEY: string = 'currentConfig';
  static readonly INJECTABLE_TENANT_ID_KEY: string = 'tenantId';
  static readonly INJECTABLE_EDIT_MODE_KEY: string = 'editMode';

  readonly messages = ConnectorMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly eyeIcon: IconDefinition = faEye;
  readonly eyeSlashIcon: IconDefinition = faEyeSlash;
  readonly show52Features: boolean = isCurrentVersion52OrAbove();

  availableServices: ExternalSignatureProvider[] = [
    ExternalSignatureProvider.YousignV2,
    ExternalSignatureProvider.YousignV3,
    ExternalSignatureProvider.Universign
  ];

  availableSignatureLevels: ExternalSignatureLevel[] = [
    ExternalSignatureLevel.Simple,
    ExternalSignatureLevel.Advanced,
    ExternalSignatureLevel.Qualified,
  ];

  showPasswordField: boolean = false;
  showTokenField: boolean = false;
  showLoginField: boolean = false;
  showSignatureLevelField: boolean = false;

  isProcessing: boolean = false;
  hidePassword: boolean = true;

  urlValidator: ValidatorFn = (control: AbstractControl): ValidationErrors => {
    const malformedUrlError: ValidationErrors = {malformedUrlError: true};
    return urlRfc3986Regex.test(control.value) ? null : malformedUrlError;
  };

  extSigForm: FormGroup<ExternalSignatureConnectorForm> = new FormGroup<ExternalSignatureConnectorForm>({
    name: new FormControl<string>(this.currentConfig?.name, [Validators.required, Validators.minLength(2)]),
    url: new FormControl<string>(this.currentConfig?.url, [Validators.required, Validators.minLength(2), this.urlValidator]),
    serviceName: new FormControl<ExternalSignatureProvider>(this.currentConfig?.serviceName, [Validators.required]),
    signatureLevel: new FormControl<ExternalSignatureLevel>(this.currentConfig?.signatureLevel ?? ExternalSignatureLevel.Simple, [Validators.required]),
    token: new FormControl<string>(this.currentConfig?.token, [Validators.minLength(2)]),
    login: new FormControl<string>(this.currentConfig?.login, [Validators.minLength(2)]),
    password: new FormControl<string>(this.currentConfig?.password, [Validators.minLength(2)])
  });


  // <editor-fold desc="LifeCycle">


  constructor(private notificationService: NotificationsService,
              private adminExternalSignatureService: AdminExternalSignatureService,
              private internalAdminExternalSignatureService: InternalAdminExternalSignatureService,
              private activeModal: NgbActiveModal,
              @Inject(AddExternalSignatureConnectorPopupComponent.INJECTABLE_TENANT_ID_KEY) private tenantId: string,
              @Inject(AddExternalSignatureConnectorPopupComponent.INJECTABLE_EDIT_MODE_KEY) public editMode: boolean,
              @Inject(AddExternalSignatureConnectorPopupComponent.INJECTABLE_CURRENT_EXTERNAL_SIGNATURE_CONF_KEY) public currentConfig: ExternalSignatureConfigDto) {}


  ngOnInit(): void {

    if (this.show52Features) {
      this.availableServices.push(ExternalSignatureProvider.Docage, ExternalSignatureProvider.OodriveSign);
    }
    this.availableServices.sort();

    this.onServiceChange();
    if (this.editMode) {
      this.extSigForm.controls.serviceName.disable();
    }
  }


  // </editor-fold desc="LifeCycle">


  onServiceChange(): void {

    const serviceName: ExternalSignatureProvider = this.extSigForm.controls.serviceName.value;
    this.showTokenField = [ExternalSignatureProvider.YousignV2, ExternalSignatureProvider.YousignV3, ExternalSignatureProvider.OodriveSign].includes(serviceName);
    this.showLoginField = [ExternalSignatureProvider.Docage, ExternalSignatureProvider.Universign].includes(serviceName);
    this.showPasswordField = [ExternalSignatureProvider.Docage, ExternalSignatureProvider.Universign].includes(serviceName);
    this.showSignatureLevelField = !!serviceName;

    if (!this.editMode) {

      if (this.showTokenField) {
        this.extSigForm.controls.token.addValidators(Validators.required);
      } else {
        this.extSigForm.controls.token.removeValidators(Validators.required);
      }

      if (this.showPasswordField) {
        this.extSigForm.controls.password.addValidators(Validators.required);
      } else {
        this.extSigForm.controls.password.removeValidators(Validators.required);
      }

    }

    switch (serviceName) {

      case ExternalSignatureProvider.YousignV3:
      case ExternalSignatureProvider.OodriveSign:
        this.availableSignatureLevels = [
          ExternalSignatureLevel.Simple,
          ExternalSignatureLevel.Advanced,
          ExternalSignatureLevel.Qualified,
        ];
        this.extSigForm.controls.signatureLevel.enable();
        break;

      default:
        this.availableSignatureLevels = [ExternalSignatureLevel.Simple];
        this.currentConfig.signatureLevel = ExternalSignatureLevel.Simple;
        this.extSigForm.patchValue({signatureLevel: ExternalSignatureLevel.Simple});
        this.extSigForm.controls.signatureLevel.disable();
        break;

    }

  }

  executeForm(): void {

    if (!this.extSigForm.valid) {
      return;
    }

    this.isProcessing = true;

    this.currentConfig.name = this.extSigForm.controls.name.value;
    this.currentConfig.url = this.extSigForm.controls.url.value;
    this.currentConfig.serviceName = this.extSigForm.controls.serviceName.value;
    this.currentConfig.signatureLevel = this.extSigForm.controls.signatureLevel.value;
    this.currentConfig.login = this.extSigForm.controls.login.value;
    this.currentConfig.password = this.extSigForm.controls.password.value;
    this.currentConfig.token = this.extSigForm.controls.token.value;

    const testRequest$: Observable<any> = (this.currentConfig.password?.length > 0 || this.currentConfig.token?.length > 0)
      ? this.internalAdminExternalSignatureService.testExternalSignatureConfig(this.tenantId, this.currentConfig)
      : of(1);

    const saveRequest$: Observable<ExternalSignatureConfigDto> = this.editMode
      ? this.adminExternalSignatureService.editExternalSignatureConfig(this.tenantId, this.currentConfig.id, this.currentConfig)
      : this.adminExternalSignatureService.createExternalSignatureConfig(this.tenantId, this.currentConfig);

    testRequest$
      .subscribe({
          next: () => {
            saveRequest$.subscribe({
              next: () => {
                this.notificationService.showSuccessMessage(this.editMode
                  ? ConnectorMessages.SUCCESS_EDITING_CONFIGURATION
                  : ConnectorMessages.SUCCESS_CREATING_CONFIGURATION,
                );
                this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
              },
              error: e => this.notificationService.showErrorMessage(
                this.editMode
                  ? ConnectorMessages.ERROR_EDITING_CONFIGURATION
                  : ConnectorMessages.ERROR_CREATING_CONFIGURATION,
                e.message
              )
            });
          },
          error: e => this.notificationService.showErrorMessage(
            this.editMode
              ? ConnectorMessages.ERROR_EDITING_CONFIGURATION
              : ConnectorMessages.ERROR_CREATING_CONFIGURATION,
            e.message
          )
        }
      )
      .add(() => this.isProcessing = false);
  }


  cancel(): void {
    this.activeModal.dismiss(CommonMessages.ACTION_RESULT_CANCEL);
  }


}
