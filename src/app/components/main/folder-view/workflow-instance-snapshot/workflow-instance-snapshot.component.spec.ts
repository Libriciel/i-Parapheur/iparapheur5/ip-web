/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { WorkflowInstanceSnapshotComponent } from './workflow-instance-snapshot.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FolderViewBlockNamePipe } from '../../../../utils/folder-view-block-name.pipe';
import { ActionIconPipe } from '../../../../shared/utils/action-icon.pipe';
import { ActionNamePipe } from '../../../../shared/utils/action-name.pipe';
import { NamePipe } from '../../../../shared/utils/name.pipe';
import { IpWorkflowInstance } from '../../../../models/workflows/ip-workflow-instance';

describe('WorkflowInstanceSnapshotComponent', () => {
  let component: WorkflowInstanceSnapshotComponent;
  let fixture: ComponentFixture<WorkflowInstanceSnapshotComponent>;


  // FINISHED TRANSFER
  // CASE 1 : simple transfer

  const case1: IpWorkflowInstance = {
    isOver: true,
    currentStepIdx: 0,
    rejected: false,
    isRejected: false,
    id: "5c92462f-e881-11ef-9271-0242ac120014",
    name: "case 1",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "5c946937-e881-11ef-9271-0242ac120014",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:06:18.844Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "5d81afb0-e881-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "TRANSFERRED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:08:09.938Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "9fb502cf-e881-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: new Date("2025-02-11T14:09:54.748Z"),
        delegatedByDesk: null
      }
    ]
  };

  // CASE 2 : double transfer

  const case2: IpWorkflowInstance = {
    isOver: true,
    currentStepIdx: 0,
    rejected: false,
    isRejected: false,
    id: "6b0d8d22-e881-11ef-9271-0242ac120014",
    name: "case 2",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "6b0e298a-e881-11ef-9271-0242ac120014",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:06:42.657Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "6bb2ce43-e881-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "TRANSFERRED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:08:32.610Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "ad3a29bf-e881-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "TRANSFERRED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: new Date("2025-02-11T14:10:10.871Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "e7c8b44c-e881-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau C')],
        completedDate: new Date("2025-02-11T14:16:19.999Z"),
        delegatedByDesk: null
      }
    ]
  };

  // CASE 3 : simple transfer rejected

  const case3: IpWorkflowInstance = {
    isOver: true,
    currentStepIdx: 0,
    rejected: false,
    isRejected: false,
    id: "7377f005-e881-11ef-9271-0242ac120014",
    name: "case 3",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "737928ad-e881-11ef-9271-0242ac120014",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:06:56.831Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "7423c0d6-e881-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "TRANSFERRED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:08:45.361Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "b4d0e9bf-e881-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "REJECTED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: new Date("2025-02-11T14:10:25.620Z"),
        delegatedByDesk: null
      }
    ]
  };

  // FINISHED SECOND OPINION

  // CASE 4 : simple second opinion

  const case4: IpWorkflowInstance = {
    isOver: true,
    currentStepIdx: 0,
    rejected: false,
    isRejected: false,
    id: "7dd3eab8-e881-11ef-9271-0242ac120014",
    name: "case 4",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "7dd57180-e881-11ef-9271-0242ac120014",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:07:14.259Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "7e87f8e9-e881-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "SECONDED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:09:02.533Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "bf0e38ad-e881-11ef-9271-0242ac120014",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: new Date("2025-02-11T14:10:50.303Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "ff4c4b52-e881-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:17:08.438Z"),
        delegatedByDesk: null
      }
    ]
  };


  // CASE 5 : double second opinion

  const case5: IpWorkflowInstance = {
    isOver: true,
    currentStepIdx: 0,
    rejected: false,
    isRejected: false,
    id: "87db0eab-e881-11ef-9271-0242ac120014",
    name: "case 5",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "87dbf833-e881-11ef-9271-0242ac120014",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:07:31.198Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "889f969c-e881-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "SECONDED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:09:17.080Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "c7ba383b-e881-11ef-9271-0242ac120014",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "SECONDED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: new Date("2025-02-11T14:11:24.107Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "136fa127-e882-11ef-9271-0242ac120014",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau C')],
        completedDate: new Date("2025-02-11T14:16:39.928Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "cfb47f46-e882-11ef-9271-0242ac120014",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: new Date("2025-02-11T14:16:54.096Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "d81fcd44-e882-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:17:15.092Z"),
        delegatedByDesk: null
      }
    ]
  };


  // CASE 6 : ping pong double second opinion

  const case6: IpWorkflowInstance = {
    isOver: true,
    currentStepIdx: 0,
    rejected: false,
    isRejected: false,
    id: "9474ea6e-e881-11ef-9271-0242ac120014",
    name: "case 6",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "9476e666-e881-11ef-9271-0242ac120014",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:07:52.218Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "95280e3f-e881-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "SECONDED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:09:25.046Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "cc78f909-e881-11ef-9271-0242ac120014",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "SECONDED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: new Date("2025-02-11T14:11:43.180Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "1ecf4ffc-e882-11ef-9271-0242ac120014",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:17:27.922Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "ec4a7680-e882-11ef-9271-0242ac120014",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: new Date("2025-02-11T14:17:41.280Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "f442b75e-e882-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T14:17:54.257Z"),
        delegatedByDesk: null
      }
    ]
  };

  // PENDING TRANSFER

  // CASE 7 : simple pending transfer

  const case7: IpWorkflowInstance = {
    isOver: false,
    currentStepIdx: 2,
    rejected: false,
    isRejected: false,
    id: "0f59e238-e893-11ef-9271-0242ac120014",
    name: "case 7",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "0f5b1ae0-e893-11ef-9271-0242ac120014",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T16:12:59.862Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "10114bc9-e893-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "TRANSFERRED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getUser()],
        completedDate: new Date("2025-02-11T16:19:00.021Z"),
        delegatedByDesk: null
      },
      {
        delegatedByDesk: null,
        validatedBy: [],
        id: "e6b6f0b9-e893-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "CURRENT",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: null
      }
    ]
  };

  // CASE 8 : double pending transfer

  const case8: IpWorkflowInstance = {
    isOver: false,
    currentStepIdx: 3,
    rejected: false,
    isRejected: false,
    id: "7fd8ddbb-e893-11ef-9271-0242ac120014",
    name: "case 8",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "7fd9c743-e893-11ef-9271-0242ac120014",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T16:16:08.683Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "809c543c-e893-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "TRANSFERRED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T16:19:15.427Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "efe4a49a-e893-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "TRANSFERRED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: new Date("2025-02-11T16:19:45.605Z"),
        delegatedByDesk: null
      },
      {
        delegatedByDesk: null,
        validatedBy: [],
        id: "01e567cf-e894-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "CURRENT",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau C')],
        completedDate: null
      }
    ]
  };

  // PENDING SECOND OPINION

  // CASE 9 : simple pending second opinion

  const case9: IpWorkflowInstance = {
    isOver: false,
    currentStepIdx: 2,
    rejected: false,
    isRejected: false,
    id: "cb1593c8-e893-11ef-9271-0242ac120014",
    name: "case 9",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "cb167e50-e893-11ef-9271-0242ac120014",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T16:18:14.821Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "cbc8b699-e893-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "SECONDED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T16:19:22.583Z"),
        delegatedByDesk: null
      },
      {
        delegatedByDesk: null,
        validatedBy: [],
        id: "f4286808-e893-11ef-9271-0242ac120014",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "CURRENT",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: null
      },
      {
        id: null,
        delegatedByDesk: null,
        validatedBy: [],
        name: "VISA",
        type: "VISA",
        state: "UPCOMING",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: null
      }
    ]
  };

  // CASE 10 : double pending second opinion

  const case10: IpWorkflowInstance = {
    isOver: false,
    currentStepIdx: 3,
    rejected: false,
    isRejected: false,
    id: "a7121081-e893-11ef-9271-0242ac120014",
    name: "case 10",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "a712ace9-e893-11ef-9271-0242ac120014",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T16:17:14.405Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "a7c6e102-e893-11ef-9271-0242ac120014",
        name: "VISA",
        type: "VISA",
        state: "SECONDED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-11T16:19:08.271Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "eba1ca8a-e893-11ef-9271-0242ac120014",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "SECONDED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: new Date("2025-02-11T16:19:58.388Z"),
        delegatedByDesk: null
      },
      {
        delegatedByDesk: null,
        validatedBy: [],
        id: "0983eff4-e894-11ef-9271-0242ac120014",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "CURRENT",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau C')],
        completedDate: null
      },
      {
        id: null,
        delegatedByDesk: null,
        validatedBy: [],
        name: "VISA",
        type: "VISA",
        state: "UPCOMING",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: null
      }
    ]
  };


  // CASE 11 : finished multiple validator OR step transfer

  const case11: IpWorkflowInstance = {
    isOver: true,
    currentStepIdx: 0,
    rejected: false,
    isRejected: false,
    id: "37ea2ff4-e929-11ef-88f2-0242ac12000d",
    name: "case11",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "37f838dc-e929-11ef-88f2-0242ac12000d",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:07:53.011Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "39090ce4-e929-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "TRANSFERRED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:16:30.947Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "6dadf594-e92a-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau C')],
        completedDate: new Date("2025-02-12T10:17:49.932Z"),
        delegatedByDesk: null
      }
    ]
  };

  // CASE 12 : finished multiple validator OR step second opinion

  const case12: IpWorkflowInstance = {
    isOver: true,
    currentStepIdx: 0,
    rejected: false,
    isRejected: false,
    id: "47b12fc7-e929-11ef-88f2-0242ac12000d",
    name: "case12",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "47b3c7ff-e929-11ef-88f2-0242ac12000d",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:08:19.035Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "48859737-e929-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "SECONDED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:16:40.045Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "731a0d68-e92a-11ef-88f2-0242ac12000d",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau C')],
        completedDate: new Date("2025-02-12T10:18:00.936Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "a3532ce6-e92a-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:20:09.130Z"),
        delegatedByDesk: null
      }
    ]
  };

  // CASE 13 : finished multiple validator AND step transfer

  const case13: IpWorkflowInstance = {
    isOver: true,
    currentStepIdx: 0,
    rejected: false,
    isRejected: false,
    id: "5b5dcf3a-e929-11ef-88f2-0242ac12000d",
    name: "case13",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "5b5ff242-e929-11ef-88f2-0242ac12000d",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:08:51.990Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "5c245317-e929-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "TRANSFERRED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:16:48.916Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "7861fb68-e92a-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau C')],
        completedDate: new Date("2025-02-12T10:18:14.448Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "5c25b2b0-e929-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: new Date("2025-02-12T10:20:28.378Z"),
        delegatedByDesk: null
      }
    ]
  };

  // CASE 14 : finished multiple validator AND step second opinion

  const case14: IpWorkflowInstance = {
    isOver: true,
    currentStepIdx: 0,
    rejected: false,
    isRejected: false,
    id: "657df892-e929-11ef-88f2-0242ac12000d",
    name: "case14",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "65801b9a-e929-11ef-88f2-0242ac12000d",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:09:08.959Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "66447d6f-e929-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "SECONDED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:16:56.794Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "7d168206-e92a-11ef-88f2-0242ac12000d",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau C')],
        completedDate: new Date("2025-02-12T10:18:37.116Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "66458de8-e929-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: new Date("2025-02-12T10:20:46.341Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "b8e295ed-e92a-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:21:13.924Z"),
        delegatedByDesk: null
      }
    ]
  };

  // CASE 15 : pending multiple validator OR step transfer

  const case15: IpWorkflowInstance = {
    isOver: false,
    currentStepIdx: 2,
    rejected: false,
    isRejected: false,
    id: "71ff1c5a-e929-11ef-88f2-0242ac12000d",
    name: "case15",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "72005502-e929-11ef-88f2-0242ac12000d",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:09:30.076Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "72d6df2a-e929-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "TRANSFERRED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:17:02.913Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [],
        id: "80ba840c-e92a-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "CURRENT",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau C')],
        completedDate: null,
        delegatedByDesk: null
      }
    ]
  };

  // CASE 16 : pending multiple validator OR step second opinion

  const case16: IpWorkflowInstance = {
    isOver: false,
    currentStepIdx: 2,
    rejected: false,
    isRejected: false,
    id: "7cbc97fd-e929-11ef-88f2-0242ac12000d",
    name: "case16",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "7cbdd0a5-e929-11ef-88f2-0242ac12000d",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:09:48.098Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "7d925efd-e929-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "SECONDED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:17:10.213Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [],
        id: "851245b0-e92a-11ef-88f2-0242ac12000d",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "CURRENT",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau C')],
        completedDate: null,
        delegatedByDesk: null
      },
      {
        id: "",
        validatedBy: [],
        name: "VISA",
        type: "VISA",
        state: "UPCOMING",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [
          getValidator('Bureau A'),
          getValidator('Bureau B')
        ],
        completedDate: null,
        delegatedByDesk: null
      }
    ]
  };

  // CASE 17 : pending multiple validator AND step transfer

  const case17: IpWorkflowInstance = {
    isOver: false,
    currentStepIdx: 2,
    rejected: false,
    isRejected: false,
    id: "96acc340-e929-11ef-88f2-0242ac12000d",
    name: "case17",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "96adacc8-e929-11ef-88f2-0242ac12000d",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:10:32.026Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "97c4263d-e929-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "TRANSFERRED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:17:16.622Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [],
        id: "88e56e00-e92a-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "CURRENT",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau C')],
        completedDate: null,
        delegatedByDesk: null
      },
      {
        validatedBy: [],
        id: "97c55ec6-e929-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "PENDING",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: null,
        delegatedByDesk: null
      }
    ]
  };

  // CASE 18 : pending multiple validator AND step second opinion

  const case18: IpWorkflowInstance = {
    isOver: false,
    currentStepIdx: 3,
    rejected: false,
    isRejected: false,
    id: "aa9b25c8-e929-11ef-88f2-0242ac12000d",
    name: "case18",
    finalDesk: getValidator('Bureau A'),
    steps: [
      {
        validatedBy: [getUser()],
        id: "aa9d21c0-e929-11ef-88f2-0242ac12000d",
        name: "START",
        type: "START",
        state: "VALIDATED",
        actedUponBy: getUser(),
        workflowIndex: 0,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:11:05.367Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [getUser()],
        id: "aba0ae75-e929-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "SECONDED",
        actedUponBy: getUser(),
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: new Date("2025-02-12T10:17:24.746Z"),
        delegatedByDesk: null
      },
      {
        validatedBy: [],
        id: "aba198de-e929-11ef-88f2-0242ac12000d",
        name: "VISA",
        type: "VISA",
        state: "PENDING",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: null,
        delegatedByDesk: null
      },
      {
        validatedBy: [],
        id: "8dbd340e-e92a-11ef-88f2-0242ac12000d",
        name: "SECOND_OPINION",
        type: "SECOND_OPINION",
        state: "CURRENT",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau C')],
        completedDate: null,
        delegatedByDesk: null
      },
      {
        id: "",
        delegatedByDesk: null,
        validatedBy: [],
        name: "VISA",
        type: "VISA",
        state: "UPCOMING",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau A')],
        completedDate: null
      },
      {
        id: "",
        delegatedByDesk: null,
        validatedBy: [],
        name: "VISA",
        type: "VISA",
        state: "UPCOMING",
        actedUponBy: null,
        workflowIndex: 2,
        stepIndex: 0,
        validators: [getValidator('Bureau B')],
        completedDate: null
      }
    ]
  };

  function getUser() {
    return {
      id: "2cb41c11-e2ed-4cb7-ae30-0094d4d310ca",
      name: "user",
      firstname: "Initial",
      lastname: "Admin"
    };
  }

  function getValidator(name: string) {
    return {
      id: "desk_id_" + name,
      name: name,
      tenantId: null
    };
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [],
      declarations: [
        WorkflowInstanceSnapshotComponent,
        FolderViewBlockNamePipe,
        ActionIconPipe,
        ActionNamePipe,
        NamePipe
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(WorkflowInstanceSnapshotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display correctly a finished simple transfer', () => {
    component.workflowInstance = case1;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case1.steps[0]
      ],
      [
        case1.steps[1],
        case1.steps[2]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case1.steps[0])).toBeTrue();
    expect(component.areParallel(case1.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case1.steps)).toEqual(0);
  });

  it('should display correctly a finished double transfer', () => {
    component.workflowInstance = case2;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case2.steps[0]
      ],
      [
        case2.steps[1],
        case2.steps[2],
        case2.steps[3]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case2.steps[0])).toBeTrue();
    expect(component.areParallel(case2.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case2.steps)).toEqual(0);
  });

  it('should display correctly a rejected simple transfer', () => {
    component.workflowInstance = case3;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case3.steps[0]
      ],
      [
        case3.steps[1],
        case3.steps[2]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case3.steps[0])).toBeTrue();
    expect(component.areParallel(case3.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case3.steps)).toEqual(0);
  });

  it('should display correctly a finished simple second opinion', () => {
    component.workflowInstance = case4;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case4.steps[0]
      ],
      [
        case4.steps[1],
        case4.steps[2],
        case4.steps[3]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case4.steps[0])).toBeTrue();
    expect(component.areParallel(case4.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case4.steps)).toEqual(0);
  });

  it('should display correctly a finished double second opinion', () => {
    component.workflowInstance = case5;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case5.steps[0]
      ],
      [
        case5.steps[1],
        case5.steps[2],
        case5.steps[3],
        case5.steps[4],
        case5.steps[5]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case5.steps[0])).toBeTrue();
    expect(component.areParallel(case5.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case5.steps)).toEqual(0);
  });

  it('should display correctly a finished double second opinion with a second opinion going back to the first desk', () => {
    component.workflowInstance = case6;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case6.steps[0]
      ],
      [
        case6.steps[1],
        case6.steps[2],
        case6.steps[3],
        case6.steps[4],
        case6.steps[5]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case6.steps[0])).toBeTrue();
    expect(component.areParallel(case6.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case6.steps)).toEqual(0);
  });

  it('should display correctly a pending simple transfer', () => {
    component.workflowInstance = case7;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case7.steps[0]
      ],
      [
        case7.steps[1],
        case7.steps[2]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case7.steps[0])).toBeTrue();
    expect(component.areParallel(case7.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case7.steps)).toEqual(1);
  });

  it('should display correctly a pending double transfer', () => {
    component.workflowInstance = case8;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case8.steps[0]
      ],
      [
        case8.steps[1],
        case8.steps[2],
        case8.steps[3]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case8.steps[0])).toBeTrue();
    expect(component.areParallel(case8.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case8.steps)).toEqual(1);
  });

  it('should display correctly a pending simple second opinion', () => {
    component.workflowInstance = case9;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case9.steps[0]
      ],
      [
        case9.steps[1],
        case9.steps[2],
        case9.steps[3]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case9.steps[0])).toBeTrue();
    expect(component.areParallel(case9.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case9.steps)).toEqual(1);
  });

  it('should display correctly a pending double second opinion', () => {
    component.workflowInstance = case10;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case10.steps[0]
      ],
      [
        case10.steps[1],
        case10.steps[2],
        case10.steps[3],
        case10.steps[4]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case10.steps[0])).toBeTrue();
    expect(component.areParallel(case10.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case10.steps)).toEqual(1);
  });

  it('should display correctly a finished multiple validator OR step transfer', () => {
    component.workflowInstance = case11;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case11.steps[0]
      ],
      [
        case11.steps[1],
        case11.steps[2]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case11.steps[0])).toBeTrue();
    expect(component.areParallel(case11.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case11.steps)).toEqual(0);
  });

  it('should display correctly a finished multiple validator OR step second opinion', () => {
    component.workflowInstance = case12;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case12.steps[0]
      ],
      [
        case12.steps[1],
        case12.steps[2],
        case12.steps[3]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case12.steps[0])).toBeTrue();
    expect(component.areParallel(case12.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case12.steps)).toEqual(0);
  });

  it('should display correctly a finished multiple validator AND step transfer', () => {
    component.workflowInstance = case13;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case13.steps[0]
      ],
      [
        case13.steps[1],
        case13.steps[2],
        case13.steps[3]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case13.steps[0])).toBeTrue();
    expect(component.areParallel(case13.steps)).toBeTrue();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case13.steps)).toEqual(0);
  });

  it('should display correctly a finished multiple validator AND step second opinion', () => {
    component.workflowInstance = case14;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case14.steps[0]
      ],
      [
        case14.steps[1],
        case14.steps[2],
        case14.steps[3],
        case14.steps[4]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case14.steps[0])).toBeTrue();
    expect(component.areParallel(case14.steps)).toBeTrue();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case14.steps)).toEqual(0);
  });

  it('should display correctly a pending multiple validator OR step transfer', () => {
    component.workflowInstance = case15;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case15.steps[0]
      ],
      [
        case15.steps[1],
        case15.steps[2]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case15.steps[0])).toBeTrue();
    expect(component.areParallel(case15.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case15.steps)).toEqual(1);
  });

  it('should display correctly a pending multiple validator OR step second opinion', () => {
    component.workflowInstance = case16;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case16.steps[0]
      ],
      [
        case16.steps[1],
        case16.steps[2],
        case16.steps[3]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case16.steps[0])).toBeTrue();
    expect(component.areParallel(case16.steps)).toBeFalse();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case16.steps)).toEqual(1);
  });

  it('should display correctly a pending multiple validator AND step transfer', () => {
    component.workflowInstance = case17;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case17.steps[0]
      ],
      [
        case17.steps[1],
        case17.steps[2],
        case17.steps[3]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case17.steps[0])).toBeTrue();
    expect(component.areParallel(case17.steps)).toBeTrue();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case17.steps)).toEqual(2);
  });

  it('should display correctly a pending multiple validator AND step second opinion', () => {
    component.workflowInstance = case18;
    expect(component).toBeTruthy();
    expect(component.steps).toEqual([
      [
        case18.steps[0]
      ],
      [
        case18.steps[1],
        case18.steps[2],
        case18.steps[3],
        case18.steps[4],
        case18.steps[5]
      ]
    ]);

    expect(component.isWorkflowFirstStep(case18.steps[0])).toBeTrue();
    expect(component.areParallel(case18.steps)).toBeTrue();
    expect(component.hasCreationWorkflow()).toBeFalse();
    expect(component.countPending(case18.steps)).toEqual(2);
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
