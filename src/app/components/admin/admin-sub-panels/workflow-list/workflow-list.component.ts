/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { faCopy } from '@fortawesome/free-regular-svg-icons';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsService } from '../../../../services/notifications.service';
import { CommonMessages } from '../../../../shared/common-messages';
import { AdminMessages } from '../admin-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { GlobalPopupService } from '../../../../shared/service/global-popup.service';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { AdminWorkflowDefinitionService, WorkflowDefinitionSortBy, WorkflowDefinitionRepresentation } from '@libriciel/iparapheur-provisioning';
import { catchError } from 'rxjs/operators';
import { WorkflowEditorMessages } from '../workflow-editor/workflow-editor-messages';
import { getFirstErrorMessage } from '../../../../utils/string-utils';


@Component({
  selector: 'app-workflow-list',
  templateUrl: './workflow-list.component.html',
  styleUrls: ['./workflow-list.component.scss']
})
export class WorkflowListComponent implements OnInit {

  readonly pageSizes = [10, 15, 20, 50, 100];
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly caretUp = faCaretUp;
  readonly caretDown = faCaretDown;
  readonly sortByEnum = WorkflowDefinitionSortBy;
  readonly messages = AdminMessages;
  readonly copyIcon = faCopy;

  tenantId: string;
  currentSearchTerm = null;

  workflowDefinitionRepresentationList: WorkflowDefinitionRepresentation[] = [];
  page = 1;
  total = 0;
  pageSizeIndex = 1;
  sortBy: WorkflowDefinitionSortBy = WorkflowDefinitionSortBy.Name;
  asc = true;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  loading: boolean = false;
  colspan: number;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              private route: ActivatedRoute,
              public globalPopupService: GlobalPopupService,
              private adminWorkflowDefinitionService: AdminWorkflowDefinitionService,
              private router: Router) { }


  ngOnInit(): void {
    this.route.parent.parent.data.subscribe(data => {
      this.userPreferences = data["userPreferences"];
      this.colspan = 4 + (this.userPreferences.showAdminIds ? 1 : 0);
    });
    this.route.parent.params.subscribe(params => {
      this.tenantId = params['tenantId'];
      this.requestWorkflowList(false);
    });
  }


  // </editor-fold desc="LifeCycle">


  updateSearchTerm(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentSearchTerm = newTerm;
    this.page = 1;
    this.requestWorkflowList(false);
  }


  requestWorkflowList(isNewPageSize: boolean) {

    this.loading = true;
    if (isNewPageSize) {
      this.page = 1;
    }

    const requestSortBy = [this.sortBy + (this.asc ? ',ASC' : ',DESC')];
    this.adminWorkflowDefinitionService
      .listWorkflowDefinitions(this.tenantId, this.page - 1, this.getPageSize(this.pageSizeIndex), requestSortBy, this.currentSearchTerm)
      .pipe(catchError(this.notificationsService.handleHttpError('listWorkflowDefinitions')))
      .subscribe({
        next: workflowsRetrieved => {
          this.workflowDefinitionRepresentationList = workflowsRetrieved.content;
          this.total = workflowsRetrieved.totalElements;
        },
        error: e => this.notificationsService.showErrorMessage(WorkflowEditorMessages.ERROR_RETRIEVING_WORKFLOW_DEFINITIONS, getFirstErrorMessage(e))
      })
      .add(() => this.loading = false);
  }


  onRowOrderClicked(row: WorkflowDefinitionSortBy) {
    this.asc = (row === this.sortBy) ? !this.asc : true;
    this.sortBy = row;
    this.requestWorkflowList(false);
  }


  prettyPrintId(id: string) {
    return id.split(/[:]+/).pop();
  }


  onCreateWorkflowClicked() {
    this.router
      .navigate([`/admin/${this.tenantId}/workflows/new`])
      .then(() => console.log('navigated to workflow creation'));
  }

  // FIXME import workflow from a BPMN file
  /*  onImportWorkflowClicked() {
      this.modalService
        .open(ImportWorkflowPopupComponent, {
          injector: Injector.create({
            providers: [{provide: ImportWorkflowPopupComponent.INJECTABLE_TENANT_KEY, useValue: this.tenantId}]
          })
        })
        .result
        .then(
          (result) => {
            if (result === CommonMessages.ACTION_RESULT_OK) {
              this.requestWorkflowList();
            }
          },
          () => { /!* Dismissed *!/ }
        );
    }*/


  onCloneButtonClicked(workflowDefinition: WorkflowDefinitionRepresentation) {
    this.router
      .navigate([`/admin/${this.tenantId}/workflows/definitions/${workflowDefinition.key}/clone`])
      .then(() => console.log('navigated to workflow edition'));
  }


  onEditButtonClicked(workflowDefinition: WorkflowDefinitionRepresentation) {
    this.router
      .navigate([`/admin/${this.tenantId}/workflows/definitions/${workflowDefinition.key}`])
      .then(() => console.log('navigated to workflow edition'));
  }


  onDeleteButtonClicked(workflowDefinition: WorkflowDefinitionRepresentation) {
    this.globalPopupService
      .showDeleteValidationPopup(this.messages.workflowValidationPopupLabel(workflowDefinition.name))
      .then(
        () => this.deleteWorkflow(workflowDefinition),
        () => {/* dismissed */}
      );
  }


  deleteWorkflow(workflowDefinition: WorkflowDefinitionRepresentation) {
    this.adminWorkflowDefinitionService
      .deleteWorkflowDefinition(this.tenantId, workflowDefinition.key)
      .pipe(catchError(this.notificationsService.handleHttpError('deleteWorkflowDefinition')))
      .subscribe({
        next: () => {
          this.notificationsService.showSuccessMessage(WorkflowEditorMessages.SUCCESS_DELETING_WORKFLOW_DEFINITION);
          this.requestWorkflowList(false);
        },
        error: e => this.notificationsService.showErrorMessage(WorkflowEditorMessages.ERROR_DELETING_WORKFLOW_DEFINITION, getFirstErrorMessage(e))
      });
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


}
