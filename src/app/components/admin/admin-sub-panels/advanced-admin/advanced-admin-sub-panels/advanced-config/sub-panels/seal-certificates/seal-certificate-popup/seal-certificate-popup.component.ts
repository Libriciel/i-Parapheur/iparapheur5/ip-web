/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { SealCertificateMessages } from '../seal-certificate-messages';
import { AdminSealCertificateService, SealCertificateDto } from '@libriciel/iparapheur-provisioning';
import { AdminSealCertificateService as InternalAdminSealCertificateService } from '@libriciel/iparapheur-internal';
import { catchError } from 'rxjs/operators';
import { getFirstErrorMessage } from '../../../../../../../../../utils/string-utils';
import { faEyeSlash, faEye } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-seal-certificate-popup',
  templateUrl: './seal-certificate-popup.component.html',
  styleUrls: ['./seal-certificate-popup.component.scss']
})
export class SealCertificatePopupComponent implements OnInit, OnDestroy {

  static readonly INJECTABLE_TENANT_ID_KEY = 'tenantId';
  static readonly INJECTABLE_SEAL_CERTIFICATE_KEY = 'sealCertificate';

  readonly commonMessages = CommonMessages;
  readonly messages = SealCertificateMessages;
  readonly commonIcons = CommonIcons;
  readonly eyeIcon = faEye;
  readonly eyeSlashIcon = faEyeSlash;


  originalSignatureImageBlobSrc: string;
  isProcessing = false;

  hidePassword = true;
  pendingPassword: string;
  pendingCertificateFile: File;
  pendingSignatureImageFile: File;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public adminSealCertificateService: AdminSealCertificateService,
              public internalAdminSealCertificateService: InternalAdminSealCertificateService,
              public activeModal: NgbActiveModal,
              @Inject(SealCertificatePopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string,
              @Inject(SealCertificatePopupComponent.INJECTABLE_SEAL_CERTIFICATE_KEY) public sealCertificate: SealCertificateDto) {}


  ngOnInit(): void {
    if (!!this.sealCertificate.signatureImageContentId) {
      this.internalAdminSealCertificateService
        .getSealSignatureImage(this.tenantId, this.sealCertificate.id, 'body', false, {httpHeaderAccept: 'application/octet-stream'})
        .subscribe(blob => this.originalSignatureImageBlobSrc = URL.createObjectURL(blob));
    }
  }


  ngOnDestroy() {
    if (!!this.originalSignatureImageBlobSrc) {
      URL.revokeObjectURL(this.originalSignatureImageBlobSrc);
    }
  }


  // </editor-fold desc="LifeCycle">


  onSaveButtonClicked() {

    this.isProcessing = true;
    this.sealCertificate['password' as any] = this.pendingPassword;

    const sealCertificateId = this.sealCertificate.id;
    const certificateFile = this.pendingCertificateFile ?? null;
    const signatureImageFile = this.pendingSignatureImageFile ?? null;

    const request$ = !!this.sealCertificate.id
      ? this.adminSealCertificateService.updateSealCertificate(this.tenantId, sealCertificateId, this.sealCertificate, certificateFile, signatureImageFile)
      : this.adminSealCertificateService.createSealCertificate(this.tenantId, this.sealCertificate, certificateFile, signatureImageFile);

    request$
      .pipe(catchError(this.notificationsService.handleHttpError('create/update sealCertificate')))
      .subscribe({
        next: () => {
          const errorMessage = !!sealCertificateId
            ? SealCertificateMessages.SUCCESS_EDITING_SEAL_CERTIFICATE
            : SealCertificateMessages.SUCCESS_CREATING_SEAL_CERTIFICATE;
          this.notificationsService.showSuccessMessage(errorMessage);
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error: e => {
          const errorMessage = !!sealCertificateId
            ? SealCertificateMessages.ERROR_EDITING_SEAL_CERTIFICATE
            : SealCertificateMessages.ERROR_CREATING_SEAL_CERTIFICATE;
          this.notificationsService.showErrorMessage(errorMessage, getFirstErrorMessage(e));
        }
      })
      .add(() => this.isProcessing = false);
  }


  onCertificateFileChanged(event) {
    const selectedFiles: FileList = (event.target as HTMLInputElement).files;
    this.pendingCertificateFile = selectedFiles.item(0);
  }


  onSignatureImageFileDeleted() {
    this.sealCertificate['signatureImageContentId' as any] = null;
    if (!!this.originalSignatureImageBlobSrc) {
      URL.revokeObjectURL(this.originalSignatureImageBlobSrc);
      this.originalSignatureImageBlobSrc = null;
    }
  }


  onSignatureImageFileChanged(file: File) {
    this.pendingSignatureImageFile = file;
  }


}
