/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, OnChanges, Injector } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { StepMessages } from './step-messages';
import { CommonMessages } from '../../common-messages';
import { tap } from 'rxjs/operators';
import { CommonIcons } from '@libriciel/ls-elements';
import { CustomMap } from '../../models/custom-types';
import { DeskService, PageDeskRepresentation, DeskRepresentation } from '@libriciel/iparapheur-internal';
import { isEmpty } from '../../../utils/string-utils';
import { WorkflowDefinitionDto, StepDefinitionDto, StepDefinitionType } from '@libriciel/iparapheur-provisioning';
import { Action, UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { FolderViewMessages } from '../../../components/main/folder-view/folder-view-messages';
import { DeskListPopupComponent } from '../desk-list-popup/desk-list-popup.component';
import { WorkflowUtils } from '../../utils/workflow-utils';


@Component({
  selector: 'app-workflow-steps',
  templateUrl: './workflow-steps.component.html',
  styleUrls: ['./workflow-steps.component.scss']
})
export class WorkflowStepsComponent implements OnChanges {

  readonly messages = FolderViewMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly stepMessages = StepMessages;
  readonly maxDisplayedValidators: number = 8;
  readonly actionEnum = Action;

  @Input() deskId: string;
  @Input() tenantId: string;
  @Input() allowVariableDeskEdition?: boolean = true;
  @Input() variableDesksIds: CustomMap;
  @Input() userPreferences: UserPreferencesDto;

  // for validation workflows, when there is a creation workflow before
  @Input() stepOffset: number = 0;
  @Input() showStart: boolean = true;
  @Input() showEnd: boolean = true;
  @Input() workflowDefinition: WorkflowDefinitionDto;
  @Input() originDesk: DeskRepresentation;

  desks: DeskRepresentation[][] = [];
  definitionSteps: StepDefinitionDto[] = [];

  // <editor-fold desc="LifeCycle">


  constructor(public modalService: NgbModal,
              public deskService: DeskService,
              public route: ActivatedRoute) {
  }


  ngOnChanges(): void {
    this.updateDefinition();
  }


  // </editor-fold desc="LifeCycle">


  updateDefinition() {
    this.definitionSteps = this.workflowDefinition.steps
      .filter((step: StepDefinitionDto) => !(!this.showStart && step.type.toUpperCase() === Action.Start.toUpperCase()))
      .filter((step: StepDefinitionDto) => !(!this.showEnd && step.type.toUpperCase() === StepDefinitionType.Archive.toUpperCase()));

    this.desks = [];

    this.definitionSteps.forEach(() => {
      this.desks.push([]);
    });

    for (let i = 0 ; i < this.stepOffset ; i++) {
      this.desks.push([]);
    }
  }


  showValidators(validators: DeskRepresentation[]) {
    this.modalService
      .open(
        DeskListPopupComponent, {
          injector: Injector.create({
            providers: [
              {provide: DeskListPopupComponent.INJECTABLE_DESK_LIST_KEY, useValue: validators},
              {provide: DeskListPopupComponent.INJECTABLE_USER_PREFERENCES, useValue: this.userPreferences},
            ]
          }),
          size: 'lg'
        }
      )
      .result
      .then(
        result => console.log(result),
        () => {/* dismissed */}
      );
  }


  getIncrement(): number {
    return this.stepOffset + 2 - (this.showStart ? 0 : 1);
  }


  isVariableDesksStep(step: StepDefinitionDto): boolean {
    return step.validatingDesks.map(validator => validator.id).includes(WorkflowUtils.GENERIC_DESK_IDS.VARIABLE_DESK_ID);
  }


  retrieveAssociatedDesksFn = (page: number, pageSize: number, searchTerm: string): Observable<PageDeskRepresentation> => {
    searchTerm = !!searchTerm ? searchTerm.toLowerCase() : searchTerm;
    return this.deskService.getAssociatedDesks(this.tenantId, this.deskId, page, pageSize, [], searchTerm)
      .pipe(
        tap(deskPage => {
          if (page === 0) {
            const currentDesk: DeskRepresentation = {id: this.deskId, name: WorkflowUtils.GENERIC_DESK_NAMES.EMITTER};
            deskPage.content.unshift(currentDesk);
          }
        }),
        tap(deskPage => deskPage.content = isEmpty(searchTerm)
          ? deskPage.content
          : deskPage.content.filter(desk => desk.name.toLowerCase().includes(searchTerm))
        )
      );
  };


  setVariableDesks(index: number, deskList: DeskRepresentation []): void {
    if (deskList.length > 0) {
      this.variableDesksIds[index] = deskList[0]?.id as any;
    }
  }

}
