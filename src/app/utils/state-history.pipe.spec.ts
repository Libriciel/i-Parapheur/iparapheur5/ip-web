/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { StateHistoryPipe } from './state-history.pipe';
import { Task } from '../models/task';
import { State, Action } from '@libriciel/iparapheur-internal';


describe('StateHistoryPipe', () => {


  it('create an instance', () => {
    const pipe = new StateHistoryPipe();
    expect(pipe).toBeTruthy();
  });


  it('create an instance', () => {
    const pipe = new StateHistoryPipe();
    expect(pipe).toBeTruthy();
  });


  it('return not null on every state', () => {
    Object
      .keys(State)
      .map(key => State[key])
      .filter(state => state != State.Current)
      .filter(state => state != State.Downstream)
      .filter(state => state != State.Delegated)
      .filter(state => state != State.Draft)
      .filter(state => state != State.Finished)
      .filter(state => state != State.Late)
      .filter(state => state != State.Pending)
      .filter(state => state != State.Retrievable)
      .filter(state => state != State.Upcoming)
      .map(state => {
        const task = {} as Task;
        task.action = Action.Visa;
        task.state = state;
        return task;
      })
      .forEach(task => expect(StateHistoryPipe.compute(task)).toBeTruthy());
  });


  it('return not null on validated task', () => {
    Object
      .keys(Action)
      .map(key => Action[key])
      .filter(action => action != Action.Unknown)
      .map(action => {
        const task = new Task();
        task.action = action;
        task.state = State.Validated;
        return task;
      })
      .forEach(task => expect(StateHistoryPipe.compute(task)).toBeTruthy());
  });


  it('return default value on invalid input', () => {

    expect(StateHistoryPipe.compute(undefined)).toBe('');
    expect(StateHistoryPipe.compute(null)).toBe('');

    const task = new Task();

    task.action = undefined;
    task.state = undefined;
    expect(StateHistoryPipe.compute(task)).toBe('');

    task.action = null;
    task.state = null;
    expect(StateHistoryPipe.compute(task)).toBe('');
  });


});
