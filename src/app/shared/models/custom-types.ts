/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { DeskRepresentation, DeskCount, TenantRepresentation, TemplateType, ExternalSignatureProvider } from '@libriciel/iparapheur-legacy';
import { WorkflowActor, WorkflowMetadata } from '@libriciel/ls-workflow';
import { ExternalSignatureLevel } from '@libriciel/iparapheur-internal';

// Custom NgChanges for NgOnChanges
// example :
// ngOnChanges(changes: NgChanges<Component>): void { }

export type NgChanges<Component extends object, Props = ExcludeFunctions<Component>> = {
  [Key in keyof Props]: {
    previousValue: Props[Key];
    currentValue: Props[Key];
    firstChange: boolean;
    isFirstChange(): boolean;
  }
}
type MarkFunctionPropertyNames<Component> = {
  [Key in keyof Component]: Component[Key] extends Function | Subject<any> ? never : Key;
}
type ExcludeFunctionPropertyNames<T extends object> = MarkFunctionPropertyNames<T>[keyof T];
type ExcludeFunctions<T extends object> = Pick<T, ExcludeFunctionPropertyNames<T>>;

// Custom Map

export type CustomMap = { [key: string]: string };
export type CustomNumberMap<T> = { [key: number]: T };

// pdf.js
export type CustomPdfJsEvent = {
  value: {
    identifierName: string,
    page: number,
    pageRotation: number,
    rect: number[],
    contentsObj: {
      str: string
    },
  }
}


export type DeskWithCount = {
  desk: DeskRepresentation,
  count: DeskCount
}

// Desk list

export class FlatTreeNode {
  tenant: TenantRepresentation;
  deskWithCount: DeskWithCount;
  isDesk: boolean = false;
  expanded: boolean = true;
}

export enum DisplayModeEnum {
  THUMBS,
  LIST
}

// Forms
export type DraftForm = {
  name: FormControl<string>,
  dueDate: FormControl<string>
}


export type ExternalSignatureMemberForm = {
  firstname: FormControl<string>;
  lastname: FormControl<string>;
  mail: FormControl<string>;
  phone: FormControl<string>;
}

export interface ExternalSignatureConnectorForm {
  name: FormControl<string>;
  url: FormControl<string>;
  serviceName: FormControl<ExternalSignatureProvider>;
  signatureLevel: FormControl<ExternalSignatureLevel>;
  token: FormControl<string>;
  login: FormControl<string>;
  password: FormControl<string>;
}

export type StepForm = {
  name: FormControl<string>;
  type: FormControl<string>;
  validationMode: FormControl<string>;
  desks: FormControl<WorkflowActor[]>;
  notifiedDesks: FormControl<WorkflowActor[]>;
  validationMetadata: FormControl<WorkflowMetadata[]>;
  rejectionMetadata: FormControl<WorkflowMetadata[]>;
}

export interface AnnotationForm {
  content: FormControl<string | null>;
  signatureTemplate: FormControl<TemplateType>
}

export type PartialAnnotationDataChange = Partial<{ content: string, signatureTemplate: TemplateType }>

export interface DefaultTemplatesForm {
  signatureTemplate: FormControl<TemplateType>;
  sealTemplate: FormControl<TemplateType>
}

export type PartialTemplatesDataChange = Partial<{ signatureTemplate: TemplateType, sealTemplate: TemplateType }>

// Errors

export type CoreApiError = {
  timestamp: string;
  status: number;
  error: string;
  message: string;
  errors?: CoreApiValidationError[];
}

export type CoreApiValidationError = {
  defaultMessage: string;
  objectName: string;
  field: string;
  code: string;
  rejectedValue: string;
}

// ext sig popup

export type IndexedExternalSignatureMemberAttribute = {
  index: number;
  key: string;
  value: string;
}