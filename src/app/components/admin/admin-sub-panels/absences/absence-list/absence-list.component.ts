/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Injector, ViewChild, Input, OnChanges } from '@angular/core';
import { AbsencesMessages } from '../absences-messages';
import { CommonIcons, Weight, Style } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../../shared/common-messages';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { AbsencePopupComponent } from '../absence-popup/absence-popup.component';
import { DeskSearchInputComponent } from '../../../../../shared/components/desk-search-input/desk-search-input.component';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CurrentUserService } from '@libriciel/iparapheur-legacy';
import { catchError } from 'rxjs/operators';
import { AdminDeskService as ProvisioningAdminDeskService, DelegationSortBy, DeskDto, DelegationDto } from '@libriciel/iparapheur-provisioning';
import { DeskService } from '@libriciel/iparapheur-internal';

@Component({
  selector: 'app-absence-list',
  templateUrl: './absence-list.component.html',
  styleUrls: ['./absence-list.component.scss']
})
export class AbsenceListComponent implements OnInit, OnChanges {

  readonly caretUp = faCaretUp;
  readonly caretDown = faCaretDown;
  readonly LsButtonWeight = Weight;
  readonly LsButtonStyle = Style;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = AbsencesMessages;
  readonly sortByEnum = DelegationSortBy;

  @Input() tenantId?: string;
  @Input() asAdmin: boolean = false;
  @Input() asFunctionalAdmin: boolean = false;

  @ViewChild('deskSelector') deskSelector: DeskSearchInputComponent;

  sortBy: DelegationSortBy = DelegationSortBy.Start;
  asc = true;

  selectedDesk: DeskDto;
  _selectedDeskList: DeskDto[] = [];
  absenceList: DelegationDto[] = [];
  loading: boolean = false;
  colspan: number = 5;


  // <editor-fold desc="LifeCycle">


  constructor(private provisioningAdminDeskService: ProvisioningAdminDeskService,
              private deskService: DeskService,
              private currentUserService: CurrentUserService,
              private notificationsService: NotificationsService,
              private modalService: NgbModal) { }


  ngOnInit(): void {
    this.resetAndUpdateAbsences();
  }


  ngOnChanges(): void {
    this.resetAndUpdateAbsences();
  }


  // </editor-fold desc="LifeCycle">


  selectedSourceDeskChanged() {
    this.selectedDesk = this._selectedDeskList[0];
    this.requestAbsences();
  }


  onRowOrderClicked(sortByProperty: DelegationSortBy) {

    if (this.sortBy === sortByProperty) {
      this.asc = !this.asc;
    } else {
      this.sortBy = sortByProperty;
    }

    this.requestAbsences();
  }


  openCreationModal() {

    // This may be a tenant-less panel,
    const currentTenantId = this.tenantId ?? this.selectedDesk.tenantId;

    if (!this.selectedDesk) {
      return;
    }

    this.modalService
      .open(
        AbsencePopupComponent, {
          injector: Injector.create({
            providers: [
              {provide: AbsencePopupComponent.INJECTABLE_DESK_KEY, useValue: this.selectedDesk},
              {provide: AbsencePopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: currentTenantId},
              {provide: AbsencePopupComponent.INJECTABLE_AS_ADMIN_KEY, useValue: this.asAdmin},
            ]
          }),
          size: 'xl',
          backdrop: 'static'
        }
      )
      .result
      .then(
        () => this.requestAbsences(),
        () => { /* Dismissed */ }
      );
  }


  retrieveDesksFn = (page: number, pageSize: number, searchTerm) => {
    console.log('retrieveDesks - this.asAdmin : ', this.asAdmin);


    if (!this.asAdmin) {
      return this.currentUserService.getManagedDesks(page, pageSize, [], searchTerm);
    }

    return this.asFunctionalAdmin
      ? this.currentUserService.getAdministeredDesksForTenant(this.tenantId, page, pageSize, searchTerm)
      : this.provisioningAdminDeskService.listDesks(this.tenantId, page, pageSize, [], searchTerm);

  };


  deleteDelegation(delegation: DelegationDto) {

    delegation.delegatingDeskId = delegation.delegatingDesk.id;
    delegation.substituteDeskId = delegation.substituteDesk.id;
    delegation.typeId = delegation.type?.id ?? null;
    delegation.subtypeId = delegation.subtype?.id ?? null;

    const delegationRequest$ = this.asAdmin
      ? this.provisioningAdminDeskService.deleteDelegationAsAdmin(this.tenantId, this.selectedDesk.id, delegation.id)
      : this.deskService.deleteDelegation(this.selectedDesk.tenantId, this.selectedDesk.id, delegation.id);

    delegationRequest$
      .pipe(catchError(this.notificationsService.handleHttpError('deleteDelegation')))
      .subscribe({
        next: () => {
          this.notificationsService.showSuccessMessage(AbsencesMessages.DELEGATION_DELETE_SUCCESS);
          this.requestAbsences();
        },
        error: e => this.notificationsService.showErrorMessage(AbsencesMessages.DELEGATION_DELETE_ERROR, e.message)
      });
  }


  private requestAbsences() {
    console.log('requestAbsence');

    if (!this.selectedDesk) {
      return;
    }

    this.loading = true;

    const requestSortBy = [this.sortBy + (this.asc ? ',ASC' : ',DESC')];
    const delegationRequest$ = this.asAdmin
      ? this.provisioningAdminDeskService.listDeskDelegationsAsAdmin(this.tenantId, this.selectedDesk.id, 0, 1000, requestSortBy)
      : this.deskService.listDelegations(this.selectedDesk.tenantId, this.selectedDesk.id, 0, 1000, requestSortBy);

    delegationRequest$
      .subscribe({
        next: result => this.absenceList = result.content,
        error: e => this.notificationsService.showErrorMessage(AbsencesMessages.DELEGATIONS_GET_ERROR, e.message)
      })
      .add(() => this.loading = false);
  }


  resetAndUpdateAbsences(): void {
    this._selectedDeskList = [];
    this.absenceList = [];
    this.deskSelector?.updateAvailableDesksList(false);
  }


}
