/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { LayersMessages } from '../../layers-messages';
import { CommonMessages } from '../../../../../../../../../../shared/common-messages';
import { Layer } from '../../../../../../../../../../models/pdf-stamp/layer';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';

@Component({
  selector: 'app-edit-layer-general',
  templateUrl: './edit-layer-general.component.html',
  styleUrls: ['./edit-layer-general.component.scss']
})
export class EditLayerGeneralComponent {

  readonly messages = LayersMessages;
  readonly commonMessages = CommonMessages;

  @Input() editMode: boolean;
  @Input() layer: Layer;
  @Input() userPreferences: UserPreferencesDto;
  @Output() validityStateChanged = new EventEmitter<boolean>();

  generalForm: UntypedFormGroup;


  // <editor-fold desc="LifeCycle">


  constructor() {
    this.generalForm = new UntypedFormGroup({
      idControl: new UntypedFormControl(this.layer?.id),
      nameControl: new UntypedFormControl(this.layer?.name, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(64)
      ])
    });

    this.generalForm.controls['idControl'].disable();
    this.generalForm.valueChanges.subscribe(() => this.validityStateChanged.emit(this.generalForm.valid));
  }


  // </editor-fold desc="LifeCycle">


}
