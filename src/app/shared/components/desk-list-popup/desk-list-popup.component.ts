/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonMessages } from '../../common-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { WorkflowActor } from '@libriciel/ls-workflow';
import { DeskRepresentation, UserPreferencesDto } from '@libriciel/iparapheur-legacy';

@Component({
  selector: 'app-desk-list-popup',
  templateUrl: './desk-list-popup.component.html',
  styleUrls: ['./desk-list-popup.component.scss']
})
export class DeskListPopupComponent {


  static readonly INJECTABLE_USER_PREFERENCES = 'userPreferences';
  static readonly INJECTABLE_DESK_LIST_KEY = 'deskList';

  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;


  // <editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              @Inject(DeskListPopupComponent.INJECTABLE_USER_PREFERENCES) public userPreferences: UserPreferencesDto,
              @Inject(DeskListPopupComponent.INJECTABLE_DESK_LIST_KEY) public deskList: WorkflowActor[] | DeskRepresentation[]) { }


  // </editor-fold desc="LifeCycle">


}
