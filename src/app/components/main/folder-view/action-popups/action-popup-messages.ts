/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Folder } from '../../../../models/folder/folder';
import { ActionNamePipe } from '../../../../shared/utils/action-name.pipe';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';
import { Action } from '@libriciel/iparapheur-legacy';
import { getFirstErrorMessage } from '../../../../utils/string-utils';

export class ActionPopupMessages {

  static readonly MAIL_TO_LABEL = 'À\u00a0:';
  static readonly CARBON_COPY_LABEL = 'CC\u00a0:';
  static readonly BLIND_CARBON_COPY_LABEL = 'CCI\u00a0:';
  static readonly MAIL_OBJECT_LABEL = 'Objet\u00a0:';
  static readonly MAIL_BODY_LABEL = 'Message\u00a0:';

  static readonly TARGET_DESK = 'Bureau cible';

  static readonly INCLUDE_DOCKET = 'Envoyer le bordereau';

  static readonly CANNOT_RETRIEVE_SUBTYPE = 'Erreur à la récupération du sous-type';
  static readonly ALL_TASKS_SUCCESSFUL = 'Toutes les tâches se sont terminées avec succès.';

  static readonly INCOMPATIBLE_FORMATS = 'Le format de signature du dossier est incompatible avec le format de signature de la typologie sélectionnée.';

  static readonly SECURE_MAIL_STATUS_TITLE = 'État du dossier sur la plate-forme de Mail Securisé';
  static readonly REFRESH = 'Actualiser';
  static readonly CONFIRMATION_DATE = 'Date de confirmation';

  static readonly AVAILABLE_DESKS_LABEL = 'Bureau(x) disponible(s)';
  static readonly SELECTED_DESKS_LABEL = 'Bureau(x) sélectionné(s)';
  static readonly DESK_ALREADY_SELECTED_ERROR = 'Le bureau est déjà associé.';

  static readonly RESULT_PREFIX = 'Retour de';
  static readonly SEND_PREFIX = 'Envoi de';

  static parseMultipleActionErrorMessage = (resultsOnError: { folderId: string, isSignature?: boolean, error: Error }[]) => `
    <p>${resultsOnError.length > 1 ? resultsOnError.length + ' erreurs sont survenues' : 'Une erreur est survenue'}</p>
    <p>${getFirstErrorMessage(resultsOnError[0].error)}</p>
  `;

  static mandatoryValidationMetadataMessage = (metadataCount: number) => `Métadonnée${metadataCount > 1 ? 's' : ''} obligatoire${metadataCount > 1 ? 's' : ''} sur l'étape`;

  static mandatoryRejectionMetadataMessage = (metadataCount: number) => `Métadonnée${metadataCount > 1 ? 's' : ''} obligatoire${metadataCount > 1 ? 's' : ''} sur l'étape de rejet`;

  static actionSuccessMessage = (action: Action | SecondaryAction) => `L'action ${ActionNamePipe.compute(action)} a été effectuée avec succès`;

  static actionFolderSuccessMessage = (action: Action | SecondaryAction, folder: Folder) => `L'action ${ActionNamePipe.compute(action)} sur le dossier ${folder.name} a été effectuée avec succès`;

  static actionErrorMessage = (action: Action | SecondaryAction) => `Erreur lors de l'action ${ActionNamePipe.compute(action)}`;

  static actionFolderErrorMessage = (action: Action | SecondaryAction, folder: Folder) => `Erreur lors de l'action ${ActionNamePipe.compute(action)} sur le dossier ${folder.name}`;

  static parseMultipleActionsErrorMessage = (action: Action, errors: Error[]) => `
    <p>Une erreur est survenue lors de l'action ${ActionNamePipe.compute(action)}</p>
    <p>${errors.length > 1 ? errors.length + ' erreurs sont survenues' : 'Une erreur est survenue'}</p>
    <p>${getFirstErrorMessage(errors[0])}</p>
  `;
}
