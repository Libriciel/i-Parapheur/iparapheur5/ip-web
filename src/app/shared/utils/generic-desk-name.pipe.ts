/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { DeskDto } from '@libriciel/iparapheur-provisioning';
import { WorkflowUtils } from './workflow-utils';

@Pipe({
  name: 'genericDeskName'
})
export class GenericDeskNamePipe implements PipeTransform {


  public static compute(desk: DeskDto): string {
    switch (desk?.id) {
      case WorkflowUtils.GENERIC_DESK_IDS.BOSS_OF_ID: { return WorkflowUtils.GENERIC_DESK_NAMES.BOSS_OF; }
      case WorkflowUtils.GENERIC_DESK_IDS.EMITTER_ID: { return WorkflowUtils.GENERIC_DESK_NAMES.EMITTER; }
      case WorkflowUtils.GENERIC_DESK_IDS.VARIABLE_DESK_ID: { return WorkflowUtils.GENERIC_DESK_NAMES.VARIABLE_DESK; }
      default : { return desk?.name; }
    }
  }


  transform(value: any, _args?: any): string {
    return GenericDeskNamePipe.compute(value);
  }

}
