/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { User } from '../auth/user';
import { DeskDto } from '@libriciel/iparapheur-provisioning';

export class IpWorkflowActor {
  id: number | string;
  lastname?: string;
  firstname?: string;
  name?: string;

  constructor(data?: DeskDto | User | { id: string, name: string }) {

    if (data) {
      this.id = data.id;

      if (data instanceof User) {
        this.name = data.userName;
        this.firstname = data.firstName;
        this.lastname = data.lastName;
      } else {
        this.name = data.name;
      }
    }
  }

}
