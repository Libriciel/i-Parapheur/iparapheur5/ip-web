/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

export class LayersMessages {
  static readonly ELEMENT_TABLE_TITLE = 'Liste des éléments';
  static readonly LAYER_MANAGEMENT = 'Gestion des calques';
  static readonly LAYER_CREATION = 'Création de calque';
  static readonly LAYER_EDITION = 'Modification du calque';
  static readonly LAYER_CREATE = 'Créer un calque';

  static readonly TAB_GENERAL = 'Général';
  static readonly TAB_STAMPS = 'Éléments';

  static readonly ERROR_SENDING_FILE = `Erreur à l'envoi du fichier`;
  static readonly ERROR_CLEANING_TEMP_IMAGE_FILES = 'Erreur à la suppression des images temporaires';
  static readonly ERROR_RETRIEVING_LAYERS = 'Erreur à la récupération des calques';

  static readonly SIGNATURE_TOOLTIP = 'Ajouter après la signature, ou au rang de signature.';
  static readonly PAGE_TOOLTIP = `
    <p class="mt-2">
      L'index des pages commence à <span class="badge badge-secondary text-monospace">1</span>.<br/>
      Si une valeur dépasse le nombre de pages, l'élément sera ignoré.
      <ul>
          <li><span class="badge badge-secondary text-monospace">1</span>\u00a0: La première page</li>
          <li><span class="badge badge-secondary text-monospace">2</span>\u00a0: La deuxième page</li>
          <li><span class="badge badge-secondary text-monospace">3</span>\u00a0: La troisème page...</li>
      </ul>
    </p>
    <p>
      Une valeur spéciale est disponible.
      <ul>
        <li><span class="badge badge-secondary text-monospace">0</span>\u00a0: L'élément sera placé sur toutes les pages</li>
      </ul>
    </p>
    <p>
      L'index peut également être négatif.<br/>
      Si une valeur négative dépasse le nombre de pages, l'élément sera ignoré.
      <ul>
          <li><span class="badge badge-secondary text-monospace">-1</span>\u00a0: La dernière page</li>
          <li><span class="badge badge-secondary text-monospace">-2</span>\u00a0: L'avant-dernière page</li>
          <li><span class="badge badge-secondary text-monospace">-3</span>\u00a0: L'avant-avant-dernière page...</li>
      </ul>
    </p>
  `;

  static readonly TYPE = 'Type';
  static readonly VALUE = 'Valeur';

  static readonly ADD_ELEMENT = 'Ajouter un élément';
  static readonly COLOR = 'Couleur';
  static readonly FONT_SIZE = 'Police';
  static readonly X = 'X';
  static readonly Y = 'Y';
  static readonly PAGE = 'Page';
  static readonly SIGNATURE = 'Sig.';

}
