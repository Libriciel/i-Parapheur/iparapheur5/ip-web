/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Injector } from '@angular/core';
import { SettingsMessages } from '../settings-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ThirdPartyLicensePopupComponent } from '../third-party-license-popup/third-party-license-popup.component';
import { HttpClient } from '@angular/common/http';
import { NotificationsService } from '../../../services/notifications.service';

@Component({
  selector: 'app-third-party-license',
  templateUrl: './third-party-license.component.html',
  styleUrls: ['./third-party-license.component.scss']
})
export class ThirdPartyLicenseComponent {

  readonly messages = SettingsMessages;
  readonly commonIcons = CommonIcons;

constructor(
  private modalService: NgbModal,
  private http: HttpClient,
  private notificationsServices: NotificationsService,
  ) { }

  openLicensesPopup() {
    this.http.get('/3rdpartylicenses.txt', {responseType: 'text'})
      .subscribe({
        next: thirdpartylicenses => this.modalService
          .open(
            ThirdPartyLicensePopupComponent,
            {
              injector: Injector.create({
                providers: [
                  {provide: ThirdPartyLicensePopupComponent.INJECTABLE_LICENSE_TXT, useValue: thirdpartylicenses},
                ]
              }),
              size: 'lg'
            }
          )
          .result
          .then(),
        error: () => this.notificationsServices.showErrorMessage(this.messages.THIRD_PARTY_LICENSE_ERROR)
      });
  }
}
