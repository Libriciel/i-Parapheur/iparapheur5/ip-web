/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { combineLatest, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../shared/common-messages';
import { WorkflowEditorMessages } from '../workflow-editor-messages';
import { AdminWorkflowDefinitionService, WorkflowDefinitionDto, AdminMetadataService, PageMetadataRepresentation, MetadataSortBy } from '@libriciel/iparapheur-provisioning';
import { WorkflowEditorOptions, WorkflowDefinition, WorkflowStep, WorkflowStepEvent, WorkflowMetadata, WorkflowActor, WorkflowStepColor } from '@libriciel/ls-workflow';
import { Weight, Style } from '@libriciel/ls-elements';
import { Action } from '@libriciel/iparapheur-internal';
import { StepValidationMode } from '../../../../../models/workflows/step-validation-mode';
import { ActionNamePipe } from '../../../../../shared/utils/action-name.pipe';
import { DataConversionService } from '../../../../../services/data-conversion.service';
import { getFirstErrorMessage, isCurrentVersion52OrAbove } from '../../../../../utils/string-utils';
import { ActionIconPipe } from '../../../../../shared/utils/action-icon.pipe';
import { WorkflowUtils } from '../../../../../shared/utils/workflow-utils';
import { GlobalPopupService } from '../../../../../shared/service/global-popup.service';

@Component({
  selector: 'app-workflow-editor',
  templateUrl: './workflow-editor.component.html',
  styleUrls: ['./workflow-editor.component.scss']
})
export class WorkflowEditorComponent implements AfterViewInit {

  readonly show52Features: boolean = isCurrentVersion52OrAbove();

  protected readonly styles = Style;
  protected readonly weight = Weight;
  protected readonly actions = Action;
  protected readonly messages = WorkflowEditorMessages;
  protected readonly commonMessages = CommonMessages;

  options: WorkflowEditorOptions = new WorkflowEditorOptions();


  workflowDefinition: WorkflowDefinition = new WorkflowDefinition();
  metadataList: WorkflowMetadata[];

  workflowForm?: FormGroup = new FormGroup<{ name: FormControl<string> }>({
    name: new FormControl<string>(
      "",
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255),
        Validators.pattern(new RegExp('^[^\\r\\n\b\u00A0]*$'))
      ]
    )
  });

  selectedStepEvent: WorkflowStepEvent = null;

  editMode: boolean = false;
  tenantId?: string;


  // <editor-fold desc="LifeCycle">


  constructor(private adminMetadataService: AdminMetadataService,
              private adminWorkflowDefinitionService: AdminWorkflowDefinitionService,
              private notificationsService: NotificationsService,
              private globalPopupService: GlobalPopupService,
              private route: ActivatedRoute,
              private router: Router) {}


  ngAfterViewInit(): void {
    this.options.workflow.hideEnd = true;
    this.options.workflow.lockLastStep = true;

    combineLatest([this.route.params, this.route.data])
      .subscribe(([params, data]) => {

        this.tenantId = params.tenantId;

        if (!!data.workflowDefinition) {
          this.workflowDefinition = data.workflowDefinition;
          this.workflowForm.patchValue({name: this.workflowDefinition.name});
        } else {
          const finalStep: WorkflowStep = {
            type: Action.Archive,
            name: 'Fin de circuit',
            validationMode: StepValidationMode.Simple,
            validators: [WorkflowUtils.GENERIC_WORKFLOW_ACTORS.EMITTER],
            notifiedValidators: [],
            validationMetadata: [],
            rejectionMetadata: []
          };
          this.workflowDefinition.steps.push(finalStep);
        }

        if (this.router.url.endsWith("/clone")) {
          this.editMode = false;
          this.workflowDefinition.id = null;
          this.workflowDefinition.key = null;
          this.workflowDefinition.name = this.workflowDefinition.name + this.messages.COPY_TXT;
          this.workflowForm.patchValue({name: this.workflowDefinition.name});
        } else {
          this.editMode = !!this.workflowDefinition.id;
        }

        this.fetchMetadata();
      });
  }

  leaveEditor() {
    this.router.navigate([`/admin/${this.tenantId}/workflows/`])
      .then(() => 'leaving editor.');
  }

  fetchMetadata() {
    this.adminMetadataService
      .listMetadata(this.tenantId, false, null, 0, 250, [MetadataSortBy.Name, ",ASC"])
      .pipe(catchError(this.notificationsService.handleHttpError('listMetadataAsAdmin')))
      .subscribe((data: PageMetadataRepresentation) => {
        this.metadataList = data.content as WorkflowMetadata[];
        this.workflowDefinition.steps.forEach((step: WorkflowStep) => {
          step.validationMetadata?.forEach((metadata: WorkflowMetadata) => {
            const foundMetadata: WorkflowMetadata = this.metadataList
              .find((tenantMetadata: WorkflowMetadata): boolean => tenantMetadata.id === metadata.id);
            if (!!foundMetadata) {
              metadata.name = foundMetadata.name;
            }
          });
          step.rejectionMetadata?.forEach((metadata: WorkflowMetadata) => {
            const foundMetadata: WorkflowMetadata = this.metadataList
              .find((tenantMetadata: WorkflowMetadata): boolean => tenantMetadata.id === metadata.id);
            if (!!foundMetadata) {
              metadata.name = foundMetadata.name;
            }
          });
        });

      });
  }

  // </editor-fold desc="LifeCycle">


  // <editor-fold desc="Form">


  validateWorkflow() {

    const containsSecureMail: boolean = this.workflowDefinition.steps.map(step => step.type).includes(Action.SecureMail);
    if (this.show52Features && containsSecureMail) {
      this.globalPopupService.showAlertValidationPopup(null, WorkflowEditorMessages.SECURE_MAIL_WARNING_MESSAGE, null, false).then();
    }

    this.workflowDefinition.name = this.workflowForm.value.name;
    const parsedWorkflow: WorkflowDefinitionDto = DataConversionService.workflowDefinitionToWorkflowDefinitionDto(this.workflowDefinition);

    const request: Observable<WorkflowDefinitionDto> = this.editMode
      ? this.adminWorkflowDefinitionService.updateWorkflowDefinition(this.tenantId, parsedWorkflow.key, parsedWorkflow)
      : this.adminWorkflowDefinitionService.createWorkflowDefinition(this.tenantId, parsedWorkflow);

    request
      .pipe(catchError(this.notificationsService.handleHttpError('create/update workflow definition error')))
      .subscribe({
        next: () => {
          const successMessage: string = this.editMode
            ? WorkflowEditorMessages.SUCCESS_EDITING_WORKFLOW_DEFINITION
            : WorkflowEditorMessages.SUCCESS_CREATING_WORKFLOW_DEFINITION;
          this.notificationsService.showSuccessMessage(successMessage);
          this.leaveEditor();
        },
        error: error => {
          const errorMessage: string = this.editMode
            ? WorkflowEditorMessages.ERROR_EDITING_WORKFLOW_DEFINITION
            : WorkflowEditorMessages.ERROR_CREATING_WORKFLOW_DEFINITION;
          this.notificationsService.showErrorMessage(errorMessage, getFirstErrorMessage(error));
        }
      });
  }

  allStepsAreValid(): boolean {
    if (!this.workflowDefinition?.steps) {
      return false;
    }

    const noValidatorsAreNull: boolean = this.workflowDefinition
      .steps?.every((step: WorkflowStep): boolean => {
        if (step == null) {
          return false;
        }

        if (step.validators?.includes(null)) {
          return false;
        }

        return !step.validators?.map((v: WorkflowActor) => v?.id).includes(null);
      });

    return noValidatorsAreNull;
  }


  // </editor-fold desc="Form">


  // <editor-fold desc="Workflow Editor Parameters">

  public stepToColorClassFunction(step: WorkflowStep): WorkflowStepColor {
    switch (step.type?.toUpperCase()) {
      case Action.Visa:
        return WorkflowStepColor.GREEN;
      case Action.Seal:
        return WorkflowStepColor.ORANGE;
      case Action.ExternalSignature:
        return WorkflowStepColor.RED;
      case Action.Signature:
        return WorkflowStepColor.RED;
      case Action.SecureMail:
        return WorkflowStepColor.LIGHT_BLUE;
      default:
        return WorkflowStepColor.WHITE;
    }
  }

  public stepToActionIconClassFunction(step: WorkflowStep): string {
    return ActionIconPipe.computeToString(step.type?.toUpperCase() as Action, true);
  }

  public stepToValidationModeIconFunction(step: WorkflowStep): string {
    switch (step.validationMode?.toUpperCase()) {
      case StepValidationMode.Or:
        return 'ls-icon-etape-concurrente';
      case StepValidationMode.And:
        return 'fa fa-users';
      default:
        return 'fa fa-user';
    }
  }

  public stepToTooltipFunction(step: WorkflowStep): string {
    return ActionNamePipe.compute(step.type);
  }

  // </editor-fold desc="Workflow Editor Parameters">

}
