/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { inject } from '@angular/core';
import { RouterStateSnapshot, ActivatedRouteSnapshot, ResolveFn } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { NotificationsService } from '../notifications.service';
import { DataConversionService } from '../data-conversion.service';
import { AdminWorkflowDefinitionService } from '@libriciel/iparapheur-provisioning';
import { WorkflowDefinition } from '@libriciel/ls-workflow';

export const WorkflowDefinitionResolver: ResolveFn<WorkflowDefinition> = (
  route: ActivatedRouteSnapshot,
  _state: RouterStateSnapshot,
  notificationsService: NotificationsService = inject(NotificationsService),
  adminWorkflowDefinitionService: AdminWorkflowDefinitionService = inject(AdminWorkflowDefinitionService)
): Observable<WorkflowDefinition> => {

  const workflowKey: string = route?.paramMap?.get('workflowKey');
  const tenantId: string = route?.paramMap?.get('tenantId');

  if (!workflowKey) {
    return of(new WorkflowDefinition());
  }

  return adminWorkflowDefinitionService
    .getWorkflowDefinition(tenantId, workflowKey)
    .pipe(
      map(dto => DataConversionService.workflowDefinitionDtoToWorkflowDefinition(dto)),
      catchError(notificationsService.handleHttpError('getWorkflowDefinition'))
    );
};

