/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import ngParser from '@angular-eslint/template-parser';
import js from '@eslint/js';
import tseslint from 'typescript-eslint';
import ts from '@typescript-eslint/eslint-plugin';
import ng from '@angular-eslint/eslint-plugin';
import ngTemplate from '@angular-eslint/eslint-plugin-template';


/**
 * Note: ngParser and TsEsLint can share the same config.
 */
export default [
    {
        files: ["**/*.ts"],
        plugins: {
            '@typescript-eslint': ts,
            '@angular-eslint': ng,
        },
        languageOptions: {parser: tseslint.parser},
        rules: {
            ...js.configs.recommended.rules,
            ...ts.configs.recommended.rules,
            ...tseslint.configs.base.rules,
            ...tseslint.configs.eslintRecommended.rules,
            ...ng.configs.recommended.rules,
            // Some rules are too low, by default. We upgrade these to errors.
            "semi": "error",
            // TODO: Every "warn" overridden parameter here is usually set as "error", by default.
            //  we should fix every one of these, and remove the override altogether, to leave the default "error" behavior.
            "no-async-promise-executor": "warn",
            "no-case-declarations": "warn",
            "no-control-regex": "warn",
            "no-extra-boolean-cast": "warn",
            "no-irregular-whitespace": "warn",
            "no-useless-escape": "warn",
            "prefer-spread": "warn",
            "@typescript-eslint/no-empty-object-type": "warn",
            "@typescript-eslint/no-explicit-any": "warn",
            "@typescript-eslint/no-require-imports": "warn",
            "@typescript-eslint/no-unsafe-function-type": "warn",
            "@typescript-eslint/no-unused-vars": "warn",
        },
    },
    {
        files: ["**/*.html"],
        plugins: {'@angular-eslint/template': ngTemplate},
        languageOptions: {parser: ngParser},
        rules: {
            ...ngTemplate.configs.recommended.rules,
            ...ngTemplate.configs.accessibility.rules,
            // TODO: Every "warn" overridden parameter here is usually set as "error", by default.
            //  we should fix every one of these, and remove the override altogether, to leave the default "error" behavior.
            //  AND declare the RGAA fixed rule in the contributing file, if any.
            "@angular-eslint/template/click-events-have-key-events": "warn",
            "@angular-eslint/template/elements-content": "warn",
            "@angular-eslint/template/eqeqeq": "warn",
            "@angular-eslint/template/interactive-supports-focus": "warn",
            "@angular-eslint/template/label-has-associated-control": "warn",
            "@angular-eslint/template/no-autofocus": "warn",
        },
    },
];
