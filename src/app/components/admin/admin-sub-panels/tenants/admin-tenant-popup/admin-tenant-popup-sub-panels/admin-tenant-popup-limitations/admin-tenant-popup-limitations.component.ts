/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';
import { TenantMessages } from '../../../tenant-messages';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { TenantDto } from '@libriciel/iparapheur-legacy';

@Component({
  selector: 'app-admin-tenant-popup-limitations',
  templateUrl: './admin-tenant-popup-limitations.component.html',
  styleUrls: ['./admin-tenant-popup-limitations.component.scss']
})
export class AdminTenantPopupLimitationsComponent implements OnInit {

  readonly messages = TenantMessages;

  limitationsForm: UntypedFormGroup;
  @Output() valid = new EventEmitter<boolean>();
  @Input() pendingTenant: TenantDto;


  ngOnInit() {
    this.initializeForm();
    this.subscribeToFormChanges();
  }


  private initializeForm() {

    const deskLimit = this.pendingTenant.deskLimit != null ? this.pendingTenant.deskLimit : null;
    const userLimit = this.pendingTenant.userLimit != null ? this.pendingTenant.userLimit : null;
    const workflowLimit = this.pendingTenant.workflowLimit != null ? this.pendingTenant.workflowLimit : null;
    const typeLimit = this.pendingTenant.typeLimit != null ? this.pendingTenant.typeLimit : null;
    const subtypeLimit = this.pendingTenant.subtypeLimit != null ? this.pendingTenant.subtypeLimit : null;
    const folderLimit = this.pendingTenant.folderLimit != null ? this.pendingTenant.folderLimit : null;
    const metadataLimit = this.pendingTenant.metadataLimit != null ? this.pendingTenant.metadataLimit : null;
    const layerLimit = this.pendingTenant.layerLimit != null ? this.pendingTenant.layerLimit : null;

    this.limitationsForm = new UntypedFormGroup({
      deskLimitationInput: new UntypedFormControl(deskLimit, [Validators.min(0), Validators.max(250000)]),
      userLimitationInput: new UntypedFormControl(userLimit, [Validators.min(0), Validators.max(50000)]),
      workflowLimitationInput: new UntypedFormControl(workflowLimit, [Validators.min(0), Validators.max(50000)]),
      typeLimitationInput: new UntypedFormControl(typeLimit, [Validators.min(0), Validators.max(50000)]),
      subtypeLimitationInput: new UntypedFormControl(subtypeLimit, [Validators.min(0), Validators.max(50000)]),
      folderLimitationInput: new UntypedFormControl(folderLimit, [Validators.min(0), Validators.max(1000000)]),
      metadataLimitationInput: new UntypedFormControl(metadataLimit, [Validators.min(0), Validators.max(50000)]),
      layerLimitationInput: new UntypedFormControl(layerLimit, [Validators.min(0), Validators.max(50000)])
    });
  }


  private subscribeToFormChanges() {
    this.limitationsForm.get('deskLimitationInput')?.valueChanges.subscribe(value => this.pendingTenant.deskLimit = value);
    this.limitationsForm.get('userLimitationInput')?.valueChanges.subscribe(value => this.pendingTenant.userLimit = value);
    this.limitationsForm.get('workflowLimitationInput')?.valueChanges.subscribe(value => this.pendingTenant.workflowLimit = value);
    this.limitationsForm.get('typeLimitationInput')?.valueChanges.subscribe(value => this.pendingTenant.typeLimit = value);
    this.limitationsForm.get('subtypeLimitationInput')?.valueChanges.subscribe(value => this.pendingTenant.subtypeLimit = value);
    this.limitationsForm.get('folderLimitationInput')?.valueChanges.subscribe(value => this.pendingTenant.folderLimit = value);
    this.limitationsForm.get('metadataLimitationInput')?.valueChanges.subscribe(value => this.pendingTenant.metadataLimit = value);
    this.limitationsForm.get('layerLimitationInput')?.valueChanges.subscribe(value => this.pendingTenant.layerLimit = value);

    this.limitationsForm.statusChanges.subscribe(() => {
      this.onChange();
    });
  }


  onChange() {
    this.valid.emit(this.limitationsForm.valid);
  }
}
