/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalSignaturePopupLegacyComponent } from './external-signature-popup-legacy.component';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { Action, ExternalSignatureProcedure, ExternalSignatureMember } from '@libriciel/iparapheur-legacy';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionNamePipe } from '../../../../../shared/utils/action-name.pipe';
import { ActionIconPipe } from '../../../../../shared/utils/action-icon.pipe';

describe('ExternalSignaturePopupLegacyComponent', () => {

  let component: ExternalSignaturePopupLegacyComponent;
  let fixture: ComponentFixture<ExternalSignaturePopupLegacyComponent>;

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [RouterModule.forRoot([]), ToastrModule.forRoot()],
        providers: [
          HttpClient, HttpHandler, NgbActiveModal,
          {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_FOLDERS_KEY, useValue: []},
          {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_DESK_ID_KEY, useValue: 'deskId'},
          {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: Action.ExternalSignature},
          {provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_TENANT_ID_KEY, useValue: 'tenant01'},
          {
            provide: ExternalSignaturePopupLegacyComponent.INJECTABLE_EXTERNAL_SIGNATURE_PROCEDURE_KEY,
            useValue: {
              members: [{} as ExternalSignatureMember]
            } as ExternalSignatureProcedure
          }
        ],
        declarations: [ActionIconPipe, ActionNamePipe, ExternalSignaturePopupLegacyComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalSignaturePopupLegacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
