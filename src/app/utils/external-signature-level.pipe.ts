/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { ExternalSignatureLevel } from '@libriciel/iparapheur-internal';

@Pipe({
  name: 'externalSignatureLevel'
})
export class ExternalSignatureLevelPipe implements PipeTransform {

  public static compute(value: ExternalSignatureLevel): string {
    switch (value) {
      case ExternalSignatureLevel.Advanced: {return 'Avancée'; }
      case ExternalSignatureLevel.Qualified: {return 'Qualifiée'; }
      default: { return 'Simple'; }
    }
  }

  transform(value: ExternalSignatureLevel): string {
    return ExternalSignatureLevelPipe.compute(value);
  }

}
