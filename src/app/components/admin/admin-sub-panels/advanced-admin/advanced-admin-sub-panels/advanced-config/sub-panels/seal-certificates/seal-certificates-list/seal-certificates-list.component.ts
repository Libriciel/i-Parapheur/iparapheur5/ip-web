/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Injector, OnInit } from '@angular/core';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SealCertificate } from '../../../../../../../../../models/seal-certificate';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { ActivatedRoute } from '@angular/router';
import { CommonIcons } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { SealCertificateMessages } from '../seal-certificate-messages';
import { GlobalPopupService } from '../../../../../../../../../shared/service/global-popup.service';
import { AdminMessages } from '../../../../../../admin-messages';
import { catchError } from 'rxjs/operators';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { SealCertificateSortBy, AdminSealCertificateService, SealCertificateRepresentation } from '@libriciel/iparapheur-provisioning';
import { AdminSealCertificateService as InternalAdminSealCertificateService } from '@libriciel/iparapheur-internal';
import { FormUtils } from '../../../../../../../../../utils/form-utils';
import { of } from 'rxjs';
import { SealCertificatePopupComponent } from '../seal-certificate-popup/seal-certificate-popup.component';
import { getFirstErrorMessage } from '../../../../../../../../../utils/string-utils';

@Component({
  selector: 'app-seal-certificates-list',
  templateUrl: './seal-certificates-list.component.html',
  styleUrls: ['./seal-certificates-list.component.scss']
})
export class SealCertificatesListComponent implements OnInit {

  readonly pageSizes = [10, 15, 20, 50];
  readonly usageSubtypesMaxSize = 5;
  readonly sortByEnum = SealCertificateSortBy;
  readonly caretUpIcon = faCaretUp;
  readonly caretDownIcon = faCaretDown;
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = SealCertificateMessages;
  readonly adminMessages = AdminMessages;


  tenantId: string;
  sealCertificateList: SealCertificate[] = [];
  sortBy = SealCertificateSortBy.Name;
  asc = true;
  page = 1;
  pageSizeIndex = 1;
  total = 0;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  loading: boolean = false;
  colspan: number;


  // <editor-fold desc="LifeCycle">


  constructor(public notificationsService: NotificationsService,
              public modalService: NgbModal,
              public globalPopupService: GlobalPopupService,
              public route: ActivatedRoute,
              public internalAdminSealCertificateService: InternalAdminSealCertificateService,
              public adminSealCertificateService: AdminSealCertificateService) { }


  ngOnInit() {
    this.route?.parent?.parent?.parent?.parent?.data?.subscribe(data => {
      this.userPreferences = data["userPreferences"];
      this.colspan = 3 + (this.userPreferences.showAdminIds ? 1 : 0);
    });
    this.route?.parent?.parent?.parent?.params?.subscribe(params => {
      this.tenantId = params['tenantId'];
      this.refreshSealCertificateList();
    });
  }


  // </editor-fold desc="LifeCycle">


  refreshSealCertificateList() {

    this.loading = true;
    const requestSortBy = [this.sortBy + (this.asc ? ',ASC' : ',DESC')];
    this.adminSealCertificateService
      .listSealCertificate(this.tenantId, this.page - 1, this.getPageSize(this.pageSizeIndex), requestSortBy)
      .pipe(catchError(this.notificationsService.handleHttpError('listSealCertificate')))
      .subscribe({
        next: sealCertificatesResult => {
          this.sealCertificateList = sealCertificatesResult.content.map(dto => Object.assign(new SealCertificate(), dto));
          this.total = sealCertificatesResult.totalElements;
        },
        error: e => this.notificationsService.showErrorMessage(SealCertificateMessages.ERROR_RETRIEVING_SEAL_CERTIFICATES, getFirstErrorMessage(e))
      })
      .add(() => this.loading = false);
  }


  refreshUsage(sealCertificate: SealCertificate) {

    // Preventing useless request
    if ((sealCertificate.usageCount == 0) || (!!sealCertificate.usageSubtypes && (sealCertificate.usageSubtypes.length > 0))) {
      return;
    }

    this.internalAdminSealCertificateService
      .listSubtypeUsage(this.tenantId, sealCertificate.id, 0, this.usageSubtypesMaxSize)
      .pipe(catchError(this.notificationsService.handleHttpError('listSubtypeUsage')))
      .subscribe(subtypePaginated => sealCertificate.usageSubtypes = subtypePaginated.content);
  }


  onRowOrderClicked(row: SealCertificateSortBy) {
    this.asc = (row === this.sortBy) ? !this.asc : true;
    this.sortBy = row;
    this.refreshSealCertificateList();
  }


  openEditSealPopup(sealCertificateId: string) {

    const $getFullSealCertificateObject = !!sealCertificateId
      ? this.adminSealCertificateService.getSealCertificate(this.tenantId, sealCertificateId)
      : of(FormUtils.generateBlankSealCertificateDto());

    $getFullSealCertificateObject
      .pipe(catchError(this.notificationsService.handleHttpError('getSealCertificate')))
      .subscribe({
        next: fullObject => {
          this.modalService
            .open(SealCertificatePopupComponent, {
                injector: Injector.create({
                  providers: [
                    {provide: SealCertificatePopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                    {provide: SealCertificatePopupComponent.INJECTABLE_SEAL_CERTIFICATE_KEY, useValue: Object.assign(new SealCertificate(), fullObject)}
                  ]
                }),
                size: 'lg',
                backdrop: 'static'
              }
            )
            .result
            .then(
              () => this.refreshSealCertificateList(),
              () => { /* Dismissed */ }
            );
        },
        error: e => this.notificationsService.showErrorMessage(SealCertificateMessages.ERROR_RETRIEVING_SEAL_CERTIFICATE, getFirstErrorMessage(e))
      });
  }


  onDeleteButtonClicked(certificate: SealCertificateRepresentation) {
    this.globalPopupService
      .showDeleteValidationPopup(this.adminMessages.certificateValidationPopupLabel(certificate.name))
      .then(
        () => this.deleteSealCertificate(certificate),
        () => {/* dismissed */}
      );
  }


  deleteSealCertificate(certificate: SealCertificateRepresentation): void {
    this.adminSealCertificateService
      .deleteSealCertificate(this.tenantId, certificate.id)
      .pipe(catchError(this.notificationsService.handleHttpError('deleteSealCertificate')))
      .subscribe({
        next: () => {
          this.notificationsService.showSuccessMessage(SealCertificateMessages.SUCCESS_DELETING_SEAL_CERTIFICATE);
          this.refreshSealCertificateList();
        },
        error: e => this.notificationsService.showErrorMessage(SealCertificateMessages.ERROR_DELETING_SEAL_CERTIFICATE, getFirstErrorMessage(e))
      });
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  isExpired(sealCertificate: SealCertificate): boolean {
    return (sealCertificate.expirationDate != null) && (new Date(sealCertificate.expirationDate).getTime() < new Date().getTime());
  }


}
