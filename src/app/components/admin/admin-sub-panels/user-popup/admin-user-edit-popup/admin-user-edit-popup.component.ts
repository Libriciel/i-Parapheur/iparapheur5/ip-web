/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Inject, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from '../../../../../services/notifications.service';
import { CommonIcons } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../../shared/common-messages';
import { LoggableSignatureImage } from '../../../../../models/auth/loggable-signature-image';
import { LoggableUser } from '../../../../../models/auth/loggable-user';
import { CrudOperation } from '../../../../../services/crud-operation';
import { UserPopupMessages } from '../user-popup-messages';
import { Observable } from 'rxjs';
import { AdminTenantUserService as ProvisioningAdminTenantUserService, AdminAllUsersService } from '@libriciel/iparapheur-provisioning';
import { UserDto } from '@libriciel/iparapheur-provisioning/model/userDto';
import { catchError } from 'rxjs/operators';
import { DeskRepresentation, TenantRepresentation } from '@libriciel/iparapheur-legacy';
import { getFirstErrorMessage } from '../../../../../utils/string-utils';


@Component({
  selector: 'app-admin-user-edit-popup',
  templateUrl: './admin-user-edit-popup.component.html',
  styleUrls: ['./admin-user-edit-popup.component.scss']
})
export class AdminUserEditPopupComponent implements OnInit {


  public static readonly INJECTABLE_TENANT_ID_KEY: string = 'tenantId';
  public static readonly INJECTABLE_USER_KEY: string = 'user';

  readonly commonMessages = CommonMessages;
  readonly messages = UserPopupMessages;
  readonly commonIcons = CommonIcons;


  isProcessing: boolean = false;


  signatureImageBlob: Blob;
  signatureImageSrc: string;
  startedSessionWithASignatureImage: boolean = false;
  signatureImageWasModified: boolean = false;


  // <editor-fold desc="LifeCycle">


  constructor(private adminAllUsersService: AdminAllUsersService,
              private notificationService: NotificationsService,
              private provisioningAdminTenantUserService: ProvisioningAdminTenantUserService,
              private activeModal: NgbActiveModal,
              @Inject(AdminUserEditPopupComponent.INJECTABLE_USER_KEY) public user: UserDto,
              @Inject(AdminUserEditPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string) {
  }


  ngOnInit(): void {
    this.signatureImageWasModified = false;
    this.getSignatureImage()
      .subscribe({
        next: blob => {
          this.startedSessionWithASignatureImage = true;
          this.signatureImageSrc = URL.createObjectURL(blob);
        },
        error: () => {
          this.startedSessionWithASignatureImage = false;
          this.signatureImageSrc = null;
        }
      });
  }


  // </editor-fold desc="LifeCycle">


  // <editor-fold desc="UI Callbacks">


  onSaveButtonClicked() {

    this.user.administeredDeskIds = this.user.administeredDesks?.map((desk: DeskRepresentation) => desk.id) ?? null;
    this.user.administeredTenantIds = this.user.administeredTenants?.map((tenant: TenantRepresentation) => tenant.id) ?? null;
    this.user.associatedTenantIds = this.user.associatedTenants?.map(tenant => tenant.id) ?? null;
    this.user.associatedDeskIds = this.user.associatedDesks?.map((desk: DeskRepresentation) => desk.id) ?? null;
    this.user.supervisedDeskIds = this.user.supervisedDesks?.map((desk: DeskRepresentation) => desk.id) ?? null;
    this.user.delegationManagedDeskIds = this.user.delegationManagedDesks?.map((desk: DeskRepresentation) => desk.id) ?? null;

    const requestObservable$: Observable<any> = !this.tenantId
      ? this.adminAllUsersService.updateUserAsSuperAdmin(this.user.id, this.user)
      : this.provisioningAdminTenantUserService.updateUser(this.tenantId, this.user.id, this.user);

    this.isProcessing = true;

    this.handleSignatureImage();

    requestObservable$
      .pipe(catchError(this.notificationService.handleHttpError('updateUser')))
      .subscribe({
        next: () => {
          this.notificationService.showCrudMessage(CrudOperation.Update, new LoggableUser(this.user));
          this.activeModal.close(CommonMessages.ACTION_RESULT_OK);
        },
        error: e => this.notificationService.showCrudMessage(CrudOperation.Update, new LoggableUser(this.user), getFirstErrorMessage(e), false)
      })
      .add(() => this.isProcessing = false);
  }


  signatureImageChanged(file: Blob) {
    this.signatureImageBlob = file;
    this.signatureImageWasModified = true;
  }


  signatureImageDeleted() {
    this.signatureImageSrc = null;
    this.signatureImageBlob = null;
    this.signatureImageWasModified = true;
  }


  cancel() {
    this.activeModal.dismiss(CommonMessages.ACTION_RESULT_CANCEL);
  }


  // </editor-fold desc="UI Callbacks">


  handleSignatureImage() {
    if (!this.signatureImageWasModified) {
      return;
    }

    if (this.startedSessionWithASignatureImage && !this.signatureImageBlob) {
      this.deleteSignatureImage()
        .subscribe({
          next: () => console.debug("Signature image deleted successfully"),
          error: e => this.notificationService.showCrudMessage(CrudOperation.Delete, new LoggableSignatureImage(), e.message, false)
        });
      return;
    }

    const $request: Observable<any> = this.startedSessionWithASignatureImage
      ? this.updateSignatureImage()
      : this.setSignatureImage();

    const operation: CrudOperation = this.startedSessionWithASignatureImage ? CrudOperation.Update : CrudOperation.Create;

    $request
      .subscribe({
        next: () => console.debug("signature image updated or created successfully"),
        error: e => this.notificationService.showCrudMessage(operation, new LoggableSignatureImage(), e.message, false)
      });
  }

  getSignatureImage(): Observable<any> {
    return !!this.tenantId
      ? this.provisioningAdminTenantUserService.getSignatureImage(
        this.tenantId,
        this.user.id,
        'body',
        false,
        {httpHeaderAccept: 'application/octet-stream'}
      )
      : this.adminAllUsersService.getSignatureImageAsSuperAdmin(
        this.user.id,
        'body',
        false,
        {httpHeaderAccept: 'application/octet-stream'}
      );
  }

  setSignatureImage(): Observable<any> {
    return !!this.tenantId
      ? this.provisioningAdminTenantUserService.setSignatureImage(this.tenantId, this.user.id, this.signatureImageBlob)
      : this.adminAllUsersService.setSignatureImageAsSuperAdmin(this.user.id, this.signatureImageBlob);
  }

  updateSignatureImage(): Observable<any> {
    return !!this.tenantId
      ? this.provisioningAdminTenantUserService.updateSignatureImage(this.tenantId, this.user.id, this.signatureImageBlob)
      : this.adminAllUsersService.updateSignatureImageAsSuperAdmin(this.user.id, this.signatureImageBlob);
  }

  deleteSignatureImage(): Observable<any> {
    return !!this.tenantId
      ? this.provisioningAdminTenantUserService.deleteSignatureImage(this.tenantId, this.user.id)
      : this.adminAllUsersService.deleteSignatureImageAsSuperAdmin(this.user.id);
  }


}
