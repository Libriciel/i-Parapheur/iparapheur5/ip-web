/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CommonIcons } from '@libriciel/ls-elements';
import { UserEditSubPanelsMessages } from '../user-edit-sub-panels-messages';
import { CommonMessages } from '../../../../../shared/common-messages';
import { TemplateType, TenantRepresentation, UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { TemplateUtils } from '../../../../../utils/template-utils';
import { DomSanitizer } from '@angular/platform-browser';
import { NotificationsService } from '../../../../../services/notifications.service';
import { TemplateService } from '@libriciel/iparapheur-internal';
import { FormGroup, FormControl } from '@angular/forms';
import { DefaultTemplatesForm, PartialTemplatesDataChange } from '../../../../../shared/models/custom-types';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserDto } from '@libriciel/iparapheur-provisioning';
import { CONFIG } from '../../../../../shared/config/config';

@Component({
  selector: 'app-user-edit-signature-template',
  templateUrl: './user-edit-signature-template.component.html',
  styleUrls: ['./user-edit-signature-template.component.scss']
})
export class UserEditSignatureTemplateComponent implements OnInit, OnChanges {

  readonly signatureTemplates: TemplateType[] = TemplateUtils.SIGNATURE_TEMPLATE_TYPES;
  readonly sealTemplates: TemplateType[] = TemplateUtils.SEAL_TEMPLATE_TYPES;

  protected readonly commonIcons = CommonIcons;
  protected readonly messages = UserEditSubPanelsMessages;
  protected readonly commonMessages = CommonMessages;

  lastModificationDate: string = Date.now().toString();


  @Input() currentUser: UserDto;
  @Input() userPreferences: UserPreferencesDto;
  @Input() signatureImageBlob: Blob;

  defaultTemplatesForm: FormGroup<DefaultTemplatesForm>;
  signatureTemplateSrc: Blob;
  sealTemplateSrc: Blob;

  showTemplateExamples: boolean;

  constructor(public sanitizer: DomSanitizer,
              private notificationService: NotificationsService,
              private templateService: TemplateService) { }


  ngOnInit(): void {
    this.showTemplateExamples = this.currentUser?.associatedTenants.length === 1;

    this.resetTemplateExamples();

    this.initForm();
  }

  ngOnChanges(_: SimpleChanges): void {
    this.resetTemplateExamples();
  }

  resetTemplateExamples() {
    if (!this.userPreferences) {
      return;
    }

    this.testTemplate(this.userPreferences.defaultSignatureTemplate || TemplateType.SignatureMedium)
      .subscribe((blob: Blob) => this.signatureTemplateSrc = blob);

    this.testTemplate(this.userPreferences.defaultSealTemplate || TemplateType.SealMedium)
      .subscribe((blob: Blob) => this.sealTemplateSrc = blob);
  }


  initForm() {

    if (!this.userPreferences) {
      return;
    }

    this.defaultTemplatesForm = new FormGroup<DefaultTemplatesForm>(
      {
        signatureTemplate: new FormControl<TemplateType>(this.userPreferences.defaultSignatureTemplate || TemplateType.SignatureMedium),
        sealTemplate: new FormControl<TemplateType>(this.userPreferences.defaultSealTemplate || TemplateType.SealMedium)
      }
    );

    this.defaultTemplatesForm.valueChanges.subscribe((changes: PartialTemplatesDataChange) => {
      if (!!changes.signatureTemplate && this.userPreferences.defaultSignatureTemplate !== changes.signatureTemplate) {
        this.userPreferences.defaultSignatureTemplate = changes.signatureTemplate;
        this.testTemplate(this.userPreferences.defaultSignatureTemplate)
          .subscribe((blob: Blob) => this.signatureTemplateSrc = blob);
      }

      if (!!changes.sealTemplate && this.userPreferences.defaultSealTemplate !== changes.sealTemplate) {
        this.userPreferences.defaultSealTemplate = changes.sealTemplate;
        this.testTemplate(this.userPreferences.defaultSealTemplate)
          .subscribe((blob: Blob) => this.sealTemplateSrc = blob);
      }
    });
  }

  testTemplate(template: TemplateType): Observable<Blob> {

    const tenant: TenantRepresentation = this.currentUser?.associatedTenants[0];

    if (!tenant) {
      return of(null);
    }

    return this.templateService
      .testSignatureTemplate(
        tenant.id,
        template,
        true,
        this.signatureImageBlob,
        'body',
        true,
        {httpHeaderAccept: 'application/pdf'}
      )
      .pipe(catchError(this.notificationService.handleHttpError('createSecureMailServer')));
  }


}
