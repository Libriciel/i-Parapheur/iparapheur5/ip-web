/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { faLock } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute } from '@angular/router';
import { AdminMessages } from '../../admin-messages';
import { isCurrentVersion52OrAbove } from '../../../../../utils/string-utils';

@Component({
  selector: 'app-advanced-admin-general',
  templateUrl: './advanced-admin-general.component.html',
  styleUrls: ['./advanced-admin-general.component.scss']
})
export class AdvancedAdminGeneralComponent implements OnInit {


  readonly messages = AdminMessages;
  readonly show52Features: boolean = isCurrentVersion52OrAbove();
  readonly menuRows = [
    {route: './gdpr', label: 'RGPD', icon: faLock},
    {route: './signature-validation', label: 'Validation de signature', icon: faLock, hide: !this.show52Features},
  ];


  constructor(public route: ActivatedRoute) { }


  ngOnInit(): void {
    this.route.parent?.params.subscribe();
  }

}
