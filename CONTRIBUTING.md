# Contributing


## Keycloak conf

Check in http://localhost/auth :  
- `Client` → `ipcore-web` → `Direct Access Grants Enabled` : `✓`


## Nexus libraries

Some libs may not be accessible, and need a configuration.

Production libraries are sent to Libriciel's Nexus and the official Maven Repository.  
Development libraries (`0.0.0-*`) are only sent to Libriciel's Nexus.

We may need some conf in the `~/.npmrc` file:

```
@libriciel:registry = https://nexus.libriciel.fr/repository/npm/
#registry = https://nexus.libriciel.fr/repository/npm/
```


## Starting project locally
 
Debug mode, without cross-platform check :

```bash
$ ng serve --host 0.0.0.0 --allowed-hosts iparapheur.dom.local
```

Or closer to the final build :

```bash
$ ng serve --prod --optimization=false --host 0.0.0.0 --allowed-hosts iparapheur.dom.local
```

Standalone dev mode, with mocked backend :

```bash
$ ng serve --configuration=debug
```


## Code style

The `.idea` folder has been committed. It should bring the appropriate code-style.  
Obviously, every modified file should be auto-formatted before any git push.  
No hook would reject a mis-formatted file for now... But if everything goes bananas, we may set it up.

If you are not an IntelliJ user, a `.editorconfig` file is also available, that should be recognized by most code editors,
 and contains ALL the rules used to enforce the code style of this project.


## RGAA 4.1 Accessibility

Some rules already exist on the ESLint. Prior to any accessibility declaration, a simple check can be made:

```bash
eslint src/app src/index.html --config eslint.config.ng-ts.mjs
eslint src/app src/index.html --config eslint.config.html-eslint.mjs
```

Some of the RGAA's 106 rules are easily linted, and are already automatically checked.  

For now, we have:
* 🔵: `27/106` Non-applicability checked automatically, or app-wise
* 🟢: `10/106` Validity checked automatically
* 🟠: `07/106` Partially checked automatically
* 🔴: `07/106` Invalid
* 🟤: `21/106` Should be checked manually
* ⚪️: `34/106` Undetermined

### Evaluation

First, the ESlint should be error-free, obviously.  
Some HTML elements are forbidden (weird images alternatives, legacy elements...), to ease some of the required following tests.  
Checking automatically the plain absence of these elements will just avoid a lot of tests, later.   

Then, on a started evaluation, we can check the following greens (valid), reds (invalid) and blues (non-applicable) in the form.  

_Note: "ESLint", "@angular-eslint" and "@html-eslint" should be quoted as used tools at the end of the declaration._  
_Note 2: The existence of a smartphone/tablet version should be declared at the end of the declaration,
to justify the "non-applicability" of smartphone-related rules._

- `1`: Images
  * 🟢 `1.1`: Images shall have a text alternative `@angular-eslint/template/alt-text` & `@html-eslint/no-restricted-attrs` & `@html-eslint/require-attrs`
  * 🟠 `1.2`: Decorations shall be ignored
    - 🟤 `1.2.1`: decorations shall have a `role="presentation"`
    - 🔵 `1.2.[2-3,5-6]`: `@html-eslint/no-restricted-attrs`
    - 🟤 `1.2.4`: Check `<svg>`'s `aria-hidden`
  * 🟠 `1.3`: Description pertinence
    - 🟤 `1.3.[1,3,6,9]`: Check `alt|title|aria-label[led-by]` pertinence
    - 🔵 `1.3.[2,4,5,7-8]`: `@html-eslint/no-restricted-attrs`
  * 🔵 `1.[4-5]`: Captcha image description/alternative, non applicable
  * 🟠 `1.6`: Text alternative pertinence
    - 🟤 `1.6.1`: Check `<img>` titles
    - 🔵 `1.6.[2-3,7-8]`: `@html-eslint/no-restricted-attrs`
    - 🟤 `1.6.4`: Check `<input type="image">` titles
    - 🟤 `1.6.[5-6]`: Check `<svg>`'s `aria-label[ledby]` titles
    - 🟤 `1.6.9`: Check `aria-describedby` links
    - 🔵 `1.6.10`: `@html-eslint/no-restricted-attrs`
  * 🟠 `1.7`: Description pertinence
    - 🟤 `1.7.[1-2]`: Check `<img|input>`'s `longdesc|aria-describedby` pertinence
    - 🔵 `1.7.[3-4,6]`: `@html-eslint/no-restricted-attrs`
    - 🟤 `1.7.5`: Check `<svg>`'s `desc|title|aria-describedby` pertinence
  * ⚪️ `1.8`
  * 🔵 `1.9`: `@html-eslint/no-restricted-attrs`
- `2`: Frames
  * 🟢 `2.1`: `@html-eslint/require-frame-title`
  * 🟤 `2.2`: Check `<iframe>` title pertinence
- `3`: Colors
  * 🟤 `3.[1-3]`: Shall be checked visually
- `4`: Multimedia
  * 🔵 `4.[1-13]`: `@html-eslint/no-restricted-attrs`
- `5` Tables
  * 🟢 `5.[1-2,4-5]` `@html-eslint/require-attrs` forces a `aria-labelledby` into the hx title
  * ⚪️ `5.[3,6-8]`
- `6` Links
  * ⚪️ `6.1-2`
- `7` Scripts
  * 🟤 `7.[1-4]`: `<script>` accessibility
  * 🟤 `7.5`: Check alerts, status, etc.
- `8` Mandatory elements
  * 🟢 `8.1`: Doctype `@html-eslint/require-doctype`
  * 🟢 `8.2`: Valid HTML `@html-eslint/*` & `@angular-eslint/*`
  * 🟢 `8.[3-4]`: Lang `@html-eslint/require-lang`
  * 🟤 `8.[5-6]`: Check page titles in the routing module.
  * 🔵 `8.[7-8]`: Language swap, non-applicable
  * 🟤 `8.9`: Check balises pertinence
  * 🔵 `8.10`: Right-to-left reading, non-applicable
- `9` Information structure
  * 🟠 `9.1`: Appropriate titles
    - 🟢 `9.1.1`: `@html-eslint/no-skip-heading-levels` & `@html-eslint/no-multiple-h1`
    - 🟤 `9.1.2`: Check `hx` titles
    - 🟤 `9.1.3`: Forbid titles not in `hx`
  * ️🟤 `9.2`: HTML structure
  * ️🟤 `9.3`: Lists structure
  * 🟤 `9.4`: Quotes shall be in `<q>`
- `10` Presentation
  * ⚪️ `10.1-14`
- `11` Forms
  * ⚪️ `11.1-13`
- `12`: Navigation
  * 🔴 `12.1`: 2 nav systems
  * 🟤 `12.2`: Menu stability
  * 🔴 `12.[3-4]`: Site plan
  * 🔵 `12.5`: Search engine (not the folder one), doesn't exist
  * ️🔴 `12.[6-7]`: Shortcut link to the main content
  * 🟤 `12.[8-9]`: Tab nav
  * 🔵 `12.10`: `@html-eslint/no-accesskey-attrs`
  * 🟤 `12.11`: Hover/hint with keyboard
- `13`: Consultation
  * ️🔴 `13.1`: The `13.1.4` session length settings clashes with the GDPR. Everything else is OK, though.
  * ️🔴 `13.2`: No automatic popup opening, fails with the first-launch GDPR one
  * 🔵 `13.[3-4]`: Documents have accessible alternates, non-applicable on user's documents
  * ️🟤 `13.[5-6]`: ASCII-art and emoji's description/titles
  * ️🟠 `13.7`: Blink/marquee is appropriate
    - 🟤 `13.7.1`: Check for blinking in `<img|svg>`
    - 🔵 `13.7.[2-3]`: `@angular-eslint/template/no-distracting-elements`
  * 🔵 `13.8`: Blink/marquee controls: `@angular-eslint/template/no-distracting-elements`
  * 🔵 `13.[9-10,12]`: Smartphone controls/layout, non-applicable
  * ️🟤 `13.11`: Check the click behavior (triggers on up, cancels on down+drag outside the element)
