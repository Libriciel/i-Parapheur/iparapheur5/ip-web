/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { faUnlink, faLink } from '@fortawesome/free-solid-svg-icons';
import { NotificationsService } from '../../../../../../services/notifications.service';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { TypologyMessages } from '../../typology-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { AdminMetadataService, MetadataSortBy, MetadataRepresentation, SubtypeDto, SubtypeMetadataDto } from '@libriciel/iparapheur-provisioning';
import { catchError } from 'rxjs/operators';


@Component({
  selector: 'app-subtype-metadata',
  templateUrl: './subtype-metadata.component.html',
  styleUrls: ['./subtype-metadata.component.scss']
})
export class SubtypeMetadataComponent implements OnInit {

  readonly pageSizes = [6];
  readonly unlinkIcon = faUnlink;
  readonly linkIcon = faLink;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly messages = TypologyMessages;

  @Input() tenantId: string;
  @Input() subtype: SubtypeDto;

  metadataList: MetadataRepresentation[];
  filteredMetadataList: MetadataRepresentation[] = [];
  selectedMetadata: MetadataRepresentation = null;

  page = 1;
  pageSizeIndex = 1;
  total = 0;

  loading: boolean = false;

  // <editor-fold desc="LifeCycle">


  constructor(public modalService: NgbModal,
              public notificationService: NotificationsService,
              public adminMetadataService: AdminMetadataService) { }


  ngOnInit(): void {
    if (!this.subtype.subtypeMetadataList) {
      this.subtype.subtypeMetadataList = [];
    }

    this.loading = true;
    this.adminMetadataService
      .listMetadata(this.tenantId, false, null, 0, 500, [MetadataSortBy.Index + ",ASC"])
      .pipe(catchError(this.notificationService.handleHttpError('listMetadata')))
      .subscribe(metadataRetrieved => {
        this.metadataList = metadataRetrieved.content;
        this.filterAlreadyLinkedMetadataFromList();
      })
      .add(() => this.loading = false);
  }


  // </editor-fold desc="LifeCycle">


  filterAlreadyLinkedMetadataFromList() {
    this.filteredMetadataList = this.metadataList?.filter(m => !this.subtype.subtypeMetadataList.some(sm => sm.metadata.id === m.id));
  }


  onAddMetadataButtonClicked() {
    this.adminMetadataService.getMetadata(this.tenantId, this.selectedMetadata.id)
      .pipe(catchError(this.notificationService.handleHttpError('getMetadata')))
      .subscribe({
        next: updatedMetadata => {
          const newSubtypeMetadata = {
            metadataId: this.selectedMetadata.id,
            metadata: updatedMetadata
          };
          this.selectedMetadata = null;
          this.subtype.subtypeMetadataList.push(newSubtypeMetadata);
          this.filterAlreadyLinkedMetadataFromList();
        },
        error: e => this.notificationService.showErrorMessage(`Erreur lors de la récupération de la métadonnée`, e)
      });
  }


  onUnlinkButtonClicked(subtypeMetadata: SubtypeMetadataDto) {
    const index = this.subtype.subtypeMetadataList.indexOf(subtypeMetadata);
    this.subtype.subtypeMetadataList.splice(index, 1);
    this.filterAlreadyLinkedMetadataFromList();
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  searchMetadata = (term: string, item: MetadataRepresentation) => item.name.includes(term) || item.key.includes(term);

}
