/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { TemplatesEditPopupComponent } from './templates-edit-popup.component';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ToastrModule } from 'ngx-toastr';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { TemplateType } from '@libriciel/iparapheur-legacy';
import { TemplateTypeNamePipe } from '../../../../../../../../../utils/template-type-name.pipe';


describe('TemplatesEditPopupComponent', () => {

  let component: TemplatesEditPopupComponent;
  let fixture: ComponentFixture<TemplatesEditPopupComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ToastrModule.forRoot()],
      providers: [
        NgbActiveModal, HttpClient, HttpHandler,
        {provide: TemplatesEditPopupComponent.INJECTABLE_CREATION_MODE_KEY, useValue: true},
        {provide: TemplatesEditPopupComponent.INJECTABLE_ORIGINAL_TEMPLATE_KEY, useValue: 'test'},
        {provide: TemplatesEditPopupComponent.INJECTABLE_TEMPLATE_TYPE_KEY, useValue: TemplateType.MailNotificationDigest},
        {provide: TemplatesEditPopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: 'tenant01'},
      ],
      declarations: [TemplateTypeNamePipe, TemplatesEditPopupComponent]
    })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(TemplatesEditPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
