/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { Action } from '@libriciel/iparapheur-legacy';
import { SecondaryAction } from '../models/secondary-action.enum';
import { StepDefinitionType } from '@libriciel/iparapheur-provisioning';

@Pipe({
  name: 'stepToAction'
})
export class StepToActionPipe implements PipeTransform {


  public static compute(state: StepDefinitionType): Action | SecondaryAction {
    switch (state) {
      case StepDefinitionType.Archive:
        return SecondaryAction.End;
      case StepDefinitionType.ExternalSignature:
        return Action.ExternalSignature;
      case StepDefinitionType.Ipng:
        return Action.Ipng;
      case StepDefinitionType.Seal:
        return Action.Seal;
      case StepDefinitionType.SecureMail:
        return Action.SecureMail;
      case StepDefinitionType.Signature:
        return Action.Signature;
      case StepDefinitionType.Visa:
        return Action.Visa;
      default:
        return null;
    }
  }


  transform(value: StepDefinitionType): Action | SecondaryAction {
    return StepToActionPipe.compute(value);
  }


}
