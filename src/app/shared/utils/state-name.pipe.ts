/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { WorkflowStepState } from '@libriciel/ls-workflow';
import { State } from '@libriciel/iparapheur-legacy';

@Pipe({
  name: 'stateName'
})
export class StateNamePipe implements PipeTransform {


  public static compute(state: any, plural: boolean): string {
    switch (state) {
      case State.Draft:
        return plural ? 'Brouillons' : 'Brouillon';
      case State.Late:
        return plural ? 'Dossiers en retard' : 'En retard';
      case State.Finished:
        return plural ? 'Dossiers en fin de circuit' : 'En fin de circuit';
      case State.Pending:
      case WorkflowStepState.PENDING:
      case State.Current:
        return plural ? 'Dossiers à traiter' : 'À traiter';
      case State.Delegated:
        return plural ? 'Absences : Dossiers à traiter' : 'Absences : Dossier à traiter';
      case State.Upcoming:
        return plural ? 'Dossiers à venir' : 'À venir';
      case State.Validated:
      case WorkflowStepState.VALIDATED:
        return plural ? 'Dossiers validés' : 'Validée';
      case State.Retrievable:
        return plural ? 'Dossiers récupérables' : 'Récupérable';
      case WorkflowStepState.REJECTED:
      case State.Rejected:
        return plural ? 'Dossiers rejetés' : 'Rejeté';
      case State.Transferred:
        return plural ? 'Dossiers Transférés' : 'Transféré';
      case State.Seconded:
        return plural ? 'Dossiers envoyés en avis complémentaires' : `Envoyé en avis complémentaire`;
      case State.Bypassed:
        return plural ? 'Dossiers contournés' : 'Contournée';
      case State.Downstream:
        return plural ? 'Dossiers traités' : 'Traité';
      default:
        return '';
    }
  }


  transform(value: any, plural: boolean = true): string {
    return StateNamePipe.compute(value, plural);
  }


}
