/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, ViewChild } from '@angular/core';
import { DeskMessages } from '../../desk-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';
import { DoubleListComponent } from '../../../../../../shared/components/double-list/double-list.component';
import { stringifyNamedElement } from 'src/app/utils/string-utils';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { AdminDeskService, DeskRepresentation, PageDeskRepresentation } from '@libriciel/iparapheur-provisioning';

@Component({
  selector: 'app-associates',
  templateUrl: './associates.component.html',
  styleUrls: ['./associates.component.scss']
})
export class AssociatesComponent {


  readonly messages = DeskMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly infoIcon = faInfoCircle;
  readonly stringifyNamedElementFn = stringifyNamedElement;

  @Input() desk: DeskRepresentation;
  @Input() tenantId: string;
  @Input() associates: DeskRepresentation[] = [];
  @ViewChild('doubleListComponent') doubleListComponent: DoubleListComponent<DeskRepresentation>;

  currentSearch: string = null;


  // <editor-fold desc="LifeCycle">


  constructor(private adminDeskService: AdminDeskService) {}


  // </editor-fold desc="LifeCycle">


  onSearchKeyUp(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentSearch = newTerm;
    this.doubleListComponent.requestElements(true);
  }


  requestDesksFn = (page: number, pageSize: number): Observable<PageDeskRepresentation> => {
    const searchTerm = this.currentSearch?.length > 0 ? this.currentSearch : null;

    return this.adminDeskService
      .listDesks(this.tenantId, page, pageSize, [], searchTerm);
  };


}
