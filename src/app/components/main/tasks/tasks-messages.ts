/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


export class TasksMessages {

  static readonly READ: string = 'Lu';
  static readonly UNREAD: string = 'Non lu';

  static readonly TASKS_LIST: string = 'Liste des tâches';

  static readonly ERROR_RETRIEVING_FILTERS: string = 'Erreur à la récupération des filtres personnalisés';
  static readonly ERROR_RETRIEVING_FOLDERS: string = 'Erreur à la récupération des dossiers.';
  static readonly ERROR_DOWNLOADING_FOLDER: string = 'Erreur au téléchargement du dossier.';
  static readonly READING_MANDATORY: string = 'Lecture obligatoire';
  static readonly ERROR_UPDATING_TABLE_LAYOUT: string = `Erreur à l'édition du paramétrage du tableau de bord`;
  static readonly ERROR_DELETING_TABLE_LAYOUT: string = `Erreur à la suppression du paramétrage du tableau de bord`;

  static readonly NO_ACTION_FOR_SELECTED_FOLDERS: string = 'Aucune action disponible pour les dossiers sélectionnés.';
}
