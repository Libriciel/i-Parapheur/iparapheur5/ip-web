/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { IpStepInstance } from './ip-step-instance';
import { IpWorkflowActor } from './ip-workflow-actor';
import { State } from '@libriciel/iparapheur-legacy';

export class IpWorkflowInstance {

  id: string;
  name: string;
  steps: IpStepInstance[];
  finalDesk: IpWorkflowActor;
  description?: string;
  isRejected: boolean;
  isOver: boolean = false;
  currentStepIdx: number = 0;
  rejected: boolean = false;

  constructor(data?: {
    id: string;
    name: string;
    description?: string;
    steps: IpStepInstance[];
    isRejected: boolean;
    isOver: boolean;
    finalDesk?: { id: string; name: string; }
  }) {
    this.id = data.id;
    this.name = data.name;
    this.description = data.description;
    this.isRejected = data.isRejected;
    this.isOver = data.isOver;
    this.finalDesk = new IpWorkflowActor(data.finalDesk);

    if (data.steps) {
      this.steps = data.steps;
      this.currentStepIdx = data.steps.findIndex(step => step.state.toUpperCase() === State.Current.toUpperCase());
      if (this.currentStepIdx === -1) {
        this.currentStepIdx = 0;
      }
    }
  }

}
