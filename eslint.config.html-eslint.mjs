/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import html from "@html-eslint/eslint-plugin";
import htmlParser from "@html-eslint/parser";


/**
 * Note: There is a lot of documentations about eslint-plugin-jsx-a11y or eslint-plugin-lit-a11y.
 * These do not work with Angular. It is rarely said explicitly, but they just don't.
 */
export default [
    {
        ...html.configs["flat/recommended"],
        files: ["**/*.html"],
        plugins: {"@html-eslint": html,},
        languageOptions: {parser: htmlParser},
        rules: {
            ...html.configs["flat/recommended"].rules,
            // Some rules are too low, by default. We upgrade these to errors.
            "@html-eslint/no-accesskey-attrs": "error",
            "@html-eslint/no-skip-heading-levels": "error",
            "@html-eslint/require-frame-title": "error",
            "@html-eslint/no-restricted-attrs": [
                "error",
                {tagPatterns: [".*"], attrPatterns: ["^figcaption$"], message: "\'figcaption\' is restricted."},
                {tagPatterns: [".*"], attrPatterns: ["^figure$"], message: "\'figure\' is restricted."},
                {tagPatterns: ["^area$"], attrPatterns: [".*"], message: "\'area\' is restricted."},
                {tagPatterns: ["^audio$"], attrPatterns: [".*"], message: "\'audio\' is restricted."},
                {tagPatterns: ["^bgsound$"], attrPatterns: [".*"], message: "\'bgsound\' is restricted."},
                {tagPatterns: ["^canvas$"], attrPatterns: [".*"], message: "\'canvas\' is restricted."},
                {tagPatterns: ["^embed$"], attrPatterns: [".*"], message: "\'embed\' is restricted."},
                {tagPatterns: ["^img$"], attrPatterns: ["^ismap$"], message: "\'img ismap\' is restricted."},
                {tagPatterns: ["^meta$"], attrPatterns: ["^http-equiv$"], message: "\'meta http-equiv\' is restricted."},
                {tagPatterns: ["^object$"], attrPatterns: [".*"], message: "\'object\' is restricted."},
                {tagPatterns: ["^video$"], attrPatterns: [".*"], message: "\'video\' is restricted."},
            ],
            '@html-eslint/no-restricted-attr-values': [
                "error",
                {attrPatterns: ["^role$"], attrValuePatterns: ["^image$"], message: "\'role=image\' is restricted."},
            ],
            "@html-eslint/require-attrs": [
                "error",
                {tag: "svg", attr: "aria-label"},
                {tag: "table", attr: "aria-labelledby"},
            ],
            // Duplicates, already set in the official Angular one, where they work a little better
            "@html-eslint/require-img-alt": "off",
            "@html-eslint/no-duplicate-id": "off",
            "@html-eslint/attrs-newline": "off",
            // TODO: Every "warn" overridden parameter here is usually set as "error", by default.
            //  we should fix every one of these, and remove the override altogether, to leave the default "error" behavior.
            "@html-eslint/element-newline": "warn",
            "@html-eslint/require-closing-tags": "warn",
            "@html-eslint/require-li-container": "warn",
            "@html-eslint/quotes": "warn",
            "@html-eslint/indent": ["warn", 2],
        }
    }
];
