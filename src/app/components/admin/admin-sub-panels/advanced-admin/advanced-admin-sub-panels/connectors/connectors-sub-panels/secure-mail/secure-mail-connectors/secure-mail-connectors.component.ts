/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Injector, OnInit } from '@angular/core';
import { NotificationsService } from '../../../../../../../../../services/notifications.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SecureMailServer } from '../../../../../../../../../models/securemail/secure-mail-server';
import { AddSecureMailConnectorComponent } from '../add-secure-mail-connector/add-secure-mail-connector.component';
import { AdminSecureMailService } from '../../../../../../../../../services/ip-core/admin-secure-mail.service';
import { CrudOperation } from '../../../../../../../../../services/crud-operation';
import { ActivatedRoute } from '@angular/router';
import { ConnectorMessages } from '../../../connector-messages';
import { CommonIcons, Style } from '@libriciel/ls-elements';
import { CommonMessages } from '../../../../../../../../../shared/common-messages';
import { GlobalPopupService } from '../../../../../../../../../shared/service/global-popup.service';
import { AdminMessages } from '../../../../../../admin-messages';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';

@Component({
  selector: 'app-secure-mail-connectors',
  templateUrl: './secure-mail-connectors.component.html',
  styleUrls: ['./secure-mail-connectors.component.scss']
})
export class SecureMailConnectorsComponent implements OnInit {


  readonly pageSizes = [10, 15, 20, 50];
  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly messages = ConnectorMessages;
  readonly adminMessages = AdminMessages;
  readonly style = Style;


  tenantId: string;
  serverList: SecureMailServer[] = [];
  page = 1;
  total = 0;
  pageSizeIndex = 1;
  currentSearchTerm = '';
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  loading: boolean = false;
  colspan: number;

  // <editor-fold desc="LifeCycle">


  constructor(public notificationService: NotificationsService,
              public modalService: NgbModal,
              public globalPopupService: GlobalPopupService,
              public route: ActivatedRoute,
              private adminSecureMailService: AdminSecureMailService) { }


  ngOnInit() {
    this.route?.parent?.parent?.parent?.parent?.data?.subscribe(data => {
      this.userPreferences = data["userPreferences"];
    });
    this.colspan = 4 + (this.userPreferences.showAdminIds ? 1 : 0);

    this.route?.parent?.parent?.parent?.params?.subscribe(params => {
      this.tenantId = params['tenantId'];
      this.requestSecureMailServerList();
    });
  }

  updateSearchTerm(searchTerm: string) {
    this.currentSearchTerm = searchTerm;
    this.page = 1;
    this.total = 0;

    this.requestSecureMailServerList();
  }

  // </editor-fold desc="LifeCycle">


  requestSecureMailServerList() {
    this.loading = true;
    const searchTerm = this.currentSearchTerm.length > 0 ? this.currentSearchTerm : null;

    this.adminSecureMailService
      .findSecureMailServersByPage(this.tenantId, this.page - 1, this.getPageSize(this.pageSizeIndex), searchTerm)
      .subscribe(serverRetrieved => {
        this.serverList = serverRetrieved.data;
        this.total = serverRetrieved.total;
      })
      .add(() => this.loading = false);
  }


  onDeleteButtonClicked(server: SecureMailServer) {
    this.globalPopupService.showDeleteValidationPopup(this.adminMessages.securerMailConnectorPopupLabel(server.name))
      .then(result => {
        if (result === CommonMessages.ACTION_RESULT_OK) {
          this.deleteConnector(server);
        }
      });
  }

  onCreateButtonClicked() {
    this.modalService
      .open(AddSecureMailConnectorComponent, {
          injector: Injector.create(
            {
              providers: [
                {provide: AddSecureMailConnectorComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                {provide: AddSecureMailConnectorComponent.INJECTABLE_EDIT_MODE_KEY, useValue: false},
                {provide: AddSecureMailConnectorComponent.INJECTABLE_CURRENT_SECURE_MAIL_SERVER_KEY, useValue: new SecureMailServer()}
              ]
            }),
          backdrop: 'static'
        }
      )
      .result
      .then(
        () => this.requestSecureMailServerList(),
        () => { /* Dismissed */ }
      );
  }

  onEditButtonClicked(server: SecureMailServer) {
    server.entity = null;

    this.modalService
      .open(AddSecureMailConnectorComponent, {
          injector: Injector.create(
            {
              providers: [
                {provide: AddSecureMailConnectorComponent.INJECTABLE_TENANT_ID_KEY, useValue: this.tenantId},
                {provide: AddSecureMailConnectorComponent.INJECTABLE_EDIT_MODE_KEY, useValue: true},
                {provide: AddSecureMailConnectorComponent.INJECTABLE_CURRENT_SECURE_MAIL_SERVER_KEY, useValue: server}
              ]
            }),
          backdrop: 'static'
        }
      )
      .result
      .then(
        () => this.requestSecureMailServerList(),
        () => { /* Dismissed */ }
      );
  }

  deleteConnector(server: SecureMailServer): void {
    this.adminSecureMailService
      .deleteSecureMailServer(this.tenantId, server)
      .subscribe({
        next: () => {
          this.notificationService.showCrudMessage(CrudOperation.Delete, server);
          this.requestSecureMailServerList();
        },
        error: e => this.notificationService.showCrudMessage(CrudOperation.Delete, SecureMailServer, e.message, false)
      });
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


}
