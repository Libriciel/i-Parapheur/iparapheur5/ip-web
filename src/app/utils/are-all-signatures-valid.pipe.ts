/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { Document } from '../models/document';
import { CONFIG } from '../shared/config/config';

@Pipe({
  name: 'areAllSignaturesValid'
})
export class AreAllSignaturesValidPipe implements PipeTransform {


  public static compute(value: Document, hasSignatureValidationServiceActivated: boolean): boolean {
    if (!value) {
      return null;
    }

    const embeddedSignaturesValid = this.checkSignatures(value.embeddedSignatureInfos, hasSignatureValidationServiceActivated);
    if (embeddedSignaturesValid !== null) {
      return embeddedSignaturesValid;
    }

    let detachedInfo = null;
    if (value.detachedSignatures != null) {
      detachedInfo = value.detachedSignatures.map(signature => signature.signatureInfo);
    }
    const detachedSignaturesValid = this.checkSignatures(detachedInfo, hasSignatureValidationServiceActivated);
    if (detachedSignaturesValid !== null) {
      return detachedSignaturesValid;
    }

    return null;
  }


  private static checkSignatures(signInfos: any[], hasSignatureValidationServiceActivated: boolean): boolean {
    if (!signInfos || signInfos.length === 0) {
      return null;
    }

    if (hasSignatureValidationServiceActivated && CONFIG?.VERSION?.startsWith('5.2.')) {
      if (signInfos.every(signInfo => signInfo.isSignatureValid === true && signInfo.certificateBeginDate != null)) {
        return true;
      }
      if (signInfos.some(signInfo => (signInfo.isSignatureValid === false || signInfo.isSignatureValid === null) && signInfo.certificateBeginDate != null)) {
        return false;
      }
    } else {
      if (signInfos.every(signInfo => signInfo.isSignatureValid === true)) {
        return true;
      }
      if (signInfos.some(signInfo => signInfo.isSignatureValid === false)) {
        return false;
      }
    }

    return null;
  }


  transform(value: Document, hasSignatureValidationServiceActivated: boolean): boolean {
    return AreAllSignaturesValidPipe.compute(value, hasSignatureValidationServiceActivated);
  }


}
