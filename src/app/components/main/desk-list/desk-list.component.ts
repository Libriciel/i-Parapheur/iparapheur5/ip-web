/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { faDisplay, faCaretRight, faCaretDown, faCaretUp, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { CommonMessages } from '../../../shared/common-messages';
import { DeskListMessages } from './desk-list-messages';
import { NotificationsService } from '../../../services/notifications.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FolderUtils } from '../../../utils/folder-utils';
import { FilterService } from '../../../services/filter.service';
import { State, TenantService, TenantSortBy, CurrentUserService, DeskRepresentation, PageDeskRepresentation, UserPreferencesDto, TenantRepresentation, PageDeskCount } from '@libriciel/iparapheur-legacy';
import { from, Subscription, Observable, combineLatest } from 'rxjs';
import { WebsocketService } from '../../../services/websockets/websocket.service';
import { tap, catchError } from 'rxjs/operators';
import { ChunkPipe } from '../../../utils/chunk.pipe';
import { DeskWithCount, FlatTreeNode, DisplayModeEnum } from '../../../shared/models/custom-types';


@Component({
  selector: 'app-desk-list',
  templateUrl: './desk-list.component.html',
  styleUrls: ['./desk-list.component.scss']
})
export class DeskListComponent implements OnInit, OnDestroy {

  readonly pageSizes: number[] = [10, 15, 20, 50, 100];
  readonly caretRightIcon: IconDefinition = faCaretRight;
  readonly caretDownIcon: IconDefinition = faCaretDown;
  readonly caretUp: IconDefinition = faCaretUp;
  readonly caretDown: IconDefinition = faCaretDown;
  readonly deskIcon: IconDefinition = faDisplay;
  readonly commonMessages = CommonMessages;
  readonly messages = DeskListMessages;
  readonly stateEnum = State;
  readonly THUMB_MODE_PAGE_SIZE: number = 15;
  readonly LIST_MODE_PAGE_SIZE: number = 250;
  readonly DISPLAY_MODE_ENUM = DisplayModeEnum;
  readonly computeDelegatedTotalFn = FolderUtils.computeDelegatedTotal;


  currentSearchTerm = null;
  flattenTreeStructureUnfiltered: FlatTreeNode[] = [];
  flattenTreeStructure: FlatTreeNode[] = [];
  tenants: TenantRepresentation[] = [];
  desks: DeskWithCount[] = [];
  chunkDesks: DeskWithCount[][] = null;
  favoriteDesks: DeskWithCount[] = [];
  folderCountsSubscriptions: Subscription[] = [];
  userPreferences: UserPreferencesDto;
  page: number = 1;
  pageSize: number = this.THUMB_MODE_PAGE_SIZE;
  pageSizeIndex: number = 3;
  total: number = 0;
  displayMode: DisplayModeEnum = DisplayModeEnum.THUMBS;
  disableDisplayMode: boolean = false;
  showOnlyFavorites: boolean = false;
  asc: boolean = true;
  loading: boolean = false;
  colspan: number = 2;


  // <editor-fold desc="LifeCycle">


  constructor(public tenantService: TenantService,
              public currentUserService: CurrentUserService,
              public notificationService: NotificationsService,
              public modalService: NgbModal,
              private route: ActivatedRoute,
              private router: Router,
              private filterService: FilterService,
              private websocketService: WebsocketService) {}


  ngOnDestroy(): void {
    this.folderCountsSubscriptions.forEach(subscription => subscription.unsubscribe());
  }


  ngOnInit() {
    this.userPreferences = this.route.snapshot.data["userPreferences"];
    this.pageSizeIndex = this.pageSizes.findIndex(value => value === this.userPreferences.taskViewDefaultPageSize);
    this.requestTenants();

    this.requestDesks().subscribe(() => {
      this.redirectToOnlyDeskIfNeeded();

      this.websocketService.watchForDeskCountChanges(this.desks.map(data => data.desk))
        .subscribe(observables => {
          this.folderCountsSubscriptions = observables.map(observable => observable.subscribe(
            newDeskCount => {
              const index = this.desks.findIndex(existingDeskCount => existingDeskCount.desk?.id === newDeskCount.deskId);
              if (index === -1) {
                return;
              }
              console.debug(`Folder count updated, tenant : ${newDeskCount.tenantId}, desk : ${newDeskCount.deskId}`);
              this.desks[index].count = newDeskCount;
            }
          ));
        });
    }).add(() => this.loading = false);
  }


  // </editor-fold desc="LifeCycle">


  changeDisplayMode(): void {
    if (this.displayMode === DisplayModeEnum.LIST) {
      this.displayMode = DisplayModeEnum.THUMBS;
    } else {
      this.displayMode = DisplayModeEnum.LIST;
    }
  }


  forceDisplayMode() {
    this.showOnlyFavorites = false;
    if (this.userPreferences.favoriteDesks.length > 0 && this.total > this.THUMB_MODE_PAGE_SIZE && !this.currentSearchTerm) {
      this.showOnlyFavorites = true;
    } else if (this.total > this.THUMB_MODE_PAGE_SIZE && !this.currentSearchTerm) {
      this.disableDisplayMode = true;
      this.displayMode = DisplayModeEnum.LIST;
    }
  }


  /**
   * Re-fetching the desk list, updating the UI.
   */
  requestDesks(): Observable<[PageDeskRepresentation, PageDeskCount]> {
    const page = !!this.currentSearchTerm ? 0 : this.page - 1;
    const pageSize = (!!this.currentSearchTerm && this.displayMode === DisplayModeEnum.THUMBS) ? this.THUMB_MODE_PAGE_SIZE : this.LIST_MODE_PAGE_SIZE;


    // We have to request all desks (max is 250.)
    // So we can force the style to LIST if there is more than 15 results
    // Or retrieve all the favorites if we are in THUMB mode
    // page size is only used after the request in LIST mode
    this.loading = true;

    return combineLatest([
      this.currentUserService.getDesks(this.currentSearchTerm, page, pageSize),
      this.currentUserService.getDesksFolderCount(this.currentSearchTerm, page, pageSize)
    ]).pipe(
      tap((result: [PageDeskRepresentation, PageDeskCount]) => {
        const desksRetrieved: PageDeskRepresentation = result[0];
        const deskListWithCount: PageDeskCount = result[1];

        this.desks = desksRetrieved.content.map(desk => {
          return {desk: desk, count: null};
        });
        this.total = desksRetrieved.totalElements;

        this.desks.forEach(deskWithCount => {
          const correspondingDeskCount = deskListWithCount.content.find(deskCount => deskWithCount.desk.id === deskCount.deskId);
          if (!!correspondingDeskCount) {
            deskWithCount.count = correspondingDeskCount;
          }
        });

        this.forceDisplayMode();
        this.setDeskOrder();
        this.buildFlattenTreeStructure();
        this.setChunkedThumbsDesks();
      }),
      catchError(this.notificationService.handleHttpError('getDesks'))
    );
  }


  requestTenants() {
    this.tenantService
      .listTenantsForUser(0, 10000, [TenantSortBy.Name + ',ASC'], false)
      .subscribe(tenantsRetrieved => {
        this.tenants = tenantsRetrieved.content;
        this.tenants = this.sortTenants();
        this.buildFlattenTreeStructure();
      });
  }


  redirectToOnlyDeskIfNeeded(): void {
    if (this.desks.length !== 1) {
      return;
    }

    setTimeout(
      () => {
        const deskData = this.desks[0];
        from(this.router.navigate([`/tenant/${deskData.desk.tenantId}/desk/${deskData.desk.id}/${this.getDefaultFilterRoutePath()}`]))
          .subscribe({
            next: () => console.log(`Redirected to desk ${deskData.desk.id}`),
            error: () => console.log(`Error redirecting to desk ${deskData.desk.id}`)
          });
      },
      500
    );

  }


  buildFlattenTreeStructure() {

    if (!this.desks || this.desks.length === 0) {
      this.flattenTreeStructure = null;
      this.pageSize = 0;
      return;
    }

    if (!this.tenants || this.tenants.length === 0) {
      this.flattenTreeStructureUnfiltered = [{tenant: null, deskWithCount: null, isDesk: false, expanded: true}];
      this.flattenTreeStructureUnfiltered.concat(this.desks.map(d => {
        return {tenant: null, deskWithCount: d, isDesk: true, expanded: true};
      }));
      return;
    }

    const desksWithoutTenant: DeskRepresentation[] = this.desks
      .filter(deskWithCount => !deskWithCount.desk.tenantId)
      .map(deskWithCount => deskWithCount.desk);
    if (desksWithoutTenant.length > 0) {
      console.warn('Desks without matched tenant : ', desksWithoutTenant);
    }
    const newTree: FlatTreeNode[] = [];
    this.tenants.forEach(tenant => {

      const tenantDesks: FlatTreeNode[] = this.desks
        .filter(deskWithCount => deskWithCount.desk.tenantId === tenant.id)
        .sort((a, b) => this.sortByName(a.desk.name, b.desk.name))
        .map(deskWithCount => {
          return {tenant: null, deskWithCount: deskWithCount, isDesk: true, expanded: true};
        });

      if (tenantDesks.length > 0) {
        newTree.push({tenant: tenant, deskWithCount: null, isDesk: false, expanded: true});
        newTree.push(...tenantDesks);
      }
    });

    this.total = newTree.length;

    this.flattenTreeStructureUnfiltered = newTree;
    this.updatePage();
  }


  getPageSize(pageIndex: number) {
    return this.pageSizes[pageIndex - 1];
  }


  setDeskOrder() {
    const favoriteDeskInOrder = [];

    this.userPreferences.favoriteDesks.forEach(desk => {
      const deskIndex = this.desks.findIndex(deskWithCount => desk.id === deskWithCount.desk.id);
      if (deskIndex !== -1) {
        favoriteDeskInOrder.push(this.desks[deskIndex]);
      }
    });
    this.favoriteDesks = favoriteDeskInOrder;

    this.desks = this.favoriteDesks.concat(
      this.desks.filter(deskWithCount => !this.userPreferences.favoriteDesks.some(dd => dd.id === deskWithCount.desk.id))
    );
  }


  sortTenants(): TenantRepresentation[] {
    return this.tenants.sort((a, b) => this.sortByName(a.name, b.name));
  }


  sort() {
    this.asc = !this.asc;

    this.tenants = this.sortTenants();
    this.buildFlattenTreeStructure();
  }


  sortByName(objectAName: string, objectBName: string): number {
    return this.asc
      ? ((objectAName.toUpperCase() < objectBName.toUpperCase()) ? -1 : (objectAName.toUpperCase() > objectBName.toUpperCase()) ? 1 : 0)
      : ((objectBName.toUpperCase() < objectAName.toUpperCase()) ? -1 : (objectBName.toUpperCase() > objectAName.toUpperCase()) ? 1 : 0);
  }


  updateSearchTerm(newTerm: string) {
    if (newTerm?.length === 0) {
      newTerm = null;
    }
    this.currentSearchTerm = newTerm;
    this.page = 1;

    this.requestDesks().subscribe().add(() => this.loading = false);
  }


  computeTotal(): number {
    return this.flattenTreeStructureUnfiltered.filter(node => !node.isDesk || (node.isDesk && node.expanded)).length;
  }

  setChunkedThumbsDesks() {
    const desks: DeskWithCount[] = this.showOnlyFavorites
      ? this.favoriteDesks
      : this.desks;

    this.chunkDesks = new ChunkPipe<DeskWithCount>().transform(desks, 5);
  }


  updatePage() {
    this.pageSize = this.pageSizes[this.pageSizeIndex - 1];

    const startIndex: number = this.pageSize * (this.page - 1);
    const endIndex = startIndex + this.pageSize;

    this.flattenTreeStructure = this.flattenTreeStructureUnfiltered.filter(node => !node.isDesk || (node.isDesk && node.expanded)).slice(startIndex, endIndex);
  }


  navigateToDesk(elem: any): void {
    if (elem.deskWithCount?.desk) {
      const tenantId = elem.deskWithCount.desk.tenantId;
      const deskId = elem.deskWithCount.desk.id;
      const filterPath = this.getDefaultFilterRoutePath();
      this.router.navigate([`/tenant/${tenantId}/desk/${deskId}/${filterPath}`]);
    }
  }
  expandOrCollapse(tenantNode: FlatTreeNode) {
    if (tenantNode.isDesk) {
      return;
    }

    tenantNode.expanded = !tenantNode.expanded;

    // fold/unfold "by hand" all the desks nodes, as do not use a tree struct anymore
    const elemIdx = this.flattenTreeStructureUnfiltered.findIndex(elem => elem.tenant?.id === tenantNode.tenant?.id);
    for (let i = elemIdx + 1 ; i < this.flattenTreeStructureUnfiltered.length ; ++i) {
      const node = this.flattenTreeStructureUnfiltered[i];
      if (node.isDesk) {
        node.expanded = tenantNode.expanded;
      } else {
        break;
      }
    }
    this.updatePage();
  }


  getDefaultFilterRoutePath(): string {
    const targetState = this.filterService.getDefaultFilter()?.state ?? State.Pending;
    return targetState.toLowerCase();
  }
}
