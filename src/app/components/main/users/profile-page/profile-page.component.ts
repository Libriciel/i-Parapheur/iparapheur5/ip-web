/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { UserEditSubPanelsMessages } from '../user-edit-page-subpanels/user-edit-sub-panels-messages';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { GLOBAL_SETTINGS, GlobalSettings } from '../../../../shared/models/global-settings';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { LegacyUserService } from '../../../../services/ip-core/legacy-user.service';
import { CommonMessages } from '../../../../shared/common-messages';
import { UserDto } from '@libriciel/iparapheur-provisioning';
import { CONFIG } from '../../../../shared/config/config';


@Component({
  selector: 'app-profile-popup',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {

  readonly messages = UserEditSubPanelsMessages;
  readonly commonMessages = CommonMessages;
  readonly globalSettings: GlobalSettings = GLOBAL_SETTINGS;
  readonly show51features: boolean = !CONFIG?.VERSION?.startsWith('5.0.');

  currentUser: UserDto;
  userPreferences: UserPreferencesDto = {} as UserPreferencesDto;
  isTenantAdminOrMore: boolean = false;

  activeId: string;

  profileMenuRows: { label: string; route: string; displayed: boolean }[] = [];


  constructor(private legacyUserService: LegacyUserService,
              private route: ActivatedRoute,
              private router: Router) { }


  ngOnInit(): void {
    this.currentUser = this.route.snapshot.data["currentUser"];
    this.isTenantAdminOrMore = this.legacyUserService.isCurrentUserTenantAdminOfOneTenant()
      || this.legacyUserService.isCurrentUserSuperAdmin();

    this.setProfileMenuRows();

    this.syncActiveTab();
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.syncActiveTab();
      }
    });

  }


  private syncActiveTab(): void {
    const activeRoute = this.router.url;
    const activeMenu = this.profileMenuRows.find(menu => activeRoute.startsWith(menu.route));
    if (activeMenu) {
      this.activeId = activeMenu.label;
    }
  }


  private setProfileMenuRows(): void {
    this.profileMenuRows = [
    {route: '/profile/password', label: this.commonMessages.PASSWORD, displayed: this.globalSettings.user.showPassword},
    {route: '/profile/signature', label: this.commonMessages.PROFILE_SIGNATURE, displayed: this.globalSettings.user.showSignature},
    {route: '/profile/notifications', label: this.commonMessages.PROFILE_NOTIFICATION, displayed: true},
    {route: '/profile/folder-view-preferences', label: this.commonMessages.PROFILE_FOLDER_VIEW_PREFERENCES, displayed: true},
    {route: '/profile/favorite-desks', label: this.commonMessages.PROFILE_FAVORITE_DESKS, displayed: true},
    {route: '/profile/table-layout-editor', label: this.commonMessages.PROFILE_DASHBOARD, displayed: true},
    {route: '/profile/absences', label: this.commonMessages.PROFILE_ABSENCES, displayed: true},
    {route: '/profile/trash-bin', label: this.commonMessages.PROFILE_TRASH_BIN, displayed: this.isTenantAdminOrMore},
    {route: '/profile/debugging', label: this.commonMessages.PROFILE_DEBUG, displayed: true}
  ];
  }
}
