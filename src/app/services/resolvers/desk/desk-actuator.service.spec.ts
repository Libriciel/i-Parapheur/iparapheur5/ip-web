/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { TestBed } from '@angular/core/testing';

import { DeskActuatorService } from './desk-actuator.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { of } from 'rxjs';
import { EnvironmentInjector } from '@angular/core';

describe('DeskActuatorService', () => {

  const mockRoute: ActivatedRouteSnapshot = {params: {id: 100}} as unknown as ActivatedRouteSnapshot;

  describe('DeskActuatorService', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ToastrModule.forRoot()],
        providers: [
          HttpClient, HttpHandler,
          {provide: DeskActuatorService, useValue: of(true)},
        ],
      });
    });

    it('should return the requested value', () => {
      const result = TestBed.inject(EnvironmentInjector).runInContext(() =>
        DeskActuatorService(null, mockRoute, {} as RouterStateSnapshot, {} as RouterStateSnapshot)
      ) as boolean;
      expect(result).toBeTrue();
    });

  });

});
