/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FolderUtils } from '../../utils/folder-utils';

@Injectable({
  providedIn: 'root'
})
export class DeskService {

  readonly asDeskParam = FolderUtils.AS_DESK_QUERY_PARAM_NAME;

  constructor(public http: HttpClient) {
  }


  getUrlToViewFolderAsDesk(tenantId: string, deskId: string, folderId: string, asDeskId: string): string {
    return `/tenant/${tenantId}/desk/${deskId}/folder/${folderId}?${this.asDeskParam}=${asDeskId}`;
  }

  public getAsDeskQueryParam(targetDeskId: string): {[k: string]: string} {
    const queryParam = {};
    queryParam[this.asDeskParam] = targetDeskId;
    return queryParam;
  }

  getFolderViewUrl(tenantId: string, deskId: string, folderId: string): string {
    return `/tenant/${tenantId}/desk/${deskId}/folder/${folderId}`;
  }


}
