/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Metadata } from '../../../../models/metadata';
import { Folder } from '../../../../models/folder/folder';
import { ActionPopupMessages } from '../action-popups/action-popup-messages';
import { forkJoin, Observable } from 'rxjs';
import { CrudOperation } from '../../../../services/crud-operation';
import { NotificationsService } from '../../../../services/notifications.service';
import { Action, MetadataService } from '@libriciel/iparapheur-legacy';
import { catchError, map } from 'rxjs/operators';
import { SecondaryAction } from '../../../../shared/models/secondary-action.enum';

@Component({
  selector: 'app-metadata-form',
  templateUrl: './metadata-form.component.html',
  styleUrls: ['./metadata-form.component.scss']
})
export class MetadataFormComponent implements OnInit {

  public static readonly MANDATORY_VALIDATION_KEY: string = 'mandatoryValidationMetadata';
  public static readonly MANDATORY_REJECTION_KEY: string = 'mandatoryRejectionMetadata';
  public static readonly EXCLUDED_ACTIONS: (Action | SecondaryAction)[] = [
    Action.SecondOpinion, Action.AskSecondOpinion, Action.Transfer, Action.Delete, Action.Bypass, Action.Chain, Action.Recycle, Action.Undo,
    SecondaryAction.AddDesksToNotify, SecondaryAction.Download, SecondaryAction.Mail, SecondaryAction.HistoryTasks
  ];


  readonly actionPopupMessages = ActionPopupMessages;

  @Input() tenantId: string;
  @Input() performedAction: Action | SecondaryAction;

  @Input() folders: Folder[];
  @Output() foldersChange = new EventEmitter<Folder[]>();

  @Input() validationMetadataFormValid?: boolean = false;
  @Output() validationMetadataFormValidChange = new EventEmitter<boolean>();

  @Input() rejectionMetadataFormValid?: boolean = false;
  @Output() rejectionMetadataFormValidChange = new EventEmitter<boolean>();

  stepMetadata: Metadata[] = [];
  countValidationMetadata: number = 0;
  countRejectionMetadata: number = 0;


  constructor(public notificationsService: NotificationsService,
              private metadataService: MetadataService) {}


  ngOnInit(): void {
    if (MetadataFormComponent.EXCLUDED_ACTIONS.includes(this.performedAction)) {
      this.validationMetadataFormValidChange.emit(true);
      return;
    }

    this.parseMetadata();
  }


  setValidationMetadataFormValid(event: boolean): void {
    this.validationMetadataFormValid = event;
    this.setFoldersChange(this.folders);
    this.validationMetadataFormValidChange.emit(event);
  }


  setRejectionMetadataFormValid(event: boolean): void {
    this.rejectionMetadataFormValid = event;
    this.setFoldersChange(this.folders);
    this.rejectionMetadataFormValidChange.emit(event);
  }


  setFoldersChange(folders: Folder[]): void {
    this.folders = folders;
    this.parseMetadataMap();
    this.foldersChange.emit(folders);
  }


  private parseMetadata(): void {
    const validationMandatoryMetadata: Observable<Metadata> [] = this.setParsedMetadata(MetadataFormComponent.MANDATORY_VALIDATION_KEY);
    const rejectionMandatoryMetadata: Observable<Metadata> [] = this.setParsedMetadata(MetadataFormComponent.MANDATORY_REJECTION_KEY);

    if ((this.performedAction !== Action.Reject && validationMandatoryMetadata.length === 0)
      || (this.performedAction === Action.Reject && rejectionMandatoryMetadata.length === 0)) {
      this.setCountMetadata();
      return;
    }


    forkJoin([
      ...validationMandatoryMetadata,
      ...rejectionMandatoryMetadata
    ]).subscribe({
      next: (result: Metadata[]) => {
        const distinctMetadata: Metadata[] = [];
        result.forEach((meta: Metadata) => {
          if (!distinctMetadata.map(meta => meta.id).includes(meta.id)) {
            distinctMetadata.push(meta);
          }
        });
        this.stepMetadata = distinctMetadata;
        this.setCountMetadata();
      },
      error: e => this.notificationsService.showCrudMessage(CrudOperation.Read, e)
    });
  }


  private setParsedMetadata(metadataKey: string): Observable<Metadata>[] {
    if (!this.folders) {
      return [];
    }

    const metadataObservables: Observable<Metadata>[] = [];
    const requestedMetadataId: string[] = [];

    this.folders
      .forEach(folder => {
        folder
          .stepList
          .forEach(step => {
            if (!step[metadataKey]) return;

            step[metadataKey]
              .filter(id => !requestedMetadataId.includes(id))
              .forEach(id => {
                requestedMetadataId.push(id);
                metadataObservables.push(
                  this.metadataService
                    .getMetadata(this.tenantId, id)
                    .pipe(
                      map(metadataDto => Object.assign(new Metadata(), metadataDto)),
                      catchError(this.notificationsService.handleHttpError('getMetadata'))
                    )
                );
              });
          });
      });

    return metadataObservables;
  }


  private setCountMetadata(): void {
    const countValidationMetadata = this.countMetadata(MetadataFormComponent.MANDATORY_VALIDATION_KEY);
    this.countValidationMetadata = this.performedAction !== Action.Reject ? countValidationMetadata : 0;
    this.validationMetadataFormValid = this.countValidationMetadata === 0;
    this.setValidationMetadataFormValid(this.validationMetadataFormValid);

    const countRejectionMetadata = this.countMetadata(MetadataFormComponent.MANDATORY_REJECTION_KEY);
    this.countRejectionMetadata = this.performedAction === Action.Reject ? countRejectionMetadata : 0;
    this.rejectionMetadataFormValid = this.countRejectionMetadata === 0;
    this.setRejectionMetadataFormValid(this.rejectionMetadataFormValid);
  }


  private countMetadata(metadataKey: string): number {
    const ids: string[] = [];
    this.folders
      .forEach(folder => {
        folder
          .stepList
          .filter(step => !!step[metadataKey])
          .forEach(step => {
            const metadataIds: string[] = step[metadataKey]
              .filter(id => !ids.includes(id));
            ids.push(...metadataIds);
          });
      });
    return ids.length;
  }


  private parseMetadataMap(): void {
    this.folders.forEach(folder => {
      const result: { [p: string]: string } = {};
      for (const key in folder.metadata) {
        if (this.stepMetadata.filter(meta => meta.key === key).length > 0) {
          result[key] = folder.metadata[key];
        }
      }
      folder.populatedMandatoryStepMetadata = result;
    });
  }


}
