/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { RouterStateSnapshot, ActivatedRouteSnapshot, ResolveFn } from '@angular/router';
import { Observable, of } from 'rxjs';
import { SelectedDeskService } from './selected-desk.service';
import { tap, catchError } from 'rxjs/operators';
import { CommonMessages } from '../../../shared/common-messages';
import { DeskService, DeskDto } from '@libriciel/iparapheur-legacy';
import { inject } from '@angular/core';

export const DeskResolver: ResolveFn<DeskDto> = (
  route: ActivatedRouteSnapshot,
  _state: RouterStateSnapshot,
  deskService: DeskService = inject(DeskService),
  selectedDeskService: SelectedDeskService = inject(SelectedDeskService),
): Observable<DeskDto> => {
  const tenantId = route?.paramMap?.get('tenantId');
  const deskId = route?.paramMap?.get('deskId');

  if (!deskId || !tenantId) {
    return of({
      id: deskId,
      name: CommonMessages.UNREACHABLE_DESK,
      shortName: CommonMessages.UNREACHABLE_DESK,
      ownerIds: [],
      associatedDeskIds: [],
      filterableMetadataIds: [],
      supervisorIds: [],
      delegationManagerIds: []
    });
  }

  return deskService
    .getDesk(tenantId, deskId)
    .pipe(
      tap(data => selectedDeskService.update(data)),
      catchError(() => of({
        id: deskId,
        name: CommonMessages.UNREACHABLE_DESK,
        shortName: CommonMessages.UNREACHABLE_DESK,
        ownerIds: [],
        associatedDeskIds: [],
        filterableMetadataIds: [],
        supervisorIds: [],
        delegationManagerIds: []
      }))
    );
};
