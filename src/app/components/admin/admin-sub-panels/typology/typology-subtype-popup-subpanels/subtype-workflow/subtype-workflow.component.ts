/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { SecureMailServer } from '../../../../../../models/securemail/secure-mail-server';
import { AdminSecureMailService } from '../../../../../../services/ip-core/admin-secure-mail.service';
import { CommonMessages } from '../../../../../../shared/common-messages';
import { AdminTypologyMessages } from '../../admin-typology-messages';
import { isNotEmpty, compareById, getNbCharConsummedText } from '../../../../../../utils/string-utils';
import { NotificationsService } from '../../../../../../services/notifications.service';
import { CrudOperation } from '../../../../../../services/crud-operation';
import { CommonIcons } from '@libriciel/ls-elements';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { UntypedFormGroup, UntypedFormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { FormUtils } from '../../../../../../utils/form-utils';
import { SignatureFormat, Action, SignatureProtocol, ExternalSignatureConfigDto, AdminExternalSignatureService, ExternalSignatureConfigSortBy, DeskRepresentation, TypeDto, SubtypeDto } from '@libriciel/iparapheur-legacy';
import { AdminWorkflowDefinitionService as ProvisioningAdminWorkflowDefinitionService, AdminSealCertificateService, SealCertificateSortBy, WorkflowDefinitionSortBy, SealCertificateRepresentation, AdminWorkflowDefinitionService, WorkflowDefinitionDto } from '@libriciel/iparapheur-provisioning';
import { WorkflowDefinition } from '@libriciel/ls-workflow';
import { WorkflowUtils } from '../../../../../../shared/utils/workflow-utils';


@Component({
  selector: 'app-subtype-workflow',
  templateUrl: './subtype-workflow.component.html',
  styleUrls: ['./subtype-workflow.component.scss']
})
export class SubtypeWorkflowComponent implements OnInit {

  readonly commonMessages = CommonMessages;
  readonly messages = AdminTypologyMessages;
  readonly compareByIdFn = compareById;
  readonly commonIcons = CommonIcons;
  readonly infoIcon = faInfoCircle;
  readonly actionEnum = Action;
  readonly searchPageSize = 50;
  readonly dummyOriginDesk: DeskRepresentation = {id: WorkflowUtils.GENERIC_DESK_IDS.EMITTER_ID, name: WorkflowUtils.GENERIC_DESK_NAMES.EMITTER};
  readonly protocolEnum = SignatureProtocol;
  readonly maxSelectionScriptLength: number = 8192;

  @Output() valid = new EventEmitter<boolean>();
  @Input() tenantId: string;
  @Input() type: TypeDto;
  @Input() subtype: SubtypeDto = null;

  validationWorkflowDefinition: WorkflowDefinitionDto;
  creationWorkflowDefinition: WorkflowDefinitionDto;
  availableExternalSignatureConfigurations: ExternalSignatureConfigDto[];
  availableSecureMailServers: SecureMailServer[];
  sealCertificates: SealCertificateRepresentation[] = [];
  availableWorkflows: any[];

  workflowForm: UntypedFormGroup = new UntypedFormGroup({
    selectCreationWorkflow: new UntypedFormControl(this.subtype?.creationWorkflowId, this.creationWorkflowValidator()),
    selectValidationWorkflow: new UntypedFormControl(this.subtype?.validationWorkflowId, this.validationWorkflowValidator()),
    selectSealCertificate: new UntypedFormControl(this.subtype?.sealCertificate, this.actionConfigurationValidator(Action.Seal)),
    selectExternalSignatureConfig: new UntypedFormControl(this.subtype?.externalSignatureConfigId, this.actionConfigurationValidator(Action.ExternalSignature)),
    selectSecureMailServer: new UntypedFormControl(this.subtype?.secureMailServerId, this.actionConfigurationValidator(Action.SecureMail)),
    isExternalSignatureAutomatic: new UntypedFormControl(),
    isDigitalSignatureMandatory: new UntypedFormControl(),
    isSealAutomatic: new UntypedFormControl(),
  });


  // <editor-fold desc="LifeCycle">


  constructor(private adminExternalSignatureService: AdminExternalSignatureService,
              private adminSealCertificateService: AdminSealCertificateService,
              private adminSecureMailService: AdminSecureMailService,
              private adminWorkflowDefinitionService: AdminWorkflowDefinitionService,
              private provisioningAdminWorkflowDefinitionService: ProvisioningAdminWorkflowDefinitionService,
              private notificationsService: NotificationsService) {

    this.workflowForm.valueChanges.subscribe(() => this.valid.emit(this.workflowForm.valid));
  }


  ngOnInit(): void {
    this.sealCertificates = !!this.subtype.sealCertificate ? [this.subtype.sealCertificate] : [];
    this.requestWorkflows();
    this.requestExternalSignatureConfigs(0, null);
    this.requestSealCertificates(0);
    this.requestSecureMailServers(0, null);
    this.updateCurrentValidationWorkflowSteps(this.subtype.validationWorkflowId);
    this.updateCurrentCreationWorkflowSteps(this.subtype.creationWorkflowId);
  }


  // </editor-fold desc="LifeCycle">


  getWorkflowCreationTooltip(): string {

    if (!this.validationWorkflowDefinition) {
      return AdminTypologyMessages.CHOOSE_A_VALIDATION_WORKFLOW_FIRST;
    }

    return FormUtils.getFirstErrorKey(this.workflowForm, 'selectCreationWorkflow');
  }


  getWorkflowValidationTooltip(): string {
    return FormUtils.getFirstErrorKey(this.workflowForm, 'selectValidationWorkflow');
  }


  updateAvailableWorkflowsList(searchEvt: { term: string, items: any[] }) {
    this.requestWorkflows(0, searchEvt.term);
  }


  updateCurrentCreationWorkflowSteps(workflowId: string) {
    this.creationWorkflowDefinition = null;
    this.workflowForm.controls['selectCreationWorkflow'].updateValueAndValidity();
    if (!workflowId) {
      return;
    }

    this.adminWorkflowDefinitionService
      .getWorkflowDefinition(this.tenantId, workflowId)
      .pipe(catchError(this.notificationsService.handleHttpError('getWorkflowDefinition')))
      .subscribe({
        next: workflow => {
          this.creationWorkflowDefinition = workflow;
          FormUtils.updateValueAndValidity(this.workflowForm);
        },
        error: e => this.notificationsService.showErrorMessage(this.messages.ERROR_RETRIEVING_STEPS, e.message)
      });
  }


  updateCurrentValidationWorkflowSteps(workflowId: string) {
    this.validationWorkflowDefinition = null;

    if (!workflowId) {

      // Special case : If we remove the validation workflow,
      // there is no creation one neither
      this.creationWorkflowDefinition = null;
      this.subtype.creationWorkflowId = null;

      // Back to the selection script, nothing else to do
      return;
    }

    this.adminWorkflowDefinitionService
      .getWorkflowDefinition(this.tenantId, workflowId)
      .pipe(catchError(this.notificationsService.handleHttpError('getWorkflowDefinition')))
      .subscribe({
        next: workflow => {
          this.validationWorkflowDefinition = workflow;
          FormUtils.updateValueAndValidity(this.workflowForm);
        },
        error: e => this.notificationsService.showErrorMessage(this.messages.ERROR_RETRIEVING_STEPS, e.message)
      });
  }


  requestWorkflows(page: number = 0, searchTerm?: string) {
    this.provisioningAdminWorkflowDefinitionService
      .listWorkflowDefinitions(this.tenantId, page, this.searchPageSize, [WorkflowDefinitionSortBy.Name + ',ASC'], searchTerm)
      .pipe(catchError(this.notificationsService.handleHttpError('listWorkflowDefinitions')))
      .subscribe(paginatedResult => {
        this.availableWorkflows = paginatedResult.content.map(dto => Object.assign(new WorkflowDefinition(), dto));

        if (this.creationWorkflowDefinition != null) {
          if (!this.availableWorkflows.some(item => item.key === this.creationWorkflowDefinition.key)) {
            // We must create a new array object for the change detection to trigger on the selector component
            const newList = [...this.availableWorkflows, {...this.creationWorkflowDefinition}];
            this.availableWorkflows = newList;
          }
        }
        if (this.validationWorkflowDefinition != null) {
          if (!this.availableWorkflows.some(item => item.key === this.validationWorkflowDefinition.key)) {
            // We must create a new array object for the change detection to trigger on the selector component
            const newList = [...this.availableWorkflows, {...this.validationWorkflowDefinition}];
            this.availableWorkflows = newList;
          }
        }
      });
  }


  requestExternalSignatureConfigs(page: number, searchTerm: string) {
    this.adminExternalSignatureService
      .listExternalSignatureConfigs(this.tenantId, page, this.searchPageSize, [ExternalSignatureConfigSortBy.Name.toLowerCase() + ',ASC'], searchTerm)
      .subscribe(configs => this.availableExternalSignatureConfigurations = configs.content);
  }


  requestSecureMailServers(page: number, searchTerm: string) {
    this.adminSecureMailService
      .findSecureMailServersByPage(this.tenantId, page, this.searchPageSize, searchTerm)
      .subscribe({
        next: servers => this.availableSecureMailServers = servers.data,
        error: e => this.notificationsService.showCrudMessage(CrudOperation.Read, SecureMailServer, e.message, false)
      });
  }


  requestSealCertificates(page: number) {
    this.adminSealCertificateService
      .listSealCertificate(this.tenantId, page, this.searchPageSize, [SealCertificateSortBy.Name + ',ASC'])
      .subscribe(configs => this.sealCertificates = configs.content);
  }


  updateAvailableExternalSignatureConfigsList(searchEvt: { term: string, items: any[] }) {
    this.requestExternalSignatureConfigs(0, searchEvt.term);
  }


  updateAvailableSealCertificates(searchEvt: { term: string, items: any[] }) {
    this.requestExternalSignatureConfigs(0, searchEvt.term);
  }


  updateAvailableSecureMailServerList(searchEvt: { term: string, items: any[] }) {
    this.requestSecureMailServers(0, searchEvt.term);
  }


  /**
   * Special case: when nothing's selected, we're in a selection-script mode.
   * We can't determine anything. So we'll assume that every action may be triggered.
   * @param workflowDefinition workflow to be tested
   * @param action availability to test
   */
  doesWorkflowDefinitionContainsAction(workflowDefinition: WorkflowDefinitionDto, action: Action): boolean {
    return !(workflowDefinition?.steps)
      ? true
      : workflowDefinition.steps
        .some(s => s.type == action);
  }


  updateSelectionScript(evt) {
    this.subtype.workflowSelectionScript = evt;
    FormUtils.updateValueAndValidity(this.workflowForm);
  }


  actionConfigurationValidator(action: Action): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {

      if (!this.validationWorkflowDefinition) {
        return null;
      }

      const noAction = !this.doesWorkflowDefinitionContainsAction(this.validationWorkflowDefinition, action);
      const isConfProperlySet = !!control.value;
      const isValid = noAction || isConfProperlySet;

      // console.log('actionValidator ' + action + ' noAction:' + noAction);
      // console.log('actionValidator ' + action + ' isConfProperlySet:' + isConfProperlySet);
      // console.log('actionValidator ' + action + ' isValid:' + isValid)

      if (isValid) {
        return null;
      }

      const errors = {};
      if (!isConfProperlySet) {
        errors['extSigConfNotProperlySet:'] = true;
      }

      return errors;
    };
  }


  creationWorkflowValidator(): ValidatorFn {
    return (_: AbstractControl): { [key: string]: any } | null => {

      if (!this.creationWorkflowDefinition?.steps) {
        return null;
      }

      const onlyVisa: boolean = this.creationWorkflowDefinition.steps
        .filter(a => a.type !== Action.Start)
        .filter(a => a.type !== Action.Archive)
        .every(a => a.type === Action.Visa);

      const onlySingleSteps: boolean = this.creationWorkflowDefinition.steps
        .every(s => s.validatingDesks.length === 1);

      const atLeastOneVariableStep: boolean = this.creationWorkflowDefinition.steps
        .some(s => s.validatingDesks.some(vd => vd.id === WorkflowUtils.GENERIC_DESK_IDS.VARIABLE_DESK_ID));


      // Kinda hacky, we use the error messages directly as keys.
      // TODO : have some kind of key/message map somewhere ?

      const errors = {};

      if (atLeastOneVariableStep) {
        errors[AdminTypologyMessages.CREATION_WORKFLOW_CANNOT_CONTAIN_VARIABLE_DESKS] = true;
      }

      if (!onlyVisa) {
        errors[AdminTypologyMessages.CREATION_WORKFLOW_CAN_ONLY_CONTAIN_VISAS] = true;
      }

      if (!onlySingleSteps) {
        errors[AdminTypologyMessages.CREATION_WORKFLOW_CAN_ONLY_CONTAIN_SINGLE_STEPS] = true;
      }

      return errors;
    };
  }


  validationWorkflowValidator(): ValidatorFn {
    return (_: AbstractControl): { [key: string]: any } | null => {

      const errors = {};
      if (!this.validationWorkflowDefinition?.steps) {
        if (isNotEmpty(this.subtype?.workflowSelectionScript)) {
          return null;
        } else {
          errors[AdminTypologyMessages.MUST_SELECT_VALIDATION_WORKFLOW_OR_FILL_SELECTION_SCRIPT] = true;
          return errors;
        }
      }

      const containsSeal = this.doesWorkflowDefinitionContainsAction(this.validationWorkflowDefinition, Action.Seal);
      const containsSecureMail = this.doesWorkflowDefinitionContainsAction(this.validationWorkflowDefinition, Action.SecureMail);
      const containsExternalSignature = this.doesWorkflowDefinitionContainsAction(this.validationWorkflowDefinition, Action.ExternalSignature);
      const isPadesType = (this.type?.signatureFormat === SignatureFormat.Pades) || (this.type?.signatureFormat === SignatureFormat.Auto);

      // console.log('validationWorkflowValidator containsSeal:' + containsSeal);
      // console.log('validationWorkflowValidator containsSecureMail:' + containsSecureMail);
      // console.log('validationWorkflowValidator containsExternalSignature:' + containsExternalSignature);
      // console.log('validationWorkflowValidator isPadesType:' + isPadesType);

      // Kinda hacky, we use the error messages directly as keys.
      // TODO : have some kind of key/message map somewhere ?

      if (isPadesType) {
        return null;
      }

      if (containsSeal) {
        errors[AdminTypologyMessages.WORKFLOW_TYPE_CANNOT_CONTAIN_SEAL] = true;
      }

      if (containsSecureMail) {
        errors[AdminTypologyMessages.WORKFLOW_TYPE_CANNOT_CONTAIN_SECURE_MAIL] = true;
      }

      if (containsExternalSignature) {
        errors[AdminTypologyMessages.WORKFLOW_TYPE_CANNOT_CONTAIN_EXTERNAL_SIGNATURE] = true;
      }

      return errors;
    };
  }

  getNbCharInScriptLabel(value: string): string {
    return getNbCharConsummedText(value, this.maxSelectionScriptLength);
  }

}
