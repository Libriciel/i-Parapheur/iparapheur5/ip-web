/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserEditNotificationsComponent } from './user-edit-notifications.component';
import { UserPreferencesDto } from '@libriciel/iparapheur-legacy';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ToastrModule } from 'ngx-toastr';
import { ProfileUpdateService } from '../../../../../shared/service/profile-update.service';
import { SelectedUserPreferencesService } from '../../../../../services/selected-user-preferences.service';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { UserDto } from '@libriciel/iparapheur-provisioning';


describe('UserEditNotificationsComponent', () => {


  let component: UserEditNotificationsComponent;
  let fixture: ComponentFixture<UserEditNotificationsComponent>;

  const mockSelectedUserPreferencesService = {
    currentUserPreferences$: of({  }),
  };

  const mockActivatedRoute = {
    parent: {
      snapshot: {
        data: {
          currentUser: {
            email: 'test@example.com',
          },
        },
      },
    },
  };

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        declarations: [UserEditNotificationsComponent],
        imports: [
          HttpClientTestingModule,
          ToastrModule.forRoot()
        ],
        providers: [ ProfileUpdateService,
          { provide: SelectedUserPreferencesService, useValue: mockSelectedUserPreferencesService },
          {provide: ActivatedRoute, useValue: mockActivatedRoute}
  ]
      })
      .compileComponents();
  });


  beforeEach(() => {

    fixture = TestBed.createComponent(UserEditNotificationsComponent);
    component = fixture.componentInstance;

    const testUser = { email: 'test@example.com' } as UserDto;
    const testUserPreferences = {} as UserPreferencesDto;
    component.currentUser = testUser;
    component.userPreferences = testUserPreferences;

    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
