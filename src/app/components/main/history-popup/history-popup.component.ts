/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Task } from 'src/app/models/task';
import { Folder } from '../../../models/folder/folder';
import { FolderService } from '../../../services/ip-core/folder.service';
import { NotificationsService } from '../../../services/notifications.service';
import { CrudOperation } from '../../../services/crud-operation';
import { CommonMessages } from '../../../shared/common-messages';
import { CommonIcons } from '@libriciel/ls-elements';
import { HistoryPopupMessages } from '../history-popup-messages';
import { SecondaryAction } from '../../../shared/models/secondary-action.enum';
import { prettyPrintUser } from '../../../utils/string-utils';

@Component({
  selector: 'app-history-popup',
  templateUrl: './history-popup.component.html',
  styleUrls: ['./history-popup.component.scss']
})
export class HistoryPopupComponent implements OnInit {


  public static readonly INJECTABLE_FOLDER_KEY: string = 'folder';
  public static readonly INJECTABLE_DESK_ID_KEY: string = 'deskId';
  public static readonly INJECTABLE_TENANT_ID_KEY: string = 'tenantId';


  readonly secondaryActionEnum = SecondaryAction;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly messages = HistoryPopupMessages;
  readonly prettyPrintUserFn = prettyPrintUser;


  tasks: Task[] = [];


  // <editor-fold desc="LifeCycle">


  constructor(public activeModal: NgbActiveModal,
              private folderService: FolderService,
              private notificationsService: NotificationsService,
              @Inject(HistoryPopupComponent.INJECTABLE_FOLDER_KEY) public folder: Folder,
              @Inject(HistoryPopupComponent.INJECTABLE_DESK_ID_KEY) public deskId: string,
              @Inject(HistoryPopupComponent.INJECTABLE_TENANT_ID_KEY) public tenantId: string) { }


  ngOnInit(): void {
    this.folderService
      .getHistoryTasks(this.tenantId, this.deskId, this.folder)
      .subscribe({
        next: result => this.tasks = result.filter(task => !!task.date), // We just want the history here, not the pending ones
        error: e => this.notificationsService.showCrudMessage(CrudOperation.Read, this.folder, e.message, false)
      });
  }


  // </editor-fold desc="LifeCycle">


}
