/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { FolderUtils } from './folder-utils';
import { SignatureProtocol, Action, SubtypeDto, TypeDto } from '@libriciel/iparapheur-legacy';
import { Folder } from '../models/folder/folder';
import { Task } from '../models/task';
import { SecondaryAction } from '../shared/models/secondary-action.enum';
import { Document } from '../models/document';
import { ValidatedSignatureInformation } from '@libriciel/iparapheur-internal/model/validatedSignatureInformation';
import { DetachedSignature } from '../models/signature/detached-signature';


describe('FolderUtils', () => {


  it('should create an instance', () => {
    expect(new FolderUtils()).toBeTruthy();
  });


  it('should return an expected max count', () => {

    const typeWithNoProtocol: TypeDto = {name: "typeWithNoProtocol", protocol: SignatureProtocol.None};

    const typeWithActesProtocol: TypeDto = {name: "typeWithActesProtocol", protocol: SignatureProtocol.Actes};

    const subtypeWithMultiDocuments: SubtypeDto = {name: "subtypeWithMultiDocuments", maxMainDocuments: 15};

    const subtypeWithSingleDocument: SubtypeDto = {name: "subtypeWithMultiDocuments", maxMainDocuments: 1};

    expect(FolderUtils.getMaxDocumentsCount(undefined, undefined)).toBe(1);
    expect(FolderUtils.getMaxDocumentsCount(undefined, subtypeWithMultiDocuments)).toBe(15);
    expect(FolderUtils.getMaxDocumentsCount(undefined, subtypeWithSingleDocument)).toBe(1);

    expect(FolderUtils.getMaxDocumentsCount(typeWithNoProtocol, undefined)).toBe(1);
    expect(FolderUtils.getMaxDocumentsCount(typeWithNoProtocol, subtypeWithMultiDocuments)).toBe(15);
    expect(FolderUtils.getMaxDocumentsCount(typeWithNoProtocol, subtypeWithSingleDocument)).toBe(1);

    expect(FolderUtils.getMaxDocumentsCount(typeWithActesProtocol, undefined)).toBe(1);
    expect(FolderUtils.getMaxDocumentsCount(typeWithActesProtocol, subtypeWithSingleDocument)).toBe(1);
    expect(FolderUtils.getMaxDocumentsCount(typeWithActesProtocol, subtypeWithMultiDocuments)).toBe(1);
  });


  it('should return some detached signature presence', () => {

    expect(FolderUtils.doesContainDetachedSignature(undefined)).toBeFalse();

    const dummyDocument = new Document();

    dummyDocument.embeddedSignatureInfos = null;
    dummyDocument.detachedSignatures = null;
    expect(FolderUtils.doesContainDetachedSignature(dummyDocument)).toBeFalse();

    dummyDocument.embeddedSignatureInfos = [];
    dummyDocument.detachedSignatures = [];
    expect(FolderUtils.doesContainDetachedSignature(dummyDocument)).toBeFalse();

    dummyDocument.embeddedSignatureInfos = [{} as ValidatedSignatureInformation];
    dummyDocument.detachedSignatures = [];
    expect(FolderUtils.doesContainDetachedSignature(dummyDocument)).toBeFalse();

    dummyDocument.embeddedSignatureInfos = [];
    dummyDocument.detachedSignatures = [new DetachedSignature()];
    expect(FolderUtils.doesContainDetachedSignature(dummyDocument)).toBeTrue();

    dummyDocument.embeddedSignatureInfos = [{} as ValidatedSignatureInformation];
    dummyDocument.detachedSignatures = [new DetachedSignature()];
    expect(FolderUtils.doesContainDetachedSignature(dummyDocument)).toBeTrue();
  });


  it('should return some embedded signature presence', () => {

    expect(FolderUtils.doesContainEmbeddedSignature(undefined)).toBeFalse();

    const dummyDocument = new Document();

    dummyDocument.embeddedSignatureInfos = null;
    dummyDocument.detachedSignatures = null;
    expect(FolderUtils.doesContainEmbeddedSignature(dummyDocument)).toBeFalse();

    dummyDocument.embeddedSignatureInfos = [];
    dummyDocument.detachedSignatures = [];
    expect(FolderUtils.doesContainEmbeddedSignature(dummyDocument)).toBeFalse();

    dummyDocument.embeddedSignatureInfos = [{} as ValidatedSignatureInformation];
    dummyDocument.detachedSignatures = [];
    expect(FolderUtils.doesContainEmbeddedSignature(dummyDocument)).toBeTrue();

    dummyDocument.embeddedSignatureInfos = [];
    dummyDocument.detachedSignatures = [new DetachedSignature()];
    expect(FolderUtils.doesContainEmbeddedSignature(dummyDocument)).toBeFalse();

    dummyDocument.embeddedSignatureInfos = [{} as ValidatedSignatureInformation];
    dummyDocument.detachedSignatures = [new DetachedSignature()];
    expect(FolderUtils.doesContainEmbeddedSignature(dummyDocument)).toBeTrue();
  });


  it('should return some signature presence', () => {

    expect(FolderUtils.doesContainSignature(undefined)).toBeFalse();

    const dummyDocument = new Document();

    dummyDocument.embeddedSignatureInfos = null;
    dummyDocument.detachedSignatures = null;
    expect(FolderUtils.doesContainSignature(dummyDocument)).toBeFalse();

    dummyDocument.embeddedSignatureInfos = [];
    dummyDocument.detachedSignatures = [];
    expect(FolderUtils.doesContainSignature(dummyDocument)).toBeFalse();

    dummyDocument.embeddedSignatureInfos = [{} as ValidatedSignatureInformation];
    dummyDocument.detachedSignatures = [];
    expect(FolderUtils.doesContainSignature(dummyDocument)).toBeTrue();

    dummyDocument.embeddedSignatureInfos = [];
    dummyDocument.detachedSignatures = [new DetachedSignature()];
    expect(FolderUtils.doesContainSignature(dummyDocument)).toBeTrue();

    dummyDocument.embeddedSignatureInfos = [{} as ValidatedSignatureInformation];
    dummyDocument.detachedSignatures = [new DetachedSignature()];
    expect(FolderUtils.doesContainSignature(dummyDocument)).toBeTrue();
  });


  it('should return the higher stackable action', () => {

    expect(FolderUtils.getHigherStackableAction([Action.Visa, Action.Seal, Action.SecondOpinion])).toBe(Action.Seal);
    expect(FolderUtils.getHigherStackableAction([Action.Seal])).toBe(Action.Seal);

    expect(FolderUtils.getHigherStackableAction([])).toBe(null);
    expect(FolderUtils.getHigherStackableAction(null)).toBe(null);
  });


  it('should return a valid modal size', () => {

    const dummyTask = new Task();
    dummyTask.action = Action.Seal;

    const dummyFolder = new Folder();
    dummyFolder.stepList = [dummyTask];

    Object
      .keys(Action)
      .map(key => Action[key])
      .forEach(action => expect(FolderUtils.getModalSize(action, [dummyFolder])).not.toBeNull());

    Object
      .keys(SecondaryAction)
      .map(key => SecondaryAction[key])
      .forEach(secondaryAction => expect(FolderUtils.getModalSize(secondaryAction, [dummyFolder])).not.toBeNull());
  });


  it('should return an appropriate date object', () => {
    expect(FolderUtils.timestampToDate(99999999999)).not.toBeNull();
    expect(FolderUtils.timestampToDate(null)).toBeNull();
    expect(FolderUtils.timestampToDate(undefined)).toBeNull();
  });


});
