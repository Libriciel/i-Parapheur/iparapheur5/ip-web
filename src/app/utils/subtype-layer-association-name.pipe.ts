/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { Pipe, PipeTransform } from '@angular/core';
import { SubtypeLayerAssociation } from '@libriciel/iparapheur-legacy';

@Pipe({
  name: 'subtypeLayerAssociationName'
})
export class SubtypeLayerAssociationNamePipe implements PipeTransform {


  public static compute(associationType: SubtypeLayerAssociation) {
    switch (associationType) {
      case SubtypeLayerAssociation.All: { return 'Tous les documents'; }
      case SubtypeLayerAssociation.Annexe : { return 'Annexes'; }
      case SubtypeLayerAssociation.MainDocument : { return 'Documents principaux'; }
      default: { return associationType; }
    }
  }


  transform(value: SubtypeLayerAssociation, _args?: any): any {
    return SubtypeLayerAssociationNamePipe.compute(value);
  }


}
