/*
 * iparapheur Web
 * Copyright (C) 2019-2025 Libriciel SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalSignaturePopupComponent } from './external-signature-popup.component';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { Action, ExternalSignatureProcedure, ExternalSignatureMember } from '@libriciel/iparapheur-legacy';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionNamePipe } from '../../../../../shared/utils/action-name.pipe';
import { ActionIconPipe } from '../../../../../shared/utils/action-icon.pipe';

describe('ExternalSignaturePopupComponent', () => {
  let component: ExternalSignaturePopupComponent;
  let fixture: ComponentFixture<ExternalSignaturePopupComponent>;

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [RouterModule.forRoot([]), ToastrModule.forRoot()],
        providers: [
          HttpClient, HttpHandler, NgbActiveModal,
          {provide: ExternalSignaturePopupComponent.INJECTABLE_FOLDERS_KEY, useValue: []},
          {provide: ExternalSignaturePopupComponent.INJECTABLE_DESK_ID_KEY, useValue: 'deskId'},
          {provide: ExternalSignaturePopupComponent.INJECTABLE_PERFORMED_ACTION_KEY, useValue: Action.ExternalSignature},
          {provide: ExternalSignaturePopupComponent.INJECTABLE_TENANT_ID_KEY, useValue: 'tenant01'},
          {
            provide: ExternalSignaturePopupComponent.INJECTABLE_EXTERNAL_SIGNATURE_PROCEDURE_KEY,
            useValue: {
              members: [{} as ExternalSignatureMember]
            } as ExternalSignatureProcedure
          }
        ],
        declarations: [ActionIconPipe, ActionNamePipe, ExternalSignaturePopupComponent]
      })
      .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalSignaturePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  afterEach(() => {
    fixture.destroy();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
